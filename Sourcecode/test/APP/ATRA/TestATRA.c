/*
 * TestGLOG.c
 *
 *  Created on: Dec 22, 2017
 *      Author: MyoSwissEngineer
 */

#include "unity.h"
#include "unity_fixture.h"
#include "GLOG.h"
#include "RingBuffer.h"
#include "MockGOS.h"
#include "MockHI2C.h"
#include "RIMU.h"
#include "LSM9DS0_Registers.h"

#include "GINIT.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define UNITY_EXCLUDE_MATH_H

/***********************************************************************/
/* LOCAL CONSTANTS                                                     */
/***********************************************************************/

enum {
	GYROTYPE = 0x6B,
	XMTYPE   = 0x1D
};
/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/


typedef struct tag_IMUStruct_s{
	int8_t valueAccelXLow;
	int8_t valueAccelXHigh;
	int8_t valueAccelYLow;
	int8_t valueAccelYHigh;
	int8_t valueAccelZLow;
	int8_t valueAccelZHigh;
	int8_t valueGyroLow;
	int8_t valueGyroHigh;
}IMUStruct_s;

typedef struct tag_Angles_s{
	int16_t x_Axis;
	int16_t z_Axis;
	Angle_mDegree_t result;
}tag_Angles;

/***********************************************************************/
/* Local Variables					                                   */
/***********************************************************************/

static uint8_t S_HIC_ReadBuffer[10];
static uint8_t S_HIC_WriteBuffer[10];

static uint8_t S_AccelInitCommands[6] = {0x00,0x97,0x48,0x10,0x20,0x00};
static uint8_t S_GyroInitCommands[5] = {0xAF,0x00,0x00,0x10,0x00};
static uint32_t S_readCallbackInitCounter;


static IMUStruct_s S_Imu_Values;
static tag_Angles dataSetA[20];


/***********************************************************************/
/* LOCAL FUNCTION PROTOTYPES                                           */
/***********************************************************************/

void HI2C_write_CALLBACK_INIT(HI2C_Port_e port, uint32_t address, uint8_t registerAdress, uint8_t* data, uint32_t dataSize, int cmock_num_calls);

void HI2C_read_CALLBACK_INIT(HI2C_Port_e port, uint32_t address, uint8_t registerAdress, uint8_t* buf, uint32_t bufSize, int cmock_num_calls);

void HI2C_read_CALLBACK_IMU(HI2C_Port_e port, uint32_t address, uint8_t registerAdress, uint8_t* buf, uint32_t bufSize, int cmock_num_calls);

int8_t splitAngleLow(int16_t value);

int8_t splitAngleHigh(int16_t value);

/***********************************************************************/
/* TESTS					                                           */
/***********************************************************************/



TEST_GROUP(RIMU);


TEST_GROUP_RUNNER(RIMU)
{
	RUN_TEST_CASE(RIMU, RIMU_Initialize);
	RUN_TEST_CASE(RIMU, RIMU_GetAngleRaw);
}
//This is run before EACH TEST

TEST_SETUP(RIMU)
{


}

TEST_TEAR_DOWN(ATRA)
{


}

TEST(ATRA, ATRA_Initialize)
{
	S_readCallbackInitCounter = 0;

	uint32_t i;

	HI2C_read_StubWithCallback(HI2C_read_CALLBACK_INIT);
	for(i = 0; i < 6;++i){
		S_HIC_ReadBuffer[i] = S_AccelInitCommands[i];
		HI2C_write_CMockExpectAnyArgs(0);
	}

	for(i = 0; i < 5;++i){
		S_HIC_ReadBuffer[i + 6] = S_GyroInitCommands[i];
		HI2C_write_CMockExpectAnyArgs(0);
	}

	TEST_ASSERT_EQUAL(E_GINIT_ReturnSuccess,RIMU_Initialize(E_GINIT_InitLevel1));
	TEST_ASSERT_EQUAL(E_GINIT_ReturnSuccess,RIMU_Initialize(E_GINIT_InitLevel2));

	S_readCallbackInitCounter = 0;
	for(i = 0; i < 6;++i){
		S_HIC_ReadBuffer[i] = S_AccelInitCommands[6 - i];
		HI2C_write_CMockExpectAnyArgs(0);
	}

	for(i = 0; i < 5;++i){
		S_HIC_ReadBuffer[i + 6] = S_GyroInitCommands[5 - i];
		HI2C_write_CMockExpectAnyArgs(0);
	}

	TEST_ASSERT_EQUAL(E_GINIT_ReturnSuccess,RIMU_Initialize(E_GINIT_InitLevel1));
	TEST_ASSERT_EQUAL(E_GINIT_ReturnFailure,RIMU_Initialize(E_GINIT_InitLevel2));
}

TEST(RIMU, RIMU_GetAngleRaw){

	HI2C_read_StubWithCallback(HI2C_read_CALLBACK_IMU);
	uint32_t i;


	RIMU_DataStruct_s result;

	for(i = 0; i < 11; ++i){
		S_Imu_Values.valueAccelZHigh = splitAngleHigh(dataSetA[i].z_Axis);
		S_Imu_Values.valueAccelZLow = splitAngleLow(dataSetA[i].z_Axis);
		S_Imu_Values.valueAccelXHigh = splitAngleHigh(dataSetA[i].x_Axis);
		S_Imu_Values.valueAccelXLow = splitAngleLow(dataSetA[i].x_Axis);
		result = RIMU_GetIMUAngleRaw(E_RIMU_RightShank);
		TEST_ASSERT_EQUAL(dataSetA[i].result,result.imuAngle);

	}
}





/*****************************************************************************************************/

int8_t splitAngleHigh(int16_t value){

	value = value & 0xFF00;

	value = value >> 8;
	return (int8_t) value;
}

int8_t splitAngleLow(int16_t value){

	value = value & 0x00FF;
	return (int8_t) value;
}

void HI2C_write_CALLBACK_INIT(HI2C_Port_e port, uint32_t address, uint8_t registerAdress, uint8_t* data, uint32_t dataSize, int cmock_num_calls){

	S_HIC_WriteBuffer[0] = *data;
}

void HI2C_read_CALLBACK_INIT(HI2C_Port_e port, uint32_t address, uint8_t registerAdress, uint8_t* buf, uint32_t bufSize, int cmock_num_calls){

	buf[0] = S_HIC_ReadBuffer[S_readCallbackInitCounter];
	++S_readCallbackInitCounter;
}

void HI2C_read_CALLBACK_IMU(HI2C_Port_e port, uint32_t address, uint8_t registerAdress, uint8_t* buf, uint32_t bufSize, int cmock_num_calls){


	if(address == GYROTYPE){
		if(registerAdress == LSM9DS0_REGISTER_OUT_Y_L_G){
			buf[0] = S_Imu_Values.valueGyroLow;
		}
		else if(registerAdress == LSM9DS0_REGISTER_OUT_Y_H_G){
			buf[0] = S_Imu_Values.valueGyroHigh;
		}
		else{
			TEST_ABORT(); // Wrong address
		}
	}

	else if(address == XMTYPE){
		switch(registerAdress){
		case LSM9DS0_REGISTER_OUT_X_L_A:
			buf[0] = S_Imu_Values.valueAccelXLow;
			break;
		case LSM9DS0_REGISTER_OUT_X_H_A:
			buf[0] = S_Imu_Values.valueAccelXHigh;
			break;
		case LSM9DS0_REGISTER_OUT_Y_L_A:
			buf[0] = S_Imu_Values.valueAccelYLow;
			break;
		case LSM9DS0_REGISTER_OUT_Y_H_A:
			buf[0] = S_Imu_Values.valueAccelYHigh;
			break;
		case LSM9DS0_REGISTER_OUT_Z_L_A:
			buf[0] = S_Imu_Values.valueAccelZLow;
			break;
		case LSM9DS0_REGISTER_OUT_Z_H_A:
			buf[0] = S_Imu_Values.valueAccelZHigh;
			break;

		default:
			TEST_ABORT(); // Wrong address
		}

	}

}




