/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation: xxx
 ***********************************************************************/

/** @defgroup TEST_RDAC
 * @{
 * @brief
 * The next blank line is mandatory!
 *
 */


#include "unity.h"
#include "unity_fixture.h"
#include "GINIT.h"
#include "types.h"
#include "RDAC.h"

#include "MockRENC.h"
#include "MockRIMU.h"
#include "MockRMOT.h"
#include "MockGOS.h"
#include "MockRTMP.h"
#include "MockRBAT.h"




/***********************************************************************/
/* LOCAL CONSTANTS                                                     */
/***********************************************************************/

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/


/***********************************************************************/
/* Local Variables					                                   */
/***********************************************************************/
static RDAC_CalcData_s S_CalcDataSet_1;
static RDAC_CalcData_s S_CalcDataSet_2;

static RDAC_SysData_s S_SysDataSet_1;
static RDAC_SysData_s S_SysDataSet_2;

static RDAC_RawData_s S_RawDataSet_1;
static RDAC_RawData_s S_RawDataSet_2;
static GOS_TaskFunction_t S_RDAC_TaskFunction;


/***********************************************************************/
/* LOCAL FUNCTION PROTOTYPES                                           */
/***********************************************************************/
static GOS_Return_e GOS_CreateTask_Callback(GOS_TaskFunction_t pvTaskCode,int numCalls);

static void setDataSet(RDAC_SysData_s sysSet,RDAC_CalcData_s calcSet, RDAC_RawData_s rawSet);
/***********************************************************************/
/* TESTS					                                           */
/***********************************************************************/



TEST_GROUP(RDAC);


TEST_GROUP_RUNNER(RDAC)
{
	RUN_TEST_CASE(RDAC, RDAC_GetCalculationData);
	RUN_TEST_CASE(RDAC, RDAC_GetSystemData);
	RUN_TEST_CASE(RDAC,RDAC_GetRawData);
}
//This is run before EACH TEST

TEST_SETUP(RDAC)
{
	S_CalcDataSet_1.angleShankLeft = -111;
	S_CalcDataSet_1.angleShankRight = -222;
	S_CalcDataSet_1.angleThighLeft = -333;
	S_CalcDataSet_1.angleThighRight = -444;
	S_CalcDataSet_1.angleTrunk = -555;
	S_CalcDataSet_1.status = E_RDAC_Success;

	S_CalcDataSet_2.angleShankLeft = 111;
	S_CalcDataSet_2.angleShankRight = 222;
	S_CalcDataSet_2.angleThighLeft = 333;
	S_CalcDataSet_2.angleThighRight = 444;
	S_CalcDataSet_2.angleTrunk = 555;
	S_CalcDataSet_2.status = E_RDAC_Success;

	S_RawDataSet_1.motorEncoderLeft = -666;
	S_RawDataSet_1.motorEncoderRight = -777;
	S_RawDataSet_1.gyroShankRight = -1234;
	S_RawDataSet_1.accelAShankRight = -2345;
	S_RawDataSet_1.accelBShankRight = -3456;
	S_RawDataSet_1.gyroShankLeft = -4567;
	S_RawDataSet_1.accelAShankLeft = -5678;
	S_RawDataSet_1.accelBShankLeft = -6789;
	S_RawDataSet_1.gyroThighRight = -4321;
	S_RawDataSet_1.accelAThighRight = -5432;
	S_RawDataSet_1.accelBThighRight = -6543;
	S_RawDataSet_1.gyroThighLeft = -7654;
	S_RawDataSet_1.accelAThighLeft = -8765;
	S_RawDataSet_1.accelBThighLeft = -9876;
	S_RawDataSet_1.gyroTrunk = -1212;
	S_RawDataSet_1.accelATrunk = -2121;
	S_RawDataSet_1.accelBTrunk = -3232;
	S_RawDataSet_1.status = E_RDAC_Success;


	S_RawDataSet_2.motorEncoderLeft = +666;
	S_RawDataSet_2.motorEncoderRight = +777;
	S_RawDataSet_2.gyroShankRight = +1234;
	S_RawDataSet_2.accelAShankRight = +2345;
	S_RawDataSet_2.accelBShankRight = +3456;
	S_RawDataSet_2.gyroShankLeft = +4567;
	S_RawDataSet_2.accelAShankLeft = +5678;
	S_RawDataSet_2.accelBShankLeft = +6789;
	S_RawDataSet_2.gyroThighRight = +4321;
	S_RawDataSet_2.accelAThighRight = +5432;
	S_RawDataSet_2.accelBThighRight = +6543;
	S_RawDataSet_2.gyroThighLeft = +7654;
	S_RawDataSet_2.accelAThighLeft = +8765;
	S_RawDataSet_2.accelBThighLeft = +9876;
	S_RawDataSet_2.gyroTrunk = +1212;
	S_RawDataSet_2.accelATrunk = +2121;
	S_RawDataSet_2.accelBTrunk = +3232;
	S_RawDataSet_2.status = E_RDAC_Success;


	S_SysDataSet_1.batteryVoltage = 13;
	S_SysDataSet_1.batteryTemperature = -13;
	S_SysDataSet_1.batteryCapacity = 252;
	S_SysDataSet_1.motorCurrentLeft = -123;
	S_SysDataSet_1.motorCurrentRight = -234;
	S_SysDataSet_1.temperatureMotorLeft = -502;
	S_SysDataSet_1.temperatureMotorRight = -601;
	S_SysDataSet_1.status = E_RDAC_Success;

	S_SysDataSet_2.batteryVoltage = 2400;
	S_SysDataSet_1.batteryTemperature = +100;
	S_SysDataSet_1.batteryCapacity = 5;
	S_SysDataSet_2.motorCurrentLeft = +123;
	S_SysDataSet_2.motorCurrentRight = +234;
	S_SysDataSet_2.temperatureMotorLeft = 202;
	S_SysDataSet_2.temperatureMotorRight = 303;
	S_SysDataSet_2.status = E_RDAC_Success;


}

TEST_TEAR_DOWN(RDAC)
{

	MockRMOT_Verify();
	MockRMOT_Destroy();
	MockRENC_Verify();
	MockRENC_Destroy();
	MockRIMU_Verify();
	MockRIMU_Destroy();

}

TEST(RDAC, RDAC_GetCalculationData)
{
	GOS_EnterCriticalSection_Ignore();
	GOS_ExitCriticalSection_Ignore();

	GOS_CreateTask_StubWithCallback(GOS_CreateTask_Callback);
	setDataSet(S_SysDataSet_1, S_CalcDataSet_1, S_RawDataSet_1);

	RDAC_Initialize(E_GINIT_InitLevel1);
	RDAC_Initialize(E_GINIT_InitLevel2);

	(*S_RDAC_TaskFunction)(NULL);

	RDAC_CalcData_s actual;
	actual = RDAC_GetCalculationData();

	TEST_ASSERT_EQUAL_MEMORY(&S_CalcDataSet_1,&actual,sizeof(actual));

	setDataSet(S_SysDataSet_2, S_CalcDataSet_2,S_RawDataSet_2);
	(*S_RDAC_TaskFunction)(NULL);
	actual = RDAC_GetCalculationData();

	TEST_ASSERT_EQUAL_MEMORY(&S_CalcDataSet_2,&actual,sizeof(actual));


}

TEST(RDAC, RDAC_GetSystemData)
{
	GOS_EnterCriticalSection_Ignore();
	GOS_ExitCriticalSection_Ignore();

	GOS_CreateTask_StubWithCallback(GOS_CreateTask_Callback);
	setDataSet(S_SysDataSet_1, S_CalcDataSet_1, S_RawDataSet_1);

	RDAC_Initialize(E_GINIT_InitLevel1);
	RDAC_Initialize(E_GINIT_InitLevel2);

	(*S_RDAC_TaskFunction)(NULL);

	RDAC_SysData_s actual;
	actual = RDAC_GetSystemData();

	TEST_ASSERT_EQUAL_MEMORY(&S_SysDataSet_1,&actual,sizeof(actual));

	setDataSet(S_SysDataSet_2, S_CalcDataSet_2, S_RawDataSet_2);
	(*S_RDAC_TaskFunction)(NULL);
	actual = RDAC_GetSystemData();

	TEST_ASSERT_EQUAL_MEMORY(&S_SysDataSet_2,&actual,sizeof(actual));


}

TEST(RDAC, RDAC_GetRawData)
{
	GOS_EnterCriticalSection_Ignore();
	GOS_ExitCriticalSection_Ignore();

	GOS_CreateTask_StubWithCallback(GOS_CreateTask_Callback);
	setDataSet(S_SysDataSet_1, S_CalcDataSet_1, S_RawDataSet_1);

	RDAC_Initialize(E_GINIT_InitLevel1);
	RDAC_Initialize(E_GINIT_InitLevel2);

	(*S_RDAC_TaskFunction)(NULL);

	RDAC_RawData_s actual;
	actual = RDAC_GetRawData();

	TEST_ASSERT_EQUAL_MEMORY(&S_RawDataSet_1,&actual,sizeof(actual));

	setDataSet(S_SysDataSet_2, S_CalcDataSet_2, S_RawDataSet_2);
	(*S_RDAC_TaskFunction)(NULL);
	actual = RDAC_GetRawData();

	TEST_ASSERT_EQUAL_MEMORY(&S_RawDataSet_2,&actual,sizeof(actual));


}



/********************Helper functions**********************************/
static void setDataSet(RDAC_SysData_s sysSet,RDAC_CalcData_s calcSet, RDAC_RawData_s rawSet){
	/**************calc and raw data ***************************/
	RIMU_DataStruct_s imuRetVal;
	imuRetVal.imuStatusOfData = E_RIMU_ReturnSuccess;
	imuRetVal.imuAngle = calcSet.angleShankRight;
	imuRetVal.accelValueA = rawSet.accelAShankRight;
	imuRetVal.accelValueB = rawSet.accelBShankRight;
	imuRetVal.gyroValue = rawSet.gyroShankRight;
	RIMU_GetIMUAngleFiltered_ExpectAndReturn(E_RIMU_RightShank,imuRetVal);

	imuRetVal.imuAngle = calcSet.angleShankLeft;
	imuRetVal.accelValueA = rawSet.accelAShankLeft;
	imuRetVal.accelValueB = rawSet.accelBShankLeft;
	imuRetVal.gyroValue = rawSet.gyroShankLeft;
	RIMU_GetIMUAngleFiltered_ExpectAndReturn(E_RIMU_LeftShank,imuRetVal);

	imuRetVal.imuAngle = calcSet.angleThighLeft;
	imuRetVal.accelValueA = rawSet.accelAThighLeft;
	imuRetVal.accelValueB = rawSet.accelBThighLeft;
	imuRetVal.gyroValue = rawSet.gyroThighLeft;
	RIMU_GetIMUAngleFiltered_ExpectAndReturn(E_RIMU_LeftThigh,imuRetVal);

	imuRetVal.imuAngle = calcSet.angleThighRight;
	imuRetVal.accelValueA = rawSet.accelAThighRight;
	imuRetVal.accelValueB = rawSet.accelBThighRight;
	imuRetVal.gyroValue = rawSet.gyroThighRight;
	RIMU_GetIMUAngleFiltered_ExpectAndReturn(E_RIMU_RightThigh,imuRetVal);

	imuRetVal.imuAngle = calcSet.angleTrunk;
	imuRetVal.accelValueA = rawSet.accelATrunk;
	imuRetVal.accelValueB = rawSet.accelBTrunk;
	imuRetVal.gyroValue = rawSet.gyroTrunk;
	RIMU_GetIMUAngleFiltered_ExpectAndReturn(E_RIMU_Trunk,imuRetVal);

	RENC_EncoderCounts_s encoderCounts;
	encoderCounts.motorEncoderCountLeft = rawSet.motorEncoderLeft;
	encoderCounts.motorEncoderCountRight = rawSet.motorEncoderRight;
	encoderCounts.status = E_RENC_ReturnSuccess;
	RENC_getEncoderCounts_ExpectAndReturn(encoderCounts);

	/**************sys data ***************************/


	RMOT_MotorCurrents_s motorCurrents;
	motorCurrents.status = E_RMOT_ReturnSuccess;
	motorCurrents.motorCurrentRightSide = sysSet.motorCurrentRight;
	motorCurrents.motorCurrentLeftSide = sysSet.motorCurrentLeft;
	RMOT_getMotorCurrents_ExpectAndReturn(motorCurrents);


	RTMP_Temperatures_s temperatures;
	temperatures.status = E_RTMP_ReturnSuccess;
	temperatures.temperatureMotorLeft = sysSet.temperatureMotorLeft;
	temperatures.temperatureMotorRight = sysSet.temperatureMotorRight;
	RTMP_GetTemperatures_ExpectAndReturn(temperatures);

	RBAT_Voltage_s batData;
	batData.status = E_RBAT_ReturnSuccess;
	batData.batteryVoltage = sysSet.batteryVoltage;
	RBAT_GetVoltage_ExpectAndReturn(batData);


	RBAT_Capacity_s batCap;
	batCap.status = E_RBAT_ReturnSuccess;
	batCap.batteryCapacity = sysSet.batteryCapacity;
	RBAT_GetCapacity_ExpectAndReturn(batCap);

	RBAT_Temp_s batTemp;
	batTemp.status = E_RBAT_ReturnSuccess;
	batTemp.batteryTemperature = sysSet.batteryTemperature;
	RBAT_GetTemperature_ExpectAndReturn(batTemp);




}


GOS_Return_e GOS_CreateTask_Callback(GOS_TaskFunction_t pvTaskCode, int numCalls){

	//store the function pointer to call the task function manually from test
	S_RDAC_TaskFunction = pvTaskCode;

	//skip endless while loop of Taskfunction in GLOG, only used for testing
	RDAC_UnitTest_TaskRunOnce();
	return E_GOS_ReturnSuccess;

}
