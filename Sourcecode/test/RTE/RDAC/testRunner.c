#include "unity_fixture.h"
#include "unity.h"

static void RunAllTests(void)
{
  RUN_TEST_GROUP(RDAC);
}

int main(int argc, const char * argv[])
{

    extern void __gcov_flush(void);
    __gcov_flush(); //ensure all coverage data is flushed before shutdown
  return UnityMain(argc, argv, RunAllTests);
}
