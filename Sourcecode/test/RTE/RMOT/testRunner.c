#include "unity_fixture.h"
#include "unity.h"

static void RunAllTests(void)
{
  RUN_TEST_GROUP(RMOT);
}

int main(int argc, const char * argv[])
{

  return UnityMain(argc, argv, RunAllTests);
}
