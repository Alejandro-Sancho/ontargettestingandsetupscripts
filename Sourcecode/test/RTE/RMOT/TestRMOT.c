/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation: xxx
 ***********************************************************************/

/** @defgroup TEST_RDAC
 * @{
 * @brief
 * The next blank line is mandatory!
 *
 */


#include "unity.h"
#include "unity_fixture.h"
#include "GINIT.h"
#include "types.h"
#include "RMOT.h"
#include "MockRCAN.h"




/***********************************************************************/
/* LOCAL CONSTANTS                                                     */
/***********************************************************************/

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/


/***********************************************************************/
/* Local Variables					                                   */
/***********************************************************************/
static int32_t testValueRight;
static int32_t testValueLeft;

static int32_t arrayOfForces[7] = {-1000, -10, -100, 0, 10, 100, 1000};
static int32_t arrayOfForcesFail[6] = {-2000, -1500, -1200, 1200, 1500, 2000 };
static Current_mA_t arrayOfCurrents[8] = {-100, 0, 10, 100, 1000, 1100, 1200, 10000};
/***********************************************************************/
/* LOCAL FUNCTION PROTOTYPES                                           */
/***********************************************************************/

static void getDataStubCallback(RCAN_MotorData_s* motorStruct, uint8_t nodeID, int cmock_num_calls);

/***********************************************************************/
/* TESTS					                                           */
/***********************************************************************/



TEST_GROUP(RMOT);


TEST_GROUP_RUNNER(RMOT)
{
	RUN_TEST_CASE(RMOT, RMOT_Initialize);
	RUN_TEST_CASE(RMOT, RMOT_SetForcePositive);
	RUN_TEST_CASE(RMOT, RMOT_SetForceNegative);
	RUN_TEST_CASE(RMOT, RMOT_GetCurrent);
	RUN_TEST_CASE(RMOT, RMOT_GetEncoder);
}
//This is run before EACH TEST

TEST_SETUP(RMOT)
{


}

TEST_TEAR_DOWN(RMOT)
{


}
/*********************************************************/
/*This tests if initialization of the component is fine. */
/*********************************************************/
TEST(RMOT, RMOT_Initialize)
{

	GINIT_Return_e retVal;

	retVal = RMOT_Initialize(E_GINIT_InitLevel1);

	TEST_ASSERT_EQUAL(E_GINIT_ReturnSuccess,retVal);

}

/*********************************************************/
/* This tests if the function setForce does send current   *
 * to the motor. At this point, we expect to receive a force
 * that is already limited and only the current limits will be checked
 * inside the current RangeCheck. MAX CURRENT = 1500
*********************************************************/
TEST(RMOT, RMOT_SetForcePositive)
{
	RMOT_ReturnStatus_e retValRmot;
	RMOT_MotorForces_s forces;
	RMOT_MotorEncoders_s calcData;
	int8_t i;
	//RCAN_MotorCurrentsPerThousand_s tryMotorCurrentPerThousand;
	//tryMotorCurrentPerThousand.motorCurrentPerThousandRightSide = 1425;
	//tryMotorCurrentPerThousand.motorCurrentPerThousandLeftSide = -1325;


	for (i=0; i<6; i++){
	forces.motorForceLeftSide= arrayOfForces[i];
	forces.motorForceRightSide= arrayOfForces[i];


	RCAN_getDataFromMotor_ExpectAnyArgs();
	RCAN_getDataFromMotor_ExpectAnyArgs();
	RCAN_sendCurrentToMotor_ExpectAnyArgsAndReturn(E_RCAN_ReturnSuccess);

	//RCAN_sendCurrentToMotor_ExpectAndReturn(tryMotorCurrentPerThousand, E_RCAN_ReturnSuccess);

	retValRmot = RMOT_setForces(forces, calcData);

	TEST_ASSERT_EQUAL(E_RMOT_ReturnSuccess, retValRmot);
	}

}

/*********************************************************/
/*tests 6 failure cases we expect to return fail state if we pass forces that produce current higher than 1500
 *which we have set as max/min possible current.
*********************************************************/
TEST(RMOT, RMOT_SetForceNegative)
{
	RMOT_ReturnStatus_e retValRmot;
	RMOT_MotorForces_s forces;
	RMOT_MotorEncoders_s calcData;
	int8_t i;
	//RCAN_MotorCurrentsPerThousand_s tryMotorCurrentPerThousand;

	for (i=0; i<6; i++){
	forces.motorForceLeftSide = arrayOfForcesFail[i];
	forces.motorForceRightSide = arrayOfForcesFail[i];


	RCAN_getDataFromMotor_ExpectAnyArgs();
	RCAN_getDataFromMotor_ExpectAnyArgs();
	RCAN_sendCurrentToMotor_ExpectAnyArgsAndReturn(E_RCAN_ReturnSuccess);

	//RCAN_sendCurrentToMotor_ExpectAndReturn(tryMotorCurrentPerThousand, E_RCAN_ReturnSuccess);

	retValRmot = RMOT_setForces(forces, calcData);

	TEST_ASSERT_EQUAL(E_RMOT_DataNotValid, retValRmot);
	}

}

/*********************************************************/
/* This tests if the function GETCURRENT provides the right current*
*********************************************************/

TEST(RMOT, RMOT_GetCurrent)
{

	RMOT_MotorForces_s forces;
	RMOT_MotorEncoders_s calcData;
	testValueRight=  arrayOfCurrents[7];
	testValueLeft=  arrayOfCurrents[7];


	RMOT_MotorCurrents_s retVal;
    RMOT_MotorCurrents_s testSet;

	testSet.motorCurrentRightSide = arrayOfCurrents[7];
	testSet.motorCurrentLeftSide = arrayOfCurrents[7];

	RCAN_getDataFromMotor_StubWithCallback(getDataStubCallback);
	RCAN_sendCurrentToMotor_ExpectAnyArgsAndReturn(E_RCAN_ReturnSuccess);
	RMOT_setForces(forces, calcData);


	retVal = RMOT_getMotorCurrents();


	TEST_ASSERT_EQUAL(testSet.motorCurrentRightSide, retVal.motorCurrentRightSide);
	TEST_ASSERT_EQUAL(testSet.motorCurrentLeftSide, retVal.motorCurrentLeftSide);

}

TEST(RMOT, RMOT_GetEncoder)
{
	RMOT_MotorForces_s forces;
	RMOT_MotorEncoders_s calcData;
	RMOT_MotorEncoders_s retVal;
	RMOT_MotorEncoders_s testSet;
	testValueRight= arrayOfCurrents[7];;
	testValueLeft=arrayOfCurrents[7];

	RCAN_getDataFromMotor_StubWithCallback(getDataStubCallback);
	RCAN_sendCurrentToMotor_ExpectAnyArgsAndReturn(E_RCAN_ReturnSuccess);
	RMOT_setForces(forces, calcData);

	retVal = RMOT_getMotorEncoders();

	testSet.motorEncoderRightSide = arrayOfCurrents[7];;
	testSet.motorEncoderLeftSide = arrayOfCurrents[7];

	//TEST_ASSERT_EQUAL(testSet.motorEncoderRightSide, retVal.motorEncoderRightSide);
	//TEST_ASSERT_EQUAL(testSet.motorEncoderLeftSide, retVal.motorEncoderLeftSide);

	TEST_ASSERT_EQUAL_MEMORY(&testSet, &retVal,sizeof(RMOT_MotorEncoders_s));

}




/*******************************************************************************************************************/
/*                            Functions to support tests                                                           */
/*******************************************************************************************************************/


static void getDataStubCallback(RCAN_MotorData_s* motorStruct, uint8_t nodeID, int cmock_num_calls){


	motorStruct->motorCurrent =  arrayOfCurrents[7];

	motorStruct->motorEncoder =  arrayOfCurrents[7];

}


