/* AUTOGENERATED FILE. DO NOT EDIT. */
#include <string.h>
#include <stdlib.h>
#include <setjmp.h>
#include "unity.h"
#include "cmock.h"
#include "MockGOS.h"

static const char* CMockString_GOS_CreateTask = "GOS_CreateTask";
static const char* CMockString_GOS_EnterCriticalSection = "GOS_EnterCriticalSection";
static const char* CMockString_GOS_ExitCriticalSection = "GOS_ExitCriticalSection";
static const char* CMockString_GOS_Initialize = "GOS_Initialize";
static const char* CMockString_pvTaskCode = "pvTaskCode";

typedef struct _CMOCK_GOS_Initialize_CALL_INSTANCE
{
  UNITY_LINE_TYPE LineNumber;
  CMOCK_ARG_MODE IgnoreMode;
  GOS_Return_e ReturnVal;

} CMOCK_GOS_Initialize_CALL_INSTANCE;

typedef struct _CMOCK_GOS_CreateTask_CALL_INSTANCE
{
  UNITY_LINE_TYPE LineNumber;
  CMOCK_ARG_MODE IgnoreMode;
  GOS_Return_e ReturnVal;
  GOS_TaskFunction_t Expected_pvTaskCode;

} CMOCK_GOS_CreateTask_CALL_INSTANCE;

typedef struct _CMOCK_GOS_EnterCriticalSection_CALL_INSTANCE
{
  UNITY_LINE_TYPE LineNumber;
  CMOCK_ARG_MODE IgnoreMode;

} CMOCK_GOS_EnterCriticalSection_CALL_INSTANCE;

typedef struct _CMOCK_GOS_ExitCriticalSection_CALL_INSTANCE
{
  UNITY_LINE_TYPE LineNumber;
  CMOCK_ARG_MODE IgnoreMode;

} CMOCK_GOS_ExitCriticalSection_CALL_INSTANCE;

static struct MockGOSInstance
{
  int GOS_Initialize_IgnoreBool;
  GOS_Return_e GOS_Initialize_FinalReturn;
  CMOCK_GOS_Initialize_CALLBACK GOS_Initialize_CallbackFunctionPointer;
  int GOS_Initialize_CallbackCalls;
  CMOCK_MEM_INDEX_TYPE GOS_Initialize_CallInstance;
  int GOS_CreateTask_IgnoreBool;
  GOS_Return_e GOS_CreateTask_FinalReturn;
  CMOCK_GOS_CreateTask_CALLBACK GOS_CreateTask_CallbackFunctionPointer;
  int GOS_CreateTask_CallbackCalls;
  CMOCK_MEM_INDEX_TYPE GOS_CreateTask_CallInstance;
  int GOS_EnterCriticalSection_IgnoreBool;
  CMOCK_GOS_EnterCriticalSection_CALLBACK GOS_EnterCriticalSection_CallbackFunctionPointer;
  int GOS_EnterCriticalSection_CallbackCalls;
  CMOCK_MEM_INDEX_TYPE GOS_EnterCriticalSection_CallInstance;
  int GOS_ExitCriticalSection_IgnoreBool;
  CMOCK_GOS_ExitCriticalSection_CALLBACK GOS_ExitCriticalSection_CallbackFunctionPointer;
  int GOS_ExitCriticalSection_CallbackCalls;
  CMOCK_MEM_INDEX_TYPE GOS_ExitCriticalSection_CallInstance;
} Mock;

extern jmp_buf AbortFrame;

void MockGOS_Verify(void)
{
  UNITY_LINE_TYPE cmock_line = TEST_LINE_NUM;
  if (Mock.GOS_Initialize_IgnoreBool)
    Mock.GOS_Initialize_CallInstance = CMOCK_GUTS_NONE;
  UNITY_SET_DETAIL(CMockString_GOS_Initialize);
  UNITY_TEST_ASSERT(CMOCK_GUTS_NONE == Mock.GOS_Initialize_CallInstance, cmock_line, CMockStringCalledLess);
  if (Mock.GOS_Initialize_CallbackFunctionPointer != NULL)
    Mock.GOS_Initialize_CallInstance = CMOCK_GUTS_NONE;
  if (Mock.GOS_CreateTask_IgnoreBool)
    Mock.GOS_CreateTask_CallInstance = CMOCK_GUTS_NONE;
  UNITY_SET_DETAIL(CMockString_GOS_CreateTask);
  UNITY_TEST_ASSERT(CMOCK_GUTS_NONE == Mock.GOS_CreateTask_CallInstance, cmock_line, CMockStringCalledLess);
  if (Mock.GOS_CreateTask_CallbackFunctionPointer != NULL)
    Mock.GOS_CreateTask_CallInstance = CMOCK_GUTS_NONE;
  if (Mock.GOS_EnterCriticalSection_IgnoreBool)
    Mock.GOS_EnterCriticalSection_CallInstance = CMOCK_GUTS_NONE;
  UNITY_SET_DETAIL(CMockString_GOS_EnterCriticalSection);
  UNITY_TEST_ASSERT(CMOCK_GUTS_NONE == Mock.GOS_EnterCriticalSection_CallInstance, cmock_line, CMockStringCalledLess);
  if (Mock.GOS_EnterCriticalSection_CallbackFunctionPointer != NULL)
    Mock.GOS_EnterCriticalSection_CallInstance = CMOCK_GUTS_NONE;
  if (Mock.GOS_ExitCriticalSection_IgnoreBool)
    Mock.GOS_ExitCriticalSection_CallInstance = CMOCK_GUTS_NONE;
  UNITY_SET_DETAIL(CMockString_GOS_ExitCriticalSection);
  UNITY_TEST_ASSERT(CMOCK_GUTS_NONE == Mock.GOS_ExitCriticalSection_CallInstance, cmock_line, CMockStringCalledLess);
  if (Mock.GOS_ExitCriticalSection_CallbackFunctionPointer != NULL)
    Mock.GOS_ExitCriticalSection_CallInstance = CMOCK_GUTS_NONE;
}

void MockGOS_Init(void)
{
  MockGOS_Destroy();
}

void MockGOS_Destroy(void)
{
  CMock_Guts_MemFreeAll();
  memset(&Mock, 0, sizeof(Mock));
  Mock.GOS_Initialize_CallbackFunctionPointer = NULL;
  Mock.GOS_Initialize_CallbackCalls = 0;
  Mock.GOS_CreateTask_CallbackFunctionPointer = NULL;
  Mock.GOS_CreateTask_CallbackCalls = 0;
  Mock.GOS_EnterCriticalSection_CallbackFunctionPointer = NULL;
  Mock.GOS_EnterCriticalSection_CallbackCalls = 0;
  Mock.GOS_ExitCriticalSection_CallbackFunctionPointer = NULL;
  Mock.GOS_ExitCriticalSection_CallbackCalls = 0;
}

GOS_Return_e GOS_Initialize(void)
{
  UNITY_LINE_TYPE cmock_line = TEST_LINE_NUM;
  CMOCK_GOS_Initialize_CALL_INSTANCE* cmock_call_instance;
  UNITY_SET_DETAIL(CMockString_GOS_Initialize);
  cmock_call_instance = (CMOCK_GOS_Initialize_CALL_INSTANCE*)CMock_Guts_GetAddressFor(Mock.GOS_Initialize_CallInstance);
  Mock.GOS_Initialize_CallInstance = CMock_Guts_MemNext(Mock.GOS_Initialize_CallInstance);
  if (Mock.GOS_Initialize_IgnoreBool)
  {
    UNITY_CLR_DETAILS();
    if (cmock_call_instance == NULL)
      return Mock.GOS_Initialize_FinalReturn;
    memcpy(&Mock.GOS_Initialize_FinalReturn, &cmock_call_instance->ReturnVal, sizeof(GOS_Return_e));
    return cmock_call_instance->ReturnVal;
  }
  if (Mock.GOS_Initialize_CallbackFunctionPointer != NULL)
  {
    return Mock.GOS_Initialize_CallbackFunctionPointer(Mock.GOS_Initialize_CallbackCalls++);
  }
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringCalledMore);
  cmock_line = cmock_call_instance->LineNumber;
  UNITY_CLR_DETAILS();
  return cmock_call_instance->ReturnVal;
}

void GOS_Initialize_CMockIgnoreAndReturn(UNITY_LINE_TYPE cmock_line, GOS_Return_e cmock_to_return)
{
  CMOCK_MEM_INDEX_TYPE cmock_guts_index = CMock_Guts_MemNew(sizeof(CMOCK_GOS_Initialize_CALL_INSTANCE));
  CMOCK_GOS_Initialize_CALL_INSTANCE* cmock_call_instance = (CMOCK_GOS_Initialize_CALL_INSTANCE*)CMock_Guts_GetAddressFor(cmock_guts_index);
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringOutOfMemory);
  memset(cmock_call_instance, 0, sizeof(*cmock_call_instance));
  Mock.GOS_Initialize_CallInstance = CMock_Guts_MemChain(Mock.GOS_Initialize_CallInstance, cmock_guts_index);
  Mock.GOS_Initialize_IgnoreBool = (int)0;
  cmock_call_instance->LineNumber = cmock_line;
  cmock_call_instance->IgnoreMode = CMOCK_ARG_ALL;
  cmock_call_instance->ReturnVal = cmock_to_return;
  Mock.GOS_Initialize_IgnoreBool = (int)1;
}

void GOS_Initialize_CMockExpectAnyArgsAndReturn(UNITY_LINE_TYPE cmock_line, GOS_Return_e cmock_to_return)
{
  CMOCK_MEM_INDEX_TYPE cmock_guts_index = CMock_Guts_MemNew(sizeof(CMOCK_GOS_Initialize_CALL_INSTANCE));
  CMOCK_GOS_Initialize_CALL_INSTANCE* cmock_call_instance = (CMOCK_GOS_Initialize_CALL_INSTANCE*)CMock_Guts_GetAddressFor(cmock_guts_index);
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringOutOfMemory);
  memset(cmock_call_instance, 0, sizeof(*cmock_call_instance));
  Mock.GOS_Initialize_CallInstance = CMock_Guts_MemChain(Mock.GOS_Initialize_CallInstance, cmock_guts_index);
  Mock.GOS_Initialize_IgnoreBool = (int)0;
  cmock_call_instance->LineNumber = cmock_line;
  cmock_call_instance->IgnoreMode = CMOCK_ARG_ALL;
  cmock_call_instance->ReturnVal = cmock_to_return;
  cmock_call_instance->IgnoreMode = CMOCK_ARG_NONE;
}

void GOS_Initialize_CMockExpectAndReturn(UNITY_LINE_TYPE cmock_line, GOS_Return_e cmock_to_return)
{
  CMOCK_MEM_INDEX_TYPE cmock_guts_index = CMock_Guts_MemNew(sizeof(CMOCK_GOS_Initialize_CALL_INSTANCE));
  CMOCK_GOS_Initialize_CALL_INSTANCE* cmock_call_instance = (CMOCK_GOS_Initialize_CALL_INSTANCE*)CMock_Guts_GetAddressFor(cmock_guts_index);
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringOutOfMemory);
  memset(cmock_call_instance, 0, sizeof(*cmock_call_instance));
  Mock.GOS_Initialize_CallInstance = CMock_Guts_MemChain(Mock.GOS_Initialize_CallInstance, cmock_guts_index);
  Mock.GOS_Initialize_IgnoreBool = (int)0;
  cmock_call_instance->LineNumber = cmock_line;
  cmock_call_instance->IgnoreMode = CMOCK_ARG_ALL;
  memcpy(&cmock_call_instance->ReturnVal, &cmock_to_return, sizeof(GOS_Return_e));
  UNITY_CLR_DETAILS();
}

void GOS_Initialize_StubWithCallback(CMOCK_GOS_Initialize_CALLBACK Callback)
{
  Mock.GOS_Initialize_IgnoreBool = (int)0;
  Mock.GOS_Initialize_CallbackFunctionPointer = Callback;
}

GOS_Return_e GOS_CreateTask(GOS_TaskFunction_t pvTaskCode)
{
  UNITY_LINE_TYPE cmock_line = TEST_LINE_NUM;
  CMOCK_GOS_CreateTask_CALL_INSTANCE* cmock_call_instance;
  UNITY_SET_DETAIL(CMockString_GOS_CreateTask);
  cmock_call_instance = (CMOCK_GOS_CreateTask_CALL_INSTANCE*)CMock_Guts_GetAddressFor(Mock.GOS_CreateTask_CallInstance);
  Mock.GOS_CreateTask_CallInstance = CMock_Guts_MemNext(Mock.GOS_CreateTask_CallInstance);
  if (Mock.GOS_CreateTask_IgnoreBool)
  {
    UNITY_CLR_DETAILS();
    if (cmock_call_instance == NULL)
      return Mock.GOS_CreateTask_FinalReturn;
    memcpy(&Mock.GOS_CreateTask_FinalReturn, &cmock_call_instance->ReturnVal, sizeof(GOS_Return_e));
    return cmock_call_instance->ReturnVal;
  }
  if (Mock.GOS_CreateTask_CallbackFunctionPointer != NULL)
  {
    return Mock.GOS_CreateTask_CallbackFunctionPointer(pvTaskCode, Mock.GOS_CreateTask_CallbackCalls++);
  }
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringCalledMore);
  cmock_line = cmock_call_instance->LineNumber;
  if (cmock_call_instance->IgnoreMode != CMOCK_ARG_NONE)
  {
  {
    UNITY_SET_DETAILS(CMockString_GOS_CreateTask,CMockString_pvTaskCode);
    UNITY_TEST_ASSERT_EQUAL_MEMORY((void*)(&cmock_call_instance->Expected_pvTaskCode), (void*)(&pvTaskCode), sizeof(GOS_TaskFunction_t), cmock_line, CMockStringMismatch);
  }
  }
  UNITY_CLR_DETAILS();
  return cmock_call_instance->ReturnVal;
}

void CMockExpectParameters_GOS_CreateTask(CMOCK_GOS_CreateTask_CALL_INSTANCE* cmock_call_instance, GOS_TaskFunction_t pvTaskCode)
{
  memcpy(&cmock_call_instance->Expected_pvTaskCode, &pvTaskCode, sizeof(GOS_TaskFunction_t));
}

void GOS_CreateTask_CMockIgnoreAndReturn(UNITY_LINE_TYPE cmock_line, GOS_Return_e cmock_to_return)
{
  CMOCK_MEM_INDEX_TYPE cmock_guts_index = CMock_Guts_MemNew(sizeof(CMOCK_GOS_CreateTask_CALL_INSTANCE));
  CMOCK_GOS_CreateTask_CALL_INSTANCE* cmock_call_instance = (CMOCK_GOS_CreateTask_CALL_INSTANCE*)CMock_Guts_GetAddressFor(cmock_guts_index);
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringOutOfMemory);
  memset(cmock_call_instance, 0, sizeof(*cmock_call_instance));
  Mock.GOS_CreateTask_CallInstance = CMock_Guts_MemChain(Mock.GOS_CreateTask_CallInstance, cmock_guts_index);
  Mock.GOS_CreateTask_IgnoreBool = (int)0;
  cmock_call_instance->LineNumber = cmock_line;
  cmock_call_instance->IgnoreMode = CMOCK_ARG_ALL;
  cmock_call_instance->ReturnVal = cmock_to_return;
  Mock.GOS_CreateTask_IgnoreBool = (int)1;
}

void GOS_CreateTask_CMockExpectAnyArgsAndReturn(UNITY_LINE_TYPE cmock_line, GOS_Return_e cmock_to_return)
{
  CMOCK_MEM_INDEX_TYPE cmock_guts_index = CMock_Guts_MemNew(sizeof(CMOCK_GOS_CreateTask_CALL_INSTANCE));
  CMOCK_GOS_CreateTask_CALL_INSTANCE* cmock_call_instance = (CMOCK_GOS_CreateTask_CALL_INSTANCE*)CMock_Guts_GetAddressFor(cmock_guts_index);
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringOutOfMemory);
  memset(cmock_call_instance, 0, sizeof(*cmock_call_instance));
  Mock.GOS_CreateTask_CallInstance = CMock_Guts_MemChain(Mock.GOS_CreateTask_CallInstance, cmock_guts_index);
  Mock.GOS_CreateTask_IgnoreBool = (int)0;
  cmock_call_instance->LineNumber = cmock_line;
  cmock_call_instance->IgnoreMode = CMOCK_ARG_ALL;
  cmock_call_instance->ReturnVal = cmock_to_return;
  cmock_call_instance->IgnoreMode = CMOCK_ARG_NONE;
}

void GOS_CreateTask_CMockExpectAndReturn(UNITY_LINE_TYPE cmock_line, GOS_TaskFunction_t pvTaskCode, GOS_Return_e cmock_to_return)
{
  CMOCK_MEM_INDEX_TYPE cmock_guts_index = CMock_Guts_MemNew(sizeof(CMOCK_GOS_CreateTask_CALL_INSTANCE));
  CMOCK_GOS_CreateTask_CALL_INSTANCE* cmock_call_instance = (CMOCK_GOS_CreateTask_CALL_INSTANCE*)CMock_Guts_GetAddressFor(cmock_guts_index);
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringOutOfMemory);
  memset(cmock_call_instance, 0, sizeof(*cmock_call_instance));
  Mock.GOS_CreateTask_CallInstance = CMock_Guts_MemChain(Mock.GOS_CreateTask_CallInstance, cmock_guts_index);
  Mock.GOS_CreateTask_IgnoreBool = (int)0;
  cmock_call_instance->LineNumber = cmock_line;
  cmock_call_instance->IgnoreMode = CMOCK_ARG_ALL;
  CMockExpectParameters_GOS_CreateTask(cmock_call_instance, pvTaskCode);
  memcpy(&cmock_call_instance->ReturnVal, &cmock_to_return, sizeof(GOS_Return_e));
  UNITY_CLR_DETAILS();
}

void GOS_CreateTask_StubWithCallback(CMOCK_GOS_CreateTask_CALLBACK Callback)
{
  Mock.GOS_CreateTask_IgnoreBool = (int)0;
  Mock.GOS_CreateTask_CallbackFunctionPointer = Callback;
}

void GOS_EnterCriticalSection(void)
{
  UNITY_LINE_TYPE cmock_line = TEST_LINE_NUM;
  CMOCK_GOS_EnterCriticalSection_CALL_INSTANCE* cmock_call_instance;
  UNITY_SET_DETAIL(CMockString_GOS_EnterCriticalSection);
  cmock_call_instance = (CMOCK_GOS_EnterCriticalSection_CALL_INSTANCE*)CMock_Guts_GetAddressFor(Mock.GOS_EnterCriticalSection_CallInstance);
  Mock.GOS_EnterCriticalSection_CallInstance = CMock_Guts_MemNext(Mock.GOS_EnterCriticalSection_CallInstance);
  if (Mock.GOS_EnterCriticalSection_IgnoreBool)
  {
    UNITY_CLR_DETAILS();
    return;
  }
  if (Mock.GOS_EnterCriticalSection_CallbackFunctionPointer != NULL)
  {
    Mock.GOS_EnterCriticalSection_CallbackFunctionPointer(Mock.GOS_EnterCriticalSection_CallbackCalls++);
    return;
  }
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringCalledMore);
  cmock_line = cmock_call_instance->LineNumber;
  UNITY_CLR_DETAILS();
}

void GOS_EnterCriticalSection_CMockIgnore(void)
{
  Mock.GOS_EnterCriticalSection_IgnoreBool = (int)1;
}

void GOS_EnterCriticalSection_CMockExpectAnyArgs(UNITY_LINE_TYPE cmock_line)
{
  CMOCK_MEM_INDEX_TYPE cmock_guts_index = CMock_Guts_MemNew(sizeof(CMOCK_GOS_EnterCriticalSection_CALL_INSTANCE));
  CMOCK_GOS_EnterCriticalSection_CALL_INSTANCE* cmock_call_instance = (CMOCK_GOS_EnterCriticalSection_CALL_INSTANCE*)CMock_Guts_GetAddressFor(cmock_guts_index);
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringOutOfMemory);
  memset(cmock_call_instance, 0, sizeof(*cmock_call_instance));
  Mock.GOS_EnterCriticalSection_CallInstance = CMock_Guts_MemChain(Mock.GOS_EnterCriticalSection_CallInstance, cmock_guts_index);
  Mock.GOS_EnterCriticalSection_IgnoreBool = (int)0;
  cmock_call_instance->LineNumber = cmock_line;
  cmock_call_instance->IgnoreMode = CMOCK_ARG_ALL;
  cmock_call_instance->IgnoreMode = CMOCK_ARG_NONE;
}

void GOS_EnterCriticalSection_CMockExpect(UNITY_LINE_TYPE cmock_line)
{
  CMOCK_MEM_INDEX_TYPE cmock_guts_index = CMock_Guts_MemNew(sizeof(CMOCK_GOS_EnterCriticalSection_CALL_INSTANCE));
  CMOCK_GOS_EnterCriticalSection_CALL_INSTANCE* cmock_call_instance = (CMOCK_GOS_EnterCriticalSection_CALL_INSTANCE*)CMock_Guts_GetAddressFor(cmock_guts_index);
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringOutOfMemory);
  memset(cmock_call_instance, 0, sizeof(*cmock_call_instance));
  Mock.GOS_EnterCriticalSection_CallInstance = CMock_Guts_MemChain(Mock.GOS_EnterCriticalSection_CallInstance, cmock_guts_index);
  Mock.GOS_EnterCriticalSection_IgnoreBool = (int)0;
  cmock_call_instance->LineNumber = cmock_line;
  cmock_call_instance->IgnoreMode = CMOCK_ARG_ALL;
  UNITY_CLR_DETAILS();
}

void GOS_EnterCriticalSection_StubWithCallback(CMOCK_GOS_EnterCriticalSection_CALLBACK Callback)
{
  Mock.GOS_EnterCriticalSection_IgnoreBool = (int)0;
  Mock.GOS_EnterCriticalSection_CallbackFunctionPointer = Callback;
}

void GOS_ExitCriticalSection(void)
{
  UNITY_LINE_TYPE cmock_line = TEST_LINE_NUM;
  CMOCK_GOS_ExitCriticalSection_CALL_INSTANCE* cmock_call_instance;
  UNITY_SET_DETAIL(CMockString_GOS_ExitCriticalSection);
  cmock_call_instance = (CMOCK_GOS_ExitCriticalSection_CALL_INSTANCE*)CMock_Guts_GetAddressFor(Mock.GOS_ExitCriticalSection_CallInstance);
  Mock.GOS_ExitCriticalSection_CallInstance = CMock_Guts_MemNext(Mock.GOS_ExitCriticalSection_CallInstance);
  if (Mock.GOS_ExitCriticalSection_IgnoreBool)
  {
    UNITY_CLR_DETAILS();
    return;
  }
  if (Mock.GOS_ExitCriticalSection_CallbackFunctionPointer != NULL)
  {
    Mock.GOS_ExitCriticalSection_CallbackFunctionPointer(Mock.GOS_ExitCriticalSection_CallbackCalls++);
    return;
  }
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringCalledMore);
  cmock_line = cmock_call_instance->LineNumber;
  UNITY_CLR_DETAILS();
}

void GOS_ExitCriticalSection_CMockIgnore(void)
{
  Mock.GOS_ExitCriticalSection_IgnoreBool = (int)1;
}

void GOS_ExitCriticalSection_CMockExpectAnyArgs(UNITY_LINE_TYPE cmock_line)
{
  CMOCK_MEM_INDEX_TYPE cmock_guts_index = CMock_Guts_MemNew(sizeof(CMOCK_GOS_ExitCriticalSection_CALL_INSTANCE));
  CMOCK_GOS_ExitCriticalSection_CALL_INSTANCE* cmock_call_instance = (CMOCK_GOS_ExitCriticalSection_CALL_INSTANCE*)CMock_Guts_GetAddressFor(cmock_guts_index);
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringOutOfMemory);
  memset(cmock_call_instance, 0, sizeof(*cmock_call_instance));
  Mock.GOS_ExitCriticalSection_CallInstance = CMock_Guts_MemChain(Mock.GOS_ExitCriticalSection_CallInstance, cmock_guts_index);
  Mock.GOS_ExitCriticalSection_IgnoreBool = (int)0;
  cmock_call_instance->LineNumber = cmock_line;
  cmock_call_instance->IgnoreMode = CMOCK_ARG_ALL;
  cmock_call_instance->IgnoreMode = CMOCK_ARG_NONE;
}

void GOS_ExitCriticalSection_CMockExpect(UNITY_LINE_TYPE cmock_line)
{
  CMOCK_MEM_INDEX_TYPE cmock_guts_index = CMock_Guts_MemNew(sizeof(CMOCK_GOS_ExitCriticalSection_CALL_INSTANCE));
  CMOCK_GOS_ExitCriticalSection_CALL_INSTANCE* cmock_call_instance = (CMOCK_GOS_ExitCriticalSection_CALL_INSTANCE*)CMock_Guts_GetAddressFor(cmock_guts_index);
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringOutOfMemory);
  memset(cmock_call_instance, 0, sizeof(*cmock_call_instance));
  Mock.GOS_ExitCriticalSection_CallInstance = CMock_Guts_MemChain(Mock.GOS_ExitCriticalSection_CallInstance, cmock_guts_index);
  Mock.GOS_ExitCriticalSection_IgnoreBool = (int)0;
  cmock_call_instance->LineNumber = cmock_line;
  cmock_call_instance->IgnoreMode = CMOCK_ARG_ALL;
  UNITY_CLR_DETAILS();
}

void GOS_ExitCriticalSection_StubWithCallback(CMOCK_GOS_ExitCriticalSection_CALLBACK Callback)
{
  Mock.GOS_ExitCriticalSection_IgnoreBool = (int)0;
  Mock.GOS_ExitCriticalSection_CallbackFunctionPointer = Callback;
}

