/* AUTOGENERATED FILE. DO NOT EDIT. */
#ifndef _MOCKRIMU_H
#define _MOCKRIMU_H

#include "RIMU.h"

/* Ignore the following warnings, since we are copying code */
#if defined(__GNUC__) && !defined(__ICC) && !defined(__TMS470__)
#if __GNUC__ > 4 || (__GNUC__ == 4 && (__GNUC_MINOR__ > 6 || (__GNUC_MINOR__ == 6 && __GNUC_PATCHLEVEL__ > 0)))
#pragma GCC diagnostic push
#endif
#if !defined(__clang__)
#pragma GCC diagnostic ignored "-Wpragmas"
#endif
#pragma GCC diagnostic ignored "-Wunknown-pragmas"
#pragma GCC diagnostic ignored "-Wduplicate-decl-specifier"
#endif

void MockRIMU_Init(void);
void MockRIMU_Destroy(void);
void MockRIMU_Verify(void);




#define RIMU_Initialize_IgnoreAndReturn(cmock_retval) RIMU_Initialize_CMockIgnoreAndReturn(__LINE__, cmock_retval)
void RIMU_Initialize_CMockIgnoreAndReturn(UNITY_LINE_TYPE cmock_line, GINIT_Return_e cmock_to_return);
#define RIMU_Initialize_ExpectAnyArgsAndReturn(cmock_retval) RIMU_Initialize_CMockExpectAnyArgsAndReturn(__LINE__, cmock_retval)
void RIMU_Initialize_CMockExpectAnyArgsAndReturn(UNITY_LINE_TYPE cmock_line, GINIT_Return_e cmock_to_return);
#define RIMU_Initialize_ExpectAndReturn(initLevel, cmock_retval) RIMU_Initialize_CMockExpectAndReturn(__LINE__, initLevel, cmock_retval)
void RIMU_Initialize_CMockExpectAndReturn(UNITY_LINE_TYPE cmock_line, GINIT_InitLevel_e initLevel, GINIT_Return_e cmock_to_return);
typedef GINIT_Return_e (* CMOCK_RIMU_Initialize_CALLBACK)(GINIT_InitLevel_e initLevel, int cmock_num_calls);
void RIMU_Initialize_StubWithCallback(CMOCK_RIMU_Initialize_CALLBACK Callback);
#define RIMU_GetIMUAngleFiltered_IgnoreAndReturn(cmock_retval) RIMU_GetIMUAngleFiltered_CMockIgnoreAndReturn(__LINE__, cmock_retval)
void RIMU_GetIMUAngleFiltered_CMockIgnoreAndReturn(UNITY_LINE_TYPE cmock_line, RIMU_DataStruct_s cmock_to_return);
#define RIMU_GetIMUAngleFiltered_ExpectAnyArgsAndReturn(cmock_retval) RIMU_GetIMUAngleFiltered_CMockExpectAnyArgsAndReturn(__LINE__, cmock_retval)
void RIMU_GetIMUAngleFiltered_CMockExpectAnyArgsAndReturn(UNITY_LINE_TYPE cmock_line, RIMU_DataStruct_s cmock_to_return);
#define RIMU_GetIMUAngleFiltered_ExpectAndReturn(pos, cmock_retval) RIMU_GetIMUAngleFiltered_CMockExpectAndReturn(__LINE__, pos, cmock_retval)
void RIMU_GetIMUAngleFiltered_CMockExpectAndReturn(UNITY_LINE_TYPE cmock_line, RIMU_IMUPosition_e pos, RIMU_DataStruct_s cmock_to_return);
typedef RIMU_DataStruct_s (* CMOCK_RIMU_GetIMUAngleFiltered_CALLBACK)(RIMU_IMUPosition_e pos, int cmock_num_calls);
void RIMU_GetIMUAngleFiltered_StubWithCallback(CMOCK_RIMU_GetIMUAngleFiltered_CALLBACK Callback);
#define RIMU_GetIMUAngleRaw_IgnoreAndReturn(cmock_retval) RIMU_GetIMUAngleRaw_CMockIgnoreAndReturn(__LINE__, cmock_retval)
void RIMU_GetIMUAngleRaw_CMockIgnoreAndReturn(UNITY_LINE_TYPE cmock_line, RIMU_DataStruct_s cmock_to_return);
#define RIMU_GetIMUAngleRaw_ExpectAnyArgsAndReturn(cmock_retval) RIMU_GetIMUAngleRaw_CMockExpectAnyArgsAndReturn(__LINE__, cmock_retval)
void RIMU_GetIMUAngleRaw_CMockExpectAnyArgsAndReturn(UNITY_LINE_TYPE cmock_line, RIMU_DataStruct_s cmock_to_return);
#define RIMU_GetIMUAngleRaw_ExpectAndReturn(pos, cmock_retval) RIMU_GetIMUAngleRaw_CMockExpectAndReturn(__LINE__, pos, cmock_retval)
void RIMU_GetIMUAngleRaw_CMockExpectAndReturn(UNITY_LINE_TYPE cmock_line, RIMU_IMUPosition_e pos, RIMU_DataStruct_s cmock_to_return);
typedef RIMU_DataStruct_s (* CMOCK_RIMU_GetIMUAngleRaw_CALLBACK)(RIMU_IMUPosition_e pos, int cmock_num_calls);
void RIMU_GetIMUAngleRaw_StubWithCallback(CMOCK_RIMU_GetIMUAngleRaw_CALLBACK Callback);
#define RIMU_StartCalibration_Ignore() RIMU_StartCalibration_CMockIgnore()
void RIMU_StartCalibration_CMockIgnore(void);
#define RIMU_StartCalibration_ExpectAnyArgs() RIMU_StartCalibration_CMockExpectAnyArgs(__LINE__)
void RIMU_StartCalibration_CMockExpectAnyArgs(UNITY_LINE_TYPE cmock_line);
#define RIMU_StartCalibration_Expect() RIMU_StartCalibration_CMockExpect(__LINE__)
void RIMU_StartCalibration_CMockExpect(UNITY_LINE_TYPE cmock_line);
typedef void (* CMOCK_RIMU_StartCalibration_CALLBACK)(int cmock_num_calls);
void RIMU_StartCalibration_StubWithCallback(CMOCK_RIMU_StartCalibration_CALLBACK Callback);
#define RIMU_DoCalibration_Ignore() RIMU_DoCalibration_CMockIgnore()
void RIMU_DoCalibration_CMockIgnore(void);
#define RIMU_DoCalibration_ExpectAnyArgs() RIMU_DoCalibration_CMockExpectAnyArgs(__LINE__)
void RIMU_DoCalibration_CMockExpectAnyArgs(UNITY_LINE_TYPE cmock_line);
#define RIMU_DoCalibration_Expect() RIMU_DoCalibration_CMockExpect(__LINE__)
void RIMU_DoCalibration_CMockExpect(UNITY_LINE_TYPE cmock_line);
typedef void (* CMOCK_RIMU_DoCalibration_CALLBACK)(int cmock_num_calls);
void RIMU_DoCalibration_StubWithCallback(CMOCK_RIMU_DoCalibration_CALLBACK Callback);
#define RIMU_StopCalibration_Ignore() RIMU_StopCalibration_CMockIgnore()
void RIMU_StopCalibration_CMockIgnore(void);
#define RIMU_StopCalibration_ExpectAnyArgs() RIMU_StopCalibration_CMockExpectAnyArgs(__LINE__)
void RIMU_StopCalibration_CMockExpectAnyArgs(UNITY_LINE_TYPE cmock_line);
#define RIMU_StopCalibration_Expect() RIMU_StopCalibration_CMockExpect(__LINE__)
void RIMU_StopCalibration_CMockExpect(UNITY_LINE_TYPE cmock_line);
typedef void (* CMOCK_RIMU_StopCalibration_CALLBACK)(int cmock_num_calls);
void RIMU_StopCalibration_StubWithCallback(CMOCK_RIMU_StopCalibration_CALLBACK Callback);
#define RIMU_AbortCalibration_Ignore() RIMU_AbortCalibration_CMockIgnore()
void RIMU_AbortCalibration_CMockIgnore(void);
#define RIMU_AbortCalibration_ExpectAnyArgs() RIMU_AbortCalibration_CMockExpectAnyArgs(__LINE__)
void RIMU_AbortCalibration_CMockExpectAnyArgs(UNITY_LINE_TYPE cmock_line);
#define RIMU_AbortCalibration_Expect() RIMU_AbortCalibration_CMockExpect(__LINE__)
void RIMU_AbortCalibration_CMockExpect(UNITY_LINE_TYPE cmock_line);
typedef void (* CMOCK_RIMU_AbortCalibration_CALLBACK)(int cmock_num_calls);
void RIMU_AbortCalibration_StubWithCallback(CMOCK_RIMU_AbortCalibration_CALLBACK Callback);

#if defined(__GNUC__) && !defined(__ICC) && !defined(__TMS470__)
#if __GNUC__ > 4 || (__GNUC__ == 4 && (__GNUC_MINOR__ > 6 || (__GNUC_MINOR__ == 6 && __GNUC_PATCHLEVEL__ > 0)))
#pragma GCC diagnostic pop
#endif
#endif

#endif
