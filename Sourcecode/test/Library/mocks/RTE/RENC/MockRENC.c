/* AUTOGENERATED FILE. DO NOT EDIT. */
#include <string.h>
#include <stdlib.h>
#include <setjmp.h>
#include "unity.h"
#include "cmock.h"
#include "MockRENC.h"

static const char* CMockString_RENC_Initialize = "RENC_Initialize";
static const char* CMockString_RENC_getEncoderCounts = "RENC_getEncoderCounts";
static const char* CMockString_initLevel = "initLevel";

typedef struct _CMOCK_RENC_Initialize_CALL_INSTANCE
{
  UNITY_LINE_TYPE LineNumber;
  CMOCK_ARG_MODE IgnoreMode;
  GINIT_Return_e ReturnVal;
  GINIT_InitLevel_e Expected_initLevel;

} CMOCK_RENC_Initialize_CALL_INSTANCE;

typedef struct _CMOCK_RENC_getEncoderCounts_CALL_INSTANCE
{
  UNITY_LINE_TYPE LineNumber;
  CMOCK_ARG_MODE IgnoreMode;
  RENC_EncoderCounts_s ReturnVal;

} CMOCK_RENC_getEncoderCounts_CALL_INSTANCE;

static struct MockRENCInstance
{
  int RENC_Initialize_IgnoreBool;
  GINIT_Return_e RENC_Initialize_FinalReturn;
  CMOCK_RENC_Initialize_CALLBACK RENC_Initialize_CallbackFunctionPointer;
  int RENC_Initialize_CallbackCalls;
  CMOCK_MEM_INDEX_TYPE RENC_Initialize_CallInstance;
  int RENC_getEncoderCounts_IgnoreBool;
  RENC_EncoderCounts_s RENC_getEncoderCounts_FinalReturn;
  CMOCK_RENC_getEncoderCounts_CALLBACK RENC_getEncoderCounts_CallbackFunctionPointer;
  int RENC_getEncoderCounts_CallbackCalls;
  CMOCK_MEM_INDEX_TYPE RENC_getEncoderCounts_CallInstance;
} Mock;

extern jmp_buf AbortFrame;

void MockRENC_Verify(void)
{
  UNITY_LINE_TYPE cmock_line = TEST_LINE_NUM;
  if (Mock.RENC_Initialize_IgnoreBool)
    Mock.RENC_Initialize_CallInstance = CMOCK_GUTS_NONE;
  UNITY_SET_DETAIL(CMockString_RENC_Initialize);
  UNITY_TEST_ASSERT(CMOCK_GUTS_NONE == Mock.RENC_Initialize_CallInstance, cmock_line, CMockStringCalledLess);
  if (Mock.RENC_Initialize_CallbackFunctionPointer != NULL)
    Mock.RENC_Initialize_CallInstance = CMOCK_GUTS_NONE;
  if (Mock.RENC_getEncoderCounts_IgnoreBool)
    Mock.RENC_getEncoderCounts_CallInstance = CMOCK_GUTS_NONE;
  UNITY_SET_DETAIL(CMockString_RENC_getEncoderCounts);
  UNITY_TEST_ASSERT(CMOCK_GUTS_NONE == Mock.RENC_getEncoderCounts_CallInstance, cmock_line, CMockStringCalledLess);
  if (Mock.RENC_getEncoderCounts_CallbackFunctionPointer != NULL)
    Mock.RENC_getEncoderCounts_CallInstance = CMOCK_GUTS_NONE;
}

void MockRENC_Init(void)
{
  MockRENC_Destroy();
}

void MockRENC_Destroy(void)
{
  CMock_Guts_MemFreeAll();
  memset(&Mock, 0, sizeof(Mock));
  Mock.RENC_Initialize_CallbackFunctionPointer = NULL;
  Mock.RENC_Initialize_CallbackCalls = 0;
  Mock.RENC_getEncoderCounts_CallbackFunctionPointer = NULL;
  Mock.RENC_getEncoderCounts_CallbackCalls = 0;
}

GINIT_Return_e RENC_Initialize(GINIT_InitLevel_e initLevel)
{
  UNITY_LINE_TYPE cmock_line = TEST_LINE_NUM;
  CMOCK_RENC_Initialize_CALL_INSTANCE* cmock_call_instance;
  UNITY_SET_DETAIL(CMockString_RENC_Initialize);
  cmock_call_instance = (CMOCK_RENC_Initialize_CALL_INSTANCE*)CMock_Guts_GetAddressFor(Mock.RENC_Initialize_CallInstance);
  Mock.RENC_Initialize_CallInstance = CMock_Guts_MemNext(Mock.RENC_Initialize_CallInstance);
  if (Mock.RENC_Initialize_IgnoreBool)
  {
    UNITY_CLR_DETAILS();
    if (cmock_call_instance == NULL)
      return Mock.RENC_Initialize_FinalReturn;
    memcpy(&Mock.RENC_Initialize_FinalReturn, &cmock_call_instance->ReturnVal, sizeof(GINIT_Return_e));
    return cmock_call_instance->ReturnVal;
  }
  if (Mock.RENC_Initialize_CallbackFunctionPointer != NULL)
  {
    return Mock.RENC_Initialize_CallbackFunctionPointer(initLevel, Mock.RENC_Initialize_CallbackCalls++);
  }
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringCalledMore);
  cmock_line = cmock_call_instance->LineNumber;
  if (cmock_call_instance->IgnoreMode != CMOCK_ARG_NONE)
  {
  {
    UNITY_SET_DETAILS(CMockString_RENC_Initialize,CMockString_initLevel);
    UNITY_TEST_ASSERT_EQUAL_MEMORY((void*)(&cmock_call_instance->Expected_initLevel), (void*)(&initLevel), sizeof(GINIT_InitLevel_e), cmock_line, CMockStringMismatch);
  }
  }
  UNITY_CLR_DETAILS();
  return cmock_call_instance->ReturnVal;
}

void CMockExpectParameters_RENC_Initialize(CMOCK_RENC_Initialize_CALL_INSTANCE* cmock_call_instance, GINIT_InitLevel_e initLevel)
{
  memcpy(&cmock_call_instance->Expected_initLevel, &initLevel, sizeof(GINIT_InitLevel_e));
}

void RENC_Initialize_CMockIgnoreAndReturn(UNITY_LINE_TYPE cmock_line, GINIT_Return_e cmock_to_return)
{
  CMOCK_MEM_INDEX_TYPE cmock_guts_index = CMock_Guts_MemNew(sizeof(CMOCK_RENC_Initialize_CALL_INSTANCE));
  CMOCK_RENC_Initialize_CALL_INSTANCE* cmock_call_instance = (CMOCK_RENC_Initialize_CALL_INSTANCE*)CMock_Guts_GetAddressFor(cmock_guts_index);
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringOutOfMemory);
  memset(cmock_call_instance, 0, sizeof(*cmock_call_instance));
  Mock.RENC_Initialize_CallInstance = CMock_Guts_MemChain(Mock.RENC_Initialize_CallInstance, cmock_guts_index);
  Mock.RENC_Initialize_IgnoreBool = (int)0;
  cmock_call_instance->LineNumber = cmock_line;
  cmock_call_instance->IgnoreMode = CMOCK_ARG_ALL;
  cmock_call_instance->ReturnVal = cmock_to_return;
  Mock.RENC_Initialize_IgnoreBool = (int)1;
}

void RENC_Initialize_CMockExpectAnyArgsAndReturn(UNITY_LINE_TYPE cmock_line, GINIT_Return_e cmock_to_return)
{
  CMOCK_MEM_INDEX_TYPE cmock_guts_index = CMock_Guts_MemNew(sizeof(CMOCK_RENC_Initialize_CALL_INSTANCE));
  CMOCK_RENC_Initialize_CALL_INSTANCE* cmock_call_instance = (CMOCK_RENC_Initialize_CALL_INSTANCE*)CMock_Guts_GetAddressFor(cmock_guts_index);
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringOutOfMemory);
  memset(cmock_call_instance, 0, sizeof(*cmock_call_instance));
  Mock.RENC_Initialize_CallInstance = CMock_Guts_MemChain(Mock.RENC_Initialize_CallInstance, cmock_guts_index);
  Mock.RENC_Initialize_IgnoreBool = (int)0;
  cmock_call_instance->LineNumber = cmock_line;
  cmock_call_instance->IgnoreMode = CMOCK_ARG_ALL;
  cmock_call_instance->ReturnVal = cmock_to_return;
  cmock_call_instance->IgnoreMode = CMOCK_ARG_NONE;
}

void RENC_Initialize_CMockExpectAndReturn(UNITY_LINE_TYPE cmock_line, GINIT_InitLevel_e initLevel, GINIT_Return_e cmock_to_return)
{
  CMOCK_MEM_INDEX_TYPE cmock_guts_index = CMock_Guts_MemNew(sizeof(CMOCK_RENC_Initialize_CALL_INSTANCE));
  CMOCK_RENC_Initialize_CALL_INSTANCE* cmock_call_instance = (CMOCK_RENC_Initialize_CALL_INSTANCE*)CMock_Guts_GetAddressFor(cmock_guts_index);
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringOutOfMemory);
  memset(cmock_call_instance, 0, sizeof(*cmock_call_instance));
  Mock.RENC_Initialize_CallInstance = CMock_Guts_MemChain(Mock.RENC_Initialize_CallInstance, cmock_guts_index);
  Mock.RENC_Initialize_IgnoreBool = (int)0;
  cmock_call_instance->LineNumber = cmock_line;
  cmock_call_instance->IgnoreMode = CMOCK_ARG_ALL;
  CMockExpectParameters_RENC_Initialize(cmock_call_instance, initLevel);
  memcpy(&cmock_call_instance->ReturnVal, &cmock_to_return, sizeof(GINIT_Return_e));
  UNITY_CLR_DETAILS();
}

void RENC_Initialize_StubWithCallback(CMOCK_RENC_Initialize_CALLBACK Callback)
{
  Mock.RENC_Initialize_IgnoreBool = (int)0;
  Mock.RENC_Initialize_CallbackFunctionPointer = Callback;
}

RENC_EncoderCounts_s RENC_getEncoderCounts(void)
{
  UNITY_LINE_TYPE cmock_line = TEST_LINE_NUM;
  CMOCK_RENC_getEncoderCounts_CALL_INSTANCE* cmock_call_instance;
  UNITY_SET_DETAIL(CMockString_RENC_getEncoderCounts);
  cmock_call_instance = (CMOCK_RENC_getEncoderCounts_CALL_INSTANCE*)CMock_Guts_GetAddressFor(Mock.RENC_getEncoderCounts_CallInstance);
  Mock.RENC_getEncoderCounts_CallInstance = CMock_Guts_MemNext(Mock.RENC_getEncoderCounts_CallInstance);
  if (Mock.RENC_getEncoderCounts_IgnoreBool)
  {
    UNITY_CLR_DETAILS();
    if (cmock_call_instance == NULL)
      return Mock.RENC_getEncoderCounts_FinalReturn;
    memcpy(&Mock.RENC_getEncoderCounts_FinalReturn, &cmock_call_instance->ReturnVal, sizeof(RENC_EncoderCounts_s));
    return cmock_call_instance->ReturnVal;
  }
  if (Mock.RENC_getEncoderCounts_CallbackFunctionPointer != NULL)
  {
    return Mock.RENC_getEncoderCounts_CallbackFunctionPointer(Mock.RENC_getEncoderCounts_CallbackCalls++);
  }
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringCalledMore);
  cmock_line = cmock_call_instance->LineNumber;
  UNITY_CLR_DETAILS();
  return cmock_call_instance->ReturnVal;
}

void RENC_getEncoderCounts_CMockIgnoreAndReturn(UNITY_LINE_TYPE cmock_line, RENC_EncoderCounts_s cmock_to_return)
{
  CMOCK_MEM_INDEX_TYPE cmock_guts_index = CMock_Guts_MemNew(sizeof(CMOCK_RENC_getEncoderCounts_CALL_INSTANCE));
  CMOCK_RENC_getEncoderCounts_CALL_INSTANCE* cmock_call_instance = (CMOCK_RENC_getEncoderCounts_CALL_INSTANCE*)CMock_Guts_GetAddressFor(cmock_guts_index);
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringOutOfMemory);
  memset(cmock_call_instance, 0, sizeof(*cmock_call_instance));
  Mock.RENC_getEncoderCounts_CallInstance = CMock_Guts_MemChain(Mock.RENC_getEncoderCounts_CallInstance, cmock_guts_index);
  Mock.RENC_getEncoderCounts_IgnoreBool = (int)0;
  cmock_call_instance->LineNumber = cmock_line;
  cmock_call_instance->IgnoreMode = CMOCK_ARG_ALL;
  cmock_call_instance->ReturnVal = cmock_to_return;
  Mock.RENC_getEncoderCounts_IgnoreBool = (int)1;
}

void RENC_getEncoderCounts_CMockExpectAnyArgsAndReturn(UNITY_LINE_TYPE cmock_line, RENC_EncoderCounts_s cmock_to_return)
{
  CMOCK_MEM_INDEX_TYPE cmock_guts_index = CMock_Guts_MemNew(sizeof(CMOCK_RENC_getEncoderCounts_CALL_INSTANCE));
  CMOCK_RENC_getEncoderCounts_CALL_INSTANCE* cmock_call_instance = (CMOCK_RENC_getEncoderCounts_CALL_INSTANCE*)CMock_Guts_GetAddressFor(cmock_guts_index);
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringOutOfMemory);
  memset(cmock_call_instance, 0, sizeof(*cmock_call_instance));
  Mock.RENC_getEncoderCounts_CallInstance = CMock_Guts_MemChain(Mock.RENC_getEncoderCounts_CallInstance, cmock_guts_index);
  Mock.RENC_getEncoderCounts_IgnoreBool = (int)0;
  cmock_call_instance->LineNumber = cmock_line;
  cmock_call_instance->IgnoreMode = CMOCK_ARG_ALL;
  cmock_call_instance->ReturnVal = cmock_to_return;
  cmock_call_instance->IgnoreMode = CMOCK_ARG_NONE;
}

void RENC_getEncoderCounts_CMockExpectAndReturn(UNITY_LINE_TYPE cmock_line, RENC_EncoderCounts_s cmock_to_return)
{
  CMOCK_MEM_INDEX_TYPE cmock_guts_index = CMock_Guts_MemNew(sizeof(CMOCK_RENC_getEncoderCounts_CALL_INSTANCE));
  CMOCK_RENC_getEncoderCounts_CALL_INSTANCE* cmock_call_instance = (CMOCK_RENC_getEncoderCounts_CALL_INSTANCE*)CMock_Guts_GetAddressFor(cmock_guts_index);
  UNITY_TEST_ASSERT_NOT_NULL(cmock_call_instance, cmock_line, CMockStringOutOfMemory);
  memset(cmock_call_instance, 0, sizeof(*cmock_call_instance));
  Mock.RENC_getEncoderCounts_CallInstance = CMock_Guts_MemChain(Mock.RENC_getEncoderCounts_CallInstance, cmock_guts_index);
  Mock.RENC_getEncoderCounts_IgnoreBool = (int)0;
  cmock_call_instance->LineNumber = cmock_line;
  cmock_call_instance->IgnoreMode = CMOCK_ARG_ALL;
  memcpy(&cmock_call_instance->ReturnVal, &cmock_to_return, sizeof(RENC_EncoderCounts_s));
  UNITY_CLR_DETAILS();
}

void RENC_getEncoderCounts_StubWithCallback(CMOCK_RENC_getEncoderCounts_CALLBACK Callback)
{
  Mock.RENC_getEncoderCounts_IgnoreBool = (int)0;
  Mock.RENC_getEncoderCounts_CallbackFunctionPointer = Callback;
}

