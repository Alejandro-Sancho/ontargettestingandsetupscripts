/*
 * TestGLOG.c
 *
 *  Created on: Dec 22, 2017
 *      Author: MyoSwissEngineer
 */

#include <GUTL/inc/GUTL_GuardedValues.h>
#include "unity.h"
#include "unity_fixture.h"
#include "GINIT.h"
#include "types.h"
#include "MockGERR.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>



/***********************************************************************/
/* LOCAL CONSTANTS                                                     */
/***********************************************************************/
/**
 * Be sure this param matches with GLOG.c maxCharacterLengthOfSerializedLogMsg
 */

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/


/***********************************************************************/
/* Local Variables					                                   */
/***********************************************************************/


/***********************************************************************/
/* LOCAL FUNCTION PROTOTYPES                                           */
/***********************************************************************/


/***********************************************************************/
/* TESTS					                                           */
/***********************************************************************/



TEST_GROUP(GUTL_GuardedValue);


TEST_GROUP_RUNNER(GUTL_GuardedValue)
{
	RUN_TEST_CASE(GUTL_GuardedValue, GuardedValue_Bool);
	RUN_TEST_CASE(GUTL_GuardedValue, GuardedValue_Uint8);
	RUN_TEST_CASE(GUTL_GuardedValue, GuardedValue_Uint32);
	RUN_TEST_CASE(GUTL_GuardedValue, GuardedValue_int32);
	RUN_TEST_CASE(GUTL_GuardedValue, GuardedValue_int16);

	RUN_TEST_CASE(GUTL_GuardedValue, GuardedValue_Force_Newton);
	RUN_TEST_CASE(GUTL_GuardedValue, GuardedValue_Angle_Degree);
	RUN_TEST_CASE(GUTL_GuardedValue, GuardedValue_Temp_Celcius);
	RUN_TEST_CASE(GUTL_GuardedValue, GuardedValue_Voltage_mV_t_s);
	RUN_TEST_CASE(GUTL_GuardedValue, GuardedValue_Current_mA_t_s);


}
//This is run before EACH TEST

TEST_SETUP(GUTL_GuardedValue)
{
}

TEST_TEAR_DOWN(GUTL_GuardedValue)
{
	MockGERR_Verify();
	MockGERR_Destroy();


}

TEST(GUTL_GuardedValue, GuardedValue_Bool)
{
	GUTL_GuardedValue_Bool_t_s guardedValBool;

	// check bool true
	guardedValBool.value = false;
	GUTL_GuardedValue_set_Bool_t(&guardedValBool,true);
	TEST_ASSERT_EQUAL(true,GUTL_GuardedValue_get_Bool_t(guardedValBool));

	// check bool false
	guardedValBool.value = true;
	GUTL_GuardedValue_set_Bool_t(&guardedValBool,false);
	TEST_ASSERT_EQUAL(false,GUTL_GuardedValue_get_Bool_t(guardedValBool));


	// check bitflip occurred

	GERR_ErrorSelect_CMockIgnore();
	guardedValBool.value = true;
	guardedValBool.inv_value = true;
	TEST_ASSERT_EQUAL(true,GUTL_GuardedValue_get_Bool_t(guardedValBool));

}


TEST(GUTL_GuardedValue, GuardedValue_Uint8)
{
	GUTL_GuardedValue_uint8_t_s guardedVal;


	guardedVal.value = 100u;
	GUTL_GuardedValue_set_Uint8(&guardedVal,255u);
	TEST_ASSERT_EQUAL(255u,GUTL_GuardedValue_get_Uint8_t(guardedVal));

	GUTL_GuardedValue_set_Uint8(&guardedVal,0);
	TEST_ASSERT_EQUAL(0,GUTL_GuardedValue_get_Uint8_t(guardedVal));

	// check bitflip occurred
	GERR_ErrorSelect_CMockIgnore();
	guardedVal.value = 5;
	guardedVal.inv_value = (~(5|0x02));
	TEST_ASSERT_EQUAL(5,GUTL_GuardedValue_get_Uint8_t(guardedVal));
}

TEST(GUTL_GuardedValue, GuardedValue_Uint32)
{
	GUTL_GuardedValue_uint32_t_s guardedVal;

	guardedVal.value = 100u;
	GUTL_GuardedValue_set_Uint32_t(&guardedVal, 301202);
	TEST_ASSERT_EQUAL(301202,GUTL_GuardedValue_getUint32_t(guardedVal));

	GUTL_GuardedValue_set_Uint32_t(&guardedVal,0);
	TEST_ASSERT_EQUAL(0,GUTL_GuardedValue_getUint32_t(guardedVal));


	// check bitflip occurred
	GERR_ErrorSelect_CMockIgnore();
	guardedVal.value = 5;
	guardedVal.inv_value = (~(5|0x02));
	TEST_ASSERT_EQUAL(5,GUTL_GuardedValue_getUint32_t(guardedVal));
}

TEST(GUTL_GuardedValue, GuardedValue_int32)
{
	GUTL_GuardedValue_int32_t_s guardedVal;

	guardedVal.value = -100;
	GUTL_GuardedValue_set_Int32_t(&guardedVal, 4582);
	TEST_ASSERT_EQUAL(4582,GUTL_GuardedValue_getInt32_t(guardedVal));

	// check bool false
	GUTL_GuardedValue_set_Int32_t(&guardedVal,-123456);
	TEST_ASSERT_EQUAL(-123456,GUTL_GuardedValue_getInt32_t(guardedVal));

	GUTL_GuardedValue_set_Int32_t(&guardedVal,0);
	TEST_ASSERT_EQUAL(0,GUTL_GuardedValue_getInt32_t(guardedVal));

	// check bitflip occurred
	GERR_ErrorSelect_CMockIgnore();
	guardedVal.value = 5;
	guardedVal.inv_value = (~(5|0x02));
	TEST_ASSERT_EQUAL(5,GUTL_GuardedValue_getInt32_t(guardedVal));

}

TEST(GUTL_GuardedValue, GuardedValue_Force_Newton)
{
	GUTL_GuardedValue_Force_Newton_t_s guardedVal;

	guardedVal.value = 100;
	GUTL_GuardedValue_set_Force_Newton_t(&guardedVal, 123456);
	TEST_ASSERT_EQUAL(123456,GUTL_GuardedValue_get_Force_Newton_t(guardedVal));

	GUTL_GuardedValue_set_Force_Newton_t(&guardedVal,-123456);
	TEST_ASSERT_EQUAL(-123456,GUTL_GuardedValue_get_Force_Newton_t(guardedVal));

	GUTL_GuardedValue_set_Force_Newton_t(&guardedVal,0);
	TEST_ASSERT_EQUAL(0,GUTL_GuardedValue_get_Force_Newton_t(guardedVal));

	// check bitflip occurred
	GERR_ErrorSelect_CMockIgnore();
	guardedVal.value = 5;
	guardedVal.inv_value = (~(5|0x02));
	TEST_ASSERT_EQUAL(5,GUTL_GuardedValue_get_Force_Newton_t(guardedVal));

}

TEST(GUTL_GuardedValue, GuardedValue_Angle_Degree)
{
	GUTL_GuardedValue_Angle_Degree_t_s guardedVal;

	guardedVal.value = 100;
	GUTL_GuardedValue_set_Angle_Degree_t(&guardedVal, 123456);
	TEST_ASSERT_EQUAL(123456,GUTL_GuardedValue_get_Angle_Degree_t(guardedVal));

	GUTL_GuardedValue_set_Angle_Degree_t(&guardedVal,-123456);
	TEST_ASSERT_EQUAL(-123456,GUTL_GuardedValue_get_Angle_Degree_t(guardedVal));

	GUTL_GuardedValue_set_Angle_Degree_t(&guardedVal,0);
	TEST_ASSERT_EQUAL(0,GUTL_GuardedValue_get_Angle_Degree_t(guardedVal));

	// check bitflip occurred
	GERR_ErrorSelect_CMockIgnore();
	guardedVal.value = 5;
	guardedVal.inv_value = (~(5|0x02));
	TEST_ASSERT_EQUAL(5,GUTL_GuardedValue_get_Angle_Degree_t(guardedVal));

}

TEST(GUTL_GuardedValue, GuardedValue_Temp_Celcius)
{
	GUTL_GuardedValue_Temp_DegreeCelsius_t_s guardedVal;

	// check bool true
	guardedVal.value = 100;
	GUTL_GuardedValue_set_Temp_DegreeCelsius_t(&guardedVal, 123456);
	TEST_ASSERT_EQUAL(123456,GUTL_GuardedValue_get_Temp_DegreeCelsius_t(guardedVal));

	// check bool false
	GUTL_GuardedValue_set_Temp_DegreeCelsius_t(&guardedVal,-123456);
	TEST_ASSERT_EQUAL(-123456,GUTL_GuardedValue_get_Temp_DegreeCelsius_t(guardedVal));

	GUTL_GuardedValue_set_Temp_DegreeCelsius_t(&guardedVal,0);
	TEST_ASSERT_EQUAL(0,GUTL_GuardedValue_get_Temp_DegreeCelsius_t(guardedVal));

	// check bitflip occurred
	GERR_ErrorSelect_CMockIgnore();
	guardedVal.value = 5;
	guardedVal.inv_value = (~(5|0x02));
	TEST_ASSERT_EQUAL(5,GUTL_GuardedValue_get_Temp_DegreeCelsius_t(guardedVal));

}

TEST(GUTL_GuardedValue, GuardedValue_Voltage_mV_t_s)
{
	GUTL_GuardedValue_Voltage_mV_t_s guardedVal;

	// check bool true
	guardedVal.value = 100;
	GUTL_GuardedValue_set_Voltage_mV_t(&guardedVal, 123456);
	TEST_ASSERT_EQUAL(123456,GUTL_GuardedValue_get_Voltage_mV_t(guardedVal));

	// check bool false
	GUTL_GuardedValue_set_Voltage_mV_t(&guardedVal,-123456);
	TEST_ASSERT_EQUAL(-123456,GUTL_GuardedValue_get_Voltage_mV_t(guardedVal));

	GUTL_GuardedValue_set_Voltage_mV_t(&guardedVal,0);
	TEST_ASSERT_EQUAL(0,GUTL_GuardedValue_get_Voltage_mV_t(guardedVal));

	// check bitflip occurred
	GERR_ErrorSelect_CMockIgnore();
	guardedVal.value = 5;
	guardedVal.inv_value = (~(5|0x02));
	TEST_ASSERT_EQUAL(5,GUTL_GuardedValue_get_Voltage_mV_t(guardedVal));

}


TEST(GUTL_GuardedValue, GuardedValue_Current_mA_t_s)
{
	GUTL_GuardedValue_Current_mA_t_s guardedVal;

	// check bool true
	guardedVal.value = 100;
	GUTL_GuardedValue_set_Current_mA_t(&guardedVal, 123456);
	TEST_ASSERT_EQUAL(123456,GUTL_GuardedValue_get_Current_mA_t(guardedVal));

	// check bool false
	GUTL_GuardedValue_set_Current_mA_t(&guardedVal,-123456);
	TEST_ASSERT_EQUAL(-123456,GUTL_GuardedValue_get_Current_mA_t(guardedVal));

	GUTL_GuardedValue_set_Current_mA_t(&guardedVal,0);
	TEST_ASSERT_EQUAL(0,GUTL_GuardedValue_get_Current_mA_t(guardedVal));

	// check bitflip occurred
	GERR_ErrorSelect_CMockIgnore();
	guardedVal.value = 5;
	guardedVal.inv_value = (~(5|0x02));
	TEST_ASSERT_EQUAL(5,GUTL_GuardedValue_get_Current_mA_t(guardedVal));

}

TEST(GUTL_GuardedValue, GuardedValue_int16)
{
	GUTL_GuardedValue_int16_t_s guardedVal;

	guardedVal.value = -100;
	GUTL_GuardedValue_set_Int16_t(&guardedVal, 4582);
	TEST_ASSERT_EQUAL(4582,GUTL_GuardedValue_getInt16_t(guardedVal));

	// check bool false
	GUTL_GuardedValue_set_Int16_t(&guardedVal,-123456);
	TEST_ASSERT_EQUAL(-123456,GUTL_GuardedValue_getInt16_t(guardedVal));

	GUTL_GuardedValue_set_Int16_t(&guardedVal,0);
	TEST_ASSERT_EQUAL(0,GUTL_GuardedValue_getInt16_t(guardedVal));

	// check bitflip occurred
	GERR_ErrorSelect_CMockIgnore();
	guardedVal.value = 5;
	guardedVal.inv_value = (~(5|0x02));
	TEST_ASSERT_EQUAL(5,GUTL_GuardedValue_getInt16_t(guardedVal));

}
