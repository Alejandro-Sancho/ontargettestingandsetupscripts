/*
 * TestGOS_HW.c
 *
 *  Created on: Feburary 7 2018
 *      Author: Michael Lehmann
 *
 *  This file basically allows to test the basic functions of the scheduling part.
 *  It needs to be tested on HW.
 */





#include "FreeRTOS.h"
#include "task.h"
#include "GINIT.h"
#include "HI2C.h"
#include "HTIM.h"
#include "RIMU.h"
#include "GOS.h"
#include "fsl_debug_console.h"
#include "fsl_gpio.h"
#include "portmacro.h"
#include "pin_mux.h"
#include "board.h"

#include "SEGGER_SYSVIEW_Config_FreeRTOS.c"
#include "SEGGER_RTT.h"

static void liveLED(void *pvParameters){
	gpio_pin_config_t led_config = {
			kGPIO_DigitalOutput, 0,
	};
	GPIO_PinInit(BOARD_LED_RED_GPIO, BOARD_LED_RED_GPIO_PIN, &led_config);
	while(1){
		GPIO_TogglePinsOutput(BOARD_LED_RED_GPIO, 1u << BOARD_LED_RED_GPIO_PIN);
		vTaskDelay(1000);

	}

}





static void taskY(void *pvParameters){
	PRINTF("\n\rTask Y started");
	static gpio_pin_config_t config = {
				kGPIO_DigitalOutput, 0,
		};
	GPIO_PinInit(GPIOA, 27U, &config);

	while(1){
		taskENTER_CRITICAL();
		GPIO_TogglePinsOutput(GPIOA,1u<< 27U);
		taskEXIT_CRITICAL();
		GOS_WaitForSignal(E_GOS_CollectData);
		GOS_NotifyAll(E_GOS_NewDataAvaiable);

	}
}

static void taskZ(void *pvParameters){
	PRINTF("\n\rTask Z started");

	while(1){
		taskENTER_CRITICAL();
		PRINTF("\n\rTask Z is running");
		taskEXIT_CRITICAL();
		GOS_WaitForSignal(E_GOS_NewDataAvaiable);
	}
}


int main(void)
{
	/* Init board hardware. */
	BOARD_InitPins();
	BOARD_BootClockHSRUN();
	BOARD_InitDebugConsole();
	SEGGER_SYSVIEW_Conf();


	GOS_Initialize(E_GINIT_InitLevel1);
	HTIM_Initialize(E_GINIT_InitLevel1);
	GOS_Initialize(E_GINIT_InitLevel2);
	HTIM_Initialize(E_GINIT_InitLevel2);
	GOS_CreateTask(E_GOS_LiveLedTask,liveLED);
	GOS_CreateTask(E_ACEM_CalculationEngineManagerTask,taskZ);
	GOS_CreateTask(E_RDAC_DataAquisitonTask,taskY);


	GOS_StartScheduler();


	for (;;){}
}



