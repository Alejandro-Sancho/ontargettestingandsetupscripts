/*
 * TestGLOG.c
 *
 *  Created on: Dec 22, 2017
 *      Author: MyoSwissEngineer
 */

#include "unity.h"
#include "unity_fixture.h"
#include "GLOG.h"
#include "RingBuffer.h"
#include "MockGOS.h"
#include "MockRLOG.h"
#include "GINIT.h"
#include "types.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>



/***********************************************************************/
/* LOCAL CONSTANTS                                                     */
/***********************************************************************/
/**
 * Be sure this param matches with GLOG.c maxCharacterLengthOfSerializedLogMsg
 */
enum {
	maxCharacterLengthOfSerializedLogMsg = 100u
};
/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/
typedef struct LoggerBufferStruct{
	Char_t loggerBuffer[maxCharacterLengthOfSerializedLogMsg];
	size_t numOfElem;
}LoggerBufferStruct;

/***********************************************************************/
/* Local Variables					                                   */
/***********************************************************************/

static GOS_TaskFunction_t sendBufferFunction;

static LoggerBufferStruct S_LoggerBufferStruct;

/***********************************************************************/
/* LOCAL FUNCTION PROTOTYPES                                           */
/***********************************************************************/

GOS_Return_e GOS_CreateTask_Callback(GOS_TaskFunction_t pvTaskCode,int numCalls);

RLOG_ReturnValue RLOG_SendBuffer_Callback(const Char_t* buf, size_t numOfElements, int numCalls);

/***********************************************************************/
/* TESTS					                                           */
/***********************************************************************/



TEST_GROUP(GLOG);


TEST_GROUP_RUNNER(GLOG)
{
	RUN_TEST_CASE(GLOG, GLOG_Initialize);
	RUN_TEST_CASE(GLOG,GLOG_BUFFER);
	RUN_TEST_CASE(GLOG,GLOG_LogMsg_unsignedParam);
	RUN_TEST_CASE(GLOG,GLOG_LogMsg_signedParam);
	RUN_TEST_CASE(GLOG,GLOG_setLevelToLog);
	RUN_TEST_CASE(GLOG,GLOG_MessageLength);

}
//This is run before EACH TEST

TEST_SETUP(GLOG)
{
	sendBufferFunction = NULL;
}

TEST_TEAR_DOWN(GLOG)
{
	MockRLOG_Verify();
	MockRLOG_Destroy();
	MockGOS_Verify();
	MockGOS_Destroy();
}

// Test initialization
TEST(GLOG, GLOG_Initialize)
{

	//RLOG_Initialize_ExpectAnyArgsAndReturn(RLOG_SUCCESS);
	GOS_CreateTask_IgnoreAndReturn(E_GOS_ReturnSuccess);


	TEST_ASSERT_EQUAL(E_GINIT_ReturnSuccess, GLOG_Initialize(E_GINIT_InitLevel1));
	TEST_ASSERT_EQUAL(E_GINIT_ReturnSuccess, GLOG_Initialize(E_GINIT_InitLevel2));


	//RLOG_Initialize_ExpectAndReturn(RLOG_FAIL);
	//TEST_ASSERT_EQUAL(E_GINIT_ReturnFailure, GLOG_Initialize(initLevel));

	//RLOG_Initialize_ExpectAndReturn(RLOG_SUCCESS);
	GOS_CreateTask_IgnoreAndReturn(E_GOS_ReturnFailed);
	TEST_ASSERT_EQUAL(E_GINIT_ReturnFailure, GLOG_Initialize(E_GINIT_InitLevel2));

}

TEST(GLOG,GLOG_BUFFER){

	GLOG_LogMessage testMsg;
	testMsg.logLevel = E_GLOG_DEBUG;
	testMsg.logNr = 5;
	testMsg.msg = "This is a log msg";
	testMsg.params[0] = 3;
	testMsg.params[1] = 2;
	testMsg.params[2] = 1;
	testMsg.sysTik = 111;

	GLOG_LogMessage emptyMsg;

	//Test when empty
	TEST_ASSERT_EQUAL(E_RingBuffer_ReturnFailed,RingBuffer_get(&testMsg));
	TEST_ASSERT_EQUAL(E_RingBuffer_ReturnFailed,RingBuffer_get(NULL));

	TEST_ASSERT_EQUAL(RINGBUFFER_SIZE,RingBuffer_space());
	//Test no data is lost in structure during cpy
	TEST_ASSERT_EQUAL(0,RingBuffer_put(&testMsg));
	TEST_ASSERT_EQUAL(0,RingBuffer_get(&emptyMsg));
	TEST_ASSERT_EQUAL_MEMORY(&emptyMsg,&testMsg,sizeof(GLOG_LogMessage));
	TEST_ASSERT_EQUAL(RINGBUFFER_SIZE,RingBuffer_space());

	uint8_t i = 0;
	for(i = 0; i < RINGBUFFER_SIZE; ++i){
		TEST_ASSERT_EQUAL(RINGBUFFER_SIZE - i,RingBuffer_space());
		TEST_ASSERT_EQUAL(0,RingBuffer_put(&testMsg));

	}
	//Buffer is full, lets check if putting is possible
	TEST_ASSERT_EQUAL(E_RingBuffer_ReturnFailed,RingBuffer_put(&testMsg));
	TEST_ASSERT_EQUAL(0,RingBuffer_space());

}



//check MSG Format
TEST(GLOG,GLOG_LogMsg_unsignedParam){

	uint32_t expectedStringLength = 0;
	//Check MSG Format**************************************************

	RLOG_Initialize_IgnoreAndReturn(RLOG_SUCCESS);
	GOS_CreateTask_IgnoreAndReturn(E_GOS_ReturnSuccess);

	//mocks the Thread Create Call
	GOS_CreateTask_StubWithCallback(GOS_CreateTask_Callback);
	RLOG_SendBuffer_StubWithCallback(RLOG_SendBuffer_Callback);

	//*******************************************************************
	//*******************************************************************
	GINIT_InitLevel_e initLevel = E_GINIT_InitLevel1;

	GLOG_Initialize(initLevel);
	GLOG_Initialize(E_GINIT_InitLevel2);

	//check for format case 1********************************************
	Char_t* logMsg = "LOG MSG : PARAM1 is %i, PARAM2 is %i PARAM3 is %i";
	Char_t expectedString[100]; //TODO remove magic number
	strcpy(expectedString,"0 INFO at 0 LOG MSG : PARAM1 is 0, PARAM2 is 210 PARAM3 is 4294967295");
	expectedStringLength = strlen(expectedString);

	GLOG_LogMsg_UnsignedParam(E_GLOG_INFO,logMsg,0,210,4294967295);
	// call the thread function
	(*sendBufferFunction)(NULL);
	// check in local buffer if msgs match
	TEST_ASSERT_EQUAL_MEMORY(expectedString,S_LoggerBufferStruct.loggerBuffer,S_LoggerBufferStruct.numOfElem);
	TEST_ASSERT_EQUAL(expectedStringLength,S_LoggerBufferStruct.numOfElem);

	//check for format case 2********************************************
	Char_t* logMsgFormat2 = "LOG MSG : PARAM1 is NOTHING, PARAM2 is %i PARAM3 is %i";
	Char_t expectedStringFormat2[100]; //TODO remove magic number
	strcpy(expectedStringFormat2,"1 INFO at 0 LOG MSG : PARAM1 is NOTHING, PARAM2 is 0 PARAM3 is 210");
	expectedStringLength = strlen(expectedStringFormat2);

	GLOG_LogMsg_UnsignedParam(E_GLOG_INFO,logMsgFormat2,0,210,999);
	// call the thread function
	(*sendBufferFunction)(NULL);
	// check in local buffer if msgs match
	TEST_ASSERT_EQUAL_MEMORY(expectedStringFormat2,S_LoggerBufferStruct.loggerBuffer,S_LoggerBufferStruct.numOfElem);
	TEST_ASSERT_EQUAL(expectedStringLength,S_LoggerBufferStruct.numOfElem);

	//check for format case 3********************************************
	Char_t* logMsgFormat3 = "LOG MSG : PARAM1 is NOTHING, PARAM2 is %i PARAM3 is NOTHING";
	Char_t expectedStringFormat3[100]; //TODO remove magic number
	strcpy(expectedStringFormat3,"2 INFO at 0 LOG MSG : PARAM1 is NOTHING, PARAM2 is 0 PARAM3 is NOTHING");
	expectedStringLength = strlen(expectedStringFormat3);

	GLOG_LogMsg_UnsignedParam(E_GLOG_INFO,logMsgFormat3,0,210,999);
	// call the thread function
	(*sendBufferFunction)(NULL);
	// check in local buffer if msgs match
	TEST_ASSERT_EQUAL_MEMORY(expectedStringFormat3,S_LoggerBufferStruct.loggerBuffer,S_LoggerBufferStruct.numOfElem);
	TEST_ASSERT_EQUAL(expectedStringLength,S_LoggerBufferStruct.numOfElem);

	//check for format case 4********************************************
	Char_t* logMsgFormat4 = "LOG MSG : PARAM1 is NOTHING, PARAM2 is NOTHING PARAM3 is NOTHING";
	Char_t expectedStringFormat4[100]; //TODO remove magic number
	strcpy(expectedStringFormat4,"3 INFO at 0 LOG MSG : PARAM1 is NOTHING, PARAM2 is NOTHING PARAM3 is NOTHING");
	expectedStringLength = strlen(expectedStringFormat4);

	GLOG_LogMsg_UnsignedParam(E_GLOG_INFO,logMsgFormat4,0,210,999);
	// call the thread function
	(*sendBufferFunction)(NULL);
	// check in local buffer if msgs match
	TEST_ASSERT_EQUAL_MEMORY(expectedStringFormat4,S_LoggerBufferStruct.loggerBuffer,S_LoggerBufferStruct.numOfElem);
	TEST_ASSERT_EQUAL(expectedStringLength,S_LoggerBufferStruct.numOfElem);

	//check for format case 5********************************************
	Char_t* logMsgFormat5 = "%i LOG MSG : PARAM1 is NOTHING, PARAM2 is NOTHING PARAM3 is NOTHING";
	Char_t expectedStringFormat5[100]; //TODO remove magic number
	strcpy(expectedStringFormat5,"4 INFO at 0 0 LOG MSG : PARAM1 is NOTHING, PARAM2 is NOTHING PARAM3 is NOTHING");
	expectedStringLength = strlen(expectedStringFormat5);

	GLOG_LogMsg_UnsignedParam(E_GLOG_INFO,logMsgFormat5,0,210,999);
	// call the thread function
	(*sendBufferFunction)(NULL);
	// check in local buffer if msgs match
	TEST_ASSERT_EQUAL_MEMORY(expectedStringFormat5,S_LoggerBufferStruct.loggerBuffer,S_LoggerBufferStruct.numOfElem);
	TEST_ASSERT_EQUAL(expectedStringLength,S_LoggerBufferStruct.numOfElem);

	//*******************************************************************
	//Check MSG Counter**************************************************


	GLOG_Initialize(E_GINIT_InitLevel1);
	GLOG_Initialize(E_GINIT_InitLevel2);
	strcpy(expectedString,"0 INFO");

	uint32_t i = 0;

	for(i = 0; i < 1025; ++i){
		GLOG_LogMsg_UnsignedParam(E_GLOG_INFO,logMsg,0,210,999);
		(*sendBufferFunction)(NULL);
		sprintf(expectedString,"%i INFO",i);
		expectedStringLength = strlen(expectedString);

		TEST_ASSERT_EQUAL_MEMORY(expectedString,S_LoggerBufferStruct.loggerBuffer,expectedStringLength);

	}

}


TEST(GLOG,GLOG_LogMsg_signedParam){



	/*******************************************************************************************************/

	uint32_t expectedStringLength = 0;
	//Check MSG Format**************************************************

	RLOG_Initialize_IgnoreAndReturn(RLOG_SUCCESS);
	GOS_CreateTask_IgnoreAndReturn(E_GOS_ReturnSuccess);

	//mocks the Thread Create Call
	GOS_CreateTask_StubWithCallback(GOS_CreateTask_Callback);
	RLOG_SendBuffer_StubWithCallback(RLOG_SendBuffer_Callback);

	//*******************************************************************
	//*******************************************************************

	GLOG_Initialize(E_GINIT_InitLevel1);
	GLOG_Initialize(E_GINIT_InitLevel2);

	//check for format case 1********************************************
	Char_t* logMsg = "LOG MSG : PARAM1 is %i, PARAM2 is %i PARAM3 is %i";
	Char_t expectedString[100]; //TODO remove magic number
	strcpy(expectedString,"0 INFO at 0 LOG MSG : PARAM1 is 0, PARAM2 is -210 PARAM3 is -2147483647");
	expectedStringLength = strlen(expectedString);

	GLOG_LogMsg_SignedParam(E_GLOG_INFO,logMsg,0,-210,-2147483647);
	// call the thread function
	(*sendBufferFunction)(NULL);
	// check in local buffer if msgs match
	TEST_ASSERT_EQUAL_MEMORY(expectedString,S_LoggerBufferStruct.loggerBuffer,S_LoggerBufferStruct.numOfElem);
	TEST_ASSERT_EQUAL(expectedStringLength,S_LoggerBufferStruct.numOfElem);

	//check for format case 2********************************************
	Char_t* logMsgFormat2 = "LOG MSG : PARAM1 is NOTHING, PARAM2 is %i PARAM3 is %i";
	Char_t expectedStringFormat2[100]; //TODO remove magic number
	strcpy(expectedStringFormat2,"1 INFO at 0 LOG MSG : PARAM1 is NOTHING, PARAM2 is 0 PARAM3 is -210");
	expectedStringLength = strlen(expectedStringFormat2);

	GLOG_LogMsg_SignedParam(E_GLOG_INFO,logMsgFormat2,0,-210,-999);
	// call the thread function
	(*sendBufferFunction)(NULL);
	// check in local buffer if msgs match
	TEST_ASSERT_EQUAL_MEMORY(expectedStringFormat2,S_LoggerBufferStruct.loggerBuffer,S_LoggerBufferStruct.numOfElem);
	TEST_ASSERT_EQUAL(expectedStringLength,S_LoggerBufferStruct.numOfElem);

	//check for format case 3********************************************
	Char_t* logMsgFormat3 = "LOG MSG : PARAM1 is NOTHING, PARAM2 is %i PARAM3 is NOTHING";
	Char_t expectedStringFormat3[100]; //TODO remove magic number
	strcpy(expectedStringFormat3,"2 INFO at 0 LOG MSG : PARAM1 is NOTHING, PARAM2 is 0 PARAM3 is NOTHING");
	expectedStringLength = strlen(expectedStringFormat3);

	GLOG_LogMsg_SignedParam(E_GLOG_INFO,logMsgFormat3,0,210,999);
	// call the thread function
	(*sendBufferFunction)(NULL);
	// check in local buffer if msgs match
	TEST_ASSERT_EQUAL_MEMORY(expectedStringFormat3,S_LoggerBufferStruct.loggerBuffer,S_LoggerBufferStruct.numOfElem);
	TEST_ASSERT_EQUAL(expectedStringLength,S_LoggerBufferStruct.numOfElem);

	//check for format case 4********************************************
	Char_t* logMsgFormat4 = "LOG MSG : PARAM1 is NOTHING, PARAM2 is NOTHING PARAM3 is NOTHING";
	Char_t expectedStringFormat4[100]; //TODO remove magic number
	strcpy(expectedStringFormat4,"3 INFO at 0 LOG MSG : PARAM1 is NOTHING, PARAM2 is NOTHING PARAM3 is NOTHING");
	expectedStringLength = strlen(expectedStringFormat4);

	GLOG_LogMsg_SignedParam(E_GLOG_INFO,logMsgFormat4,0,-210,-999);
	// call the thread function
	(*sendBufferFunction)(NULL);
	// check in local buffer if msgs match
	TEST_ASSERT_EQUAL_MEMORY(expectedStringFormat4,S_LoggerBufferStruct.loggerBuffer,S_LoggerBufferStruct.numOfElem);
	TEST_ASSERT_EQUAL(expectedStringLength,S_LoggerBufferStruct.numOfElem);

	//check for format case 5********************************************
	Char_t* logMsgFormat5 = "%i LOG MSG : PARAM1 is NOTHING, PARAM2 is NOTHING PARAM3 is NOTHING";
	Char_t expectedStringFormat5[100]; //TODO remove magic number
	strcpy(expectedStringFormat5,"4 INFO at 0 0 LOG MSG : PARAM1 is NOTHING, PARAM2 is NOTHING PARAM3 is NOTHING");
	expectedStringLength = strlen(expectedStringFormat5);

	GLOG_LogMsg_SignedParam(E_GLOG_INFO,logMsgFormat5,0,-210,-999);
	// call the thread function
	(*sendBufferFunction)(NULL);
	// check in local buffer if msgs match
	TEST_ASSERT_EQUAL_MEMORY(expectedStringFormat5,S_LoggerBufferStruct.loggerBuffer,S_LoggerBufferStruct.numOfElem);
	TEST_ASSERT_EQUAL(expectedStringLength,S_LoggerBufferStruct.numOfElem);


}

/**
 * This Test checks if the log Message will be truncated
 * Be sure the param maxCharacterLengthOfSerializedLogMsg in GLOG.c and in this Test-Scenario are the same valuesd!
 */
TEST(GLOG,GLOG_MessageLength){

	Char_t longLogMessage[3*maxCharacterLengthOfSerializedLogMsg];
	Char_t expectedlongLogMessage[3*maxCharacterLengthOfSerializedLogMsg];

	uint32_t i;


	RLOG_Initialize_IgnoreAndReturn(RLOG_SUCCESS);
	GOS_CreateTask_IgnoreAndReturn(E_GOS_ReturnSuccess);

	//mocks the Thread Create Call
	GOS_CreateTask_StubWithCallback(GOS_CreateTask_Callback);
	RLOG_SendBuffer_StubWithCallback(RLOG_SendBuffer_Callback);


	GLOG_Initialize(E_GINIT_InitLevel1);
	GLOG_Initialize(E_GINIT_InitLevel2);

	strcpy(expectedlongLogMessage,"0 INFO at 0 ");

	for(i = 0; i < 2*maxCharacterLengthOfSerializedLogMsg; ++i){
		longLogMessage[i] = 'h';
		expectedlongLogMessage[12 + i] = 'h';
	}
	longLogMessage[2*maxCharacterLengthOfSerializedLogMsg] = '%';
	longLogMessage[2*maxCharacterLengthOfSerializedLogMsg + 1] = 's';

	GLOG_LogMsg_UnsignedParam(E_GLOG_INFO,longLogMessage,0,0,0);
	(*sendBufferFunction)(NULL);
	TEST_ASSERT_EQUAL(maxCharacterLengthOfSerializedLogMsg,S_LoggerBufferStruct.numOfElem);
	TEST_ASSERT_EQUAL_MEMORY(expectedlongLogMessage,S_LoggerBufferStruct.loggerBuffer,S_LoggerBufferStruct.numOfElem);


}


TEST(GLOG,GLOG_setLevelToLog){

	Char_t* logMsg = "LOG MSG : PARAM1 is %i, PARAM2 is %i PARAM3 is %i";

	//RLOG_Initialize_ExpectAnyArgsAndReturn(RLOG_SUCCESS);
	GOS_CreateTask_IgnoreAndReturn(E_GOS_ReturnSuccess);


	GOS_CreateTask_StubWithCallback(GOS_CreateTask_Callback);

	GLOG_Initialize(E_GINIT_InitLevel1);
	GLOG_Initialize(E_GINIT_InitLevel2);

	//*********************************************************
	GLOG_SetLevelToLog(E_GLOG_INFO);

	GLOG_LogMsg_UnsignedParam(E_GLOG_INFO,logMsg,0,210,999);
	RLOG_SendBuffer_ExpectAnyArgsAndReturn(RLOG_SUCCESS);
	(*sendBufferFunction)(NULL);

	GLOG_LogMsg_UnsignedParam(E_GLOG_DEBUG,logMsg,0,210,999);
	RLOG_SendBuffer_ExpectAnyArgsAndReturn(RLOG_SUCCESS);
	(*sendBufferFunction)(NULL);

	GLOG_LogMsg_UnsignedParam(E_GLOG_ERROR,logMsg,0,210,999);
	RLOG_SendBuffer_ExpectAnyArgsAndReturn(RLOG_SUCCESS);
	(*sendBufferFunction)(NULL);

	//*********************************************************
	GLOG_SetLevelToLog(E_GLOG_DEBUG);

	GLOG_LogMsg_UnsignedParam(E_GLOG_INFO,logMsg,0,210,999);
	// do not except call
	(*sendBufferFunction)(NULL);

	GLOG_LogMsg_UnsignedParam(E_GLOG_DEBUG,logMsg,0,210,999);
	RLOG_SendBuffer_ExpectAnyArgsAndReturn(RLOG_SUCCESS);
	(*sendBufferFunction)(NULL);

	GLOG_LogMsg_UnsignedParam(E_GLOG_ERROR,logMsg,0,210,999);
	RLOG_SendBuffer_ExpectAnyArgsAndReturn(RLOG_SUCCESS);
	(*sendBufferFunction)(NULL);

	//*********************************************************
	GLOG_SetLevelToLog(E_GLOG_ERROR);

	GLOG_LogMsg_UnsignedParam(E_GLOG_INFO,logMsg,0,210,999);
	// do not except call
	(*sendBufferFunction)(NULL);

	GLOG_LogMsg_UnsignedParam(E_GLOG_DEBUG,logMsg,0,210,999);
	// do not except call
	(*sendBufferFunction)(NULL);

	GLOG_LogMsg_UnsignedParam(E_GLOG_ERROR,logMsg,0,210,999);
	RLOG_SendBuffer_ExpectAnyArgsAndReturn(RLOG_SUCCESS);
	(*sendBufferFunction)(NULL);

}





// 	Helperfunctions*******************************************************************
GOS_Return_e GOS_CreateTask_Callback(GOS_TaskFunction_t pvTaskCode, int numCalls){

	//store the function pointer
	sendBufferFunction = pvTaskCode;

	//skip endless while loop of Taskfunction in GLOG, only used for testing
	GLOG_UnitTest_TaskRunOnce();
	return E_GOS_ReturnSuccess;

}



RLOG_ReturnValue RLOG_SendBuffer_Callback(const Char_t* buf, size_t numOfElements, int numCalls){

	//clean local test buffer
	memset(S_LoggerBufferStruct.loggerBuffer,'\0',maxCharacterLengthOfSerializedLogMsg);
	memcpy(S_LoggerBufferStruct.loggerBuffer,buf,numOfElements);
	S_LoggerBufferStruct.numOfElem = numOfElements;

	return RLOG_SUCCESS;
}




