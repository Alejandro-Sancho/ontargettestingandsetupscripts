/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation: xxx
 ***********************************************************************/

/** @defgroup GLOG Logger Module
 * @{
 * @brief
 * The next blank line is mandatory!
 *
 * This module caches logging messages, which are passed with maximum 3 integer
 * parameters. The module builds a string and sends them to the Module RLOG.
 * The module creates a background task with low priority which builds the log messages.
 *
 * @file
 * This header file contains the declaration of the public
 * interface to module GLOG.
 */

#ifndef GLOG_H_
#define GLOG_H_


/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/

#include "types.h"
#include "GINIT.h"

/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/
/**
 * The different levels to log. The order is GLOG_INFO < GLOG_DEBUG < GLOG_ERROR
 *
 */
typedef enum tag_GLOG_LogLevel{
	E_GLOG_INFO 	= 0,
	E_GLOG_DEBUG	= 1,
	E_GLOG_ERROR	= 2
} GLOG_LogLevel_e;

/**
 * The possible return values of each public function of GLOG
 */
typedef enum tag_GLOG_Return_e{
	E_GLOG_ReturnSuccess,
	E_GLOG_ReturnFailed
}GLOG_Return_e;
/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL FUNCTION PROTOTYPES                                          */
/***********************************************************************/

/** Initialize the GLOG-Module
 *  @return E_GINIT_ReturnFailure if failed
 */
GINIT_Return_e GLOG_Initialize(GINIT_InitLevel_e initLevel);

 /** Logs a log msg with unsigned parameters. Use %i to place the params in the
  * @param logLevel the level the log msg belongs to. If the current LogLevel is highter than this param, the logMsg will be ignored
  * @param msg log msg passed as string. Use %i to place the parameter values.
  * @param param1 will be ignored if @param msg contains no %i
  * @param param1 will be ignored if @param msg contains only one times a %i
  * @param param1 will be ignored if @param msg contains only two times a %i
  * @return GLOG_SUCCESS if logMsg is successfully buffered
  */
GLOG_Return_e GLOG_LogMsg_UnsignedParam(GLOG_LogLevel_e logLevel, const Char_t* msg, uint32_t param1, uint32_t param2, uint32_t param3);

 /** Logs a log msg with signed parameters. Use %i to place the params in the
  * @param logLevel the level the log msg belongs to. If the current LogLevel is highter than this param, the logMsg will be ignored
  * @param msg log msg passed as string. Use %i to place the parameter values.
  * @param param1 will be ignored if @param msg contains no %i
  * @param param1 will be ignored if @param msg contains only one times a %i
  * @param param1 will be ignored if @param msg contains only two times a %i
  * @return GLOG_SUCCESS if logMsg is successfully buffered
  */

GLOG_Return_e GLOG_LogMsg_SignedParam(GLOG_LogLevel_e logLevel, const Char_t*  msg, int32_t param1, int32_t param2, int32_t param3);

 /** Sets the current level to log. Log msg with lower levers will be ignored
  *  @return GLOG_SUCCESS
  */
GLOG_Return_e GLOG_SetLevelToLog(GLOG_LogLevel_e logLevel);

 /** This function is only used for Unittests. Do not use this function!
  *
  */
 void GLOG_UnitTest_TaskRunOnce(void); // only used for unittests


#endif /* GLOG_H_ */

/** @} */
