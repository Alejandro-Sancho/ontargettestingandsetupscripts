/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation: xxx
 ***********************************************************************/

/** @addtogroup GLOG
 * @{
 * @file
 * This source file contains the definition of the functions of
 * module RingBuffer (long name).
 */

/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/

#include "Ringbuffer.h"


/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/

/***********************************************************************/
/* LOCAL CONSTANTS                                                     */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/

/***********************************************************************/
/* LOCAL VARIABLES                                                     */
/***********************************************************************/
/**
 * This is the buffer in which the log messages are stored
 */
static GLOG_LogMessage S_Buffer[RINGBUFFER_SIZE];
/*
 * This is the current index of S_Buffer on which the buffer does write
 */
static uint32_t S_CurrentWriteIndex = 0u;
/*
 * This is the current index of S_Buffer on which the buffer does read
 */
static uint32_t S_CurrentReadIndex = 0u;
/*
 * This is the number of current elements are stored in the buffer
 */
static volatile uint32_t S_NumOfElements = 0u;

/***********************************************************************/
/* LOCAL FUNCTION PROTOTYPES                                           */
/***********************************************************************/
/** Returns the next index to read or write of a RingBuffer
 *
 *  This function checks based on given index against overflow
 *
 *  @param index last used index
 *  @returns next index to use
 */
static uint32_t nextIndex(uint32_t index);
/***********************************************************************/
/* GLOBAL FUNCTIONS                                                    */
/***********************************************************************/

void RingBuffer_Init(void){

	S_CurrentWriteIndex = 0u;
	S_CurrentReadIndex = 0u;
	S_NumOfElements = 0u;

}

RingBuffer_ReturnValue_e RingBuffer_put(const GLOG_LogMessage* const elem){

	uint32_t i;
	RingBuffer_ReturnValue_e retVal;


	if(elem == NULL){
		retVal = E_RingBuffer_ReturnFailed; //Error there is no element behind
	}

	else if(S_NumOfElements == RINGBUFFER_SIZE){
		retVal = E_RingBuffer_ReturnFailed; //Error Buffer is full
	}
	else{


		S_Buffer[S_CurrentWriteIndex].typeOfParameters = elem->typeOfParameters;
		S_Buffer[S_CurrentWriteIndex].logLevel = elem->logLevel;
		S_Buffer[S_CurrentWriteIndex].msg = elem->msg;
		S_Buffer[S_CurrentWriteIndex].sysTik = elem->sysTik;
		S_Buffer[S_CurrentWriteIndex].logNr = elem->logNr;

		for(i = 0u; i < NUMBERS_OF_PARAMETERS; ++i){

			S_Buffer[S_CurrentWriteIndex].params[i] = elem->params[i];
		}



		S_CurrentWriteIndex = nextIndex(S_CurrentWriteIndex);
		++S_NumOfElements;
		retVal = E_RingBuffer_ReturnSuccess;
	}
	return retVal;
}

RingBuffer_ReturnValue_e RingBuffer_get(GLOG_LogMessage* const elem){

	uint32_t i;

	RingBuffer_ReturnValue_e retVal;

	if(elem == NULL){
		retVal = E_RingBuffer_ReturnFailed; //Error there is no element behind
	}
	else if(S_NumOfElements == 0u){
		retVal = E_RingBuffer_ReturnFailed; //Error there is no element in the Buffer
	}

	else {


		elem->typeOfParameters 	= S_Buffer[S_CurrentReadIndex].typeOfParameters;
		elem->logLevel 	= S_Buffer[S_CurrentReadIndex].logLevel;
		elem->msg 		= S_Buffer[S_CurrentReadIndex].msg;
		elem->sysTik 	= S_Buffer[S_CurrentReadIndex].sysTik;
		elem->logNr 	= S_Buffer[S_CurrentReadIndex].logNr;

		for(i = 0u; i < NUMBERS_OF_PARAMETERS; ++i){

			elem->params[i] = S_Buffer[S_CurrentReadIndex].params[i];
		}

		S_CurrentReadIndex = nextIndex(S_CurrentReadIndex);
		--S_NumOfElements;
		retVal = E_RingBuffer_ReturnSuccess;

	}

	return retVal;
}

uint32_t RingBuffer_space(){

	return RINGBUFFER_SIZE - S_NumOfElements;
}
Bool_t RingBuffer_IsEmpty(){


	return (Bool_t)(S_NumOfElements == 0u);
}

/***********************************************************************/
/* LOCAL FUNCTIONS                                                     */
/***********************************************************************/


static uint32_t nextIndex(uint32_t index){

	uint32_t retVal;
	if(index == (RINGBUFFER_SIZE - 1u)){
		retVal = 0;
	}

	else{ // (index < logMsgBufSize -1)
		retVal = index + 1u;
	}

	return retVal;
}


/** @} */
