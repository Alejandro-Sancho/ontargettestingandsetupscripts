/***********************************************************************
 * $Id$
 * Copyright MyoSwiss
 * Project: MyoSuite
 * Documentation: xxx
 ***********************************************************************/

/** @addtogroup GLOG
 * @{
 * @file
 * This source file contains the definition of the functions of
 * module GLOG.
 */

/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/
#include <GUTL/inc/GUTILS.h>
#include "Ringbuffer.h"
#include "GLOG.h"
#include "RLOG.h"
#include "GOS.h"
#include "types.h"


/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/


/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/


/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/


/***********************************************************************/
/* LOCAL CONSTANTS                                                     */
/***********************************************************************/

#define maxCharacterLengthOfSerializedLogMsg (100u)
/*
enum {
	maxCharacterLengthOfSerializedLogMsg = 100u
};
*/

/*this should go into ROM and not on stack*/
/*lint -esym(9003,LogLevelNames)*/
static const char* const S_LogLevelNames[3] = {"INFO", "DEBUG", "ERROR"};



/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/


/***********************************************************************/
/* LOCAL VARIABLES                                                     */
/***********************************************************************/

/**
 * This variable holds the current logging level
 */
static GLOG_LogLevel_e S_CurrentLogLevel;

/**
 * This variable holds the current message number
 */
static uint32_t S_CurrentMessageNr;

/**
 * This is the current buffer which holds the builded log string
 */
/* cant be on stack cause this buffer maybe goes directly to HW-SPI (not fixed yet)*/
/*lint -esym(9003,S_sendBuffer)*/
static Char_t S_sendBuffer[maxCharacterLengthOfSerializedLogMsg];

/**
 * This flag is used in the while loop of the logger task. This is needed for unit tests
 * and will be always true during normal usage
 */
static volatile Bool_t S_LoggerTaskShallRun;
/***********************************************************************/
/* LOCAL FUNCTION PROTOTYPES                                           */
/***********************************************************************/

/** Sends all stored log message from buffer
 * @return E_GLOG_ReturnSuccess if log messages were sended
 *
 */
static GLOG_Return_e sendLogMsgFromBuffer(void);

/** Returns a new Lognumber
 *
 *  This function increases the current LogNumber
 */
static uint32_t getNewLogNr(void);

/** Builds an formats a string out of the stored log message
 *  @param logMsgPoints to the log message which should be builded to a string
 *  @param buf The buffer where the the string will be stored
 *
 *  @return The number of characters in @param buf
 */

static uint32_t serializeLogMsgToString(const GLOG_LogMessage* const logMsg, Char_t buf[]);

/** The function which is passed to the logger task
 * @param param Parameter for the task, not used yet
 */
static void loggerTaskFunction(void* param);

static GLOG_Return_e logMessage(GLOG_LogLevel_e logLevel, GLOG_LogMessageType typeOfParams, const Char_t* msg, uint32_t param1, uint32_t param2, uint32_t param3);

static uint32_t buildStringWithParams(const GLOG_LogMessage* const logMsg, Char_t buf[], uint32_t bufSize);
/***********************************************************************/
/* GLOBAL FUNCTIONS                                                    */
/***********************************************************************/
GINIT_Return_e GLOG_Initialize(GINIT_InitLevel_e initLevel){
	GINIT_Return_e retVal;
	GOS_Return_e retValGOS;

	switch(initLevel){

	case E_GINIT_InitLevel1:
	{
		S_LoggerTaskShallRun = true; // its a flag used only for test
		S_CurrentMessageNr = 0;
		S_CurrentLogLevel = E_GLOG_INFO;
		RingBuffer_Init();
		retVal = E_GINIT_ReturnSuccess;
	}
		break;
	case E_GINIT_InitLevel2:
	{
		retValGOS = GOS_CreateTask(E_GLOG_LoggerTask,loggerTaskFunction);
		if(retValGOS != E_GOS_ReturnSuccess){
			retVal = E_GINIT_ReturnFailure;
		}
		else{
			retVal = E_GINIT_ReturnSuccess;
		}
	}
		break;

	default:
	{
		retVal = E_GINIT_ReturnSuccess;

	}
		break;
	}


	return retVal;
}


GLOG_Return_e GLOG_LogMsg_UnsignedParam(GLOG_LogLevel_e logLevel, const Char_t* msg, uint32_t param1, uint32_t param2, uint32_t param3){

	GLOG_Return_e retVal;

	/*This function has no side effects, its not implemented yet*/
	/*lint -e{522}*/
	GOS_EnterCriticalSection();

	retVal = logMessage(logLevel,GLOG_LogMessageTypeUnsigned,msg,param1,param2,param3);

	/*This function has no side effects, its not implemented yet*/
	/*lint -e{522}*/
	GOS_ExitCriticalSection();

	return retVal;
}

GLOG_Return_e GLOG_LogMsg_SignedParam(GLOG_LogLevel_e logLevel, const Char_t* const msg, int32_t param1, int32_t param2, int32_t param3){

	GLOG_Return_e retVal;

	/*This function has no side effects, its not implemented yet*/
	/*lint -e{522}*/
	GOS_EnterCriticalSection();

	/*lint -e{522}*/
	retVal = logMessage(logLevel,GLOG_LogMessageTypeSigned,msg,(uint32_t)param1,(uint32_t)param2,(uint32_t)param3);

	/*This function has no side effects, its not implemented yet*/
	/*lint -e{522}*/
	GOS_ExitCriticalSection();

	return retVal;
}


GLOG_Return_e GLOG_SetLevelToLog(GLOG_LogLevel_e logLevel){

	S_CurrentLogLevel = logLevel;
	return E_GLOG_ReturnSuccess;

}


void GLOG_UnitTest_TaskRunOnce(){

	S_LoggerTaskShallRun = false;
}

/***********************************************************************/
/* LOCAL FUNCTIONS                                                     */
/***********************************************************************/
static GLOG_Return_e sendLogMsgFromBuffer(void){

	GLOG_LogMessage log;
	uint32_t numOfElements = 0;
	RLOG_ReturnValue checkRetValRLOG = RLOG_FAIL;
	GLOG_Return_e retVal = E_GLOG_ReturnSuccess;
	RingBuffer_ReturnValue_e retValRingBuffer;
	uint32_t i;

	while(RingBuffer_IsEmpty() == false){ // as long as there are some stored in the buffer
		retValRingBuffer = RingBuffer_get(&log);

		if(retValRingBuffer == E_RingBuffer_ReturnFailed){
			retVal = E_GLOG_ReturnFailed;
		}


		if(log.logLevel >= S_CurrentLogLevel){
			/* try 3 times to send */
			for(i = 0; (i < 3u) && (checkRetValRLOG == RLOG_FAIL); ++i){
				numOfElements = serializeLogMsgToString(&log,S_sendBuffer);
				checkRetValRLOG = RLOG_SendBuffer(S_sendBuffer,numOfElements);
			}


			if(checkRetValRLOG == RLOG_FAIL){
				// fired and forgot
				retVal = E_GLOG_ReturnFailed;
				break;
			}
		}
	}


	return retVal;
}


static uint32_t serializeLogMsgToString(const GLOG_LogMessage* const logMsg, Char_t buf[]){

	const Char_t* debugLevelString = S_LogLevelNames[logMsg->logLevel];

	uint32_t bufferIndex = 0;

	bufferIndex = bufferIndex + UTILS_ConvertIntToChar(&buf[bufferIndex],(maxCharacterLengthOfSerializedLogMsg),logMsg->logNr);
	bufferIndex = bufferIndex + UTILS_StringCopy(&buf[bufferIndex]," ",(maxCharacterLengthOfSerializedLogMsg - bufferIndex));
	bufferIndex = bufferIndex + UTILS_StringCopy(&buf[bufferIndex],debugLevelString,(maxCharacterLengthOfSerializedLogMsg - bufferIndex));


	bufferIndex = bufferIndex + UTILS_StringCopy(&buf[bufferIndex]," at ",maxCharacterLengthOfSerializedLogMsg - bufferIndex);
	bufferIndex = bufferIndex + UTILS_ConvertIntToChar(&buf[bufferIndex], maxCharacterLengthOfSerializedLogMsg - bufferIndex,logMsg->sysTik);
	bufferIndex = bufferIndex + UTILS_StringCopy(&buf[bufferIndex]," ",maxCharacterLengthOfSerializedLogMsg - bufferIndex);


	bufferIndex = bufferIndex + buildStringWithParams(logMsg,&buf[bufferIndex],(maxCharacterLengthOfSerializedLogMsg - bufferIndex));



	return bufferIndex;
}

static uint32_t buildStringWithParams(const GLOG_LogMessage* const logMsg, Char_t buf[], uint32_t bufSize){

	uint32_t bufferIndex = 0;
	uint32_t sections;
	uint32_t sectionStartIndex = 0;
	uint32_t sectionEndsIndex = 0;
	int32_t relativeSubStringIndex = 0;


	for(sections = 0u; sections < NUMBERS_OF_PARAMETERS; ++sections){

			relativeSubStringIndex = UTILS_FindSubString(&logMsg->msg[sectionStartIndex],"%i");
			if(relativeSubStringIndex < 0){ // substring not found
				break;
			}
			sectionEndsIndex = sectionStartIndex + (uint32_t)relativeSubStringIndex;

			if((sectionEndsIndex - sectionStartIndex) <= (bufSize - bufferIndex)){
				bufferIndex = bufferIndex + UTILS_StringCopy(&buf[bufferIndex],&logMsg->msg[sectionStartIndex],((size_t)sectionEndsIndex - (size_t)sectionStartIndex));
			}
			else{
				bufferIndex = bufferIndex + UTILS_StringCopy(&buf[bufferIndex],&logMsg->msg[sectionStartIndex], (bufSize - bufferIndex));
			}

			if(logMsg->typeOfParameters == GLOG_LogMessageTypeUnsigned){
				bufferIndex = bufferIndex + UTILS_ConvertIntToChar(&buf[bufferIndex],bufSize - bufferIndex,logMsg->params[sections]);
			}
			else{
				bufferIndex = bufferIndex + UTILS_ConvertSignedIntToChar(&buf[bufferIndex],bufSize - bufferIndex,(int32_t)logMsg->params[sections]);
			}
			sectionStartIndex = sectionEndsIndex + 2u;
		}

	bufferIndex = bufferIndex + UTILS_StringCopy(&buf[bufferIndex],&logMsg->msg[sectionStartIndex],bufSize - bufferIndex);

	return bufferIndex;
}

static uint32_t getNewLogNr(void){

	uint32_t retVal = S_CurrentMessageNr;
	S_CurrentMessageNr = S_CurrentMessageNr + 1u;

	return retVal;
}


static void loggerTaskFunction(void* param){

	GLOG_Return_e retValGlog;
	(void)param; // is not used
	do{
		retValGlog = sendLogMsgFromBuffer();

		if(retValGlog == E_GLOG_ReturnFailed){
			//TODO : Handle error, inform error handler, count fails, discuss in review
		}
	}while(S_LoggerTaskShallRun == true);

}

static GLOG_Return_e logMessage(GLOG_LogLevel_e logLevel, GLOG_LogMessageType typeOfParams, const Char_t* msg, uint32_t param1, uint32_t param2, uint32_t param3){

		GLOG_Return_e retVal = E_GLOG_ReturnSuccess;
		GLOG_LogMessage log;
		RingBuffer_ReturnValue_e bufferRetVal;



		if(RingBuffer_space() == 0u){
			retVal = E_GLOG_ReturnSuccess;
		}
		log.typeOfParameters = typeOfParams;
		log.logLevel = logLevel;

		/*we only give logNumbers if the logMessage shall be logged*/
		if(log.logLevel >= S_CurrentLogLevel){
			log.logNr = getNewLogNr();
		}

		log.sysTik = 0; //TODO getSysTik()
		log.msg = msg;
		// typeCast
		log.params[0] = (uint32_t)param1;
		log.params[1] = (uint32_t)param2;
		log.params[2] = (uint32_t)param3;

	    bufferRetVal = RingBuffer_put(&log);
		if(bufferRetVal == E_RingBuffer_ReturnFailed){
			retVal = E_GLOG_ReturnFailed;

		}


		return retVal;

}

/** @} */
