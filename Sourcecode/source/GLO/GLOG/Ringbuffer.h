/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation: xxx
 ***********************************************************************/

/** @defgroup GLOG
 * @{
 * @brief
 * The next blank line is mandatory!
 *
 * This is a RingBuffer implementation used by the GLOG Module
 * @file
 * This header file contains the declaration of the public or private
 * interface module RingBuffer (long name).
 */
#ifndef RINGBUFFER_H_
#define RINGBUFFER_H_
/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/

#include "GLOG.h"
#include "types.h"

/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/

#define RINGBUFFER_SIZE (100u)
#define NUMBERS_OF_PARAMETERS (3u)


/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/

/** Type of the stored parameters into GLOG_LogMessage
 *  This is needed to save space
 *
 */

typedef enum tag_GLOG_LogMessageType{
	GLOG_LogMessageTypeUnsigned = 0,
	GLOG_LogMessageTypeSigned = 1
}GLOG_LogMessageType;

/**
 * The structure a log message is stored in the RingBuffer
 */
typedef struct tag_GLOG_LogMessage{

	GLOG_LogLevel_e logLevel;
	uint32_t sysTik;
	uint32_t logNr;
	const char* msg;
	uint32_t params[NUMBERS_OF_PARAMETERS];
	GLOG_LogMessageType typeOfParameters;

} GLOG_LogMessage;

/**
 * The possible return values of each public function of GLOG_RingBuffer
 */
typedef enum tag_RingBuffer_ReturnValue{
	E_RingBuffer_ReturnSuccess,
	E_RingBuffer_ReturnFailed,
	E_RingBuffer_IsEmpty
}RingBuffer_ReturnValue_e;

/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL FUNCTION PROTOTYPES                                          */
/***********************************************************************/
/**
 * Initializes the RingBuffer
 *
 */
extern void RingBuffer_Init(void);

/**Puts the data which are behind elem into the buffer.
 * @param elem The GLOG_LogMessage which sould be buffered, must not be a null pointer
 * @return Returns GLOG_RingBuffer_SUCCESS if successful, GLOG_RingBuffer_FAIL if errors occured
 *
 */
extern RingBuffer_ReturnValue_e RingBuffer_put(const GLOG_LogMessage* const elem);

/**Puts the data which are behind elem into the buffer.
 * @param elem The GLOG_LogMessage which sould be buffered, must not be a null pointer
 * @return Returns GLOG_RingBuffer_SUCCESS if successful, GLOG_RingBuffer_FAIL if errors occured
 */
extern RingBuffer_ReturnValue_e RingBuffer_get(GLOG_LogMessage* const elem);

/**
 * @return Returns the number of remaining log messages can be buffered
 */
extern uint32_t RingBuffer_space(void);

/**
 * @return Returns true if RingBuffer is empty
 */
Bool_t RingBuffer_IsEmpty(void);

#endif /* RINGBUFFER_H_ */
/** @} */



