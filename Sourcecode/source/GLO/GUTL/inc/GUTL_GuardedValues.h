/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation: xxx
 ***********************************************************************/

/** @defgroup GUTL_GuardedValues
 * @{
 * @brief
 * The next blank line is mandatory!
 *
 * This module offers guarded values for a set of datatypes to detect
 * bitflips of stored variables.
 *
 * @file
 * This header file contains the declaration of the public
 * interface to module GUTL_GuardedValues.
 */

#ifndef GUTL_GuardedValues_H
#define GUTL_GuardedValues_H


/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/
#include "types.h"

/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/

/**
 * Guarded bool structure
 * Notice: bool_t is implemented as Uint8 therefore the inverse of
 * true (1) is 0xfe.
 */
typedef struct tag_GUTL_GuardedValue_Bool_t_s
{
	Bool_t value; /**< value*/
	Bool_t inv_value; /**< inverse guard*/
} GUTL_GuardedValue_Bool_t_s;

/**
 * Guarded Uint8_t structure
 */
typedef struct tag_GUTL_GuardedValue_uint8_t_s
{
	uint8_t value; /**< value*/
	uint8_t inv_value; /**< inverse guard*/
}GUTL_GuardedValue_uint8_t_s;

/**
 * Guarded Int16_t structure
 */
typedef struct tag_GUTL_GuardedValue_int16_t_s
{
	int16_t value; /**< value*/
	int16_t inv_value; /**< inverse guard*/
}GUTL_GuardedValue_int16_t_s;

/**
 * Guarded Uint32_t structure
 */
typedef struct tag_GUTL_GuardedValue_uint32_t_s
{
	uint32_t value; /**< value*/
	uint32_t inv_value; /**< inverse guard*/
}GUTL_GuardedValue_uint32_t_s;

/**
 * Guarded Int32_t structure
 */
typedef struct tag_GUTL_GuardedValue_int32_t_s
{
	int32_t value; /**< value*/
	int32_t inv_value; /**< inverse guard*/
}GUTL_GuardedValue_int32_t_s;

/**
 * Guarded Force_Newton_t structure
 */
typedef struct tag_GUTL_GuardedValue_Force_Newton_t_s
{
	Force_Newton_t value; /**< value*/
	Force_Newton_t inv_value; /**< inverse guard*/
}GUTL_GuardedValue_Force_Newton_t_s;

/**
 * Guarded Angle_Degree_t structure
 */
typedef struct tag_GUTL_GuardedValue_Angle_Degree_t_s
{
	Angle_Degree_t value; /**< value*/
	Angle_Degree_t inv_value; /**< inverse guard*/
}GUTL_GuardedValue_Angle_Degree_t_s;

/**
 * Guarded Temp_DegreeCelsius_t structure
 */
typedef struct tag_GUTL_GuardedValue_Temp_DegreeCelsius_t_s
{
	Temp_DegreeCelsius_t value; /**< value*/
	Temp_DegreeCelsius_t inv_value; /**< inverse guard*/
}GUTL_GuardedValue_Temp_DegreeCelsius_t_s;

typedef struct tag_GUTL_GuardedValue_Voltage_mV_t_s
{
	Voltage_mV_t value; /**< value*/
	Voltage_mV_t inv_value; /**< inverse guard*/
}GUTL_GuardedValue_Voltage_mV_t_s;

typedef struct tag_GUTL_GuardedValue_Current_mA_t_s
{
	Current_mA_t value; /**< value*/
	Current_mA_t inv_value; /**< inverse guard*/
}GUTL_GuardedValue_Current_mA_t_s;




/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL FUNCTION PROTOTYPES                                          */
/***********************************************************************/



/**
 * Getting and verifying a boolean value from a guard.
 * @param guardedValue Guarded value struct
 * @return extracted value
 */
Bool_t GUTL_GuardedValue_get_Bool_t(GUTL_GuardedValue_Bool_t_s guardedValue);

/**
 * Getting and verifying an Uint8_t value from a guard.
 * @param guardedValue Guarded value struct
 * @return extracted value
 */
uint8_t  GUTL_GuardedValue_get_Uint8_t(GUTL_GuardedValue_uint8_t_s guardedValue);

/**
 * Getting and verifying an Int16_t value from a guard.
 * @param guardedValue Guarded value struct
 * @return extracted value
 */
int16_t GUTL_GuardedValue_getInt16_t(GUTL_GuardedValue_int16_t_s guardedValue);

/**
 * Getting and verifying an Uint32_t value from a guard.
 * @param guardedValue Guarded value struct
 * @return extracted value
 */
uint32_t GUTL_GuardedValue_getUint32_t(GUTL_GuardedValue_uint32_t_s guardedValue);

/**
 * Getting and verifying an Int32_t value from a guard.
 * @param guardedValue Guarded value struct
 * @return extracted value
 */
int32_t GUTL_GuardedValue_getInt32_t(GUTL_GuardedValue_int32_t_s guardedValue);

/**
 * Getting and verifying an Force_Newton_t value from a guard.
 * @param guardedValue Guarded value struct
 * @return extracted value
 */
Force_Newton_t GUTL_GuardedValue_get_Force_Newton_t(GUTL_GuardedValue_Force_Newton_t_s guardedValue);

/**
 * Getting and verifying an Angle_Degree_t from a guard.
 * @param guardedValue Guarded value struct
 * @return extracted value
 */
Angle_Degree_t GUTL_GuardedValue_get_Angle_Degree_t(GUTL_GuardedValue_Angle_Degree_t_s guardedValue);

/**
 * Getting and verifying an Temp_DegreeCelsius_t from a guard.
 * @param guardedValue Guarded value struct
 * @return extracted value
 */
Temp_DegreeCelsius_t GUTL_GuardedValue_get_Temp_DegreeCelsius_t(GUTL_GuardedValue_Temp_DegreeCelsius_t_s guardedValue);

/**
 * Getting and verifying an Voltage_mV_t from a guard.
 * @param guardedValue Guarded value struct
 * @return extracted value
 */
Voltage_mV_t GUTL_GuardedValue_get_Voltage_mV_t(GUTL_GuardedValue_Voltage_mV_t_s guardedValue);

/**
 * Getting and verifying an Current_mA_t from a guard.
 * @param guardedValue Guarded value struct
 * @return extracted value
 */
Current_mA_t GUTL_GuardedValue_get_Current_mA_t(GUTL_GuardedValue_Current_mA_t_s guardedValue);

/**
 * Storing a boolean value including its guard
 * @param guardedValue Guarded value struct
 * @param value Value to add to the guard
 */
void GUTL_GuardedValue_set_Bool_t(GUTL_GuardedValue_Bool_t_s* guardedValue, Bool_t value);

/**
 * Storing an Uint8_t including its guard
 * @param guardedValue Guarded value struct
 * @param value Value to add to the guard
 */
void GUTL_GuardedValue_set_Uint8(GUTL_GuardedValue_uint8_t_s* guardedValue, uint8_t  value);

/**
 * Storing a Int16_t value including its guard
 * @param guardedValue Guarded value struct
 * @param value Value to add to the guard
 */
void GUTL_GuardedValue_set_Int16_t(GUTL_GuardedValue_int16_t_s* guardedValue, int16_t  value);

/**
 * Storing an Uint32_t including its guard
 * @param guardedValue Guarded value struct
 * @param value Value to add to the guard
 */
void GUTL_GuardedValue_set_Uint32_t(GUTL_GuardedValue_uint32_t_s* guardedValue, uint32_t  value);
/**
 * Storing a Int32_t value including its guard
 * @param guardedValue Guarded value struct
 * @param value Value to add to the guard
 */
void GUTL_GuardedValue_set_Int32_t(GUTL_GuardedValue_int32_t_s* guardedValue, int32_t  value);
/**
 * Storing a Force_Newton_t value including its guard
 * @param guardedValue Guarded value struct
 * @param value Value to add to the guard
 */
void GUTL_GuardedValue_set_Force_Newton_t(GUTL_GuardedValue_Force_Newton_t_s* guardedValue, Force_Newton_t value);

/**
 * Storing a Angle_Degree_t value including its guard
 * @param guardedValue Guarded value struct
 * @param value Value to add to the guard
 */
void GUTL_GuardedValue_set_Angle_Degree_t(GUTL_GuardedValue_Angle_Degree_t_s* guardedValue, Angle_Degree_t value);

/**
 * Storing a Temp_DegreeCelsius_t value including its guard
 * @param guardedValue Guarded value struct
 * @param value Value to add to the guard
 */
void GUTL_GuardedValue_set_Temp_DegreeCelsius_t(GUTL_GuardedValue_Temp_DegreeCelsius_t_s* guardedValue, Temp_DegreeCelsius_t value);

/**
 * Storing a Voltage_mV_t value including its guard
 * @param guardedValue Guarded value struct
 * @param value Value to add to the guard
 */
void GUTL_GuardedValue_set_Voltage_mV_t(GUTL_GuardedValue_Voltage_mV_t_s* guardedValue, Voltage_mV_t value);
/**
 * Storing a Current_mA_t value including its guard
 * @param guardedValue Guarded value struct
 * @param value Value to add to the guard
 */
void GUTL_GuardedValue_set_Current_mA_t(GUTL_GuardedValue_Current_mA_t_s* guardedValue, Current_mA_t value);



#endif /* GUTL_GuardedValues_H */

/** @} */
