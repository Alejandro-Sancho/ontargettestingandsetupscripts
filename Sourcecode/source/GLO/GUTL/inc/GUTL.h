/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation: xxx
 ***********************************************************************/

/** @defgroup GUTL
 * @{
 * @brief
 *
 * Global functions for string manipulation
 * @file
 * This header file contains the declaration of the public
 * interface to module GUTL.
 */
#ifndef GUTL_H
#define GUTL_H


/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/
#include "types.h"
/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/


/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/


/***********************************************************************/
/* GLOBAL FUNCTION PROTOTYPES                                          */
/***********************************************************************/
/* Converts a unsigned number into character, truncates if converted string is longer than bufSize
 * @param str destination where converted number will be stored
 * @param bufSize space of str
 * @param num Number to convert
 * @return the numbers of characters which are stored in str
 */
uint32_t GUTL_ConvertIntToChar(Char_t str[], uint32_t bufSize, uint32_t num);
/* Converts a signed number into character, truncates if converted string is longer than bufSize
 * @param str destination where converted number will be stored
 * @param bufSize space of str
 * @param num Number to convert
 * @return the numbers of characters which are stored in str
 */
uint32_t GUTL_ConvertSignedIntToChar(Char_t str[], size_t bufSize,  int32_t num);

/**Find a substring inside of a string
 * @param str Pointer to string to search
 * @param substr Pointer to the pattern
 *
 * @return the index of str where the substr starts, returns -1 if no substr was found
 */
int8_t GUTL_FindSubString(const Char_t* str, const Char_t* substr);

/** This function implements the normal strlen function
 *
 * @param str C string
 *
 * @return The length of string.
 *
 */
size_t GUTL_Strlen(const char* str);

/** This function implements the normal strcpy function
 *
 * @param dest Pointer to the destination array where the content is to be copied
 * @param src the string which will be copied
 *
 * @return pointer to dest
 *
 */
Char_t* GUTL_Strcpy(Char_t* dest, const Char_t* src);

/** This function implements the normal strncpy function, but returns the numbers of
 * elements which were copied. IF bufsize is smaller then the length of src, src will be truncated.
 * In this case, no termination character is added!
 *
 * @param dest where the string will be copied
 * @param src the string which will be copied
 * @param bufsize the number of remaining space in characters in dest
 *
 * @return the number of characters which were copied
 *
 */
uint32_t GUTL_StringCopy(Char_t* dest, const Char_t* src, uint32_t bufsize);

Char_t* GUTL_Strncpy(Char_t* dest, const Char_t* src, size_t n);

Double_t GUTL_Atan2(Double_t y, Double_t x);

Double_t GUTL_RadToDeg(Double_t radValue);

int16_t GUTL_AbsInt16_t(int16_t value);

#endif /* GUTL_H */
