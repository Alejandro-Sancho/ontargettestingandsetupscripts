/*
 * UTILS.h
 *
 *  Created on: 3 Jan 2018
 *      Author: Michael Lehmann
 */

#ifndef UTILS_H
#define UTILS_H


/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/
#include "types.h"
/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/
typedef struct tag_GLOG_Return_s{
	uint32_t value;
	uint32_t invertedValue;
}GUTIL_Return_s;


/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/


/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/


/***********************************************************************/
/* GLOBAL FUNCTION PROTOTYPES                                          */
/***********************************************************************/

uint32_t UTILS_ConvertIntToChar(Char_t str[], uint32_t bufSize, uint32_t num);

uint32_t UTILS_ConvertSignedIntToChar(Char_t str[], size_t bufSize,  int32_t num);

int8_t UTILS_FindSubString(const Char_t* str, const Char_t* substr);

size_t UTILS_Strlen(const char* str);

Char_t* UTILS_Strcpy(Char_t* dest, const Char_t* src);

/** This function implements the normal strncpy function, but returns the numbers of
 * elements which were copied. IF bufsize is smaller then the length of src, src will be truncated.
 * In this case, no termination character is added!
 *
 * @param dest where the string will be copied
 * @param src the string which will be copied
 * @param bufsize the number of remaining space in characters in dest
 *
 * @return the number of characters which were copied
 *
 */
uint32_t UTILS_StringCopy(Char_t* dest, const Char_t* src, uint32_t bufsize);

Char_t* UTILS_Strncpy(Char_t* dest, const Char_t* src, size_t n);

#endif /* UTILS_H */
