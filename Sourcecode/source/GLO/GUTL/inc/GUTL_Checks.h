/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation: xxx
 ***********************************************************************/

/** @defgroup GUTL_Checks
 * @{
 * @brief
 *
 * Global check functions for logical checks of combinations of datasets
 * @file
 * This header file contains the declaration of the public
 * interface to module GUTL_Checks.
 */

#ifndef GUTL_Checks_H
#define GUTL_Checks_H


/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/
#include "types.h"
/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/

typedef enum tag_GUTL_AngleCombinitionCheck_e{
	E_GUTL_Checks_CombValid,
	E_GUTL_Checks_CombNotValid,
}GUTL_AngleCombinitionCheck_e;


typedef struct tag_GUTL_Checks_Angles_s{

	Angle_Degree_t angleShankRight;
	Angle_Degree_t angleShankLeft;
	Angle_Degree_t angleThighRight;
	Angle_Degree_t angleThighLeft;
	Angle_Degree_t angleTrunk;

}GUTL_Checks_Angles_s;
/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/


/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/


/***********************************************************************/
/* GLOBAL FUNCTION PROTOTYPES                                          */
/***********************************************************************/
/**
 * Check the combination of all angles against invalid or impossible position
 *
 * @param angles the dataset which combination will be checked
 * @return E_GUTL_Checks_CombValid if combination is valid, E_GUTL_Checks_CombNotValid otherwise
 */
GUTL_AngleCombinitionCheck_e GUTIL_AngleCombinationCheck(GUTL_Checks_Angles_s angles);

#endif /* GUTL_Checks_H */
