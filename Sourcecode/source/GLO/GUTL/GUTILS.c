/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation: xxx
 ***********************************************************************/

/** @addtogroup LABC
 * @{
 * @file
 * This source file contains the definition of the functions of
 * HAL or RTE component or module LABC (long name).
 */

/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/

#include <GUTL/inc/GUTILS.h>
#include <string.h>
#include <stdlib.h>


/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/


/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/


/***********************************************************************/
/* LOCAL CONSTANTS                                                     */
/***********************************************************************/

enum{
	maxCharacterLengthOf32BitValue = 10 // 32bit number has maximal 10 characters
};

/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/


/***********************************************************************/
/* LOCAL VARIABLES                                                     */
/***********************************************************************/

/***********************************************************************/
/* LOCAL FUNCTION PROTOTYPES                                           */
/***********************************************************************/



/***********************************************************************/
/* GLOBAL FUNCTIONS                                                    */
/***********************************************************************/


int8_t UTILS_FindSubString(const Char_t* str, const Char_t* substr){


	uint8_t substrLength = (uint8_t)UTILS_Strlen(substr); // todo rewrite as parameter
	uint8_t strSearchIndex;
	uint8_t substrIndex = 0;


	for(strSearchIndex = 0; str[strSearchIndex] != '\0'; ++strSearchIndex){

		for(substrIndex = 0; substrIndex < substrLength; ++substrIndex){
			if(str[strSearchIndex + substrIndex] != substr[substrIndex]){
				break;
			}
		}
		if(substrIndex == substrLength){
			return strSearchIndex;
		}

	}

	return -1; // string not found

}


/**
 * returns the length of the string is written into str
 */
uint32_t UTILS_ConvertIntToChar(Char_t str[], uint32_t bufSize, uint32_t num){

	uint32_t i = 0;
	uint32_t rest = 0;
	uint32_t length = 0;
	uint32_t y  = num;
	uint32_t bufSpace = bufSize;

	if(bufSize == 0){
		length = 0;
	}
	else if(num == 0){
		length = 1;
	}

    while (((y != 0) && (length <= maxCharacterLengthOf32BitValue)) && (bufSpace >= 1))
    {
    	length++;
        y /= 10;
        bufSpace = bufSpace - 1;

    }

    for (i = 0; (i < length); i++)
    {
    	rest = num % 10;
        num = num / 10;
        str[length - (i + 1)] = rest + '0';
        //bufSpace = bufSpace - 1;
    }
    str[length] = '\0';

	return length;
}


uint32_t UTILS_ConvertSignedIntToChar(Char_t str[], size_t bufSize, int32_t num){

	if(num >= 0){
		return UTILS_ConvertIntToChar(str,bufSize,num);
	}
	else{
		uint32_t y  = (uint32_t)(-1*num);
		if(bufSize>= 1){
			str[0] = '-';
			return UTILS_ConvertIntToChar(&str[1],bufSize -1,y) + 1;
		}
		else{
			return 0;
		}
	}
}



size_t UTILS_Strlen(const Char_t* str){

	return strlen(str);
}



Char_t* UTILS_Strcpy(Char_t* dest, const Char_t* src){

	return strcpy(dest,src);
}

uint32_t UTILS_StringCopy(Char_t* dest, const Char_t* src, uint32_t bufsize){

	uint32_t srcLength;
	srcLength = strlen(src);
	uint32_t retVal;

	if(bufsize >= srcLength){
		strncpy(dest,src,srcLength);
		retVal = srcLength;
	}
	else {
		strncpy(dest,src,bufsize);
		retVal = bufsize;
	}

	return retVal;
}


Char_t* UTILS_Strncpy(Char_t* dest, const Char_t* src, size_t n){

	return strncpy(dest,src,n);
}


/***********************************************************************/
/* LOCAL FUNCTIONS                                                     */
/***********************************************************************/



/** @} */
