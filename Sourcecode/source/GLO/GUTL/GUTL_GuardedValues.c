/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation: xxx
 ***********************************************************************/

/** @addtogroup GUTL_GuardedValues
 * @{
 * @file
 * This source file contains the definition of the functions of
 *  module GUTL_GuardedValues.
 */

/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/

#include "GUTL_GuardedValues.h"
#include "GERR.h"

/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/


/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/


/***********************************************************************/
/* LOCAL CONSTANTS                                                     */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/


/***********************************************************************/
/* LOCAL VARIABLES                                                     */
/***********************************************************************/

/***********************************************************************/
/* LOCAL FUNCTION PROTOTYPES                                           */
/***********************************************************************/


/***********************************************************************/
/* GLOBAL FUNCTIONS                                                    */
/***********************************************************************/

//TODO Discuss during review if GERR_ErrorSelect really needs a return value!

Bool_t GUTL_GuardedValue_get_Bool_t(GUTL_GuardedValue_Bool_t_s guardedValue)
{
	/*As bool_t values are handled as uint8 the inverse of true (1) is 0xfe.*/
	/*This is just internally used in the guard. */
	uint32_t inverse  = (uint32_t)(guardedValue.inv_value);
	if((uint32_t)guardedValue.value != (uint32_t)((~inverse)&0x0001u))
	{
		GERR_ErrorSelect(E_GUTL_BitflipDetected,__FILE__,__LINE__);
	}
	return guardedValue.value;
}

uint8_t  GUTL_GuardedValue_get_Uint8_t(GUTL_GuardedValue_uint8_t_s guardedValue)
{
	uint32_t inverse  = guardedValue.inv_value;
	if(guardedValue.value != ((~inverse)&0xFFu))
	{
		GERR_ErrorSelect(E_GUTL_BitflipDetected,__FILE__,__LINE__);
	}
	return guardedValue.value;
}

int16_t GUTL_GuardedValue_getInt16_t(GUTL_GuardedValue_int16_t_s guardedValue){

	int16_t inverse  = guardedValue.inv_value;
	if((int16_t)guardedValue.value != ((~(int16_t)inverse)&0xFFFFFFFF))
	{
		GERR_ErrorSelect(E_GUTL_BitflipDetected,__FILE__,__LINE__);
	}
	return guardedValue.value;

}

uint32_t GUTL_GuardedValue_getUint32_t(GUTL_GuardedValue_uint32_t_s guardedValue){

	uint32_t inverse  = guardedValue.inv_value;
	if(guardedValue.value != ((~inverse)&0xFFFFFFFFu))
	{
		GERR_ErrorSelect(E_GUTL_BitflipDetected,__FILE__,__LINE__);
	}
	return guardedValue.value;

}

int32_t GUTL_GuardedValue_getInt32_t(GUTL_GuardedValue_int32_t_s guardedValue){

	int32_t inverse  = guardedValue.inv_value;
	if(guardedValue.value != ((~inverse)&0xFFFFFFFF))
	{
		GERR_ErrorSelect(E_GUTL_BitflipDetected,__FILE__,__LINE__);
	}
	return guardedValue.value;

}

Force_Newton_t GUTL_GuardedValue_get_Force_Newton_t(GUTL_GuardedValue_Force_Newton_t_s guardedValue){

	Force_Newton_t inverse  = guardedValue.inv_value;
	if((uint32_t)guardedValue.value != ((~(uint32_t)inverse)& (0xFFFFFFFFu)))
	{
		GERR_ErrorSelect(E_GUTL_BitflipDetected,__FILE__,__LINE__);
	}
	return guardedValue.value;
}

Angle_Degree_t GUTL_GuardedValue_get_Angle_Degree_t(GUTL_GuardedValue_Angle_Degree_t_s guardedValue){

	Angle_Degree_t inverse  = guardedValue.inv_value;
	if((uint32_t)guardedValue.value != ((~(uint32_t)inverse)& (0xFFFFFFFFu)))
	{
		GERR_ErrorSelect(E_GUTL_BitflipDetected,__FILE__,__LINE__);
	}
	return guardedValue.value;
}

Temp_DegreeCelsius_t GUTL_GuardedValue_get_Temp_DegreeCelsius_t(GUTL_GuardedValue_Temp_DegreeCelsius_t_s guardedValue){

	Temp_DegreeCelsius_t inverse  = guardedValue.inv_value;
	if((uint32_t)guardedValue.value != ((~(uint32_t)inverse)& (0xFFFFFFFFu)))
	{
		GERR_ErrorSelect(E_GUTL_BitflipDetected,__FILE__, __LINE__);
	}
	return guardedValue.value;
}

Voltage_mV_t GUTL_GuardedValue_get_Voltage_mV_t(GUTL_GuardedValue_Voltage_mV_t_s guardedValue){

	Voltage_mV_t inverse  = guardedValue.inv_value;
	if((uint32_t)guardedValue.value != ((~(uint32_t)inverse)& (0xFFFFFFFFu)))
	{
		GERR_ErrorSelect(E_GUTL_BitflipDetected,__FILE__, __LINE__);
	}
	return guardedValue.value;
}

Current_mA_t GUTL_GuardedValue_get_Current_mA_t(GUTL_GuardedValue_Current_mA_t_s guardedValue){

	Current_mA_t inverse  = guardedValue.inv_value;
	if(guardedValue.value != ((~inverse)& (0xFFFFFFFFu)))
	{
		GERR_ErrorSelect(E_GUTL_BitflipDetected,__FILE__, __LINE__);
	}
	return guardedValue.value;
}

void GUTL_GuardedValue_set_Bool_t(GUTL_GuardedValue_Bool_t_s* guardedValue, Bool_t value)
{
	if(guardedValue == NULL){
		GERR_ErrorSelect(E_GLO_Nullpointer,__FILE__, __LINE__);
	}
	else{
		guardedValue->value = value;
		guardedValue->inv_value = (Bool_t)(!value);
	}

}
void GUTL_GuardedValue_set_Uint8(GUTL_GuardedValue_uint8_t_s* guardedValue, uint8_t  value){

	if(guardedValue == NULL){
		GERR_ErrorSelect(E_GLO_Nullpointer,__FILE__, __LINE__);
	}
	else{
		guardedValue->value = value;
		guardedValue->inv_value = ~value;
	}

}

void GUTL_GuardedValue_set_Int16_t(GUTL_GuardedValue_int16_t_s* guardedValue, int16_t  value){

	if(guardedValue == NULL){
		GERR_ErrorSelect(E_GLO_Nullpointer,__FILE__, __LINE__);
	}
	else{
		guardedValue->value = value;
		guardedValue->inv_value = ~value;
	}

}

void GUTL_GuardedValue_set_Uint32_t(GUTL_GuardedValue_uint32_t_s* guardedValue, uint32_t  value){

	if(guardedValue == NULL){
		GERR_ErrorSelect(E_GLO_Nullpointer,__FILE__, __LINE__);
	}
	else{
		guardedValue->value = value;
		guardedValue->inv_value = ~value;
	}

}

void GUTL_GuardedValue_set_Int32_t(GUTL_GuardedValue_int32_t_s* guardedValue, int32_t  value){

	if(guardedValue == NULL){
		GERR_ErrorSelect(E_GLO_Nullpointer,__FILE__, __LINE__);
	}
	else{
		guardedValue->value = value;
		guardedValue->inv_value = ~value;
	}

}
void GUTL_GuardedValue_set_Force_Newton_t(GUTL_GuardedValue_Force_Newton_t_s* guardedValue, Force_Newton_t value){

	if(guardedValue == NULL){
		GERR_ErrorSelect(E_GLO_Nullpointer,__FILE__, __LINE__);
	}
	else{
		guardedValue->value = value;
		guardedValue->inv_value = (Force_Newton_t)(~(uint32_t)value);
	}
}

void GUTL_GuardedValue_set_Angle_Degree_t(GUTL_GuardedValue_Angle_Degree_t_s* guardedValue, Angle_Degree_t value){

	if(guardedValue == NULL){
		GERR_ErrorSelect(E_GLO_Nullpointer,__FILE__, __LINE__);
	}
	else{
		guardedValue->value = value;
		guardedValue->inv_value = (Angle_Degree_t)(~(uint32_t)value);
	}

}

void GUTL_GuardedValue_set_Temp_DegreeCelsius_t(GUTL_GuardedValue_Temp_DegreeCelsius_t_s* guardedValue, Temp_DegreeCelsius_t value){

	if(guardedValue == NULL){
		GERR_ErrorSelect(E_GLO_Nullpointer,__FILE__, __LINE__);
	}
	else{
		guardedValue->value = value;
		guardedValue->inv_value = (Temp_DegreeCelsius_t)(~(uint32_t)value);	}
}


void GUTL_GuardedValue_set_Voltage_mV_t(GUTL_GuardedValue_Voltage_mV_t_s* guardedValue, Voltage_mV_t value){

	if(guardedValue == NULL){
		GERR_ErrorSelect(E_GLO_Nullpointer,__FILE__, __LINE__);
	}
	else{
		guardedValue->value = value;
		guardedValue->inv_value = (Voltage_mV_t)(~(uint32_t)value);	}
}

void GUTL_GuardedValue_set_Current_mA_t(GUTL_GuardedValue_Current_mA_t_s* guardedValue, Current_mA_t value){

	if(guardedValue == NULL){
		GERR_ErrorSelect(E_GLO_Nullpointer,__FILE__, __LINE__);
	}
	else{
		guardedValue->value = value;
		guardedValue->inv_value = (Current_mA_t)(~value);	}
}


/***********************************************************************/
/* LOCAL FUNCTIONS                                                     */
/***********************************************************************/



/** @} */
