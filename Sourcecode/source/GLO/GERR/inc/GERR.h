/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation: GERR Error handler module found in Global layer
 ***********************************************************************/

/** @defgroup GERR Error handler module found in Global layer
 * @{
 * @brief
 * The next blank line is mandatory!
 *
 * This header file has all the error codes that have been identified in the
 * system. It is the public interface to the error handler module.
 * @file
 * This header file contains the declaration of the public or private
 * interface to GERR error handler module of the global interface.
 */

#ifndef GERR_H_
#define GERR_H_

/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/

#include "types.h"
#include "GINIT.h"

/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/

/**
 * The different possible errors that can be identified in the system.
 *
 */
typedef enum tag_GERR_systemErrors_e{

	E_GLO_Nullpointer,
	E_GLO_DataNotValid,
	E_GUTL_BitflipDetected

}GERR_systemErrors_e;

/**
 * The different return values/status of each public function of GERR module
 *
 */
typedef enum tag_GERR_returnStatus_e{

	E_GERR_Success,
	E_GERR_Failed

}GERR_returnStatus_e;



/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL FUNCTION PROTOTYPES                                          */
/***********************************************************************/

/** Initialize the RERR-Module
 *  @return E_GINIT_ReturnFailure if failed
 */
GINIT_Return_e GERR_Initialize(GINIT_InitLevel_e initLevel);


/** Pass in error code and the error handler will handle the error case
 * @param errorCode the error which occurred
 * @param file the name of the file the function is called
 * @param codeLine the line of the file on where the function is called
 *
 */
void GERR_ErrorSelect(GERR_systemErrors_e errorCode, const Char_t* file, uint32_t codeLine);


#endif /* GERR_H_ */

/** @} */

