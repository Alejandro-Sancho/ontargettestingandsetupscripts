/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation: xxx
 ***********************************************************************/

/** @defgroup GOS Global Operating System
 * @{
 * @brief
 *
 *
 * This component handles the task creation, wait and signal mechanism.
 * Its global wrapper for the operating system
 * @file
 * This header file contains the declaration of the public
 * interface to module GOS (Global Operating System).
 */

#ifndef GOS_H
#define GOS_H
/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/
#include "GINIT.h"
/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/
/**
 * The possible return values of varous public function of GOS
 */
typedef enum tag_GOS_Return_e{
	E_GOS_ReturnSuccess,
	E_GOS_ReturnFailed
}GOS_Return_e;

/**
 * The different task which refers to predefined task properties.
 * For each task in the system there is a entry in this enumeration.
 */
typedef enum tag_GOS_Task_e{
	E_RDAC_DataAquisitonTask = 0,
	E_GLOG_LoggerTask,
	E_GOS_LiveLedTask,
	E_ASYS_SystemStateTask,
	E_ADEM_DeviceMonitorTask,
	E_RDIS_DisplayTask,
	E_ACEM_CalculationEngineManagerTask,
	E_AUI_UserInterfaceTask,
	E_GERR_ErrorHandlerTask

}GOS_Task_e;

typedef enum tag_GOS_TaskSignal_e{
	E_GOS_CollectData,
	E_GOS_NewDataAvaiable
}GOS_TaskSignal;

/**
 * Type definition for a task function which is passed in GOS_TaskCreate
 */
typedef void (*GOS_TaskFunction_t)( void * param);

/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/


/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/


/***********************************************************************/
/* GLOBAL FUNCTION PROTOTYPES                                          */
/***********************************************************************/

/** Initialize the GOS-Module
 * @return E_GINIT_Return if failed, E_GINIT_Success if successful
 */
GINIT_Return_e GOS_Initialize(GINIT_InitLevel_e initLevel);

/** Creates a task which executes the given function. The task properties are set during GOS_Initialize
 * The task will be started after calling GOS_StartScheduler
 * @param task the task which refers to the task properties (like priorization, stacksize etc)
 * @param pvTaskCode the function which will be executed in a seperate task
 */
GOS_Return_e GOS_CreateTask(GOS_Task_e task,GOS_TaskFunction_t pvTaskCode);

/** Starts the real time kernel and all task which were created before will start running
 * The scheduler can be started only once, multiple calls are ignored
 */
void GOS_StartScheduler(void);

/**Wakes up all blocked task which are waiting for a signal. Never create a task twice, this is not handled
 * and the behaviour is not defined
 * @param taskSignal the signal which will be notified
 * @return E_GOS_ReturnSuccessful if notification successful, otherwise E_GOS_ReturnFailed
 */
GOS_Return_e GOS_NotifyAll(GOS_TaskSignal taskSignal);
/**
 * Sets the caller task into a blocked state and wait until a notify to the referenced signal is done
 * @param taskSignal the signal for which the caller task will be waiting
 * @return E_GOS_ReturnSuccessful if wait successful, otherwise E_GOS_ReturnFailed
 */
GOS_Return_e GOS_WaitForSignal(GOS_TaskSignal taskSignal);
/** Start of a critical code region.  Preemptive context switches cannot occur when in a critical region.
 */
void GOS_EnterCriticalSection(void);
/** End of a critical code region.  Preemptive context witches cannot occur when in a critical region.
 *
 */
void GOS_ExitCriticalSection(void);

#endif /* ROS_H */

/** @} */
