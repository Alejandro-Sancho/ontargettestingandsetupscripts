/***********************************************************************
 * $Id$
 * Copyright MyoSwiss
 * Project: MyoSuite
 * Documentation: xxx
 ***********************************************************************/

/** @addtogroup GOS
 * @{
 * @file
 * This source file contains the definition of the functions of
 * module GOS (Global Operating System).
 */

/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/

#include "GOS.h"
#include "HTIM.h"

#include "FreeRTOS.h"
#include "task.h"
#include "types.h"
#include "Platform.h"
#include "fsl_debug_console.h"


/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/
/*
 * Number of different tasks defined in public interface
 */
#define GOS_NumberOfTasks (9u)
/*
 * Number of different events defined in public interface
 */
#define GOS_NumberOfEvents (1u)
/*
 * Minimal stack size
 */
#define GOS_MINIMALSTACKSIZE (256u) //TODO MYOSW-41 Define Task properties

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/
/**
 * The property struct of a single task
 */
typedef struct tag_GOS_TaskSettings_s{
	const char *  pcName;
	uint16_t usStackDepth;
	UBaseType_t uxPriority;
	TaskHandle_t xHandle;
	Bool_t doNotification[GOS_NumberOfEvents];
}GOS_TaskSettings_s;


/***********************************************************************/
/* LOCAL CONSTANTS                                                     */
/***********************************************************************/


/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/


/***********************************************************************/
/* LOCAL VARIABLES                                                     */
/***********************************************************************/
/**
 * List of all task properties
 */
static GOS_TaskSettings_s S_TaskList[GOS_NumberOfTasks];
/**
 * Status flag of scheduler, true if scheduling has started
 */
static Bool_t S_SchedulerStarted;
/***********************************************************************/
/* LOCAL FUNCTION PROTOTYPES                                           */
/***********************************************************************/
static void periodicTimerCallback(void);

/***********************************************************************/
/* GLOBAL FUNCTIONS                                                    */
/***********************************************************************/

GINIT_Return_e GOS_Initialize(GINIT_InitLevel_e initLevel){

	switch(initLevel){
	case E_GINIT_InitLevel1:

		S_SchedulerStarted = false;

		S_TaskList[E_GOS_LiveLedTask].pcName = "GOS_LiveTask";
		S_TaskList[E_GOS_LiveLedTask].uxPriority = 3;
		S_TaskList[E_GOS_LiveLedTask].usStackDepth = GOS_MINIMALSTACKSIZE;
		S_TaskList[E_GOS_LiveLedTask].xHandle = NULL;

		S_TaskList[E_RDAC_DataAquisitonTask].pcName = "RDAC_DataAquisitionTask";
		S_TaskList[E_RDAC_DataAquisitonTask].uxPriority = 4;
		S_TaskList[E_RDAC_DataAquisitonTask].usStackDepth = GOS_MINIMALSTACKSIZE;
		S_TaskList[E_RDAC_DataAquisitonTask].xHandle = NULL;

		S_TaskList[E_GLOG_LoggerTask].pcName = "GLOG_LoggerTask";
		S_TaskList[E_GLOG_LoggerTask].uxPriority = 4;
		S_TaskList[E_GLOG_LoggerTask].usStackDepth = 256;
		S_TaskList[E_GLOG_LoggerTask].xHandle = NULL;

		S_TaskList[E_ACEM_CalculationEngineManagerTask].pcName = "GOS_CalculationEngineManagerTask";
		S_TaskList[E_ACEM_CalculationEngineManagerTask].uxPriority = 4;
		S_TaskList[E_ACEM_CalculationEngineManagerTask].usStackDepth = GOS_MINIMALSTACKSIZE;
		S_TaskList[E_ACEM_CalculationEngineManagerTask].xHandle = NULL;

		S_TaskList[E_RDIS_DisplayTask].pcName = "GOS_DisplayTask";
		S_TaskList[E_RDIS_DisplayTask].uxPriority = 4;
		S_TaskList[E_RDIS_DisplayTask].usStackDepth = GOS_MINIMALSTACKSIZE;
		S_TaskList[E_RDIS_DisplayTask].xHandle = NULL;
		break;

	case E_GINIT_InitLevel2:

	HTIM_SubscribeTimer(periodicTimerCallback);

		break;
	default:
		;;
		break;
	}

	return E_GINIT_ReturnSuccess;
}

GOS_Return_e GOS_CreateTask(GOS_Task_e task,GOS_TaskFunction_t pvTaskCode){
	GOS_Return_e retVal;
	BaseType_t retValTaskCreation;
	retVal = E_GOS_ReturnSuccess;

	//TODO MYOSW-41 Define Task properties, so assert can be deleted
	assert(S_TaskList[task].uxPriority);//avoid to have a non initialized task, priority can't be zero, only for dev time used to avoid strange errors

	retValTaskCreation = xTaskCreate( pvTaskCode, S_TaskList[task].pcName, S_TaskList[task].usStackDepth, NULL, S_TaskList[task].uxPriority, &(S_TaskList[task].xHandle));
	if(retValTaskCreation != pdPASS){
		retVal = E_GOS_ReturnFailed;

	}
	if(S_TaskList[task].xHandle == NULL){
		retVal = E_GOS_ReturnFailed;
	}
	return retVal;
}

GOS_Return_e GOS_NotifyAll(GOS_TaskSignal taskSignal){
	BaseType_t retValTask;
	GOS_Return_e retVal;
	retVal = E_GOS_ReturnSuccess;
	volatile uint32_t i;

	GOS_EnterCriticalSection();
	for(i = 0; i <GOS_NumberOfTasks; ++i){

		if(S_TaskList[i].doNotification[taskSignal] == true){
			retValTask = xTaskNotifyGive(S_TaskList[i].xHandle);
			if(retValTask == pdPASS){
				S_TaskList[i].doNotification[taskSignal] = false;
			}
			else{
				retVal = E_GOS_ReturnFailed;
			}
		}

	}
	GOS_ExitCriticalSection();

	return retVal;

}

GOS_Return_e GOS_WaitForSignal(GOS_TaskSignal taskSignal){
	TaskHandle_t xTaskToNotify = xTaskGetCurrentTaskHandle();
	uint32_t retValTask;
	GOS_Return_e retVal;
	retVal = E_GOS_ReturnSuccess;

	volatile uint32_t i;

	for(i = 0; i <GOS_NumberOfTasks; ++i){
		if(S_TaskList[i].xHandle == xTaskToNotify){
			S_TaskList[i].doNotification[taskSignal] = true;
			break;
		}
	}

	retValTask = ulTaskNotifyTake( pdTRUE, portMAX_DELAY);
	if(retValTask != 0x01u){
		retVal = E_GOS_ReturnFailed;
	}

	return retVal;
}

void GOS_StartScheduler(void){
	if(!S_SchedulerStarted){
		S_SchedulerStarted = true;
		HTIM_StartTimer(E_HTIM_10msTimer);
		vTaskStartScheduler();

	}
}
void GOS_EnterCriticalSection(){

	taskENTER_CRITICAL();
}

void GOS_ExitCriticalSection(){

	taskEXIT_CRITICAL();
}

/***********************************************************************/
/* LOCAL FUNCTIONS                                                     */
/***********************************************************************/

static void periodicTimerCallback(void){
	BaseType_t retValTask;
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	uint32_t i;

		for(i = 0; i <GOS_NumberOfTasks; ++i){

			if(S_TaskList[i].doNotification[E_GOS_CollectData] == true){
				vTaskNotifyGiveFromISR(S_TaskList[i].xHandle,&xHigherPriorityTaskWoken);
				S_TaskList[i].doNotification[E_GOS_CollectData] = false;
			}
		}

    portYIELD_FROM_ISR( xHigherPriorityTaskWoken);


}

/** @} */
