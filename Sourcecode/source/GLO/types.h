/***********************************************************************
 * $Id$
 * Copyright MyoSwiss
 * Project: MyoSwiss
 * Documentation: xxx
 ***********************************************************************/

/** @defgroup LABC Long Name of the Component
 * @{
 * @brief
 * The next blank line is mandatory!
 * 
 * A bit more elaborated description focusing on the functions declared
 * here.
 * @file types.h
 * This header defines all native data types
 */
#ifndef DATA_TYPES_FOR_MODULES
#define DATA_TYPES_FOR_MODULES


/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/
//#include <stdlibs/stdint.h>
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <math.h>


//typedef double Double_t;
//typedef double Double64_t;

typedef float Double_t;
typedef float Double64_t;


//float
typedef float Float32_t;

//double

/* 	true and false is not necessary because it's C99 */
//bool
typedef _Bool Bool_t;
typedef char Char_t;
typedef unsigned char Uchar_t;


/* physical units*/
typedef int32_t Angle_Degree_t;
typedef int32_t Angle_mDegree_t;

typedef int32_t Temp_DegreeCelsius_t;
typedef int32_t Force_Newton_t;
typedef uint32_t Time_us_t;
typedef int32_t Time_ms_t;
typedef int32_t Current_mA_t;
typedef uint32_t Voltage_mV_t;
typedef Double_t Meter_m_t;
typedef Double_t Freq_hz_t;


/***********************************************************************/
/* GLOBAL CONSTANTS                                                            */
/***********************************************************************/

//const Double64_t GLO_PI = 3.1416;
#define GLO_PI 3.1416

//sampling interval
//const Double64_t GLO_dt = 0.02;
#define GLO_dt 0.01F

#endif /* DATA_TYPES_FOR_MODULES */

/** @} */
