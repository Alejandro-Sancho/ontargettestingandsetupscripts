/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation: xxx
 ***********************************************************************/

/** @defgroup GLOG Logger Module
 * @{
 * @brief
 * The next blank line is mandatory!
 *
 * This module caches logging messages, which are passed with maximum 3 integer
 * parameters. The module builds a string and sends them to the Module RLOG.
 * The module creates a background task with low priority which builds the log messages.
 *
 * @file
 * This header file contains the declaration of the public
 * interface to module GLOG.
 */

#ifndef GINIT_H
#define GINIT_H


/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/

/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/

/**
 * The possible return values of each public function of GLOG
 */

typedef enum tag_GINIT_Return_e
{
	E_GINIT_ReturnSuccess,
	E_GINIT_ReturnFailure
} GINIT_Return_e;

typedef enum tag_GINIT_InitLevel_e
{
	E_GINIT_InitLevel1,
	E_GINIT_InitLevel2
} GINIT_InitLevel_e;


/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL FUNCTION PROTOTYPES                                          */
/***********************************************************************/


#endif /* GINIT_H */

/** @} */
