/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation: RDAC Data Acquisition Module RTE layer
 ***********************************************************************/

/** @defgroup RDAC Data Acquisition module
 * @{
 * @brief
 * The next blank line is mandatory!
 *
 * This module is responsible for the gathering of the data that is
 * provided by the IMUS and all other relevant system information
 * such as temperature, voltage, currents, battery...
 *
 * @file
 * This header file contains the declaration of the public or private
 * interface to HAL or RTE component or module RDAC (Data Acquisition Module).
 */

#ifndef RDAC_H_
#define RDAC_H_


/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/

#include "types.h"
#include "GINIT.h"

/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/

/**
 * The different return values/status of each public function of RDAC module
 *
 */
typedef enum tag_RDAC_returnStatus_e{

	E_RDAC_Success,
	E_RDAC_DataNotValid,
	E_RDAC_HwFail

}RDAC_returnStatus_e;

/**
 * The different values for the IMUs are stored in this struct
 *
 */
typedef struct tag_RDAC_CalcData_s{


    Angle_Degree_t angleShankRight;
    Angle_Degree_t angleShankLeft;
    Angle_Degree_t angleThighRight;
    Angle_Degree_t angleThighLeft;
    Angle_Degree_t angleTrunk;
    RDAC_returnStatus_e status;

} RDAC_CalcData_s;


/**
 * The different values for the RAW data provided by IMUs are stored in this struct (gyro, accel and encoder values).
 *
 */
typedef struct tag_RDAC_RawData_s{

	int16_t gyroShankRight;
	int16_t accelAShankRight;
	int16_t accelBShankRight;
	int16_t gyroShankLeft;
	int16_t accelAShankLeft;
	int16_t accelBShankLeft;
	int16_t gyroThighRight;
	int16_t accelAThighRight;
	int16_t accelBThighRight;
	int16_t gyroThighLeft;
	int16_t accelAThighLeft;
	int16_t accelBThighLeft;
	int16_t gyroTrunk;
	int16_t accelATrunk;
	int16_t accelBTrunk;
	int32_t motorEncoderRight;
	int32_t motorEncoderLeft;
    RDAC_returnStatus_e status;

} RDAC_RawData_s;

/**
 * The different values for the system information are stored in this struct
 *
 */
typedef struct tag_RDAC_SysData_s{

	Temp_DegreeCelsius_t temperatureMotorRight;
	Temp_DegreeCelsius_t temperatureMotorLeft;
	Voltage_mV_t batteryVoltage;
	Temp_DegreeCelsius_t batteryTemperature;
	uint32_t batteryCapacity;
    Current_mA_t  motorCurrentRight;
    Current_mA_t  motorCurrentLeft;
    RDAC_returnStatus_e status;

}RDAC_SysData_s;

/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL FUNCTION PROTOTYPES                                          */
/***********************************************************************/

/** Initialize the RDAC-Module
 *  @return E_GINIT_ReturnFailure if failed
 */
GINIT_Return_e RDAC_Initialize(GINIT_InitLevel_e initLevel);

/** Get the data from the 5 IMUS and assemble it to provide access from other modules.
 *
 * @returns A struct with the angles of the 5 IMUs and a RDAC_ReturnValue of the status.
 */
RDAC_CalcData_s RDAC_GetCalculationData(void);

/** Get the data from the system not used for calculation purposes (temp, current, voltage...).
 *
 * @returns A struct with the system information.
 */
RDAC_SysData_s RDAC_GetSystemData(void);

/** Get the raw data from the system which are read from sensors
 * @returns A struct with the raw data
 */
RDAC_RawData_s RDAC_GetRawData(void);

/** This function is used for unittest to run the task only once
 * DO NOT USE THIS FUNCTION OUTSIDE OF UNIT TESTS!
 *
 */
void RDAC_UnitTest_TaskRunOnce(void); // only used for unittests


#endif /* RDAC_H */

/** @} */
