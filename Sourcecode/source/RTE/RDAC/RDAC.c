/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation: RDAC Data Acquisition module of RTE layer
 ***********************************************************************/

/** @ RDAC Data Acquisition module of RTE layer
 * @{
 * @file
 * This source file contains the definition of the functions of
 * RTE component RDAC Data Acquisition module.
 */

/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/

#include "RDAC.h"
#include "GOS.h"
#include "GINIT.h"
#include "GERR.h"
#include "RIMU.h"
#include "RENC.h"
#include "RTMP.h"
#include "RMOT.h"
#include "RBAT.h"
#include "GUTL_GuardedValues.h"

#include "types.h"

/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/

/**
 * Guarded value type of RDAC_CalcData_s of public interface
 */
typedef struct tag_RDAC_GuardedCalcData_s{

	GUTL_GuardedValue_Angle_Degree_t_s angleShankRight;
	GUTL_GuardedValue_Angle_Degree_t_s angleShankLeft;
	GUTL_GuardedValue_Angle_Degree_t_s angleThighRight;
	GUTL_GuardedValue_Angle_Degree_t_s angleThighLeft;
	GUTL_GuardedValue_Angle_Degree_t_s angleTrunk;


}RDAC_GuardedCalcData_s;

/**
 * Guarded value type of RDAC_SysData_s of public interface
 */
typedef struct tag_RDAC_GuardedSysData_s{

	GUTL_GuardedValue_Temp_DegreeCelsius_t_s temperatureMotorRight;
	GUTL_GuardedValue_Temp_DegreeCelsius_t_s temperatureMotorLeft;
	GUTL_GuardedValue_Voltage_mV_t_s batteryVoltage;
	GUTL_GuardedValue_Temp_DegreeCelsius_t_s batteryTemperature;
	GUTL_GuardedValue_uint32_t_s batteryCapacity;
	GUTL_GuardedValue_Current_mA_t_s  motorCurrentRight;
	GUTL_GuardedValue_Current_mA_t_s  motorCurrentLeft;

}RDAC_GuardedSysData_s;

/**
 * Guarded value type of RDAC_RawData_s of public interface
 */

typedef struct tag_RDAC_GuardedRawData_s{

	GUTL_GuardedValue_int16_t_s gyroShankRight;
	GUTL_GuardedValue_int16_t_s accelAShankRight;
	GUTL_GuardedValue_int16_t_s accelBShankRight;
	GUTL_GuardedValue_int16_t_s gyroShankLeft;
	GUTL_GuardedValue_int16_t_s accelAShankLeft;
	GUTL_GuardedValue_int16_t_s accelBShankLeft;
	GUTL_GuardedValue_int16_t_s gyroThighRight;
	GUTL_GuardedValue_int16_t_s accelAThighRight;
	GUTL_GuardedValue_int16_t_s accelBThighRight;
	GUTL_GuardedValue_int16_t_s gyroThighLeft;
	GUTL_GuardedValue_int16_t_s accelAThighLeft;
	GUTL_GuardedValue_int16_t_s accelBThighLeft;
	GUTL_GuardedValue_int16_t_s gyroTrunk;
	GUTL_GuardedValue_int16_t_s accelATrunk;
	GUTL_GuardedValue_int16_t_s accelBTrunk;
	GUTL_GuardedValue_int32_t_s motorEncoderRight;
	GUTL_GuardedValue_int32_t_s motorEncoderLeft;

}RDAC_GuardedRawData_s;

/***********************************************************************/
/* LOCAL CONSTANTS                                                     */
/***********************************************************************/
/*
 * Number of trials to read data from sensors before inform error handler
 */
#define RDAC_MaxRetries 5u
/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/

/***********************************************************************/
/* LOCAL VARIABLES                                                     */
/***********************************************************************/
/**
 * Local buffer of all calculation data, stored in guarded values
 */
static RDAC_GuardedCalcData_s S_CalcDataBuffer;
/**
 * Local buffer of all system data, stored in guarded values
 */
static RDAC_GuardedSysData_s S_SysDataBuffer;
/**
 * Local buffer of all raw data, stored in guarded values
 */
static RDAC_GuardedRawData_s S_RawDataBuffer;

/**
 * This flag is used in the while loop of the logger task. This is needed for unit tests
 * and will be always true during normal usage
 */
static volatile Bool_t S_LoggerTaskShallRun;

/***********************************************************************/
/* LOCAL FUNCTION PROTOTYPES                                           */
/***********************************************************************/
/** The function which is passed to the RDAC task
 * @param param Parameter for the task, not used yet
 */
static void dataAquisitionTaskFunction(void* param);
/*
 * Collects all calculation data
 * @param values Destination where collected calulation data will be stored
 * @return false if one part of values not valid, true if data are valid
 */
static Bool_t collectRawAndCalcData(RDAC_CalcData_s* calcData, RDAC_RawData_s* rawData);
/*
 * Collects all system data
 * @param values Destination where collected system data will be stored
 * @return false if one part of values not valid, true if data are valid
 */
static Bool_t collectSysData(RDAC_SysData_s* values);
/*
 * Collects data of all IMUs in the System
 * @param values Destination where collected IMU datas will be stored
 * @return false if one part of values not valid, true if data are valid
 */
static Bool_t collectIMUData(RDAC_CalcData_s* calcData, RDAC_RawData_s* rawData);
/*
 * Collects data of all encoder in the System
 * @param values Destination where collected encoder counts will be stored
 * @return false if one part of values not valid, true if data are valid
 */
static Bool_t collectEncoderCounts(RDAC_RawData_s* data);
/*
 * Collects data of the motor currents in the System
 * @param values Destination where collected motor currents  will be stored
 * @return false if one part of values not valid, true if data are valid
 */
static Bool_t collectMotorCurrents(RDAC_SysData_s* data);
/*
 * Collects all voltage datas in the System
 * @param values Destination where collected voltage values will be stored
 * @return false if one part of values not valid, true if data are valid
 */
static Bool_t collectVoltages(RDAC_SysData_s* data);
/*
 * Collects all voltage temperature values in the System
 * @param values Destination where collected temperatures values will be stored
 * @return false if one part of values not valid, true if data are valid
 */
static Bool_t collectTemperatures(RDAC_SysData_s* data);
/*
 * Collects all voltage capacity values in the System
 * @param values Destination where collected capacities values will be stored
 * @return false if one part of values not valid, true if data are valid
 */
static Bool_t collectCapacities(RDAC_SysData_s* data);

/*
 * Copies the value passed per parameter to the local guarded CalcData buffer
 * @param values which shall be copied to the local buffer
 */
static void updateCalcBuffer(const RDAC_CalcData_s* data);
/*
 * Copies the value passed per parameter to the local guarded SysData buffer
 * @param values which shall be copied to the local buffer
 */
static void updateSysBuffer(const RDAC_SysData_s* data);

/*
 * Copies the value passed per parameter to the local guarded RawData buffer
 * @param values which shall be copied to the local buffer
 */
static void updateRawBuffer(const RDAC_RawData_s* data);

/***********************************************************************/
/* GLOBAL FUNCTIONS                                                    */
/***********************************************************************/

GINIT_Return_e RDAC_Initialize(GINIT_InitLevel_e initLevel)
{
	GOS_Return_e retValGOS;
	GINIT_Return_e retVal;
	switch(initLevel){
	case E_GINIT_InitLevel1:
		S_LoggerTaskShallRun = true;
		retVal = E_GINIT_ReturnSuccess;

		GUTL_GuardedValue_set_Angle_Degree_t(&S_CalcDataBuffer.angleShankRight,0);
		GUTL_GuardedValue_set_Angle_Degree_t(&S_CalcDataBuffer.angleShankLeft,0);
		GUTL_GuardedValue_set_Angle_Degree_t(&S_CalcDataBuffer.angleThighLeft,0);
		GUTL_GuardedValue_set_Angle_Degree_t(&S_CalcDataBuffer.angleThighRight,0);
		GUTL_GuardedValue_set_Angle_Degree_t(&S_CalcDataBuffer.angleTrunk,0);

		GUTL_GuardedValue_set_Temp_DegreeCelsius_t(&S_SysDataBuffer.temperatureMotorRight,0);
		GUTL_GuardedValue_set_Temp_DegreeCelsius_t(&S_SysDataBuffer.temperatureMotorLeft,0);
		GUTL_GuardedValue_set_Current_mA_t(&S_SysDataBuffer.motorCurrentRight,0);
		GUTL_GuardedValue_set_Current_mA_t(&S_SysDataBuffer.motorCurrentLeft,0);
		GUTL_GuardedValue_set_Voltage_mV_t(&S_SysDataBuffer.batteryVoltage,0);
		GUTL_GuardedValue_set_Temp_DegreeCelsius_t(&S_SysDataBuffer.batteryTemperature,0);
		GUTL_GuardedValue_set_Uint32_t(&S_SysDataBuffer.batteryCapacity,0);

		GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.gyroShankRight,0);
		GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.accelAShankRight,0);
		GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.accelBShankRight,0);
		GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.gyroShankLeft,0);
		GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.accelAShankLeft,0);
		GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.accelBShankLeft,0);
		GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.gyroThighRight,0);
		GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.accelAThighRight,0);
		GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.accelBThighRight,0);
		GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.gyroThighLeft,0);
		GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.accelAThighLeft,0);
		GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.accelBThighLeft,0);
		GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.gyroTrunk,0);
		GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.accelATrunk,0);
		GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.accelBTrunk,0);
		GUTL_GuardedValue_set_Int32_t(&S_RawDataBuffer.motorEncoderRight,0);
		GUTL_GuardedValue_set_Int32_t(&S_RawDataBuffer.motorEncoderLeft,0);


		break;

	case E_GINIT_InitLevel2:

		retValGOS = GOS_CreateTask(E_RDAC_DataAquisitonTask,dataAquisitionTaskFunction);
		if(retValGOS != E_GOS_ReturnSuccess){
			retVal = E_GINIT_ReturnFailure;
		}
		else{
			retVal = E_GINIT_ReturnSuccess;
		}
		break;
	default:
		retVal = E_GINIT_ReturnSuccess;
		break;
	}

	return retVal;
}


RDAC_CalcData_s RDAC_GetCalculationData(void)
{

	RDAC_CalcData_s calcData;

	calcData.angleShankRight = GUTL_GuardedValue_get_Angle_Degree_t(S_CalcDataBuffer.angleShankRight);
	calcData.angleShankLeft = GUTL_GuardedValue_get_Angle_Degree_t(S_CalcDataBuffer.angleShankLeft);
	calcData.angleThighLeft = GUTL_GuardedValue_get_Angle_Degree_t(S_CalcDataBuffer.angleThighLeft);
	calcData.angleThighRight = GUTL_GuardedValue_get_Angle_Degree_t(S_CalcDataBuffer.angleThighRight);
	calcData.angleTrunk = GUTL_GuardedValue_get_Angle_Degree_t(S_CalcDataBuffer.angleTrunk);
	calcData.angleTrunk = GUTL_GuardedValue_get_Angle_Degree_t(S_CalcDataBuffer.angleTrunk);
	calcData.status = E_RDAC_Success;
	return calcData;

}


RDAC_SysData_s RDAC_GetSystemData(void)
{
	RDAC_SysData_s sysData;

	sysData.temperatureMotorRight = GUTL_GuardedValue_get_Temp_DegreeCelsius_t(S_SysDataBuffer.temperatureMotorRight);
	sysData.temperatureMotorLeft = GUTL_GuardedValue_get_Temp_DegreeCelsius_t(S_SysDataBuffer.temperatureMotorLeft);
	sysData.motorCurrentRight = GUTL_GuardedValue_get_Current_mA_t(S_SysDataBuffer.motorCurrentRight);
	sysData.motorCurrentLeft = GUTL_GuardedValue_get_Current_mA_t(S_SysDataBuffer.motorCurrentLeft);
	sysData.batteryVoltage = GUTL_GuardedValue_get_Voltage_mV_t(S_SysDataBuffer.batteryVoltage);
	sysData.batteryTemperature = GUTL_GuardedValue_get_Temp_DegreeCelsius_t(S_SysDataBuffer.batteryTemperature);
	sysData.batteryCapacity = GUTL_GuardedValue_getUint32_t(S_SysDataBuffer.batteryCapacity);

	sysData.status = E_RDAC_Success;
	return sysData;

}

RDAC_RawData_s RDAC_GetRawData(void){
	RDAC_RawData_s rawData;

	rawData.gyroShankRight = GUTL_GuardedValue_getInt16_t(S_RawDataBuffer.gyroShankRight);
	rawData.accelAShankRight = GUTL_GuardedValue_getInt16_t(S_RawDataBuffer.accelAShankRight);
	rawData.accelBShankRight = GUTL_GuardedValue_getInt16_t(S_RawDataBuffer.accelBShankRight);
	rawData.gyroShankLeft = GUTL_GuardedValue_getInt16_t(S_RawDataBuffer.gyroShankLeft);
	rawData.accelAShankLeft = GUTL_GuardedValue_getInt16_t(S_RawDataBuffer.accelAShankLeft);
	rawData.accelBShankLeft = GUTL_GuardedValue_getInt16_t(S_RawDataBuffer.accelBShankLeft);
	rawData.gyroThighRight = GUTL_GuardedValue_getInt16_t(S_RawDataBuffer.gyroThighRight);
	rawData.accelAThighRight = GUTL_GuardedValue_getInt16_t(S_RawDataBuffer.accelAThighRight);
	rawData.accelBThighRight = GUTL_GuardedValue_getInt16_t(S_RawDataBuffer.accelBThighRight);
	rawData.gyroThighLeft = GUTL_GuardedValue_getInt16_t(S_RawDataBuffer.gyroThighLeft);
	rawData.accelAThighLeft = GUTL_GuardedValue_getInt16_t(S_RawDataBuffer.accelAThighLeft);
	rawData.accelBThighLeft = GUTL_GuardedValue_getInt16_t(S_RawDataBuffer.accelBThighLeft);
	rawData.gyroTrunk = GUTL_GuardedValue_getInt16_t(S_RawDataBuffer.gyroTrunk);
	rawData.accelATrunk = GUTL_GuardedValue_getInt16_t(S_RawDataBuffer.accelATrunk);
	rawData.accelBTrunk = GUTL_GuardedValue_getInt16_t(S_RawDataBuffer.accelBTrunk);
    rawData.motorEncoderRight = GUTL_GuardedValue_getInt32_t(S_RawDataBuffer.motorEncoderRight);
    rawData.motorEncoderLeft = GUTL_GuardedValue_getInt32_t(S_RawDataBuffer.motorEncoderLeft);
    rawData.status = E_RDAC_Success;

    return rawData;
}

void RDAC_UnitTest_TaskRunOnce(void){

	S_LoggerTaskShallRun = false;
}

/***********************************************************************/
/* LOCAL FUNCTIONS                                                     */
/***********************************************************************/
static void dataAquisitionTaskFunction(void* param){
	RDAC_CalcData_s calcValues;
	RDAC_RawData_s rawValues;

	RDAC_SysData_s sysValues;

	(void)param; // is not used
	do{

		Bool_t retVal;

		retVal = collectRawAndCalcData(&calcValues, &rawValues);
		if(retVal == true){
			updateCalcBuffer(&calcValues);
			updateRawBuffer(&rawValues);
		}

		retVal = collectSysData(&sysValues);
		if(retVal == true){
			updateSysBuffer(&sysValues);
		}

	}while(S_LoggerTaskShallRun == true);
}

static Bool_t collectRawAndCalcData(RDAC_CalcData_s* calcData, RDAC_RawData_s* rawData){
	Bool_t retValRDAC = false;
	Bool_t retVal = true;
	uint32_t i;

	/*check RDAC_MaxRetries times before provide an error*/
	for(i = 0; (i <  RDAC_MaxRetries) && (retValRDAC == false); ++i){
		retValRDAC = collectIMUData(calcData, rawData);
	}
	if(retValRDAC == false){
		retVal = false;
		GERR_ErrorSelect(E_GLO_DataNotValid,__FILE__,__LINE__);
	}

	retValRDAC = false;
	/*check RDAC_MaxRetries times before provide an error*/
	for(i = 0; (i <  RDAC_MaxRetries) && (retValRDAC == false); ++i){
		retValRDAC = collectEncoderCounts(rawData);
	}
	if(retValRDAC == false){
		retVal = false;
		GERR_ErrorSelect(E_GLO_DataNotValid,__FILE__,__LINE__);
	}

	return retVal;
}





static Bool_t collectSysData(RDAC_SysData_s* values){

	Bool_t retValRDAC;
	Bool_t retVal = true;

	retValRDAC = collectTemperatures(values);
	if(retValRDAC == false){
		retVal = false;
	}
	retValRDAC = collectVoltages(values);
	if(retValRDAC == false){
		retVal = false;
	}
	retValRDAC = collectMotorCurrents(values);
	if(retValRDAC == false){
		retVal = false;
	}

	retValRDAC = collectCapacities(values);
	if(retValRDAC == false){
		retVal = false;
	}

	return retVal;


}

static Bool_t collectIMUData(RDAC_CalcData_s* calcData, RDAC_RawData_s* rawData){

	RIMU_DataStruct_s imuData;
	Bool_t retVal = true;

	imuData = RIMU_GetIMUAngleFiltered(E_RIMU_RightShank);
	calcData->angleShankRight = imuData.imuAngle;
	rawData->accelAShankRight = imuData.accelValueA;
	rawData->accelBShankRight = imuData.accelValueB;
	rawData->gyroShankRight = imuData.gyroValue;
	if(imuData.imuStatusOfData != E_RIMU_ReturnSuccess){
		retVal = false;
	}

	imuData = RIMU_GetIMUAngleFiltered(E_RIMU_LeftShank);
	calcData->angleShankLeft = imuData.imuAngle;
	rawData->accelAShankLeft = imuData.accelValueA;
	rawData->accelBShankLeft = imuData.accelValueB;
	rawData->gyroShankLeft = imuData.gyroValue;
	if(imuData.imuStatusOfData != E_RIMU_ReturnSuccess){
		retVal = false;
	}

	imuData = RIMU_GetIMUAngleFiltered(E_RIMU_LeftThigh);
	calcData->angleThighLeft = imuData.imuAngle;
	rawData->accelAThighLeft = imuData.accelValueA;
	rawData->accelBThighLeft = imuData.accelValueB;
	rawData->gyroThighLeft = imuData.gyroValue;
	if(imuData.imuStatusOfData != E_RIMU_ReturnSuccess){
		retVal = false;
	}

	imuData = RIMU_GetIMUAngleFiltered(E_RIMU_RightThigh);
	calcData->angleThighRight = imuData.imuAngle;
	rawData->accelAThighRight = imuData.accelValueA;
	rawData->accelBThighRight= imuData.accelValueB;
	rawData->gyroThighRight = imuData.gyroValue;
	if(imuData.imuStatusOfData != E_RIMU_ReturnSuccess){
		retVal = false;
	}

	imuData = RIMU_GetIMUAngleFiltered(E_RIMU_Trunk);
	calcData->angleTrunk = imuData.imuAngle;
	rawData->accelATrunk = imuData.accelValueA;
	rawData->accelBTrunk= imuData.accelValueB;
	rawData->gyroTrunk = imuData.gyroValue;
	if(imuData.imuStatusOfData != E_RIMU_ReturnSuccess){
		retVal = false;
	}

	return retVal;
}

static Bool_t collectEncoderCounts(RDAC_RawData_s* data){

	RENC_EncoderCounts_s encData;
	Bool_t retVal = true;

	encData = RENC_getEncoderCounts();
	data->motorEncoderLeft = encData.motorEncoderCountLeft;
	data->motorEncoderRight = encData.motorEncoderCountRight;

	if(encData.status != E_RENC_ReturnSuccess){
		retVal = false;
	}

	return retVal;

}


static Bool_t collectMotorCurrents(RDAC_SysData_s* data){

	RMOT_MotorCurrents_s motorCurrents;
	Bool_t retVal = true;

	motorCurrents = RMOT_getMotorCurrents();
	data->motorCurrentLeft = motorCurrents.motorCurrentLeftSide;
	data->motorCurrentRight = motorCurrents.motorCurrentRightSide;
	if(motorCurrents.status != E_RMOT_ReturnSuccess){
		retVal = false;
	}

	return retVal;

}

static Bool_t collectVoltages(RDAC_SysData_s* data){

	RBAT_Voltage_s voltage;
	Bool_t retVal = true;

	voltage = RBAT_GetVoltage();
	data->batteryVoltage = voltage.batteryVoltage;
	if(voltage.status != E_RBAT_ReturnSuccess){
		retVal = false;
	}

	return retVal;

}

static Bool_t collectTemperatures(RDAC_SysData_s* data){

	RTMP_Temperatures_s temperatures;
	RBAT_Temp_s batTemp;
	Bool_t retVal = true;

	temperatures = RTMP_GetTemperatures();
	data->temperatureMotorLeft = temperatures.temperatureMotorLeft;
	data->temperatureMotorRight = temperatures.temperatureMotorRight;

	batTemp = RBAT_GetTemperature();
	data->batteryTemperature = batTemp.batteryTemperature;


	if(temperatures.status != E_RTMP_ReturnSuccess){
		retVal = false;
	}
	if(batTemp.status != E_RBAT_ReturnSuccess){
		retVal = false;
	}



	return retVal;
}

static Bool_t collectCapacities(RDAC_SysData_s* data){

	RBAT_Capacity_s capacity;
	Bool_t retVal = true;

	capacity = RBAT_GetCapacity();
	data->batteryCapacity = capacity.batteryCapacity;

	if(capacity.status != E_RBAT_ReturnSuccess){
		retVal = false;
	}
	return retVal;

}

static void updateCalcBuffer(const RDAC_CalcData_s* data){


	GOS_EnterCriticalSection();

	GUTL_GuardedValue_set_Angle_Degree_t(&S_CalcDataBuffer.angleShankRight,data->angleShankRight);
	GUTL_GuardedValue_set_Angle_Degree_t(&S_CalcDataBuffer.angleShankLeft,data->angleShankLeft);
	GUTL_GuardedValue_set_Angle_Degree_t(&S_CalcDataBuffer.angleThighLeft,data->angleThighLeft);
	GUTL_GuardedValue_set_Angle_Degree_t(&S_CalcDataBuffer.angleThighRight,data->angleThighRight);
	GUTL_GuardedValue_set_Angle_Degree_t(&S_CalcDataBuffer.angleTrunk,data->angleTrunk);

	GOS_ExitCriticalSection();

}

static void updateRawBuffer(const RDAC_RawData_s* data){

	GOS_EnterCriticalSection();

	GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.gyroShankRight,data->gyroShankRight);
	GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.accelAShankRight,data->accelAShankRight);
	GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.accelBShankRight,data->accelBShankRight);
	GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.gyroShankLeft,data->gyroShankLeft);
	GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.accelAShankLeft,data->accelAShankLeft);
	GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.accelBShankLeft,data->accelBShankLeft);
	GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.gyroThighRight,data->gyroThighRight);
	GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.accelAThighRight,data->accelAThighRight);
	GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.accelBThighRight,data->accelBThighRight);
	GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.gyroThighLeft,data->gyroThighLeft);
	GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.accelAThighLeft,data->accelAThighLeft);
	GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.accelBThighLeft,data->accelBThighLeft);
	GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.gyroTrunk,data->gyroTrunk);
	GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.accelATrunk,data->accelATrunk);
	GUTL_GuardedValue_set_Int16_t(&S_RawDataBuffer.accelBTrunk,data->accelBTrunk);
	GUTL_GuardedValue_set_Int32_t(&S_RawDataBuffer.motorEncoderRight,data->motorEncoderRight);
	GUTL_GuardedValue_set_Int32_t(&S_RawDataBuffer.motorEncoderLeft,data->motorEncoderLeft);

	GOS_ExitCriticalSection();


}

static void updateSysBuffer(const RDAC_SysData_s* data){


	GOS_EnterCriticalSection();

	GUTL_GuardedValue_set_Temp_DegreeCelsius_t(&S_SysDataBuffer.temperatureMotorRight,data->temperatureMotorRight);
	GUTL_GuardedValue_set_Temp_DegreeCelsius_t(&S_SysDataBuffer.temperatureMotorLeft,data->temperatureMotorLeft);
	GUTL_GuardedValue_set_Current_mA_t(&S_SysDataBuffer.motorCurrentRight,data->motorCurrentRight);
	GUTL_GuardedValue_set_Current_mA_t(&S_SysDataBuffer.motorCurrentLeft,data->motorCurrentLeft);
	GUTL_GuardedValue_set_Voltage_mV_t(&S_SysDataBuffer.batteryVoltage,data->batteryVoltage);
	GUTL_GuardedValue_set_Temp_DegreeCelsius_t(&S_SysDataBuffer.batteryTemperature,data->batteryTemperature);
	GUTL_GuardedValue_set_Uint32_t(&S_SysDataBuffer.batteryCapacity,data->batteryCapacity);

	GOS_ExitCriticalSection();

}


/** @} */
