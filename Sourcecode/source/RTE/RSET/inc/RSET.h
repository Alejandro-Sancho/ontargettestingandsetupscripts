/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation: RSET Settings component of RTE layer
 ***********************************************************************/

/** @defgroup RSET Settings component of RTE layer
 * @{
 * @brief
 * The next blank line is mandatory!
 *
 * A bit more elaborated description focusing on the functions declared
 * here.
 * @file
 * This header file contains the declaration of the public or private
 * interface to HAL or RTE component or module LABC (long name).
 */

#ifndef RSET_H_
#define RSET_H_

/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/

#include "GINIT.h"
#include "types.h"

/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/

/**
 * Parameter used to select if the system will be used commercially or
 * for research purposes
 *
 */
typedef enum tag_RSET_useCase_e{

	E_RSET_CommercialMode,
	E_RSET_ResearchMode

}RSET_useCase_e;

/**
 * Parameter used to choose the force mode of the system
 *
 */
typedef enum tag_RSET_forceMode_e{

	E_RSET_Transparency,
	E_RSET_Eccentric,
	E_RSET_Concentric

	//torque off but mcu and motor controllers on and we can go back to operation
	//Switch off motors completely should be modes or states?

}RSET_forceMode_e;


/**
 * Parameter used to choose the force levels of the system
 *
 */
typedef enum tag_RSET_forceLevel_e{

	E_RSET_Level0,
	E_RSET_Level1,
	E_RSET_Level2,
	E_RSET_Level3,
	E_RSET_Level4,
	E_RSET_Level5

}RSET_forceLevel_e;


/**
 * This parameter identifies is used as a status of the operation parameters
 * it checks if they are valid or not.
 *
 */
typedef enum tag_RSET_returnStatus_e{

	E_RSET_Success,
	E_RSET_DataNotValid

}RSET_returnStatus_e;


/**
 * The different values for the operational parameters are stored in this struct
 *
 */
typedef struct tag_RSET_paramStruct_s{

	RSET_useCase_e useCase;
	RSET_forceMode_e forceMode;
	RSET_forceLevel_e forceLevelRight;
	RSET_forceLevel_e forceLevelLeft;
	RSET_returnStatus_e status;


} RSET_paramStruct_s;



/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL FUNCTION PROTOTYPES                                          */
/***********************************************************************/


/** Initialize the RSET-Module
 *
 *  @return E_GINIT_ReturnFailure if failed
 */
GINIT_Return_e RSET_Initialize(GINIT_InitLevel_e initLevel);


/** Get operating parameters.
 *
 * @returns a struct with the parameters of the system as well as the status of
 * the parameters (ie. if there is an error)
 */
RSET_paramStruct_s RSET_GetParameters(void);


#endif /* RSET_H_ */

/** @} */
