/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation: RMOT
 ***********************************************************************/

/** @defgroup RCAN RTE_CANOpen
 * @{
 * @brief
 *
 * Device abstraction of the CANOPEN where the basic communication with the motor is done.
 * This includes sending current or position as well as reading current and encoders.
 * @file
 * This header file contains the declaration of the public
 * interface to module RCAN (RTE_CANOPEN).
 */

#ifndef RCAN_H
#define RCAN_H
/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/
#include "GINIT.h"
#include "types.h"
#include "../../RMOT/inc/RMOT.h"

/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/

/**
 * NodeID represents the two motors in the TDU. Node 1 is the right motor and
 * Node 2 is the left motor.
 */
typedef enum tag_RCAN_NodeID_e{
	E_RCAN_Node1 = 1u,
	E_RCAN_Node2 = 2u
}RCAN_NodeID_e;

/**
 * The possible status values of public functions
 */
typedef enum tag_RCAN_ReturnStatus_e{
	E_RCAN_ReturnSuccess,
	E_RCAN_ReturnFailed,
	E_RCAN_DataNotValid
}RCAN_ReturnStatus_e;

/**
 * struct that contains the motor data ie: current and enconders.
 */
typedef struct tag_RCAN_MotorData_s{
	int32_t motorCurrent;
	int32_t motorEncoder;
	int32_t motorVelocity;
}RCAN_MotorData_s;

/**
 * struct that contains the currents for each motor. The units are not mA because
 * we send a value per1000 of the maximum current.
 */
typedef struct tag_RCAN_MotorCurrentsPerThousand_s{
	int32_t motorCurrentPerThousandRightSide;
	int32_t motorCurrentPerThousandLeftSide;
}RCAN_MotorCurrentsPerThousand_s;



/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/


/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/


/***********************************************************************/
/* GLOBAL FUNCTION PROTOTYPES                                          */
/***********************************************************************/

/** Initialize the RCAN Initialize
 *  @param initLevel level of initialization
 *  @return E_GINIT_ReturnFailure if failed
 */
GINIT_Return_e RCAN_Initialize(GINIT_InitLevel_e initLevel);

/** Provides the current to the motor
 *  @param a struct with the current required for both legs.
 *  @return returns status success if the current is sent correctly.
 */
RCAN_ReturnStatus_e RCAN_sendCurrentToMotor(RCAN_MotorCurrentsPerThousand_s motorCurrent);

/** Provides the Position to the motor (if in position mode)
 *  @param input the position and the nodeID.
 *  @return returns status success if the position is sent correctly.
 */
RCAN_ReturnStatus_e RCAN_sendPositionToMotor(int32_t position, uint8_t nodeID);

/** Reads the actual current and encoder counts a motor
 *  @param pointer to a struct on which the motor values will be placed and the motor ID.
 *  @return void.
 */
void RCAN_getDataFromMotor(RCAN_MotorData_s *motorStruct, uint8_t nodeID);


#endif /* RCAN_H */

/** @} */
