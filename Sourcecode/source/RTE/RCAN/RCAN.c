/***********************************************************************
 * $Id$
 * Copyright MyoSwiss
 * Project: MyoSuite
 * Documentation: xxx
 ***********************************************************************/

/** @addtogroup RCAN
 * @{
 * @file
 * This source file contains the definition of the functions of
 * module RCAN (RTE_CANOPEN).
 */

/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/

#include "RCAN.h"
#include "RMOT.h"
#include "types.h"
#include "CANOpen.h"

/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/


/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/


/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/


/***********************************************************************/
/* LOCAL CONSTANTS                                                     */
/***********************************************************************/


/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/


/***********************************************************************/
/* LOCAL VARIABLES                                                     */
/***********************************************************************/


/***********************************************************************/
/* LOCAL FUNCTION PROTOTYPES                                           */
/***********************************************************************/


/***********************************************************************/
/* GLOBAL FUNCTIONS                                                    */
/***********************************************************************/


GINIT_Return_e RCAN_Initialize(GINIT_InitLevel_e initLevel)
{
	// Setup of the CANOpen protocol
	CANOpenInit( );
	return E_GINIT_ReturnSuccess;
}

RCAN_ReturnStatus_e RCAN_sendCurrentToMotor(RCAN_MotorCurrentsPerThousand_s motorCurrent)
{
	CANOpenPDOSetTorque( motorCurrent.motorCurrentPerThousandLeftSide, E_RCAN_Node1 );
	CANOpenPDOSetTorque( motorCurrent.motorCurrentPerThousandRightSide, E_RCAN_Node2 );

	return E_RCAN_ReturnSuccess;
}

RCAN_ReturnStatus_e RCAN_sendPositionToMotor(int32_t position, uint8_t nodeID)
{
    CANOpenPDOSetPosition( position, nodeID );
	return E_RCAN_ReturnSuccess;
}

void RCAN_getDataFromMotor(RCAN_MotorData_s *motorStruct, RCAN_NodeID_e nodeID)
{
	CANOpenNMTandSYNC( CANOpen_SYNC );
	CANOpenReadReply( &motorStruct->motorEncoder, &motorStruct->motorCurrent, nodeID );
}



/***********************************************************************/
/* LOCAL FUNCTIONS                                                     */
/***********************************************************************/



/** @} */






