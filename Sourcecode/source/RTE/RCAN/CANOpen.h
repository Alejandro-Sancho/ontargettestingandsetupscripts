/*
 * CANOpen.h
 *
 *  Created on: 15.04.2016
 *      Author: Kai Schmidt
 */

#ifndef CANOpen_H_
#define CANOpen_H_


#include "UTIL1.h"
#include "CANbus.h"
#include "PE_Types.h"
#include "RNG1.h"
#include "RNG2.h"
#include "RNG4.h"
#include "WAIT1.h"

#define CANbus_TXBuffer0_Idx 0U
#define CANbus_TXBuffer1_Idx 1U
#define CANbus_TXBuffer2_Idx 2U
#define CANbus_TXBuffer3_Idx 3U
#define CANbus_RXBuffer0_Idx 4U
#define CANbus_RXBuffer1_Idx 5U
#define CANbus_RXBuffer2_Idx 6U
#define CANbus_RXBuffer3_Idx 7U

typedef enum {
  CANOpen_NMT 						= 0x00U,
  CANOpen_NMTOP 					= 0x01U,
  CANOpen_SYNC						= 0x02U,
  CANOpen_SDO       				= 0x04U,
  CANOpen_PDO      					= 0x06U,
} CANOpenMsgType;

typedef enum {
  CANOpen_Position 					= 0x00U,
  CANOpen_Torque					= 0x02U,
} CANOpenControlMode;

static const uint16_t ControlWord_Ready2SwitchOn = 0x6;
static const uint16_t ControlWord_SwitchedOn = 0xf;
static const uint16_t ControlWord_OperationEnabled = 0x2f;
static const uint16_t ControlWord_PreEnbaleTorque = 0xbf;
static const uint16_t ControlWord_EnbaleTorque = 0x12f;

//GENERAL SETUP PARAMETERS
static const uint32_t MaxAcceleration = 10000; //4294967295 max value in specs
static const uint32_t MaxMotorSpeed = 7140; //50000 max value in specs 7140 (50000/7)
static const uint32_t NominalCurrent = 8000; //8000mA which is the maximal nominal current allowed
static const uint32_t MaximumCurrent = 12000; //12000mA which is the maximal current allowed
static const uint8_t PolePairs = 7; //number of pole pairs of the motor
static const uint16_t ThermalConstant = 245; //thermal winding coef to 245 unit 0.1sec
static const uint32_t TorqueConstant = 46100; //motor const NM
static const uint32_t EncoderResolution = 512; //counts/rev
static const uint16_t EncoderType = 1;
static const uint16_t HallSensorType = 0;
static const uint32_t QuickStopDecel = 4000000000; //4294967295 max value in specs
static const uint32_t ProfileAcceleration = 4000000000; //4294967295 max value in specs
static const uint32_t ProfileDeceleration = 4000000000; //4294967295 max value in specs
static const int32_t MinPositionRangeLimit = -20000000;
static const int32_t MaxPositionRangeLimit = 20000000;
static const uint8_t ActivatePositionLimit = 0;


//POSITION CONTROL PARAMETERS
static const uint8_t PositionProfileMode = 1;
static const uint16_t PositionProfileLinearRamp = 0;
static const uint32_t PositionProfileVelocity = 4000;
static const uint32_t PositionProfileMaxVelocity = 5000;//10000; // dummy value 512*(7390/60) counts/sec 50000max value in specs
static const int32_t PositionInitial = 0; //we initialize the position to 0 value
static const int32_t PositionMinimumValue = -2147483640; //2147483648 this is max value in firmwarespec
static const int32_t PositionMaximumValue = 2147483640; //2147483648


//Position Gains
static const uint32_t PositionP_Gain = 7208172; //uA/rad
static const uint32_t PositionI_Gain = 89522571; //uA/rad*sec
static const uint32_t PositionD_Gain = 48366; //uA*s/rad

//TORQUE CONTROL PARAMETERS
static const uint8_t TorqueProfileMode = 10; //4
static const int16_t TorqueOffset = 0;
static const int16_t TorqueInitial = 0;

//Torque Gains
static const uint32_t TorqueP_Gain = 3646258; //uV/A 1830480
static const uint32_t TorqueI_Gain = 4565174; //uV/A*sec

/* Old torque parameters
static const uint16_t TorqueProfileLinearRamp = 0;
static const uint32_t TorqueProfileSlope = 100000; // 1/(2.34/60)*1000 torque in units per thousand of rated torque per second
static const uint32_t TorqueProfileMotorRateTorque = 137; // mNm
static const uint32_t TorqueProfileMotorRateCurrent = 2870; // mA
static const uint16_t TorqueProfileMotorMaxCurrent = 6620; // 19A --> 662% of Rate Current
static const uint16_t TorqueProfileMotorMaxTorque =  6620; // factor same as above
static const uint16_t TorqueProfileTorqueDemandValue =  285;
*/





void CANOpenOnFreeTxBuffer(uint32_t BufferIdx);
void CANOpenOnFullRxBuffer(uint32_t BufferIdx);

/* Setup */
void CANOpenInit(void);
void CANOpenNMTandSYNC(CANOpenMsgType MsgType);
void CANOpenControlModeFct(CANOpenControlMode controlmode, uint8_t CANnodeID);
void CANOpenEnableMotor(CANOpenControlMode controlmode, uint8_t CANnodeID);
void CANOpenPDOSetup(uint8_t CANnodeID);

/* Msg Assembly */
void CANOpenFrameAssembler(CANOpenMsgType MsgType, uint8_t MsgLength, uint32_t MsgID, uint8_t subindex, uint32_t data, uint8_t CANnodeID);
uint32_t CANOpenPDOSetupAssembler(uint8_t sizeParameter, uint8_t subIdx, uint16 objectIdx);

/* Read values */
void CANOpenReadReply(int32_t *EncoderCountsRead, int32_t *MotorCurrentRead, uint32_t CANnodeID); //uint16_t *temp_amp, removed

/* Send PDOs */
uint16_t CANOpenPDOControlWord(uint8_t significantBit);
void CANOpenPDOSetPosition(int32_t counts, uint8_t CANnodeID);
void CANOpenPDOSetVelocity(uint32_t velocity, uint8_t CANnodeID);
void CANOpenSDOSetTorque(int16_t targettorque, uint8_t CANnodeID);
void CANOpenPDOSetTorque(int16_t targettorque, uint8_t CANnodeID);

#endif /* CANOpen_H_ */
