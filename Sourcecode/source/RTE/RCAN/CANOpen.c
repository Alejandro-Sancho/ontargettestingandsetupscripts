/*
 * CANOpen.c
 *
 *  Created on: 15.04.2016
 *      Author: Kai Schmidt
 *      edit: Alejandro Sancho January 2018
 */

#include "CANOpen.h"
#include "SEGGER_RTT.h"

uint16_t CANOpenInitFlag = ERR_BUSY;

void CANOpenOnFreeTxBuffer(uint32_t BufferIdx) { /* buffer is empty after successful transmission */
	/* But IDX back in buffer to show it is free*/
//	PRINTF(" TX MBIdx: %i \r\n", BufferIdx);
	if(BufferIdx!=CANbus_TXBuffer0_Idx){ /* ignore dedicated NMT and SYNC buffer*/
		RNG4_Put(BufferIdx);
	}
}


void CANOpenOnFullRxBuffer(uint32_t BufferIdx) { /* buffer is full after successful reception */
	uint8_t print_buf[128];
	print_buf[0] = '\0';

	LDD_CAN_TFrame CANbus_frame_RX;
	RNG1_BufSizeType ElementsBufferRXIDX;
	LDD_CAN_TMBIndex CANbus_RXBuffer;
	uint8_t dataframe[8];
	CANbus_frame_RX.Data = dataframe;

	/* Put Message Buffer Index in ring buffer */
	if (BufferIdx > 7) {
		RNG2_Put(BufferIdx);
		ElementsBufferRXIDX = RNG2_NofElements();
	}
	else {
		RNG1_Put(BufferIdx);
		ElementsBufferRXIDX = RNG1_NofElements();
	}

//	PRINTF(" RX MBIdx: %i \r\n", BufferIdx);

	if(CANOpenInitFlag == ERR_BUSY){
//		ElementsBufferRXIDX = RNG1_NofElements();
		if(ElementsBufferRXIDX != 0 ){
			if (BufferIdx > 7) {
				RNG2_Get(&CANbus_RXBuffer);
			}
			else {
				RNG1_Get(&CANbus_RXBuffer);
			}
			CANbus_ReadFrame(CANbus_RXBuffer, &CANbus_frame_RX);
			UTIL1_strcatNum32Hex(print_buf, sizeof(print_buf), CANbus_frame_RX.MessageID);
			UTIL1_chcat(print_buf, sizeof(print_buf), ' ');
			UTIL1_strcat(print_buf, sizeof(print_buf), (unsigned char*)"Data: ");
			UTIL1_chcat(print_buf, sizeof(print_buf), ' ');
			/* print bytes */
			for(uint8_t i=0; i<CANbus_frame_RX.Length; i++) {
				UTIL1_strcatNum8Hex(print_buf, sizeof(print_buf), dataframe[i]);
				UTIL1_chcat(print_buf, sizeof(print_buf), ' ');
			}
			UTIL1_strcat(print_buf, sizeof(print_buf), (unsigned char*)"\r\n");
//			SEGGER_SYSVIEW_PrintfTarget((unsigned char*)print_buf);
			SEGGER_RTT_WriteString(0, print_buf);
		}
	}


	FLEXCAN_ClearMbStatusFlags(CAN0, 1 << BufferIdx);
}


void CANOpenFrameAssembler(CANOpenMsgType MsgType, uint8_t MsgLength, uint32_t MsgID, uint8_t subindex, uint32_t data, uint8_t CANnodeID) {
	LDD_CAN_TFrame CANbus_frame_TX;
	LDD_CAN_TMBIndex CANbus_TXBuffer;
	LDD_CAN_TMessageID CANID_TX;
	uint16_t objectindex;
	uint8_t dataframe[8];

	CANID_TX = 0x600 | CANnodeID;
//	PRINTF("%i \r\n", CANID_TX);

	if(MsgType == CANOpen_SDO) {
		for(uint8_t i = 0; i < 8; i++) { /* Init data frame */
			dataframe[i] = 0x00;
		}
		CANbus_frame_TX.Data = dataframe;

		/* MsgID is used for the object index since the controller will always have the same SDO ID */
		CANbus_frame_TX.MessageID = CANID_TX; /* SDO reply will be 0x581 || 0x582 */
		CANbus_frame_TX.FrameType = 0;   /* data frame */
		CANbus_frame_TX.Length = 0x8; /* SDOs are always 8 bytes long */
		/* Data frame
		 * MsgLength is used for the length of the actual SDO message (max 4 bytes)*/
		objectindex = (uint16_t)MsgID;
		if(MsgLength == 0) { /* SDO read request, all 4 data fields must be zero */
			CANbus_Data8Assembler(0x40, 0, &CANbus_frame_TX); /* This will send an read request, data must be 0 */
		}
		else if(MsgLength == 1) {
			CANbus_Data8Assembler(0x2f, 0, &CANbus_frame_TX);
			/* SDO data bytes */
			CANbus_Data8Assembler((uint8_t)data, 4, &CANbus_frame_TX);
		}
		else if(MsgLength == 2) {
			CANbus_Data8Assembler(0x2b, 0, &CANbus_frame_TX);
			/* SDO data bytes */
			CANbus_Data16Assembler((uint16_t)data, 4, &CANbus_frame_TX);
		}
		else if(MsgLength == 4) {
			CANbus_Data8Assembler(0x23, 0, &CANbus_frame_TX);
			/* SDO data bytes */
			CANbus_Data32Assembler(data, 4, &CANbus_frame_TX);
		}
		CANbus_Data16Assembler(objectindex, 1, &CANbus_frame_TX);
		CANbus_Data8Assembler(subindex, 3, &CANbus_frame_TX);
	}
	else if(MsgType == CANOpen_PDO) { /* subindex is used for message start */
		for(uint8_t i = 0; i < MsgLength; i++) { /* Init data frame */
			dataframe[i] = 0x00;
		}
		CANbus_frame_TX.Data = dataframe;

		CANbus_frame_TX.MessageID = MsgID;
		CANbus_frame_TX.FrameType = 0;   /* data frame */
		CANbus_frame_TX.Length = MsgLength;
		if(MsgLength == 1) {
			CANbus_Data8Assembler(data, subindex, &CANbus_frame_TX);
		}
		else if(MsgLength == 2) {
			CANbus_Data16Assembler((uint16_t)data, subindex, &CANbus_frame_TX);
		}
		else if(MsgLength == 4) {
			CANbus_Data32Assembler((uint32_t)data, subindex, &CANbus_frame_TX);
		}
	}
	/* take ID from buffer */
	RNG4_Get(&CANbus_TXBuffer);
	CANbus_SendFrame(CANbus_TXBuffer, &CANbus_frame_TX);
}


void CANOpenInit(void) { /* CANOpen_Position or CANOpen_Torque */

//	LDD_CAN_TFrame CANbus_frame_TX;
	RNG1_Init(); /* Init receive message buffer for BufferIDX, ID 1 */
	RNG2_Init(); /* Init receive message buffer for BufferIDX, ID 2 */
	RNG4_Init(); /* Init transmit message buffer for BufferIDX */
	for(uint8_t i=1; i<4; i++) { /* show that all transmit buffers are ready; CANbus_TXBuffer0_Idx - CANbus_TXBuffer7_Idx */
		RNG4_Put(i);
	}
	/* Init CANbus */
	CANbus_Init();
	/* activate CANOpen - NMT Message */
	CANOpenNMTandSYNC(CANOpen_NMT);
	for (uint8_t CANID = 1; CANID < 3; CANID++) {

		/* Control Mode Setup */
		CANOpenControlModeFct(CANOpen_Position, CANID); /* either position or torque control */ //CANOpen_Torque //CANOpen_Position also change in motorENABLE!!!!

		/* PDO Setup */
		CANOpenPDOSetup(CANID);
		/* Enable SYNC consumer */
		//CANOpenFrameAssembler(CANOpen_SDO, 4, 0x1005, 0x00, 0x80, CANID); /* enable SNYC consumer */
		//WAIT1_Waitus(300);
		/* Change RX Buffer message IDs to match PDOs */
	}

	for (uint8_t CANID = 1; CANID < 3; CANID++) {
	/* Enable Motor */
	CANOpenEnableMotor(CANOpen_Torque, CANID); /* Enables the motor for both CAN node ID 1 and 2 */
	};

	//sets NMT to operational state
	CANOpenNMTandSYNC(CANOpen_NMTOP);
	CANOpenInitFlag = ERR_OK;

}


void CANOpenNMTandSYNC(CANOpenMsgType MsgType) { /* function has dedicated message buffer */
	LDD_CAN_TFrame CANbus_frame_TX_NMTandSYNC;
	if(MsgType == CANOpen_NMT) {
		uint8_t dataframe[2];
		dataframe[0] = 0x80;
		dataframe[1] = 0x00;
		CANbus_frame_TX_NMTandSYNC.MessageID = 0x00; //0x00
		CANbus_frame_TX_NMTandSYNC.FrameType = 0;   /* data frame */
		CANbus_frame_TX_NMTandSYNC.Data = dataframe;
		CANbus_frame_TX_NMTandSYNC.Length = 0x02;
		CANbus_SendFrame(CANbus_TXBuffer0_Idx, &CANbus_frame_TX_NMTandSYNC);
	}
	else if(MsgType == CANOpen_SYNC) {
		CANbus_frame_TX_NMTandSYNC.MessageID = 0x80;
		CANbus_frame_TX_NMTandSYNC.FrameType = 0;   /* data frame */
		CANbus_frame_TX_NMTandSYNC.Length = 0x00;
		CANbus_SendFrame(CANbus_TXBuffer0_Idx, &CANbus_frame_TX_NMTandSYNC);
	}
	else {
		uint8_t dataframe[2];
				dataframe[0] = 0x01;
				dataframe[1] = 0x00;
				CANbus_frame_TX_NMTandSYNC.MessageID = 0x00; //0x00
				CANbus_frame_TX_NMTandSYNC.FrameType = 0;   /* data frame */
				CANbus_frame_TX_NMTandSYNC.Data = dataframe;
				CANbus_frame_TX_NMTandSYNC.Length = 0x02;
				CANbus_SendFrame(CANbus_TXBuffer0_Idx, &CANbus_frame_TX_NMTandSYNC);
	}
}


void CANOpenControlModeFct(CANOpenControlMode controlmode, uint8_t CANnodeID) {

	//Common parameters independent of mode
	CANOpenFrameAssembler(CANOpen_SDO, 4, 0x60C5, 0x00, MaxAcceleration, CANnodeID);
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 4, 0x6080, 0x00, MaxMotorSpeed, CANnodeID);
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 4 , 0x3001, 0x01, NominalCurrent, CANnodeID);
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 4 , 0x3001, 0x02, MaximumCurrent, CANnodeID);
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 1 , 0x3001, 0x03, PolePairs, CANnodeID);
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 2 , 0x3001, 0x04, ThermalConstant, CANnodeID);
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 4 , 0x3001, 0x05, TorqueConstant, CANnodeID);
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 4 , 0x3010, 0x01, EncoderResolution, CANnodeID);
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 2 , 0x3010, 0x02, EncoderType, CANnodeID);
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 2 , 0x301A, 0x01, HallSensorType, CANnodeID);
	WAIT1_Waitus(1000);
    CANOpenFrameAssembler(CANOpen_SDO, 4, 0x6085, 0x00, MaxAcceleration, CANnodeID);
    WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 4, 0x6083, 0x00, MaxAcceleration, CANnodeID);
    WAIT1_Waitus(1000);
    CANOpenFrameAssembler(CANOpen_SDO, 4, 0x6084, 0x00, MaxAcceleration, CANnodeID);
    WAIT1_Waitus(1000);
    CANOpenFrameAssembler(CANOpen_SDO, 4, 0x60A9, 0x00, 0x00B44700, CANnodeID); // revolutions per minute
    WAIT1_Waitus(1000);




	if(controlmode == CANOpen_Position) {
		CANOpenFrameAssembler(CANOpen_SDO, 1, 0x6060, 0x00, PositionProfileMode, CANnodeID);
		WAIT1_Waitus(1000); /* enough time for handshake */
		CANOpenFrameAssembler(CANOpen_SDO, 2, 0x6086, 0x00, PositionProfileLinearRamp, CANnodeID);
		WAIT1_Waitus(1000);
		CANOpenFrameAssembler(CANOpen_SDO, 4, 0x607f, 0x00, PositionProfileMaxVelocity, CANnodeID);
		WAIT1_Waitus(1000);
		CANOpenFrameAssembler(CANOpen_SDO, 4, 0x6081, 0x00, PositionProfileVelocity, CANnodeID);  //init speed
		WAIT1_Waitus(1000);
		CANOpenFrameAssembler(CANOpen_SDO, 4, 0x607A, 0x00, PositionInitial, CANnodeID);
	    WAIT1_Waitus(1000);
		CANOpenFrameAssembler(CANOpen_SDO, 4, 0x607D, 0x01, PositionMinimumValue, CANnodeID);
	    WAIT1_Waitus(1000);
		CANOpenFrameAssembler(CANOpen_SDO, 4, 0x607D, 0x02, PositionMaximumValue, CANnodeID);
	    WAIT1_Waitus(1000);


	    CANOpenFrameAssembler(CANOpen_SDO, 4, 0x30A1, 0x01, PositionP_Gain, CANnodeID);
	    WAIT1_Waitus(1000);
	    CANOpenFrameAssembler(CANOpen_SDO, 4, 0x30A1, 0x02, PositionI_Gain, CANnodeID);
	    WAIT1_Waitus(1000);
	    CANOpenFrameAssembler(CANOpen_SDO, 4, 0x30A1, 0x03, PositionD_Gain, CANnodeID);
	    WAIT1_Waitus(1000);


	}

	else if(controlmode == CANOpen_Torque) {
		CANOpenFrameAssembler(CANOpen_SDO, 1, 0x6060, 0x00, TorqueProfileMode, CANnodeID);
		WAIT1_Waitus(1000);
//		CANOpenFrameAssembler(CANOpen_SDO, 2, 0x6040, 0x00, ControlWord_PreEnbaleTorque, CANnodeID); /* change control word to activate torque slope */
//		WAIT1_Waitus(1000);
		CANOpenFrameAssembler(CANOpen_SDO, 2, 0x60B2, 0x00, TorqueOffset, CANnodeID);
		WAIT1_Waitus(1000);
		CANOpenFrameAssembler(CANOpen_SDO, 2, 0x6071, 0x00, TorqueInitial, CANnodeID);
		WAIT1_Waitus(1000);
//		CANOpenFrameAssembler(CANOpen_SDO, 4, 0x607f, 0x00, PositionProfileMaxVelocity, CANnodeID);
//		WAIT1_Waitus(1000);
//		CANOpenFrameAssembler(CANOpen_SDO, 4, 0x6081, 0x00, PositionProfileVelocity, CANnodeID);  //init speed
//		WAIT1_Waitus(1000);


		CANOpenFrameAssembler(CANOpen_SDO, 4, 0x30A0, 0x01, TorqueP_Gain, CANnodeID);
	    WAIT1_Waitus(1000);
	    CANOpenFrameAssembler(CANOpen_SDO, 4, 0x30A0, 0x02, TorqueI_Gain, CANnodeID);
	    WAIT1_Waitus(1000);

	}
}


void CANOpenEnableMotor(CANOpenControlMode controlmode, uint8_t CANnodeID)
{

	if(controlmode == CANOpen_Position)
	{
	    CANOpenFrameAssembler(CANOpen_SDO, 2, 0x6040, 0, 0x26, CANnodeID);
	    WAIT1_Waitus(1000);
	    //Switch on disabled --> ready to switch on
	    CANOpenFrameAssembler(CANOpen_SDO, 2, 0x6040, 0, 0x27, CANnodeID);
	    WAIT1_Waitus(1000);
	    CANOpenFrameAssembler(CANOpen_SDO, 2, 0x6040, 0, 0x2F, CANnodeID);
	    WAIT1_Waitus(1000);
	}

	else if(controlmode == CANOpen_Torque)
	{
	    CANOpenFrameAssembler(CANOpen_SDO, 2, 0x6040, 0, 0x6, CANnodeID);
	    WAIT1_Waitus(1000);
	    //Switch on disabled --> ready to switch on
	    CANOpenFrameAssembler(CANOpen_SDO, 2, 0x6040, 0, 0x7, CANnodeID);
	    WAIT1_Waitus(1000);
	    CANOpenFrameAssembler(CANOpen_SDO, 2, 0x6040, 0, 0xF, CANnodeID);
	    WAIT1_Waitus(1000);
	}

}


void CANOpenPDOSetup(uint8_t CANnodeID) {
	uint32_t PDOSetupData;
	/* RPDO setup */
	/* Map Control Word to RPDO1 */
	CANOpenFrameAssembler(CANOpen_SDO, 4, 0x1400, 0x01, (0x80000200 | CANnodeID), CANnodeID); /* Set COB-ID */
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 1, 0x1400, 0x02, 255, CANnodeID); /* asynchronous mode */
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 1, 0x1600, 0x00, 0x00, CANnodeID); /* disable Control Word RPDO */
	WAIT1_Waitus(1000);
	PDOSetupData = CANOpenPDOSetupAssembler(0x10, 0x00, 0x6040); /* communication parameters */
	CANOpenFrameAssembler(CANOpen_SDO, 4, 0x1600, 0x01, PDOSetupData, CANnodeID); /* send communication parameters */
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 1, 0x1600, 0x00, 0x01 , CANnodeID); /* enable Control Word RPDO */
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 4, 0x1400, 0x01, (0x00000200 | CANnodeID) , CANnodeID); /* Set correct COB-ID */
	WAIT1_Waitus(1000);

	/* Map Target Position to RPDO2 */
	CANOpenFrameAssembler(CANOpen_SDO, 4, 0x1401, 0x01, (0x80000300 | CANnodeID), CANnodeID); /* Set COB-ID */
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 1, 0x1401, 0x02, 255, CANnodeID); /* asynchronous mode */
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 1, 0x1601, 0x00, 0x00, CANnodeID); /* disable RPDO */
	WAIT1_Waitus(1000);
	PDOSetupData = CANOpenPDOSetupAssembler(0x20, 0x00, 0x607A); /* communication parameters */
	CANOpenFrameAssembler(CANOpen_SDO, 4, 0x1601, 0x01, PDOSetupData, CANnodeID); /* send communication parameters */
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 1, 0x1601, 0x00, 0x01, CANnodeID); /* enable Control Word RPDO */
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 4, 0x1401, 0x01, (0x00000300 | CANnodeID), CANnodeID); /* Set correct COB-ID */
	WAIT1_Waitus(1000);

	/* Map Velocity to RPDO3 */
	CANOpenFrameAssembler(CANOpen_SDO, 4, 0x1402, 0x01, (0x80000400 | CANnodeID) , CANnodeID); /* Set COB-ID */
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 1, 0x1402, 0x02, 255, CANnodeID); /* asynchronous mode */
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 1, 0x1602, 0x00, 0x00, CANnodeID); /* disable  RPDO */
	WAIT1_Waitus(1000);
	PDOSetupData = CANOpenPDOSetupAssembler(0x20, 0x00, 0x6081); /* communication parameters */
	CANOpenFrameAssembler(CANOpen_SDO, 4, 0x1602, 0x01, PDOSetupData, CANnodeID); /* send communication parameters */
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 1, 0x1602, 0x00, 0x01, CANnodeID); /* enable  RPDO */
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 4, 0x1402, 0x01, (0x00000400 | CANnodeID) , CANnodeID); /* Set correct COB-ID */
	WAIT1_Waitus(1000);

	/* Map Target Torque to RPDO4 */
	CANOpenFrameAssembler(CANOpen_SDO, 4, 0x1403, 0x01, (0x80000500 | CANnodeID), CANnodeID); /* Set COB-ID */
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 1, 0x1403, 0x02, 255, CANnodeID); /* asynchronous mode */
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 1, 0x1603, 0x00, 0x00, CANnodeID); /* disable RPDO */
	WAIT1_Waitus(1000);
	PDOSetupData = CANOpenPDOSetupAssembler(0x10, 0x00, 0x6071); /* communication parameters */
	CANOpenFrameAssembler(CANOpen_SDO, 4, 0x1603, 0x01, PDOSetupData, CANnodeID); /* send communication parameters */
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 1, 0x1603, 0x00, 0x01, CANnodeID); /* enable RPDO */
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 4, 0x1403, 0x01, (0x00000500 | CANnodeID) , CANnodeID); /* Set correct COB-ID */
	WAIT1_Waitus(1000);

	/* TPDO setup */
	/* Disable Control Word TPDO set by manufacturer */
	//CANOpenFrameAssembler(CANOpen_SDO, 1, 0x1A00, 0x00, 0); /* Disable TPDO */
	WAIT1_Waitus(1000);
	/* Map actual and commanded position to TPDO1 */
	CANOpenFrameAssembler(CANOpen_SDO, 4, 0x1801, 0x01, (0x80000180 | CANnodeID), CANnodeID); /* Set COB-ID */
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 1, 0x1801, 0x02, 1, CANnodeID); /* send TPDO every SYNC message */
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 1, 0x1A01, 0x00, 0, CANnodeID); /* Disable TPDO */
	WAIT1_Waitus(1000);
	PDOSetupData = CANOpenPDOSetupAssembler(0x20, 0x00, 0x6064); /* map actual position 0x6064 */
	CANOpenFrameAssembler(CANOpen_SDO, 4, 0x1A01, 0x01, PDOSetupData, CANnodeID); // send mapping
	WAIT1_Waitus(1000);
	PDOSetupData = CANOpenPDOSetupAssembler(0x20, 0x01, 0x30D1);  //mapping current actual value 0x30D1
	CANOpenFrameAssembler(CANOpen_SDO, 4, 0x1A01, 0x02, PDOSetupData, CANnodeID); // send mapping
	WAIT1_Waitus(1000);
//	PDOSetupData = CANOpenPDOSetupAssembler(0x20, 0x01, 0x30D3);  //mapping velocity object TI[1]
//	CANOpenFrameAssembler(CANOpen_SDO, 4, 0x1A01, 0x03, PDOSetupData, CANnodeID); // send mapping
//	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 1, 0x1A01, 0x00, 0x02, CANnodeID); /* Enable TPDO */
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 4, 0x1801, 0x01, (0x00000180 | CANnodeID) , CANnodeID); /* Set correct COB-ID */
	WAIT1_Waitus(1000);
}


uint32_t CANOpenPDOSetupAssembler(uint8_t sizeParameter, uint8_t subIdx, uint16 objectIdx) {
	uint32 data;
	uint8_t dataframe[4];

	dataframe[0] = sizeParameter;
	dataframe[1] = subIdx;
	dataframe[2] = (uint8_t)(objectIdx  & 0xff);
	dataframe[3] = (uint8_t)((objectIdx >> 8)  & 0xff);

	data = ((uint32_t )dataframe[3]) << 24;
	data |= ((uint32_t )dataframe[2]) << 16;
	data |= ((uint32_t )dataframe[1]) << 8;
	data |= dataframe[0];

	return data;
}


void CANOpenReadReply(int32_t *EncoderCountsRead, int32_t *MotorCurrentRead, uint32_t CANnodeID) { //uint16_t *temp_amp, removed

	LDD_CAN_TFrame CANbus_frame_RX;
	RNG1_BufSizeType ElementsBufferRXIDX = 0;
	LDD_CAN_TMBIndex CANbus_RXBuffer;
	uint8_t dataframe[12];

	CANbus_frame_RX.Data = dataframe;

	if (CANnodeID == 1){
		ElementsBufferRXIDX = RNG1_NofElements();
//		PRINTF("BUF %i \r\n", ElementsBufferRXIDX);
	}
	else if (CANnodeID == 2){
		ElementsBufferRXIDX = RNG2_NofElements();
	}

	if(ElementsBufferRXIDX != 0 ){
		uint8_t print_buf[128];
		print_buf[0] = '\0';

		if (CANnodeID == 1){
			RNG1_Get(&CANbus_RXBuffer);
		}
		else if (CANnodeID == 2){
			RNG2_Get(&CANbus_RXBuffer);
		}

		CANbus_ReadFrame(CANbus_RXBuffer, &CANbus_frame_RX);
		if(CANbus_frame_RX.MessageID == 0x181 || CANbus_frame_RX.MessageID == 0x182) {
			/* assemble two 32-bit numbers */
//			*temp_amp = (dataframe[7] << 8) | dataframe[6];

			*MotorCurrentRead = ((int32_t)dataframe[7]) << 24;
			*MotorCurrentRead |= ((int32_t)dataframe[6]) << 16;
			*MotorCurrentRead |= ((int32_t)dataframe[5]) << 8;
			*MotorCurrentRead |= dataframe[4];
			*EncoderCountsRead = ((int32_t)dataframe[3]) << 24;
			*EncoderCountsRead |= ((int32_t)dataframe[2]) << 16;
			*EncoderCountsRead |= ((int32_t)dataframe[1]) << 8;
			*EncoderCountsRead |= dataframe[0];


		}


		else{ /* catch errors */
			UTIL1_strcatNum32Hex(print_buf, sizeof(print_buf), CANbus_frame_RX.MessageID);
			UTIL1_chcat(print_buf, sizeof(print_buf), ' ');
			UTIL1_strcat(print_buf, sizeof(print_buf), (unsigned char*)"Data: ");
			UTIL1_chcat(print_buf, sizeof(print_buf), ' ');
			/* print bytes */
			for(uint8_t i=0; i<CANbus_frame_RX.Length; i++) {
				UTIL1_strcatNum8Hex(print_buf, sizeof(print_buf), dataframe[i]);
				UTIL1_chcat(print_buf, sizeof(print_buf), ' ');
			}
			UTIL1_strcat(print_buf, sizeof(print_buf), (unsigned char*)"\r\n");
     		//SEGGER_SYSVIEW_PrintfTarget("%s",print_buf);
//			PRINTF("%s",print_buf);
			SEGGER_RTT_WriteString(0,print_buf);
		}
	}
}


uint16_t CANOpenPDOControlWord(uint8_t significantBit) {
	uint16 data = 0;
	uint8_t dataframe[2];

	dataframe[0] = significantBit;
	dataframe[1] = 0x00;

//	data |= ((uint32_t )dataframe[1]) << 8;
//	data |= dataframe[0];

	data = (dataframe[1] << 8) | dataframe[0];

	return data;
}


void CANOpenPDOSetPosition(int32_t counts, uint8_t CANnodeID) {
/*	CANOpenFrameAssembler(CANOpen_SDO, 4, 0x607A, 0, counts, CANnodeID);
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 2, 0x6040, 0, 0x002f, CANnodeID);
	WAIT1_Waitus(1000);
	CANOpenFrameAssembler(CANOpen_SDO, 2, 0x6040, 0, 0x003f, CANnodeID);
	WAIT1_Waitus(1000);*/


	//uint16_t trigger;
	CANOpenFrameAssembler(CANOpen_PDO, 4, (0x300 | CANnodeID), 0x00, counts, CANnodeID);  //Load next position
	WAIT1_Waitus(1000);  //85 required delay

	//trigger = CANOpenPDOControlWord(0x3f);  //Control Word bit 4 = 1
	CANOpenFrameAssembler(CANOpen_PDO, 2, (0x200 | CANnodeID), 0x00, 0x2f, CANnodeID);  //Trigger next position
	WAIT1_Waitus(1000);  //180 required delay
	CANOpenFrameAssembler(CANOpen_PDO, 2, (0x200 | CANnodeID), 0x00, 0x3f, CANnodeID);  //Trigger next position */
	WAIT1_Waitus(1000);


//	CANOpenFrameAssembler(CANOpen_SDO, 4, 0x607A, 0x00, counts, CANnodeID);
//	WAIT1_Waitus(1000);
//	CANOpenFrameAssembler(CANOpen_SDO, 2, 0x6040, 0x00, 0x2f, CANnodeID);
//	WAIT1_Waitus(1000);
//	CANOpenFrameAssembler(CANOpen_SDO, 2, 0x6040, 0x00, 0x3f, CANnodeID);
//	WAIT1_Waitus(1000);
//	CANOpenFrameAssembler(CANOpen_SDO, 0, 0x6041, 0x00, 0x00, CANnodeID);
//	WAIT1_Waitus(1000);
//	CANOpenFrameAssembler(CANOpen_SDO, 0, 0x6062, 0x00, 0x00, CANnodeID);
//	WAIT1_Waitus(1000);
}



void CANOpenPDOSetVelocity(uint32_t velocity, uint8_t CANnodeID) {
	CANOpenFrameAssembler(CANOpen_PDO, 4, (0x400 | CANnodeID), 0x00, velocity, CANnodeID);
}

void CANOpenSDOSetTorque(int16_t targettorque, uint8_t CANnodeID) {

	CANOpenFrameAssembler(CANOpen_SDO, 2, 0x6071, 0x00, targettorque, CANnodeID);

}


void CANOpenPDOSetTorque(int16_t targettorque, uint8_t CANnodeID) {

	CANOpenFrameAssembler(CANOpen_PDO, 2, (0x500 | CANnodeID), 0x00, targettorque, CANnodeID); /* Load next position */

}
