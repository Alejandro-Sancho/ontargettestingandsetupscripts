///***********************************************************************
// * $Id$
// * Copyright Z�hlke Engineering AG
// * Project: MyoSwiss
// * Documentation: RTIM
// ***********************************************************************/
//
///** @addtogroup RTIM
// * @{
// * @file
// * This source file contains the definition of the functions of
// * RTIM Timer module of the RTE layer.
// */
//
///***********************************************************************/
///* INCLUDES                                                            */
///***********************************************************************/
//
//#include "RTIM.h"
//#include "types.h"
//#include "fsl_debug_console.h"
//#include "fsl_lptmr.h"
//#include "fsl_gpio.h"
//
//#include "FreeRTOS.h"
//#include "task.h"
//#include "types.h"
//#include "Platform.h"
///***********************************************************************/
///* DEFINES                                                             */
///***********************************************************************/
//#define DEMO_LPTMR_BASE LPTMR0
//
//#define DEMO_LPTMR_BASE LPTMR0
//#define DEMO_LPTMR_IRQn LPTMR0_IRQn
///* Get source clock for LPTMR driver */
//#define LPTMR_SOURCE_CLOCK CLOCK_GetFreq(kCLOCK_LpoClk)
//#define TIMER_HANDLER LPTMR0_IRQHandler
//
///***********************************************************************/
///* TYPES                                                               */
///***********************************************************************/
//
///***********************************************************************/
///* GLOBAL CONSTANTS                                                    */
///***********************************************************************/
//
///***********************************************************************/
///* LOCAL CONSTANTS                                                     */
///***********************************************************************/
//
///***********************************************************************/
///* GLOBAL VARIABLES                                                    */
///***********************************************************************/
//
///***********************************************************************/
///* LOCAL VARIABLES                                                     */
///***********************************************************************/
//static lptmr_config_t timerConfig;
//
//static TaskHandle_t xHandle;
//
//static gpio_pin_config_t led_config = {
//		kGPIO_DigitalOutput, 0,
//};
///***********************************************************************/
///* LOCAL FUNCTION PROTOTYPES                                           */
///***********************************************************************/
//static void liveLED(void *pvParameters){
//	gpio_pin_config_t led_config = {
//			kGPIO_DigitalOutput, 0,
//	};
//	GPIO_PinInit(BOARD_LED_RED_GPIO, BOARD_LED_RED_GPIO_PIN, &led_config);
//	vTaskDelay(1000);
//
//	while(1){
//		GPIO_TogglePinsOutput(BOARD_LED_RED_GPIO, 1u << BOARD_LED_RED_GPIO_PIN);
//		//vTaskDelay(1000);
//		ulTaskNotifyTake( pdTRUE, portMAX_DELAY);
//	    GPIO_TogglePinsOutput(GPIOA,1u<< 27U);
//
//	}
//
//}
//
///***********************************************************************/
///* GLOBAL FUNCTIONS                                                    */
///***********************************************************************/
//
//GINIT_Return_e RTIM_Initialize(GINIT_InitLevel_e initLevel){
//
//	switch(initLevel){
//	case E_GINIT_InitLevel1:
//		LPTMR_GetDefaultConfig(&timerConfig);
//
//		GPIO_PinInit(GPIOA, 27U, &led_config);
//
//	    xTaskCreate(liveLED, "LiveLED", 90 + 256, NULL, 1, &xHandle);
//
//
//		break;
//	case E_GINIT_InitLevel2:
//		LPTMR_Init(LPTMR0_BASE,&timerConfig);
//	    LPTMR_SetTimerPeriod(DEMO_LPTMR_BASE, USEC_TO_COUNT(950, LPTMR_SOURCE_CLOCK));
//		LPTMR_EnableInterrupts(DEMO_LPTMR_BASE, kLPTMR_TimerInterruptEnable);
//		SetPriorityIRQ(DEMO_LPTMR_IRQn,configMAX_SYSCALL_INTERRUPT_PRIORITY - 1);
//		EnableIRQ(DEMO_LPTMR_IRQn);
//	    LPTMR_StartTimer(DEMO_LPTMR_BASE);
//
//		break;
//
//	}
//	return E_GINIT_ReturnSuccess;
//};
//
//Time_ms_t RTIM_GetCurrentTime(void){
//
//		return 1;
//
//};
//
//Time_ms_t RTIM_GetTimeDifference(Time_ms_t firstTime){
//
//		return 1;
//
//};
//
//void TIMER_HANDLER(void){
//	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
//    //vTaskNotifyGiveFromISR(xHandle,&xHigherPriorityTaskWoken);
//    GPIO_TogglePinsOutput(GPIOA,1u<< 27U);
//
//   // portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
//    LPTMR_ClearStatusFlags(DEMO_LPTMR_BASE, kLPTMR_TimerCompareFlag);
//
//}
//
//
///***********************************************************************/
///* LOCAL FUNCTIONS                                                     */
///***********************************************************************/
//
//
///** @} */
