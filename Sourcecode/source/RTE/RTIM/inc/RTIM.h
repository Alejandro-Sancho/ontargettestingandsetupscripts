/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation: RTIM
 ***********************************************************************/

/** @defgroup RTIM Timer module of the RTE layer
 * @{
 * @brief
 * The next blank line is mandatory!
 *
 * .
 * @file
 * This header file contains the declaration of the public
 * interface to RTIM of the RTE layer.
 */

#ifndef RTIM_H
#define RTIM_H

/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/

#include "GINIT.h"
#include "types.h"

/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL FUNCTION PROTOTYPES                                          */
/***********************************************************************/

/** Initialize the RDAC-Module
 *  @return E_GINIT_ReturnFailure if failed
 */
GINIT_Return_e RTIM_Initialize(GINIT_InitLevel_e initLevel);

/** Get the data in systick, this data will be used as a starting time to measure from
 * if we want to, for example, find the time difference between two time periods.
 *
 * @returns returns the time in ms since the start of the system.
 */
Time_ms_t RTIM_GetCurrentTime(void);


/** The initial time in sysTick is passed in and compared to current time. The function
 * calculates the difference in time.
 *
 * @returns the difference between the input time and current time in miliseconds.
 */
Time_ms_t RTIM_GetTimeSinceStart(Time_ms_t firstTime);




#endif /* RTIM_H */

/** @} */
