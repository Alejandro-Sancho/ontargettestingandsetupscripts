/***********************************************************************
 * $Id$
 * Copyright Zühlke Engineering AG
 * Project: MyoSwiss
 * Documentation: xxx
 ***********************************************************************/

/** @defgroup LABC Long Name of the Component
 * @{
 * @brief
 * The next blank line is mandatory!
 *
 * A bit more elaborated description focusing on the functions declared
 * here.
 * @file
 * This header file contains the declaration of the public or private
 * interface to HAL or RTE component or module LABC (long name).
 */

#ifndef ROS_H
#define ROS_H
/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/

/***********************************************************************/
/* DEFINES                                                             */
/*************d**********************************************************/

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/
typedef enum ROS_ReturnValue{
	ROS_ReturnValue_SUCCESS = 0,
	ROS_ReturnValue_FAIL 	= 1
}ROS_ReturnValue;

typedef void (*ROS_TaskFunction_t)( void * param);

/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/


/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/


/***********************************************************************/
/* GLOBAL FUNCTION PROTOTYPES                                          */
/***********************************************************************/

ROS_ReturnValue ROS_Initialize(void);

ROS_ReturnValue ROS_CreateTask(ROS_TaskFunction_t pvTaskCode);

#endif /* ROS_H */

/** @} */
