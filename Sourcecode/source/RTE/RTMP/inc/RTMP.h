/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation: xxx
 ***********************************************************************/

/** @defgroup RTMP
 * @{
 * @brief
 *
 * Device abstraction of the temperature sensors
 * @file
 * This header file contains the declaration of the public
 * interface to module RTMP (RTE_Temperature).
 */

#ifndef RTMP_H
#define RTMP_H
/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/
#include "GINIT.h"
#include "types.h"

/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/
typedef enum RTMP_ReturnStatus_e{
	E_RTMP_ReturnSuccess,
	E_RTMP_ReturnFailed,
	E_RTMP_DataNotValid
}RTMP_ReturnStatus_e;

typedef struct tag_RTMP_Temperatures_s{
	Temp_DegreeCelsius_t temperatureMotorLeft;
	Temp_DegreeCelsius_t temperatureMotorRight;
	RTMP_ReturnStatus_e status;
}RTMP_Temperatures_s;
/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/


/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/


/***********************************************************************/
/* GLOBAL FUNCTION PROTOTYPES                                          */
/***********************************************************************/

GINIT_Return_e RTMP_Initialize(GINIT_InitLevel_e initLevel);

RTMP_Temperatures_s RTMP_GetTemperatures(void);

#endif /* RTMP_H */

/** @} */
