/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation: RMOT
 ***********************************************************************/

/** @defgroup RMOT RTE_MOTOR
 * @{
 * @brief
 *
 * Device abstraction of the motor where you can read actual current and
 * set the force of the motor
 * @file
 * This header file contains the declaration of the public
 * interface to module RMOT (RTE_MOTOR).
 */

#ifndef RMOT_H
#define RMOT_H
/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/
#include "GINIT.h"
#include "types.h"
#include "RDAC.h"

/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/

/**
 * The possible status values of public functions.
 */
typedef enum tag_RMOT_ReturnStatus_e{
	E_RMOT_ReturnSuccess,
	E_RMOT_DataNotValid
}RMOT_ReturnStatus_e;

/**
 * struct that contains the forces for each leg.
 */
typedef struct tag_RMOT_MotorForces_s{
	Force_Newton_t motorForceRightSide;
	Force_Newton_t motorForceLeftSide;
}RMOT_MotorForces_s;

/**
 * Struct that contains the currents for each motor. The unit is mA
 */
typedef struct tag_RMOT_MotorCurrents_s{
	Current_mA_t motorCurrentRightSide;
	Current_mA_t motorCurrentLeftSide;
	RMOT_ReturnStatus_e status;
}RMOT_MotorCurrents_s;

/**
 * struct that contains encoders for each leg. unit is encoder counts
 */
typedef struct tag_RMOT_MotorEncoder_s{
	int32_t motorEncoderRightSide;
	int32_t motorEncoderLeftSide;
}RMOT_MotorEncoders_s;



/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/


/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/


/***********************************************************************/
/* GLOBAL FUNCTION PROTOTYPES                                          */
/***********************************************************************/

/** Initialize the RMOT-Module
 *  @param initLevel level of initialization
 *  @return E_GINIT_ReturnFailure if failed
 */
GINIT_Return_e RMOT_Initialize(GINIT_InitLevel_e initLevel);

/** Provides the current to motor. Converts force to current.
 *  @param forces forces in physical unit Newton
 *  @return E_RMOT_ReturnSuccess if successful or E_RMOT_ReturnFailed if failed
 */
RMOT_ReturnStatus_e RMOT_setForces(RMOT_MotorForces_s forces, RMOT_MotorEncoders_s calcEncoders);

/** Reads the actual current of each motor
 *  @return RMOT_MotorCurrents_s, flag status is set to E_RMOT_DataNotValid if data is not valid, otherwise to E_RMOT_ReturnSuccess
 */
RMOT_MotorCurrents_s RMOT_getMotorCurrents(void);

/** The function reads the actual encoder counts from the controller.
 * @param Parameter for the task, not used yet
 */
RMOT_MotorEncoders_s RMOT_getMotorEncoders(void);





#endif /* RMOT_H */

/** @} */
