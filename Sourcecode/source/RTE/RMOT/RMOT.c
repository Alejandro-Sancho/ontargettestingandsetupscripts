/***********************************************************************
 * $Id$
 * Copyright MyoSwiss
 * Project: MyoSuit
 * Documentation: RMOT
 ***********************************************************************/

/** @addtogroup RMOT
 * @{
 * @file
 * This source file contains the definition of the functions of
 * module RMOT (RTE_MOTOR).
 */

/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/

#include "RMOT.h"
#include "types.h"
#include "RCAN.h"
#include "RDAC.h"


// for debugging only
#include "SEGGER_RTT.h"
#include "UTIL1.h"


/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/


/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/

/**
 * struct that contains the kinematic values for both motors. This is used to store
 * local values during the kinematic calculations.
 */
typedef struct tag_RMOT_MotorKinematics_s{

	Double64_t angularVelocityRightSide;
	Double64_t angularVelocityLeftSide;
	Double64_t angularAccelerationRightSide;
	Double64_t angularAccelerationLeftSide;

}RMOT_MotorKinematics_s;

/**
 * struct that contains the torques for each motor.
 */
typedef struct tag_RMOT_MotorTorque_s{
	Double64_t torqueRightSide;
	Double64_t torqueLeftSide;
}RMOT_MotorTorque_s;


/**
 * struct that contains the torques for each motor.
 */
typedef struct tag_RMOT_FilterParameters_s{
	Freq_hz_t cutofFrequency;
	Freq_hz_t samplingRate;
	Double64_t xv[3];
	Double64_t yv[3];
	Double64_t ax[3];
	Double64_t by[3];
	Double64_t QcWarp;
	Double64_t QcRaw;
	Double64_t gain;
}RMOT_FilterParameters_s;





/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/

/***********************************************************************/
/* LOCAL CONSTANTS                                                     */
/***********************************************************************/

//Motor Data
//in one revolution the encoder provides 2048 values
static const Double64_t resolutionEncoder = 2048.0;

static const Double64_t motorConstant = 0.0000461; //unit=Nm/A  = Km*current=torque
static const Double64_t nominalCurrent = 8.0; //unit=amps     11.64 is the maximum current the motor can withstand.

static const Meter_m_t radiusOutputShaft = 0.0225;
static const Double64_t gearRatio = 43.0;
static const Double64_t inertiaTotal = 37.0; //[gcm^2]

//sets the maximum limit 1500=150% of 8Amps which is 12 amps and this is the maximum current we allow.
static const int32_t minCurrentAllowed = -1500;
static const int32_t maxCurrentAllowed = 1500;

// Values used to compensate for static friction. Unit: 1/1000 of max current
static const int32_t S_rightPositiveCurrentCompensation = 0; //60
static const int32_t S_rightNegativeCurrentCompensation = 0; //10
static const int32_t S_leftPositiveCurrentCompensation = 60;
static const int32_t S_leftNegativeCurrentCompensation = 10;
static const int32_t S_maximumCurrentCompensationatZero = 90;
static const int32_t S_speedCurrentChange=1000;

// inertia
Double64_t motorInertiaComponentLeft = 0.0;
Double64_t motorInertiaComponentRight = 0.0;

/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/
uint8_t msg_buf[100];

/***********************************************************************/
/* LOCAL VARIABLES                                                     */
/***********************************************************************/


static RMOT_MotorKinematics_s S_motorKinematics;
static RMOT_MotorTorque_s S_motorTorque;
static RCAN_MotorCurrentsPerThousand_s S_motorCurrentPerThousand;
static RMOT_MotorEncoders_s S_motorEncoders;
static RCAN_MotorData_s S_leftMotorData_s;
static RCAN_MotorData_s S_rightMotorData_s;
static RMOT_MotorCurrents_s S_motorCurrent;
static int32_t S_previousEncoderRight;
static int32_t S_previousEncoderLeft;
static Double64_t S_AngularVelocityRightPrev;
static Double64_t S_AngularVelocityLeftPrev;
static RMOT_FilterParameters_s ButterworthParameters;
static int8_t directionFlag;
static int8_t counter;
static int8_t flagPeak1;
static int8_t flagPeak2;
static Double64_t minimum;
static Double64_t maximum;
static Double64_t S_previousAccelerationLeft;
static int32_t burstValue;
static int8_t flagDirection;


/***********************************************************************/
/* LOCAL FUNCTION PROTOTYPES                                           */
/***********************************************************************/
/** Calculation of motor kinematics.
 *  @param input the encoder counts for each leg.
 *  @return a struct with the angular velocity and accelerations
 */
static RMOT_MotorKinematics_s calculateMotorKinematics(int32_t currentEncoderRight, int32_t currentEncoderLeft);

/** Calculate the torque required for the motor
 *  @param input the required forces from the calculations.
 *  @return the desired torque for each leg.
 */
static RMOT_MotorTorque_s calculateMotorTorque(Force_Newton_t forceRight, Force_Newton_t forceLeft);

/** Calculate the current required for the motor
 *  @param input the required torque struct from calculations
 *  @param input the kinematics from the motor to calculate friction
 *  @return a struct with the desired currents for each leg.
 */
static RCAN_MotorCurrentsPerThousand_s calculateMotorCurrent(RMOT_MotorTorque_s totalTorque, RMOT_MotorKinematics_s motorKinematics);

/** checks if the currents that are going to be sent to the motor are within a range.
 *  @param input a struct with two currents in per1000.
 *  @return status success if within range, error if else.
 */
static RMOT_ReturnStatus_e currentRangeCheck (RCAN_MotorCurrentsPerThousand_s motorCurrentPerThousand);

/** reads the data from the motor (current and encoders) and stores it in a struct.
 *  @param -
 *  @return -
 */
static void motorGetALLData (void);

/** filter init
 *  @param -
 *  @return -
 */
static void InitIIRFilter(void);

/** filter use
 *  @param -
 *  @return -
 */
static Double64_t ButterworthIIRFilter(Double64_t actualValue);

/** filter use
 *  @param -
 *  @return -
 */
static int32_t burstDetection(void);

/***********************************************************************/
/* GLOBAL FUNCTIONS                                                    */
/***********************************************************************/

GINIT_Return_e RMOT_Initialize(GINIT_InitLevel_e initLevel)
{
	GINIT_Return_e retVal;

	switch (initLevel)
	{
	case E_GINIT_InitLevel1:
		S_previousEncoderRight = 0;
		S_previousEncoderLeft = 0;
		S_AngularVelocityRightPrev = 0;
		S_AngularVelocityLeftPrev = 0;
		S_motorKinematics.angularAccelerationLeftSide = 0;
		S_motorKinematics.angularAccelerationRightSide = 0;
		S_motorKinematics.angularVelocityLeftSide = 0;
		S_motorKinematics.angularVelocityRightSide =0;
		S_motorTorque.torqueLeftSide = 0;
		S_motorTorque.torqueRightSide = 0;
		S_motorCurrentPerThousand.motorCurrentPerThousandLeftSide = 0;
		S_motorCurrentPerThousand.motorCurrentPerThousandRightSide = 0;
		S_motorEncoders.motorEncoderLeftSide = 0;
		S_motorEncoders.motorEncoderRightSide = 0;
		S_leftMotorData_s.motorCurrent = 0;
		S_leftMotorData_s.motorEncoder = 0;
		S_rightMotorData_s.motorCurrent = 0;
		S_rightMotorData_s.motorEncoder = 0;
		S_motorCurrent.motorCurrentLeftSide = 0;
		S_motorCurrent.motorCurrentRightSide = 0;
		retVal = E_GINIT_ReturnSuccess;
		flagPeak1 = 0;
		flagPeak2 = 0;
		S_previousAccelerationLeft = 0;
		minimum = 0;
		maximum = 0;
		burstValue  = 0;
		flagDirection = 0;


		InitIIRFilter( );
		directionFlag = 0;

		break;

	case E_GINIT_InitLevel2:

		retVal = E_GINIT_ReturnSuccess;
	    break;

	default:
		retVal = E_GINIT_ReturnSuccess;
		break;
	}

	return retVal;
}


RMOT_ReturnStatus_e RMOT_setForces(RMOT_MotorForces_s forces, RMOT_MotorEncoders_s calcEncoders)
{
	RMOT_ReturnStatus_e returnStatus;
	RCAN_ReturnStatus_e returnCanStatus;

	msg_buf[0]= '\0';

    //Calculations that input angles and forces and output a current value for each leg.
	S_motorKinematics = calculateMotorKinematics(calcEncoders.motorEncoderRightSide, calcEncoders.motorEncoderLeftSide);
	S_motorTorque = calculateMotorTorque(forces.motorForceRightSide, forces.motorForceLeftSide);
	S_motorCurrentPerThousand = calculateMotorCurrent(S_motorTorque, S_motorKinematics);


	//Every time that setForces is called, we read the data from the motor and store it
	// in a struct to be accessible for other functions.
	motorGetALLData();

	returnStatus = currentRangeCheck( S_motorCurrentPerThousand );

	if (returnStatus == E_RMOT_ReturnSuccess)
	{
	    //sends the current to the motor
		//returnCanStatus = RCAN_sendCurrentToMotor( S_motorCurrentPerThousand );
		returnCanStatus = E_RCAN_ReturnSuccess;
		if (returnCanStatus != E_RCAN_ReturnSuccess)
		{
		    returnStatus = E_RMOT_DataNotValid;
		}

	}

	else
	{
		returnStatus = E_RMOT_DataNotValid;
	}

	return returnStatus;
}



RMOT_MotorCurrents_s RMOT_getMotorCurrents(void)
{

	return S_motorCurrent;
}


RMOT_MotorEncoders_s RMOT_getMotorEncoders(void)
{

	return S_motorEncoders;
}



/***********************************************************************/
/* LOCAL FUNCTIONS                                                     */
/***********************************************************************/

static RMOT_MotorKinematics_s calculateMotorKinematics(int32_t currentEncoderRight, int32_t currentEncoderLeft)
{
	//Right leg calculations
	S_motorKinematics.angularVelocityRightSide = 2.0*GLO_PI*(Double64_t)(currentEncoderRight-S_previousEncoderRight)/(resolutionEncoder*GLO_dt); //RPS
	S_motorKinematics.angularAccelerationRightSide = (S_motorKinematics.angularVelocityRightSide-S_AngularVelocityRightPrev)/GLO_dt;
	S_motorKinematics.angularAccelerationRightSide = ButterworthIIRFilter(S_motorKinematics.angularAccelerationRightSide);


	//Left leg calculations
	S_motorKinematics.angularVelocityLeftSide = 2.0*GLO_PI*(Double64_t)(currentEncoderLeft-S_previousEncoderLeft)/(resolutionEncoder*GLO_dt); //RPS
    UTIL1_strcatNum32sDotValue100(msg_buf, sizeof(msg_buf), (int32_t)(S_motorKinematics.angularVelocityRightSide*100));
    UTIL1_chcat(msg_buf, sizeof(msg_buf), '\t');

	S_motorKinematics.angularAccelerationLeftSide = (S_motorKinematics.angularVelocityLeftSide - S_AngularVelocityLeftPrev)/GLO_dt;
	S_motorKinematics.angularAccelerationLeftSide = ButterworthIIRFilter(S_motorKinematics.angularAccelerationLeftSide);
	UTIL1_strcatNum32sDotValue100(msg_buf, sizeof(msg_buf),(int32_t)(S_motorKinematics.angularAccelerationRightSide *100));
    UTIL1_chcat(msg_buf, sizeof(msg_buf), '\t');
//	UTIL1_strcat(msg_buf, sizeof(msg_buf), (unsigned char*)"\r\n");
//	SEGGER_RTT_WriteString(0, msg_buf);

	S_previousEncoderRight = currentEncoderRight;
	S_previousEncoderLeft = currentEncoderLeft;

	S_AngularVelocityRightPrev = S_motorKinematics.angularVelocityRightSide;
	S_AngularVelocityLeftPrev = S_motorKinematics.angularVelocityLeftSide;

	return S_motorKinematics;

}


static RMOT_MotorTorque_s calculateMotorTorque(Force_Newton_t forceRight, Force_Newton_t forceLeft)
{
	//Right leg calculations
	motorInertiaComponentRight = ((S_motorKinematics.angularAccelerationRightSide)*(inertiaTotal/10000000.0));
	S_motorTorque.torqueRightSide = ((forceRight * radiusOutputShaft *-1) / (gearRatio )) + motorInertiaComponentRight; // efficiency = 75% (motor 82%, gear box 72% )







    //Left leg calculations
	motorInertiaComponentLeft = ((S_motorKinematics.angularAccelerationLeftSide)*(inertiaTotal/10000000.0));
//	UTIL1_strcatNum32sDotValue100(msg_buf, sizeof(msg_buf), (int32_t)(motorInertiaComponent*100000));
//	UTIL1_chcat(msg_buf, sizeof(msg_buf), '\t');

	S_motorTorque.torqueLeftSide = ((forceLeft * radiusOutputShaft) / (gearRatio)) + motorInertiaComponentLeft; // efficiency = 75% (motor 82%, gear box 72% )
//	UTIL1_strcatNum32sDotValue100(msg_buf, sizeof(msg_buf),(int32_t)(S_motorTorque.torqueLeftSide *100000));
//    UTIL1_chcat(msg_buf, sizeof(msg_buf), '\t');
//	UTIL1_strcat(msg_buf, sizeof(msg_buf), (unsigned char*)"\r\n");
//	SEGGER_RTT_WriteString(0, msg_buf);

	return S_motorTorque;
}

static RCAN_MotorCurrentsPerThousand_s calculateMotorCurrent(RMOT_MotorTorque_s totalTorque, RMOT_MotorKinematics_s motorKinematics)
{

    //Right leg calculations 1000 = 8Amps 1412.5=11.31 amps
	S_motorCurrentPerThousand.motorCurrentPerThousandRightSide = (int32_t)(totalTorque.torqueRightSide/motorConstant);
	S_motorCurrentPerThousand.motorCurrentPerThousandRightSide = (int32_t)((S_motorCurrentPerThousand.motorCurrentPerThousandRightSide)/nominalCurrent);




	//Left leg calculations
	S_motorCurrentPerThousand.motorCurrentPerThousandLeftSide = (int32_t)(totalTorque.torqueLeftSide/motorConstant);
	S_motorCurrentPerThousand.motorCurrentPerThousandLeftSide = (int32_t)((S_motorCurrentPerThousand.motorCurrentPerThousandLeftSide)/nominalCurrent);
//	sprintf(msg_buf, "Curr: %d \r\n", (uint32_t)S_motorCurrentPerThousand.motorCurrentPerThousandLeftSide);

	UTIL1_strcatNum32sDotValue100(msg_buf, sizeof(msg_buf),(int32_t)(S_motorCurrentPerThousand.motorCurrentPerThousandRightSide));
    UTIL1_chcat(msg_buf, sizeof(msg_buf), '\t');

    //Peak Detection////////////////////////////////////////////////////////////////////////////
    //burstValue =  burstDetection( );
    //S_previousAccelerationLeft = S_motorKinematics.angularAccelerationLeftSide;
    //S_motorCurrentPerThousand.motorCurrentPerThousandLeftSide = S_motorCurrentPerThousand.motorCurrentPerThousandLeftSide + burstValue;


//    if ((flagDirection == 0) && (S_motorKinematics.angularVelocityRightSide == 0))
//    {
//    	S_motorKinematics.angularVelocityRightSide = -1;
//    	flagDirection = 1;
//    }
//    if ((flagDirection == 1) && (S_motorKinematics.angularVelocityRightSide == 0))
//    {
//    	S_motorKinematics.angularVelocityRightSide = 1;
//    	flagDirection = 0;
//    }
//



	//Static friction calculation
	//Right leg
	//If the angular velocity is positive:
	if (S_motorKinematics.angularVelocityRightSide <= 0)// S_speedCurrentChange) negative winds in
	{
		S_motorCurrentPerThousand.motorCurrentPerThousandRightSide = S_motorCurrentPerThousand.motorCurrentPerThousandRightSide + 0.036 * S_motorKinematics.angularVelocityRightSide - 21.3;;
	}

	//if the angular velocity is negative
	else
	{
		S_motorCurrentPerThousand.motorCurrentPerThousandRightSide = S_motorCurrentPerThousand.motorCurrentPerThousandRightSide + 0.036  * S_motorKinematics.angularVelocityRightSide + 15.3;
	}




	//Left leg calculations (change signs of current)
	//if angular velocity is positive
	if (S_motorKinematics.angularVelocityLeftSide >=0)
	{
		S_motorCurrentPerThousand.motorCurrentPerThousandLeftSide = S_motorCurrentPerThousand.motorCurrentPerThousandLeftSide + 0.036 * S_motorKinematics.angularVelocityLeftSide + 21.3; //0.036
	}


	else
	{
		S_motorCurrentPerThousand.motorCurrentPerThousandLeftSide = S_motorCurrentPerThousand.motorCurrentPerThousandLeftSide + 0.036  * S_motorKinematics.angularVelocityLeftSide-21.3;
		directionFlag = 0;
	}


	UTIL1_strcatNum32sDotValue100(msg_buf, sizeof(msg_buf),(int32_t)(S_motorCurrentPerThousand.motorCurrentPerThousandRightSide));
	UTIL1_chcat(msg_buf, sizeof(msg_buf), '\t');
	UTIL1_strcat(msg_buf, sizeof(msg_buf), (unsigned char*)"\r\n");
	SEGGER_RTT_WriteString(0, msg_buf);


	S_motorCurrentPerThousand.motorCurrentPerThousandLeftSide = 0;
	S_motorCurrentPerThousand.motorCurrentPerThousandRightSide = 0;

	return S_motorCurrentPerThousand;
}



 static void motorGetALLData (void)
{
	//a struct is passed by reference for left and right motor. This struct is filled in by the values received by CANOpen
	//later we grab these values and place them in specific current and encoder structs that are accessible publicly.
	RCAN_getDataFromMotor( &S_rightMotorData_s, E_RCAN_Node2 );
	RCAN_getDataFromMotor( &S_leftMotorData_s, E_RCAN_Node1 );

	S_motorCurrent.motorCurrentRightSide = S_rightMotorData_s.motorCurrent;
	S_motorCurrent.motorCurrentLeftSide = S_leftMotorData_s.motorCurrent;

	S_motorEncoders.motorEncoderRightSide = S_rightMotorData_s.motorEncoder;
	S_motorEncoders.motorEncoderLeftSide = S_leftMotorData_s.motorEncoder;


}


static RMOT_ReturnStatus_e currentRangeCheck (RCAN_MotorCurrentsPerThousand_s motorCurrentPerThousand)
{
	RMOT_ReturnStatus_e returnStatus;

	if ((motorCurrentPerThousand.motorCurrentPerThousandRightSide > maxCurrentAllowed) || (motorCurrentPerThousand.motorCurrentPerThousandRightSide < minCurrentAllowed) || (motorCurrentPerThousand.motorCurrentPerThousandLeftSide > maxCurrentAllowed) || (motorCurrentPerThousand.motorCurrentPerThousandLeftSide < minCurrentAllowed))
	{
		returnStatus = E_RMOT_DataNotValid;
	}

	else
	{
		returnStatus = E_RMOT_ReturnSuccess;
	}

	return returnStatus;
}


static void InitIIRFilter()
{

 // FILTER INITIALIZATION

	ButterworthParameters.cutofFrequency = 100;
	ButterworthParameters.samplingRate = 1000;

	ButterworthParameters.QcRaw = (2.0 * GLO_PI * ButterworthParameters.cutofFrequency) / ButterworthParameters.samplingRate;
	ButterworthParameters.QcWarp = tan(ButterworthParameters.QcRaw);
	ButterworthParameters.gain = 1.0 / (1.0 + sqrt(2.0) / ButterworthParameters.QcWarp + 2.0 / (ButterworthParameters.QcWarp * ButterworthParameters.QcWarp));

	ButterworthParameters.by[2] = (1.0 - sqrt(2.0) / ButterworthParameters.QcWarp + 2.0 / (ButterworthParameters.QcWarp * ButterworthParameters.QcWarp)) * ButterworthParameters.gain;
	ButterworthParameters.by[1] = (2 - 2 * 2 / (ButterworthParameters.QcWarp * ButterworthParameters.QcWarp)) * ButterworthParameters.gain;
	ButterworthParameters.by[0] = 1;
	ButterworthParameters.ax[0] = 1 * ButterworthParameters.gain;
	ButterworthParameters.ax[1] = 2 * ButterworthParameters.gain;
	ButterworthParameters.ax[2] = 1 * ButterworthParameters.gain;

}

static Double64_t ButterworthIIRFilter(Double64_t actualValue)
{
	Double64_t actualValueFiltered = 0;
	ButterworthParameters.xv[2] = ButterworthParameters.xv[1];
	ButterworthParameters.xv[1] = ButterworthParameters.xv[0];
	ButterworthParameters.xv[0] = actualValue;
	ButterworthParameters.yv[2] = ButterworthParameters.yv[1];
	ButterworthParameters.yv[1] = ButterworthParameters.yv[0];
	ButterworthParameters.yv[0] = (ButterworthParameters.ax[0] * ButterworthParameters.xv[0] + ButterworthParameters.ax[1] * ButterworthParameters.xv[1] + ButterworthParameters.ax[2] * ButterworthParameters.xv[2] - ButterworthParameters.by[1] * ButterworthParameters.yv[0] - ButterworthParameters.by[2] * ButterworthParameters.yv[1]);

	actualValueFiltered = ButterworthParameters.yv[0];


	return (actualValueFiltered);
}


static int32_t burstDetection(void)
{
	int32_t burstValue = 0;

	if( S_motorKinematics.angularAccelerationLeftSide <= S_previousAccelerationLeft )
	{
		 minimum = S_motorKinematics.angularAccelerationLeftSide;
		 burstValue = 0;

	}


	if  ( (S_motorKinematics.angularAccelerationLeftSide > minimum)  && (minimum < -2500) )
	{
		 flagPeak1 = 1;
		 burstValue = 0;

	}


	if (flagPeak1 ==1)
	{
        if (S_motorKinematics.angularAccelerationLeftSide > S_previousAccelerationLeft){
        	 maximum =  S_motorKinematics.angularAccelerationLeftSide;
        	 burstValue = 0;

        }


        else if  ( (S_motorKinematics.angularAccelerationLeftSide < maximum)  && (minimum > 600) )
        {
        	flagPeak2 = 1;
        	burstValue = 0;

        }

	}
    if ((flagPeak1 ==1) && (flagPeak2 == 1))
    {

        if (S_motorKinematics.angularVelocityLeftSide > 0 )
        {
            flagPeak2 = 0;
            flagPeak1 = 0;
            burstValue = 0;
        }

        else
        {
        	burstValue = 30;
        }

	}

    return burstValue;




}












/** @} */






