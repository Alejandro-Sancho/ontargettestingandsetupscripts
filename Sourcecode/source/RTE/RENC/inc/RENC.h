/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation: xxx
 ***********************************************************************/

/** @defgroup RENC RTE_MOTOR
 * @{
 * @brief
 *
 * Device abstraction of the motor where you can read Encodercounts and
 * set the force of the motor
 * @file
 * This header file contains the declaration of the public
 * interface to module RENC (RTE_Encoder).
 */

#ifndef RENC_H
#define RENC_H
/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/
#include "GINIT.h"
#include "types.h"

/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/

/**
 * The possible return values of public functions
 */
typedef enum tag_RENC_ReturnStatus_e{
	E_RENC_ReturnSuccess,
	E_RENC_ReturnFailed,
	E_RENC_DataNotValid
}RENC_ReturnStatus_e;

/**
 * The structure of the motor encoder values
 */
typedef struct tag_RENC_EncoderCounts_s{
	int32_t motorEncoderCountRight;
	int32_t motorEncoderCountLeft;
	RENC_ReturnStatus_e status;
}RENC_EncoderCounts_s;


/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/


/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/


/***********************************************************************/
/* GLOBAL FUNCTION PROTOTYPES                                          */
/***********************************************************************/

/** Initialize the RENC-Module
 *  @param initLevel level of initialization
 *  @return E_GINIT_ReturnFailure if failed
 */
GINIT_Return_e RENC_Initialize(GINIT_InitLevel_e initLevel);

/** Reads the encodercounts of each motor
 *  @return RENC_EncoderValues_s, flag status is set to E_RENC_DataNotValid if data is not valid, otherwise to E_RENC_ReturnSuccess
 */
RENC_EncoderCounts_s RENC_getEncoderCounts(void);



#endif /* RENC_H */

/** @} */
