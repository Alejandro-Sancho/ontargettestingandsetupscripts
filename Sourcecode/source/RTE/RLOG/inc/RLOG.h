/***********************************************************************
 * $Id$
 * Copyright Zühlke Engineering AG
 * Project: MyoSwiss
 * Documentation: xxx
 ***********************************************************************/

/** @defgroup RLOG
 * @{
 * @brief
 * The next blank line is mandatory!
 *
 * This is a wrapper module for the logging module.
 * @file
 * This header file contains the declaration of the public or private
 * interface to module RLOG (long name).
 */

#ifndef RLOG_H
#define RLOG_H
/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/
#include "types.h"
/***********************************************************************/
/* DEFINES                                                             */
/*************d**********************************************************/

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/
typedef enum RLOG_ReturnValue{
	RLOG_SUCCESS = 0,
	RLOG_FAIL 	= 1
}RLOG_ReturnValue;
/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/


/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/


/***********************************************************************/
/* GLOBAL FUNCTION PROTOTYPES                                          */
/***********************************************************************/

RLOG_ReturnValue RLOG_Initialize(void);

RLOG_ReturnValue RLOG_SendBuffer(const Char_t* buf, size_t numOfElements);

#endif /* RLOG_H */

/** @} */
