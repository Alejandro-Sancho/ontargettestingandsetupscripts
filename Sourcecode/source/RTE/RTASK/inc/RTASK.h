/***********************************************************************
 * $Id$
 * Copyright Zühlke Engineering AG
 * Project: MyoSwiss
 * Documentation: xxx
 ***********************************************************************/

/** @defgroup LABC Long Name of the Component
 * @{
 * @brief
 * The next blank line is mandatory!
 * 
 * A bit more elaborated description focusing on the functions declared
 * here.
 * @file
 * This header file contains the declaration of the public or private
 * interface to HAL or RTE component or module LABC (long name).
 */

#ifndef RTASK_H
#define RTASK_H

/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/
#include <types.h>

/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/

typedef void (*RTASK_Function_t)( void * );

/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/


/***********************************************************************/
/* GLOBAL FUNCTION PROTOTYPES                                          */
/***********************************************************************/

/*
 *
 * @param[in] params The task parameters 
 */
extern void RTASK_Create(RTASK_Function_t pvTaskCode,const char * const pcName,uint16_t usStackDepth,void *pvParameters,uint16_t uxPriority);



#endif /* RTASK_H */

/** @} */
