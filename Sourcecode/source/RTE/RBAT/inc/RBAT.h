/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation: xxx
 ***********************************************************************/

/** @defgroup RBAT
 * @{
 * @brief
 *
 * Device abstraction of the motor where you can read actual current and
 * set the force of the motor
 * @file
 * This header file contains the declaration of the public
 * interface to module RMOT (RTE_Battery).
 */

#ifndef RBAT_H
#define RBAT_H
/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/
#include "GINIT.h"
#include "types.h"

/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/

/**
 * The possible return values of public functions
 */
typedef enum tag_RBAT_ReturnStatus_e{
	E_RBAT_ReturnSuccess,
	E_RBAT_ReturnFailed,
	E_RBAT_DataNotValid
}RBAT_ReturnStatus_e;

typedef struct tag_RBAT_Voltage_s{
	Voltage_mV_t batteryVoltage;
	Temp_DegreeCelsius_t batteryTemperature;
	uint32_t batterCapacity;
	RBAT_ReturnStatus_e status;
}RBAT_Voltage_s;

typedef struct tag_RBAT_Temp_s{
	Temp_DegreeCelsius_t batteryTemperature;
	RBAT_ReturnStatus_e status;
}RBAT_Temp_s;

typedef struct tag_RBAT_Capacity_s{
	uint32_t batteryCapacity;
	RBAT_ReturnStatus_e status;
}RBAT_Capacity_s;



/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/


/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/


/***********************************************************************/
/* GLOBAL FUNCTION PROTOTYPES                                          */
/***********************************************************************/

/** Initialize the RBAT-Module
 *  @param initLevel level of initialization
 *  @return E_GINIT_ReturnFailure if failed
 */
GINIT_Return_e RBAT_Initialize(GINIT_InitLevel_e initLevel);

/** Reads the actual battery voltage
 * @return the battery voltage and E_RBAT_ReturnSuccess if data is valid
 */
RBAT_Voltage_s RBAT_GetVoltage();

/** Reads the actual battery temperature
 * @return the battery temperature and E_RBAT_ReturnSuccess if data is valid
 */
RBAT_Temp_s RBAT_GetTemperature();

/**Reads the actual battery capacity
 * @return the battery capacity and E_RBAT_ReturnSuccess if data is valid
 */
RBAT_Capacity_s RBAT_GetCapacity();


#endif /* RBAT_H */

/** @} */
