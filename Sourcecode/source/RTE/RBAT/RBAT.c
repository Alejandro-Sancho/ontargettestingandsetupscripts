/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation:
 ***********************************************************************/

/** @RBAT Battery module of RTE layer
 * @{
 * @file
 * This source file contains the definition of the functions of
 * RTE component RBAT Battery module.
 */

/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/

#include "types.h"
#include "RBAT.h"

/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/

/***********************************************************************/
/* LOCAL CONSTANTS                                                     */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/

/***********************************************************************/
/* LOCAL VARIABLES                                                     */
/***********************************************************************/


/***********************************************************************/
/* LOCAL FUNCTION PROTOTYPES                                           */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL FUNCTIONS                                                    */
/***********************************************************************/

GINIT_Return_e RBAT_Initialize(GINIT_InitLevel_e initLevel){

	return E_GINIT_ReturnSuccess;
}

RBAT_Voltage_s RBAT_GetVoltage(void){

	RBAT_Voltage_s dummy;

	dummy.batteryVoltage = 0;
	dummy.status = E_RBAT_ReturnSuccess;
	return dummy;
}


RBAT_Temp_s RBAT_GetTemperature(){

	RBAT_Temp_s dummy;

	dummy.batteryTemperature = 0;
	dummy.status = E_RBAT_ReturnSuccess;
	return dummy;
}


RBAT_Capacity_s RBAT_GetCapacity(){
	RBAT_Capacity_s dummy;

	dummy.batteryCapacity = 0;
	dummy.status = E_RBAT_ReturnSuccess;
	return dummy;
}


/***********************************************************************/
/* LOCAL FUNCTIONS                                                     */
/***********************************************************************/


/** @} */
