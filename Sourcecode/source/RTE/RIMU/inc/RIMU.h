/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation: RIMU IMU RTE Layer
 ***********************************************************************/

/** @defgroup RIMU IMU component of the RTE layer
 * @{
 * @brief
 * The next blank line is mandatory!
 *
 * @file
 * This header file contains the declaration of the public or private
 * interface to RTE component IMU.
 */

#ifndef RIMU_H_
#define RIMU_H_

/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/

#include "types.h"
#include "GINIT.h"

/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/

/**
 * This type definition is used to call any of the 5 IMUs in the system.
 * Each of them has a number assigned to them.
 */
typedef enum tag_RIMU_IMUPosition_e{

	E_RIMU_RightShank = 0,
	E_RIMU_RightThigh = 1,
	E_RIMU_LeftShank = 2,
	E_RIMU_LeftThigh = 3,
	E_RIMU_Trunk = 4

}RIMU_IMUPosition_e;

/**
 * The different return values/status of the GetIMUAngle() function.
 * 0 means that the data obtained is complete. 1 means that there was
 * an error in the HW or in the HAL layer (ie. not connected) 2 means
 * that there is a problem with the data received (ie. out of range).�
 */
typedef enum tag_RIMU_StatusOfData_e{

	E_RIMU_ReturnSuccess,
	E_RIMU_DataNotValid

}RIMU_StatusOfData_e;


/**
 * This struct contains the raw IMU data and the calculated IMU angles obtained from the HAL layer and the
 * status of the data. It will be called by RDAC to send the data to the APP
 * layer.
 */
typedef struct tag_RIMU_DataStruct_s{

	Angle_mDegree_t imuAngle;
	int16_t gyroValue;
	int16_t accelValueA;
	int16_t accelValueB;
    RIMU_StatusOfData_e imuStatusOfData;

} RIMU_DataStruct_s;

/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL FUNCTION PROTOTYPES                                          */
/***********************************************************************/

/** Initialize the RIMU-Module
 *  @return E_GINIT_ReturnFailure if failed
 */
GINIT_Return_e RIMU_Initialize(GINIT_InitLevel_e initLevel);

/**
 * Returns the calculated absolute angle based on two accelerometer axis and one gyro axis. The axis depending on the position
 * of the IMU. The angle is filtered with an kalman filter.
 * @param pos the IMU position
 * @return the IMU angle in miliDegree and E_RIMU_Success if data valid, otherwise E_RIMU_DataNotValid
 */
RIMU_DataStruct_s RIMU_GetIMUAngleFiltered(RIMU_IMUPosition_e pos);
/**
 * Returns the calculated absolute angle based on two axis. The axis depending on the position
 * of the IMU
 * @param pos the IMU position
 * @return the IMU angle in miliDegree and E_RIMU_Success if data valid, otherwise E_RIMU_DataNotValid
 */
RIMU_DataStruct_s RIMU_GetIMUAngleRaw(RIMU_IMUPosition_e pos);

/**
 * Informs the component that it will be initialized. Before calling RIMU_StartCalibration, all other functions for calibrations
 * do not have any influence
 */
void RIMU_StartCalibration(void);
/**
 * Measures the gyro value and averages it. The average will be set as initial gyro angle after calling RIMU_StopCalibration().
 * This mode is only working if RIMU_StartCalibration is called. After calling RIMU_StopCalibration(), the function has no influence for calibration up to
 * RIMU_StartCalibration() is called again
 */
void RIMU_DoCalibration(void);
/**
 * Stops the calibration mode and sets the average, which was measured during calling RIMU_DoCalibration() as initial gyro angle
 */
void RIMU_StopCalibration(void);
/**
 * Stops the calibration mode and rejects the measured average
 */
void RIMU_AbortCalibration(void);


#endif /* RIMU_H */

/** @} */
