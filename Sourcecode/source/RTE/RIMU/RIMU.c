/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation: RIMU
 ***********************************************************************/

/** @addtogroup RIMU
 * @{
 * @file
 * This source file contains the definition of the functions of
 * RTE component RIMU.
 */

/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/

#include "RIMU.h"
#include "GINIT.h"
#include "HI2C.h"
#include "LSM9DS1_Registers.h"
#include "GUTL.h"
#include "types.h"

/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/

#define DELTA_T		(0.011)
#define SENSITIVITY (0.0175)
#define NUMBEROFIMUS (5u)
enum {
	GYROTYPE = 0x6B,
	XMTYPE   = 0x6B
};



/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/

typedef enum tag_RIMU_GyroAxis_e
{
	E_GyroXaxis = LSM9DS1_REGISTER_OUT_X_L_G,
	E_GyroYaxis =  LSM9DS1_REGISTER_OUT_Y_L_G,
	E_GyroZaxis = LSM9DS1_REGISTER_OUT_Z_L_G
}RIMU_GyroAxis;

typedef enum tag_RIMU_AccelAxis_e
{
	E_AccelXaxis = LSM9DS1_REGISTER_OUT_X_L_XL,
	E_AccelYaxis = LSM9DS1_REGISTER_OUT_Y_L_XL,
	E_AccelZaxis = LSM9DS1_REGISTER_OUT_Z_L_XL
}RIMU_AccelAxis;

typedef struct tag_RIMU_ConfigStruct_s{

	RIMU_AccelAxis accelAxisA;
	RIMU_AccelAxis accelAxisB;
	RIMU_GyroAxis  gyroAxis;
	uint32_t gyroAddress;
	uint32_t accelAddress;
	HI2C_Port_e port;
	int16_t gyroOffset;
	Angle_mDegree_t previousFilteredAngle;
}RIMU_ConfigStruct_s;



typedef struct tag_RIMU_DataSet_s{
	int16_t accelValueA;
	int16_t accelValueB;
	int16_t gyroValue;
}RIMU_DataSet;

/***********************************************************************/
/* LOCAL CONSTANTS                                                     */
/***********************************************************************/
/*lint -esym(9003,RIMU_AccelLowerLimit)*/
static const int16_t RIMU_AccelLowerLimit = -15000;
/*lint -esym(9003,RIMU_AccelUpperLimit)*/
static const int16_t RIMU_AccelUpperLimit = +15000;
/*lint -esym(9003,RIMU_GyroUpperLimit)*/
static const int16_t RIMU_GyroUpperLimit = +15000;
/*lint -esym(9003,RIMU_GyroLowerLimit)*/
static const int16_t RIMU_GyroLowerLimit = -15000;

/*lint -esym(9003,RIMU_Positions)*/
static const RIMU_IMUPosition_e RIMU_Positions[NUMBEROFIMUS] =
{
		E_RIMU_RightShank,
		E_RIMU_RightThigh,
		E_RIMU_LeftShank,
		E_RIMU_LeftThigh,
		E_RIMU_Trunk
};

/***********************************************************************/
/* LOCAL VARIABLES                                                     */
/***********************************************************************/

static RIMU_ConfigStruct_s S_Imu[NUMBEROFIMUS];

static Double_t S_covMatrix[2][2];
static Double_t S_gainMatrix[2];


static int32_t S_CalibrationCounter;
static Angle_mDegree_t S_AveragedAngle[NUMBEROFIMUS];
static Bool_t S_IsCalibrating;

static Bool_t S_isCalibrated;

static int16_t S_OffsetAverage[NUMBEROFIMUS];

/***********************************************************************/
/* LOCAL FUNCTION PROTOTYPES                                           */
/***********************************************************************/

/**
 * Measures the 3 values from the IMU needed to calculate the angle. The values contains 2 accel axis and one gyro axis.
 * @param out dataToStore, the data struct where the values will be stored
 */
static void measure(RIMU_DataSet* dataToStore, RIMU_IMUPosition_e pos);
/** Reads gyro value
 * @param pos the position of the IMU
 * @param axis the axis on which the value is read
 * @return the raw value from the IMU
 */
static int16_t readGyroValue(RIMU_IMUPosition_e pos, RIMU_GyroAxis axis);
/** Reads accel value
 * @param pos the position of the IMU
 * @param axis the axis on which the value is read
 * @return the raw value from the IMU
 */
static int16_t readAccelValue(RIMU_IMUPosition_e pos, RIMU_AccelAxis axis);

/** Reads a register value of the gyro
 * @param pos the position of the IMU
 * @param regAdress internal address of the gyro register
 * @return the register value
 */
static uint8_t readGyroRegisterValue(RIMU_IMUPosition_e pos, lsm9ds1_XM_Gyro_Registers_t regAddress);

/** Reads a register value of the accelerometer
 * @param pos the position of the IMU
 * @param regAdress internal address of the accelerometer register
 * @return the register value
 */
static uint8_t readAccelRegisterValue(RIMU_IMUPosition_e pos,lsm9ds1_XM_Gyro_Registers_t regAdress);
/** Writes a register value of the gyro
 * @param pos the position of the IMU
 * @param regAdress internal address of the gyro register
 * @param value to write into register
 */
static void writeGyroRegisterValue(RIMU_IMUPosition_e pos,lsm9ds1_XM_Gyro_Registers_t regAdress, uint8_t value);
/** Writes a register value of the accelerometer
 * @param pos the position of the IMU
 * @param regAdress internal address of the accelerometer register
 * @param value to write into register
 */
static void writeAccelRegisterValue(RIMU_IMUPosition_e pos,lsm9ds1_XM_Gyro_Registers_t regAdress, uint8_t value);

/** Runs the initialization commands of the accelerometer
 * @param pos the position of the imu to initialize
 * @return true if initialization was successful
 */
static Bool_t initializeAccel(RIMU_IMUPosition_e pos);
/** Runs the initialization commands of the gyro
 * @param pos the position of the imu to initialize
 * @return true if initialization was successful
 */
static Bool_t initializeGyro(RIMU_IMUPosition_e pos);

/** Calculates the absolute angle of plain relative to the ground, based on two axis
 * @param accelValueA the absolute angle of axis A
 * @param accelValueB the absolute angle of axis B
 * @return angle in miliDegree
 */
static Angle_mDegree_t calculateAngle(int16_t accelValueA, int16_t accelValueB);

/** Checks the accelerometer value if its in a valid range
 * @param val the value to check
 * @return true if val is in range, false if its outside
 */
static Bool_t accelRangeCheck(int16_t val);
/** Checks the gyro value if its in a valid range
 * @param val the value to check
 * @return true if val is in range, false if its outside
 */
static Bool_t gyroRangeCheck(int16_t val);

/**
 * Implements the kalman filter which is customized for the RIMU implementation
 * @param accelAngle the angle calculated of the two accelerometer axis
 * @param gyroAngle the angle calculated of the gyrometer axis
 */
static Double_t kalmanFilter(Double_t accelAngle, Double_t gyroAngle);

/**
 * Erases all variables related to the calibration routine
 */
static void eraseCalibrationValues(void);

/***********************************************************************/
/* GLOBAL FUNCTIONS                                                    */
/***********************************************************************/


GINIT_Return_e RIMU_Initialize(GINIT_InitLevel_e initLevel)
{
	GINIT_Return_e retVal = E_GINIT_ReturnSuccess;
	Bool_t retValInit;

	switch(initLevel){
	case E_GINIT_InitLevel1:
		S_Imu[E_RIMU_RightShank].accelAddress = XL_GYRO_TYPE_SA_LOW;
		S_Imu[E_RIMU_RightShank].gyroAddress = XL_GYRO_TYPE_SA_LOW;
		S_Imu[E_RIMU_RightShank].accelAxisA = E_AccelZaxis;
		S_Imu[E_RIMU_RightShank].accelAxisB = E_AccelXaxis;
		S_Imu[E_RIMU_RightShank].gyroAxis = E_GyroYaxis;
		S_Imu[E_RIMU_RightShank].port = E_I2C_RightSide;
		S_Imu[E_RIMU_RightShank].gyroOffset = -120;
		S_Imu[E_RIMU_RightShank].previousFilteredAngle = 0;

		S_Imu[E_RIMU_RightThigh].accelAddress = XL_GYRO_TYPE_SA_HIGH;
		S_Imu[E_RIMU_RightThigh].gyroAddress = XL_GYRO_TYPE_SA_HIGH;
		S_Imu[E_RIMU_RightThigh].accelAxisA = E_AccelZaxis;
		S_Imu[E_RIMU_RightThigh].accelAxisB = E_AccelXaxis;
		S_Imu[E_RIMU_RightThigh].gyroAxis = E_GyroYaxis;
		S_Imu[E_RIMU_RightThigh].port = E_I2C_RightSide;
		S_Imu[E_RIMU_RightThigh].gyroOffset = -120;
		S_Imu[E_RIMU_RightThigh].previousFilteredAngle = 0;

		S_Imu[E_RIMU_LeftShank].accelAddress = XL_GYRO_TYPE_SA_LOW;
		S_Imu[E_RIMU_LeftShank].gyroAddress = XL_GYRO_TYPE_SA_LOW;
		S_Imu[E_RIMU_LeftShank].accelAxisA = E_AccelZaxis;
		S_Imu[E_RIMU_LeftShank].accelAxisB = E_AccelXaxis;
		S_Imu[E_RIMU_LeftShank].gyroAxis = E_GyroYaxis;
		S_Imu[E_RIMU_LeftShank].port = E_I2C_leftSide;
		S_Imu[E_RIMU_LeftShank].gyroOffset = -120;
		S_Imu[E_RIMU_LeftShank].previousFilteredAngle = 0;

		S_Imu[E_RIMU_LeftThigh].accelAddress = XL_GYRO_TYPE_SA_HIGH;
		S_Imu[E_RIMU_LeftThigh].gyroAddress = XL_GYRO_TYPE_SA_HIGH;
		S_Imu[E_RIMU_LeftThigh].accelAxisA = E_AccelZaxis;
		S_Imu[E_RIMU_LeftThigh].accelAxisB = E_AccelXaxis;
		S_Imu[E_RIMU_LeftThigh].gyroAxis = E_GyroYaxis;
		S_Imu[E_RIMU_LeftThigh].port = E_I2C_leftSide;
		S_Imu[E_RIMU_LeftThigh].gyroOffset = -120;
		S_Imu[E_RIMU_LeftThigh].previousFilteredAngle = 0;

		S_Imu[E_RIMU_Trunk].accelAddress = XL_GYRO_TYPE_SA_HIGH;
		S_Imu[E_RIMU_Trunk].gyroAddress = XL_GYRO_TYPE_SA_HIGH;
		S_Imu[E_RIMU_Trunk].accelAxisA = E_AccelZaxis;
		S_Imu[E_RIMU_Trunk].accelAxisB = E_AccelXaxis;
		S_Imu[E_RIMU_Trunk].gyroAxis = E_GyroYaxis;
		S_Imu[E_RIMU_Trunk].port = E_I2C_Trunk;
		S_Imu[E_RIMU_Trunk].gyroOffset = -120;
		S_Imu[E_RIMU_Trunk].previousFilteredAngle = 0;






		S_covMatrix[0][0] = 0.0;
		S_covMatrix[0][1] = 0.0;
		S_covMatrix[1][0] = 0.0;
		S_covMatrix[1][1] = 0.0;
		S_gainMatrix[0] = 0.0;
		S_gainMatrix[1] = 1.0;

		S_CalibrationCounter = 0;
		S_isCalibrated = false;
		retVal = E_GINIT_ReturnSuccess;

		break;

	case E_GINIT_InitLevel2:


		retValInit = initializeAccel(E_RIMU_RightShank);
		if(retValInit == false){
			retVal = E_GINIT_ReturnFailure;
		}

		retValInit = initializeAccel(E_RIMU_LeftShank);
		if(retValInit == false){
			retVal = E_GINIT_ReturnFailure;
		}

		retValInit = initializeAccel(E_RIMU_RightThigh);
		if(retValInit == false){
			retVal = E_GINIT_ReturnFailure;
		}
		retValInit = initializeAccel(E_RIMU_LeftThigh);
		if(retValInit == false){
			retVal = E_GINIT_ReturnFailure;
		}
		retValInit = initializeAccel(E_RIMU_Trunk);
		if(retValInit == false){
			retVal = E_GINIT_ReturnFailure;
		}


		retValInit = initializeGyro(E_RIMU_RightShank);
		if(retValInit == false){
			retVal = E_GINIT_ReturnFailure;
		}

		retValInit = initializeGyro(E_RIMU_RightThigh);
		if(retValInit == false){
			retVal = E_GINIT_ReturnFailure;
		}
		retValInit = initializeGyro(E_RIMU_LeftShank);
		if(retValInit == false){
			retVal = E_GINIT_ReturnFailure;
		}
		retValInit = initializeGyro(E_RIMU_LeftThigh);
		if(retValInit == false){
			retVal = E_GINIT_ReturnFailure;
		}
		retValInit = initializeGyro(E_RIMU_Trunk);
		if(retValInit == false){
			retVal = E_GINIT_ReturnFailure;
		}
		break;

	default:
		;;
		break;
	}

	return retVal;

}

RIMU_DataStruct_s RIMU_GetIMUAngleRaw(RIMU_IMUPosition_e pos){
	RIMU_DataSet rawValues;
	RIMU_DataStruct_s retVal;
	Bool_t retValCheck;
	retVal.imuStatusOfData = E_RIMU_ReturnSuccess;

	measure(&rawValues,pos);

	retValCheck = accelRangeCheck(rawValues.accelValueA);
	if(retValCheck == false){
		retVal.imuStatusOfData = E_RIMU_DataNotValid;
	}
	retVal.accelValueA = rawValues.accelValueA;

	retValCheck = accelRangeCheck(rawValues.accelValueB);
	if(retValCheck == false){
		retVal.imuStatusOfData = E_RIMU_DataNotValid;
	}
	retVal.accelValueB = rawValues.accelValueB;

	retValCheck = gyroRangeCheck(rawValues.gyroValue);
	if(retValCheck == false){
		retVal.imuStatusOfData = E_RIMU_DataNotValid;
	}
	retVal.gyroValue = rawValues.gyroValue;

	retVal.imuAngle = calculateAngle(rawValues.accelValueA,rawValues.accelValueB);


	return retVal;


}


RIMU_DataStruct_s RIMU_GetIMUAngleFiltered(RIMU_IMUPosition_e pos){

	RIMU_DataStruct_s retVal;
	RIMU_DataStruct_s retValUnfiltered;
	Double_t angleGyroIntegrated;
	Double_t angle;

	retValUnfiltered = RIMU_GetIMUAngleRaw(pos);

	retVal.accelValueA = retValUnfiltered.accelValueA;
	retVal.accelValueB = retValUnfiltered.accelValueB;
	retVal.gyroValue = retValUnfiltered.gyroValue;

	if(S_isCalibrated == false){
		retVal.imuStatusOfData = E_RIMU_DataNotValid;
	}

	Angle_mDegree_t angleAccel = retValUnfiltered.imuAngle;
	angleGyroIntegrated = (((Double_t)(S_Imu[pos].previousFilteredAngle))/1000.0) + (((Double_t)(retValUnfiltered.gyroValue- S_Imu[pos].gyroOffset)) * (Double_t)SENSITIVITY * (Double_t)DELTA_T); //500 dps; 110 is offset,  0.0175 = sensitivity

	angle = kalmanFilter(((Double_t)angleAccel)/1000.0, angleGyroIntegrated);
	retVal.imuAngle = (Angle_mDegree_t)(angle*1000.0);

	S_Imu[pos].previousFilteredAngle = retVal.imuAngle;

	return retVal;

}


void RIMU_DoCalibration(void){
	if(S_IsCalibrating){
		uint32_t i;
		RIMU_DataStruct_s measuredAngle;
		int16_t measuredGyroOffset;

		for(i = 0u; i < NUMBEROFIMUS; ++i){
			measuredAngle = RIMU_GetIMUAngleRaw(RIMU_Positions[i]);
			S_AveragedAngle[i] = ((S_AveragedAngle[i]*S_CalibrationCounter) + measuredAngle.imuAngle)/(S_CalibrationCounter + 1);

			measuredGyroOffset = readGyroValue(RIMU_Positions[i],S_Imu[i].gyroAxis);
			S_OffsetAverage[i] = (S_OffsetAverage[i]*S_CalibrationCounter) + (measuredGyroOffset/(S_CalibrationCounter + 1));
		}

		S_CalibrationCounter = S_CalibrationCounter + 1;
	}
}

void RIMU_StartCalibration(void){
	S_IsCalibrating = true;
	S_isCalibrated = false;

}

void RIMU_StopCalibration(void){

	uint32_t i;
	if(S_IsCalibrating){
		for(i = 0u; i < NUMBEROFIMUS; ++i){
			S_Imu[i].previousFilteredAngle = S_AveragedAngle[i];
			S_Imu[i].gyroOffset = S_OffsetAverage[i];
		}
		eraseCalibrationValues();
		S_IsCalibrating = false;
		S_isCalibrated = true;
	}
}

void RIMU_AbortCalibration(void){

	S_IsCalibrating = false;
	S_isCalibrated = false;
	eraseCalibrationValues();

}
/***********************************************************************/
/* LOCAL FUNCTIONS                                                     */
/***********************************************************************/

static void measure(RIMU_DataSet* dataToStore,RIMU_IMUPosition_e pos){

	dataToStore->gyroValue = readGyroValue(pos,S_Imu[pos].gyroAxis);
	dataToStore->accelValueA = readAccelValue(pos,S_Imu[pos].accelAxisA);
	dataToStore->accelValueB = readAccelValue(pos,S_Imu[pos].accelAxisB);
}

static Bool_t accelRangeCheck(int16_t val){

	return ((val <= RIMU_AccelUpperLimit) && (val >= RIMU_AccelLowerLimit));
}

static Bool_t gyroRangeCheck(int16_t val){

	return ((val <= RIMU_GyroUpperLimit) && (val >= RIMU_GyroLowerLimit));
}


static int16_t readGyroValue(RIMU_IMUPosition_e pos,RIMU_GyroAxis axis){

	uint8_t lowerByte;
	uint16_t higherByte;
	lsm9ds1_XM_Gyro_Registers_t regAdressLowerByte = LSM9DS1_REGISTER_WHO_AM_I;
	lsm9ds1_XM_Gyro_Registers_t regAdressHigherByte = LSM9DS1_REGISTER_WHO_AM_I;

	switch(axis){
	case E_GyroXaxis:
		regAdressLowerByte = LSM9DS1_REGISTER_OUT_X_L_G;
		regAdressHigherByte = LSM9DS1_REGISTER_OUT_X_H_G;

		break;
	case E_GyroYaxis:
		regAdressLowerByte = LSM9DS1_REGISTER_OUT_Y_L_G;
		regAdressHigherByte = LSM9DS1_REGISTER_OUT_Y_H_G;
		break;
	case E_GyroZaxis:
		regAdressLowerByte = LSM9DS1_REGISTER_OUT_Z_L_G;
		regAdressHigherByte = LSM9DS1_REGISTER_OUT_Z_H_G;
		break;
	default:
		;;
		break;
	}

	lowerByte = readGyroRegisterValue(pos,regAdressLowerByte);
	higherByte = (int8_t)readGyroRegisterValue(pos,regAdressHigherByte);

	higherByte <<= 8u;
	higherByte |= (uint16_t)lowerByte;

	return (int16_t)higherByte;
}

static int16_t readAccelValue(RIMU_IMUPosition_e pos, RIMU_AccelAxis axis){
	uint8_t lowerByte;
	uint16_t higherByte;
	lsm9ds1_XM_Gyro_Registers_t regAdressLowerByte = LSM9DS1_REGISTER_WHO_AM_I;
	lsm9ds1_XM_Gyro_Registers_t regAdressHigherByte = LSM9DS1_REGISTER_WHO_AM_I;

	switch(axis){
	case E_AccelXaxis:
		regAdressLowerByte = LSM9DS1_REGISTER_OUT_X_L_XL;
		regAdressHigherByte = LSM9DS1_REGISTER_OUT_X_H_XL;
		break;
	case E_AccelYaxis:
		regAdressLowerByte = LSM9DS1_REGISTER_OUT_Y_L_XL;
		regAdressHigherByte = LSM9DS1_REGISTER_OUT_Y_H_XL;
		break;
	case E_AccelZaxis:
		regAdressLowerByte = LSM9DS1_REGISTER_OUT_Z_L_XL;
		regAdressHigherByte = LSM9DS1_REGISTER_OUT_Z_H_XL;
		break;
	default:
		;;
		break;

	}

	lowerByte = readAccelRegisterValue(pos,regAdressLowerByte);
	higherByte = (int8_t)readAccelRegisterValue(pos,regAdressHigherByte);

	higherByte <<= 8u;
	higherByte |= (uint16_t)lowerByte;

	return (int16_t)higherByte;
}

static Angle_mDegree_t calculateAngle(int16_t accelValueA, int16_t accelValueB){

	Bool_t negativAngle;
	int16_t valueA;
	int16_t valueB;

	if(accelValueA < (int16_t)0u){
		negativAngle = false;
	}
	else{
		negativAngle = true;
	}

	valueA = GUTL_AbsInt16_t(accelValueA);
	valueB = GUTL_AbsInt16_t(accelValueB);
	Double_t angle = GUTL_Atan2((Double_t)valueA,(Double_t)valueB);

	angle = GUTL_RadToDeg(angle);

	if(negativAngle == true){
		angle = angle*(-1.0);
	}
	Angle_mDegree_t scaledAngle = (Angle_mDegree_t)(angle*1000.0);


	return scaledAngle;
}

static uint8_t readAccelRegisterValue(RIMU_IMUPosition_e pos,lsm9ds1_XM_Gyro_Registers_t regAdress){
	uint8_t buffer;
	HI2C_read(S_Imu[pos].port, S_Imu[pos].accelAddress,(uint8_t)regAdress,&buffer,1);

	return buffer;
}

static uint8_t readGyroRegisterValue(RIMU_IMUPosition_e pos,lsm9ds1_XM_Gyro_Registers_t regAddress){
	uint8_t buffer;
	HI2C_read(S_Imu[pos].port, S_Imu[pos].gyroAddress,(uint8_t)regAddress,&buffer,1);

	return buffer;
}

static void writeGyroRegisterValue(RIMU_IMUPosition_e pos,lsm9ds1_XM_Gyro_Registers_t regAdress, uint8_t value){

	HI2C_write(S_Imu[pos].port, S_Imu[pos].gyroAddress,(uint8_t)regAdress,&value,1);
}
static void writeAccelRegisterValue(RIMU_IMUPosition_e pos,lsm9ds1_XM_Gyro_Registers_t regAdress, uint8_t value){

	HI2C_write(S_Imu[pos].port, S_Imu[pos].accelAddress,(uint8_t)regAdress,&value,1);
}

static Bool_t initializeAccel(RIMU_IMUPosition_e pos){

	uint8_t regValue;
	Bool_t retVal = true;

	// 0b00111000 (ACC X,Y,Z enabled)
	writeAccelRegisterValue(pos,LSM9DS1_REGISTER_CTRL_REG5_XL,0x38u);
	regValue = readAccelRegisterValue(pos,LSM9DS1_REGISTER_CTRL_REG5_XL);
	if(regValue != 0x38u){
		retVal = false;
	}
	// 0b10110000 (ODR 476;+-4g;)
	writeAccelRegisterValue(pos,LSM9DS1_REGISTER_CTRL_REG6_XL,0x38u);
	regValue = readAccelRegisterValue(pos,LSM9DS1_REGISTER_CTRL_REG6_XL);
	if(regValue != 0x38u){
		retVal = false;
	}

	writeAccelRegisterValue(pos,LSM9DS1_REGISTER_CTRL_REG4, 0x38u);
	regValue = readAccelRegisterValue(pos,LSM9DS1_REGISTER_CTRL_REG4);
	if(regValue != 0x38u){
		retVal = false;
	}

	return retVal;
}

static Bool_t initializeGyro(RIMU_IMUPosition_e pos){

	uint8_t regValue;
	Bool_t retVal = true;

	// 0b10101010 (ODR 476;Cutoff 57; 500dps;)
	writeGyroRegisterValue(pos,LSM9DS1_REGISTER_CTRL_REG1_G,0xAAu);
	regValue = readGyroRegisterValue(pos,LSM9DS1_REGISTER_CTRL_REG1_G);
	if(regValue != 0xAAu){
		retVal = false;
	}
	// 0b01000000
	writeGyroRegisterValue(pos,LSM9DS1_REGISTER_CTRL_REG3_G,0x40u);
	regValue = readGyroRegisterValue(pos,LSM9DS1_REGISTER_CTRL_REG3_G);
	if(regValue != 0x40u){
		retVal = false;
	}
	return retVal;
}

static Double_t kalmanFilter(Double_t accelAngle, Double_t gyroAngle){


	Double_t convertedAccelAngle = accelAngle;
	Double_t convertedGyroAngle =  gyroAngle;
	Double_t error;
	Double_t filteredAngle;

	S_covMatrix[0][0] = S_covMatrix[0][0] + (DELTA_T * ((2.0 * S_covMatrix[0][1]) + (DELTA_T*S_covMatrix[1][1])));
	S_covMatrix[0][1] = S_covMatrix[0][1] + (DELTA_T* S_covMatrix[1][1]);

	S_covMatrix[0][0] = S_covMatrix[0][0] + (DELTA_T*0.001);
	S_covMatrix[1][1] = S_covMatrix[1][1] + (DELTA_T*0.001);


	S_gainMatrix[0] = S_covMatrix[0][0] / (S_covMatrix[0][0] + 0.03);
	S_gainMatrix[1] = S_covMatrix[0][1] / (S_covMatrix[0][1] + 0.03);

	error = convertedAccelAngle - convertedGyroAngle;
	filteredAngle = convertedGyroAngle + (S_gainMatrix[0] * error);

	S_covMatrix[0][0] = S_covMatrix[0][0]*(1.0-S_gainMatrix[0]);
	S_covMatrix[0][1] = S_covMatrix[0][1]*(1.0-S_gainMatrix[1]);
	S_covMatrix[1][1] = S_covMatrix[1][1] - (S_gainMatrix[1]*S_covMatrix[0][1]);

	return filteredAngle;
}

static void eraseCalibrationValues(void){

	uint32_t i;
	for(i = 0; i < NUMBEROFIMUS; ++i){
		S_AveragedAngle[i] = 0;
		S_OffsetAverage[i] = 0;
	}
	S_CalibrationCounter = 0;

}



/** @} */
