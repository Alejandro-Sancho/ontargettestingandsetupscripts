
#include "k66f.h"
#include "RMOT.h"
#include "RCAN.h"
#include "GINIT.h"
#include "RDAC.h"
#include "RIMU.h"
#include "HI2C.h"
#include "LSM9DS1_Registers.h"
#include "SEGGER_RTT.h"
#include "fsl_ftm.h"


#if PL_HAS_CloosedLoop_CANOpen
static void ClosedLoop_CAN(void *pvParameters);
#endif
#if PL_HAS_K66F_PI_SPI
static void K66F_PI_SPI(void *pvParameters);
#endif
#if PL_HAS_BatteryMntr_I2C
static void BatteryMntr_I2C(void *pvParameters);
#endif
static void SysMgmt_FAST(void *pvParameters);
static void SysMgmt_SLOW(void *pvParameters);
/* Task Priorities */
#define ClosedLoop_CAN_PRIORITY (configMAX_PRIORITIES - 1) /* Should always be highest */
#define K66F_PI_SPI_PRIORITY (configMAX_PRIORITIES - 2)
#define BatteryMntr_I2C_PRIORITY (configMAX_PRIORITIES - 3)
#define SysMgmt_FAST_PRIORITY (configMAX_PRIORITIES - 4)
#define SysMgmt_SLOW_PRIORITY (configMAX_PRIORITIES - 3)
/* CANBUS */
volatile bool txComplete = false;
volatile bool rxComplete = false;
#if PL_HAS_K66F_PI_SPI
void DSPI_SlaveUserCallback(SPI_Type *base, dspi_slave_handle_t *handle, status_t status, void *userData) {
    callback_message_t *cb_msg = (callback_message_t *)userData;
    BaseType_t reschedule;
    cb_msg->async_status = status;
    xSemaphoreGiveFromISR(cb_msg->sem, &reschedule);
    portYIELD_FROM_ISR(reschedule);
}


#endif

uint8_t canmsg_buf[200];
uint8_t IMUmsg_buf[200];

uint8_t i2c_buf_leftShank[10];
uint8_t i2c_buf_leftThigh[10];
uint8_t i2c_buf_rightShank[10];
uint8_t i2c_buf_rightThigh[10];
uint8_t i2c_buf_trunk[10];

uint8_t i2c_buf_print[200];

/*!
* @brief Application entry point. // Start scheduler
*/
void delay(void);
void delay(void)
{
    volatile uint32_t i = 0;
    for (i = 0; i < 100000; ++i)
    {
        __asm("NOP"); /* delay */
    }
}

// Flex timer to programm controlled motor movement
/* The Flextimer instance/channel used for board */
#define BOARD_FTM_BASEADDR FTM3
/* Interrupt number and interrupt handler for the FTM instance used */
#define BOARD_FTM_IRQ_NUM FTM3_IRQn
#define BOARD_FTM_HANDLER FTM3_IRQHandler
///* Get source clock for FTM driver */
//#define FTM_SOURCE_CLOCK (CLOCK_GetFreq(kCLOCK_BusClk)/4)

volatile bool ftmIsrFlag = false;
volatile uint32_t milisecondCounts = 0U;


int main(void)
{
	/* Init board hardware. */




	BOARD_InitPins();
    BOARD_BootClockHSRUN();
    SEGGER_SYSVIEW_Conf();

//    uint8_t msg_buf[100];
////
//    int32_t return_spri= sprintf(msg_buf, "%u %u \n", (uint32_t)(((int64_t)(-1.0))>>32),(uint32_t)(Double64_t)-1.0);
////    int32_t return_spri= sprintf(msg_buf, "%u %u \n", (uint64_t)(18446744073709551615));
//	SEGGER_RTT_WriteString(0, msg_buf);

    gpio_pin_config_t led_config = {
        kGPIO_DigitalOutput, 1,
    };
    RCAN_MotorData_s dataMotorRight;
    RCAN_MotorData_s dataMotorLeft;
    RMOT_MotorEncoders_s encoders;
    RIMU_DataStruct_s RIMU_Data;
    int32_t dataret;
    int32_t i=0;
    int32_t positiondata = 0;

    /* Init output LED GPIO. */
    GPIO_PinInit(GPIOB, 4, &led_config);

    GPIO_PinInit(GPIOB, 1, &led_config);

    GPIO_PinInit(GPIOB, 0, &led_config);

    GPIO_PinInit(GPIOA, 9, &led_config);


    WAIT1_Waitus(10000000); // start-up wait, to give motor controller some start-up time
    GPIO_PinInit(GPIOB, 5, &led_config);
	GPIO_TogglePinsOutput(GPIOB, 1u << 5);
	WAIT1_Waitus(1000000);
	GPIO_TogglePinsOutput(GPIOB, 1u << 5);
	CANOpenInit();
	RMOT_Initialize(E_GINIT_InitLevel1);
	RMOT_Initialize(E_GINIT_InitLevel2);

	//HI2C_Initialize( E_GINIT_InitLevel1 );
//	HI2C_Initialize( E_GINIT_InitLevel2 );
//    RIMU_Initialize(E_GINIT_InitLevel1);
//	RIMU_Initialize(E_GINIT_InitLevel2);

    RCAN_MotorCurrentsPerThousand_s motorCurrent;

//    motorCurrent.motorCurrentPerThousandRightSide = 0;
//    motorCurrent.motorCurrentPerThousandLeftSide = -50;

    dataMotorRight.motorCurrent = 0;
    dataMotorRight.motorEncoder = 0;
    dataMotorLeft.motorCurrent = 0;
    dataMotorLeft.motorEncoder = 0;

   RIMU_DataStruct_s imuDataRightShank;
   RIMU_DataStruct_s imuDataLeftShank;
   RIMU_DataStruct_s imuDataRightThigh;
   RIMU_DataStruct_s imuDataLeftThigh;
   RIMU_DataStruct_s imuDataTrunk;




   //RMOT TEST/////////////////////////
   RMOT_MotorForces_s motorforces;
   RMOT_MotorCurrents_s motorcurrents;
   RMOT_MotorEncoders_s motorencoders;


   motorforces.motorForceLeftSide = 6;
   motorforces.motorForceRightSide = 6;
   motorencoders.motorEncoderLeftSide = 0;
   motorencoders.motorEncoderRightSide = 0;
   motorcurrents.motorCurrentRightSide = 0;
   motorcurrents.motorCurrentLeftSide = 0;

    //HI2C_read(0, 0x6b, 0x0f, i2c_buf ,1);

	uint32_t timeDesired = 10U; //[ms]
	ftm_config_t ftmInfo;

	FTM_GetDefaultConfig(&ftmInfo);

	/* Divide FTM clock by 4 */
	ftmInfo.prescale = kFTM_Prescale_Divide_4;
	/* Initialize FTM module */
	FTM_Init(BOARD_FTM_BASEADDR, &ftmInfo);
	/* Set timer period: 1ms */
	FTM_SetTimerPeriod(BOARD_FTM_BASEADDR,
			USEC_TO_COUNT(1000U, FTM_SOURCE_CLOCK));
	FTM_EnableInterrupts(BOARD_FTM_BASEADDR, kFTM_TimeOverflowInterruptEnable);
	EnableIRQ(BOARD_FTM_IRQ_NUM);
	FTM_StartTimer(BOARD_FTM_BASEADDR, kFTM_SystemClock);

    for (;;)
    {
    	canmsg_buf[0]= '\0';
    	i2c_buf_print[0]= '\0';
    	i2c_buf_trunk[0] = '\0';
    	IMUmsg_buf[0] =  '\0';

/*************************RMOT TESTING ************************************************/
/**************************************************************************************/
    	/* If things need to be run at a certain time, it needs to go in that loop */
        if (ftmIsrFlag)
        {
            milisecondCounts ++;
            ftmIsrFlag = false;



//            //POSITION CONTROL
//            if (milisecondCounts == 6000){
//            RCAN_sendPositionToMotor( 88000, 1);
//
//            milisecondCounts = 0;
//            }
//
//            if (milisecondCounts == 3000){
//            RCAN_sendPositionToMotor( 0, 1);
//                WAIT1_Waitms(10);
//            RCAN_sendPositionToMotor( 0, 2);
//
//            }

            if (motorencoders.motorEncoderLeftSide == 0){
            	 RCAN_sendPositionToMotor( 88000, 1);
            	 RCAN_sendPositionToMotor( 0, 2);
            }

            if (motorencoders.motorEncoderLeftSide == 88000){
            	 RCAN_sendPositionToMotor( 0, 1);
            	 RCAN_sendPositionToMotor( 0, 2);
            }


            if(milisecondCounts % timeDesired == 0)
            {

            	RMOT_setForces( motorforces,  motorencoders);
            	motorcurrents = RMOT_getMotorCurrents( );
            	motorencoders = RMOT_getMotorEncoders( );

            	/* Printing */
                UTIL1_strcatNum32s(canmsg_buf, sizeof(canmsg_buf), motorencoders.motorEncoderLeftSide);
                UTIL1_chcat(canmsg_buf, sizeof(canmsg_buf), '\t');
            	UTIL1_strcatNum32s(canmsg_buf, sizeof(canmsg_buf), motorcurrents.motorCurrentLeftSide);
            	UTIL1_chcat(canmsg_buf, sizeof(canmsg_buf), '\t');

            	UTIL1_strcat(canmsg_buf, sizeof(canmsg_buf), (unsigned char*)"\r\n");
            	SEGGER_RTT_WriteString(0, canmsg_buf);


//            	motorCurrent.motorCurrentPerThousandLeftSide *=-1;
//                motorCurrent.motorCurrentPerThousandRightSide *=-1;

//                GPIO_TogglePinsOutput(GPIOB, 1u << 5);

//        	 	RCAN_sendCurrentToMotor(motorCurrent);

            	/* reset */
                //milisecondCounts = 0U;
            }
        }


/*************************RMOT TESTING END ************************************************/
 /**************************************************************************************/


    	//RCAN_getDataFromMotor( &dataMotorLeft, E_RCAN_Node1);
		//RCAN_getDataFromMotor( &dataMotorRight, E_RCAN_Node2);


		//RCAN_sendCurrentToMotor(motorCurrent);
		//RCAN_sendPositionToMotor(positiondata, E_RCAN_Node1);
		//RCAN_sendPositionToMotor(positiondata, E_RCAN_Node2);
		//positiondata = positiondata * -1;

        //RCAN_sendCurrentToMotor(motorCurrent);


//         UTIL1_strcatNum8s(canmsg_buf, sizeof(canmsg_buf), dataMotorLeft.motorCurrent);
//		 UTIL1_chcat(canmsg_buf, sizeof(canmsg_buf), '\t');
//		 UTIL1_strcatNum8s(canmsg_buf, sizeof(canmsg_buf), dataMotorLeft.motorEncoder);
//		 UTIL1_chcat(canmsg_buf, sizeof(canmsg_buf), '\t');
//		 UTIL1_strcat(canmsg_buf, sizeof(canmsg_buf), (unsigned char*)"\r\n");
//		 SEGGER_RTT_WriteString(0, canmsg_buf);


		 ////////////////////////////////IMU TEST/////////////////////////
//    	imuDataRightShank = RIMU_GetIMUAngleRaw (E_RIMU_RightShank);
//    	imuDataLeftShank = RIMU_GetIMUAngleRaw (E_RIMU_LeftShank);
//    	imuDataRightThigh = RIMU_GetIMUAngleRaw (E_RIMU_RightThigh);
//    	imuDataLeftThigh = RIMU_GetIMUAngleRaw (E_RIMU_LeftThigh);
//    	imuDataTrunk = RIMU_GetIMUAngleRaw (E_RIMU_Trunk);


    	//encoders= RMOT_getMotorEncoders();
//        HI2C_read(E_I2C_leftSide, XL_GYRO_TYPE_SA_HIGH, LSM9DS1_REGISTER_WHO_AM_I, i2c_buf_leftShank ,1);
//        HI2C_read(E_I2C_leftSide, XL_GYRO_TYPE_SA_LOW, LSM9DS1_REGISTER_WHO_AM_I, i2c_buf_leftThigh ,1);
//        HI2C_read(E_I2C_RightSide, XL_GYRO_TYPE_SA_HIGH, LSM9DS1_REGISTER_WHO_AM_I, i2c_buf_rightShank ,1);
//        HI2C_read(E_I2C_RightSide, XL_GYRO_TYPE_SA_LOW, LSM9DS1_REGISTER_WHO_AM_I, i2c_buf_rightThigh ,1);
//        HI2C_read(E_I2C_Trunk, XL_GYRO_TYPE_SA_HIGH, LSM9DS1_REGISTER_WHO_AM_I, i2c_buf_trunk ,1);
        //HI2C_read(4, 0x6b, 0x0f, i2c_buf ,1); batteries

        //delay();
    	//RCAN_sendCurrentToMotor(motorCurrent);
    	    //RIMU_Data = RIMU_GetIMUAngleRaw( E_RIMU_RightShank );
    	//Data that we want to print out (USE TELNET)



//            UTIL1_strcatNum8s(i2c_buf_print, sizeof(i2c_buf_print), i2c_buf_leftShank[0]);
//		    UTIL1_chcat(i2c_buf_print, sizeof(i2c_buf_print), '\t');
//            UTIL1_strcatNum8s(i2c_buf_print, sizeof(i2c_buf_print), i2c_buf_leftThigh[0]);
//		    UTIL1_chcat(i2c_buf_print, sizeof(i2c_buf_print), '\t');
//            UTIL1_strcatNum8s(i2c_buf_print, sizeof(i2c_buf_print), i2c_buf_rightShank[0]);
//		    UTIL1_chcat(i2c_buf_print, sizeof(i2c_buf_print), '\t');
//            UTIL1_strcatNum8s(i2c_buf_print, sizeof(i2c_buf_print), i2c_buf_rightThigh[0]);
//		    UTIL1_chcat(i2c_buf_print, sizeof(i2c_buf_print), '\t');
//		    UTIL1_strcatNum8s(i2c_buf_print, sizeof(i2c_buf_print), i2c_buf_trunk[0]);
//		    UTIL1_chcat(i2c_buf_print, sizeof(i2c_buf_print), '\t');
//		    UTIL1_strcat(i2c_buf_print, sizeof(i2c_buf_print), (unsigned char*)"\r\n");
//		    SEGGER_RTT_WriteString(0, i2c_buf_print);


//		    UTIL1_strcatNum32s(IMUmsg_buf, sizeof(IMUmsg_buf), imuDataRightShank.imuAngle);
//	     	UTIL1_chcat(IMUmsg_buf, sizeof(IMUmsg_buf), '\t');
//     		UTIL1_strcatNum32s(IMUmsg_buf, sizeof(IMUmsg_buf), imuDataLeftShank.imuAngle);
//		    UTIL1_chcat(IMUmsg_buf, sizeof(IMUmsg_buf), '\t');
//
//		    UTIL1_strcatNum32s(IMUmsg_buf, sizeof(IMUmsg_buf), imuDataRightThigh.imuAngle);
//		    UTIL1_chcat(IMUmsg_buf, sizeof(IMUmsg_buf), '\t');
//		    UTIL1_strcatNum32s(IMUmsg_buf, sizeof(IMUmsg_buf), imuDataLeftThigh.imuAngle);
//		    UTIL1_chcat(IMUmsg_buf, sizeof(IMUmsg_buf), '\t');
//
//	        UTIL1_strcatNum32s(IMUmsg_buf, sizeof(IMUmsg_buf), imuDataTrunk.imuAngle);
//    		UTIL1_chcat(IMUmsg_buf, sizeof(IMUmsg_buf), '\t');
//    		UTIL1_strcat(IMUmsg_buf, sizeof(IMUmsg_buf), (unsigned char*)"\r\n");
//    		SEGGER_RTT_WriteString(0, IMUmsg_buf);




       	    //motorCurrent.motorCurrentPerThous� andLeftSide = motorCurrent.motorCurrentPerThousandLeftSide*-1;
            //motorCurrent.motorCurrentPerThousandRightSide = motorCurrent.motorCurrentPerThousandRightSide*-1;



    	 	 //RCAN_sendCurrentToMotor(motorCurrent);

    }
    __WFI(); /* wait for interrupt */
}


void BOARD_FTM_HANDLER(void)
{
    /* Clear interrupt flag.*/
    FTM_ClearStatusFlags(BOARD_FTM_BASEADDR, kFTM_TimeOverflowFlag);
    ftmIsrFlag = true;
}



/* TASKS */
#if PL_HAS_CloosedLoop_CANOpen
static void ClosedLoop_CAN(void *pvParameters) {
/*    TickType_t xLastWakeTime;

     Declare and initialize motor values

    int16_t MotorCurrentRead_right = 0;
    int16_t MotorCurrentRead_left = 0;
    uint16_t temp_amp_right = 0;
    uint16_t temp_amp_left = 0;
    uint16_t ForceSetpointRight=0;
    uint16_t ForceSetpointLeft=0;
    float ForceSetpointRight_scaled=0;
    float ForceSetpointLeft_scaled=0;
    float OutputCurrentRight=0;
    float OutputCurrentLeft=0;

    //Inertia Moments
    float Jmotor= 0.0000023;
    float Jbiggear = 0.000004606;
    float Jshaftout = 0.000000271;
    //Geometries radius (in meter)
    float biggear = 0.022;
    float motorshaft = 0.006;
    float outputshaft= 0.004;

    //dt (in seconds)
    float dt=0.02;

    int32_t EncoderCountsRight = 0;
    int32_t EncoderCountsLeft = 0;
    int32_t EncoderCountsRightPrev=0;
    int32_t EncoderCountsLeftPrev=0;

    float AngularVelocityRight=0;
    float AngularVelocityRightPrev=0;
    float AngularVelocityLeft=0;
    float AngularVelocityLeftPrev=0;
    float AngularAccelRight=0;
    float AngularAccelLeft=0;


    double TorqueFrictionRight=0;
    float TorqueFrictionLeft=0;
    float TorqueMotorRight=0;
    float TorqueMotorLeft=0;
    float TorqueSetpointRight=0;
    float TorqueSetpointLeft=0;
    float TorqueTotalRight=0;
    float TorqueTotalLeft=0;
    float MotorConstant = 0.0921;
    float ramp = 0;

   int32_t BufferEncoder = 0;

     Analog
    uint16_t load_right = 0;
    uint16_t load_left = 0;
    uint8_t AnalogVector [TRANSFER_SIZE]= {0};
    uint8_t ForceVector [TRANSFER_SIZE]= {0};
    adc16_config_t adc16ConfigStruct1;
    adc16_config_t adc16ConfigStruct2;
    adc16_channel_config_t adc16ChannelConfigStruct1;
    //adc16_channel_config_t adc16ChannelConfigStruct2;
    adc16_channel_config_t adc16ChannelConfigStruct3;
    //adc16_channel_config_t adc16ChannelConfigStruct4;

    //sends data from canbus task to spi task
    xQueue1 = xQueueCreate( 32, sizeof(AnalogVector) );
    //sends data from spi task to canbus task
    xQueue2 = xQueueCreate( 32, sizeof(AnalogVector) );
    //Analog reading init and set
    ADC16_GetDefaultConfig(&adc16ConfigStruct1);
    ADC16_GetDefaultConfig(&adc16ConfigStruct2);

	#ifdef BOARD_ADC_USE_ALT_VREF
    	adc16ConfigStruct1.referenceVoltageSource = kADC16_ReferenceVoltageSourceValt;
    	adc16ConfigStruct2.referenceVoltageSource = kADC16_ReferenceVoltageSourceValt;
	#endif

    ADC16_Init(ADC16_BASE1, &adc16ConfigStruct1);
    ADC16_Init(ADC16_BASE2, &adc16ConfigStruct2);
    ADC16_EnableHardwareTrigger(ADC16_BASE1, false);  //Make sure the software trigger is used.
    ADC16_EnableHardwareTrigger(ADC16_BASE2, false);  //Make sure the software trigger is used.

#if defined(FSL_FEATURE_ADC16_HAS_CALIBRATION) && FSL_FEATURE_ADC16_HAS_CALIBRATION
    if (kStatus_Success == ADC16_DoAutoCalibration(ADC16_BASE1) && kStatus_Success == ADC16_DoAutoCalibration(ADC16_BASE2)) {
//        PRINTF("ADC16_DoAutoCalibration() Done.\r\n");
    } else {
//        PRINTF("ADC16_DoAutoCalibration() Failed.\r\n");
    }
#endif  FSL_FEATURE_ADC16_HAS_CALIBRATION
    adc16ChannelConfigStruct1.channelNumber = ADC16_USER_CHANNEL2;
    adc16ChannelConfigStruct1.enableInterruptOnConversionCompleted = false;
    //adc16ChannelConfigStruct2.channelNumber = ADC16_USER_CHANNEL1;
    //adc16ChannelConfigStruct2.enableInterruptOnConversionCompleted = false;
    adc16ChannelConfigStruct3.channelNumber = ADC16_USER_CHANNEL3;
    adc16ChannelConfigStruct3.enableInterruptOnConversionCompleted = false;
    //adc16ChannelConfigStruct4.channelNumber = ADC16_USER_CHANNEL4;
    //adc16ChannelConfigStruct4.enableInterruptOnConversionCompleted = false;
#if defined(FSL_FEATURE_ADC16_HAS_DIFF_MODE) && FSL_FEATURE_ADC16_HAS_DIFF_MODE
    adc16ChannelConfigStruct1.enableDifferentialConversion = false;
    //adc16ChannelConfigStruct2.enableDifferentialConversion = false;
    adc16ChannelConfigStruct3.enableDifferentialConversion = false;
    //adc16ChannelConfigStruct4.enableDifferentialConversion = false;
#endif  FSL_FEATURE_ADC16_HAS_DIFF_MODE

uint8_t canmsg_buf[200];

uint32_t counter = -1;

RNG5_Init();

for (;;) {

	canmsg_buf[0]= '\0';
	//xLastWakeTime = xTaskGetTickCount();
	counter++;

	//force setpoint receive through queue from spi
	if( xQueue2 != 0 ){
		xQueueReceive( xQueue2, &ForceVector, ( TickType_t ) 0 ) ;
		xQueueReset( xQueue2 );
	}

	//Bitshift bytes of received array
	ForceSetpointRight = ((int16_t)ForceVector[1]) << 8;
	ForceSetpointRight |= ForceVector[0];
	ForceSetpointLeft = ((int16_t)ForceVector[3]) << 8;
	ForceSetpointLeft |= ForceVector[2];
	ForceSetpointRight_scaled = ForceSetpointRight/100.0;
	ForceSetpointLeft_scaled = ForceSetpointLeft/100.0;
	//ForceSetpointRight_scaled = 5;
	//ForceSetpointLeft_scaled = 5;

	//Send TPDO SYNC and poll reply
	CANOpenNMTandSYNC(CANOpen_SYNC);
	CANOpenReadReply(&EncoderCountsRight, &MotorCurrentRead_right, &temp_amp_right, Motor_right);
	CANOpenReadReply(&EncoderCountsLeft, &MotorCurrentRead_left, &temp_amp_left, Motor_left);



	//POSITION CONTROL CODE ---------------------------------------------------------------------

	 //Change CANopen.c init function for position control

	if(counter == 0){
		//CANOpenPDOSetPosition(100000,Motor_right);
		//vTaskDelay(1);
		CANOpenPDOSetPosition(30000,Motor_left);
	}
	 else if(counter == 3000){
		//CANOpenPDOSetPosition(20000,Motor_right);
		//vTaskDelay(1);
		CANOpenPDOSetPosition(0,Motor_left);
	}
	else if (counter == 6000) {
		counter = -1;
	}
	//------------------------------------------------------------------------------------------------




	//TORQUE CONTROL CODE ---------------------------------------------------------------------
	//Update  encoder counts and angular velocity every 20 samples

	if (counter%20==0){
	//RNG5_Get(&BufferEncoder);
	AngularVelocityRight=2.0*PI*(float)(EncoderCountsRight-EncoderCountsRightPrev)/(2048.0*0.02); //RPS
	AngularAccelRight=(AngularVelocityRight-AngularVelocityRightPrev)/0.02;
	//RNG5_Put(EncoderCountsRight);


	AngularVelocityLeft=2.0*PI*(float)(EncoderCountsLeft-EncoderCountsLeftPrev)/(2048.0*0.02); //RPS
	AngularAccelLeft=(AngularVelocityLeft-AngularVelocityLeftPrev)/0.02;
	}

	//Torque Calculation RIGHT unit------------------------------------------------------------------
	TorqueSetpointRight= (ForceSetpointRight_scaled *motorshaft * motorshaft )/outputshaft ;
	// TorqueFrictionRight= (0.000088519*AngularVelocityRight);//+0.043758);//0.043758
	TorqueMotorRight=(Jmotor*AngularAccelRight+Jbiggear*Jshaftout*AngularAccelRight*(motorshaft/biggear));
	//if friction modeled as quadratic T=2.54759E*x^2-0.00029211x+0.180865

	//Set total torque and output current
	TorqueTotalRight=TorqueSetpointRight;// + TorqueMotorRight;// + TorqueFrictionRight;
	OutputCurrentRight = TorqueTotalRight/MotorConstant; //11.64 is max current
	OutputCurrentRight =(OutputCurrentRight*1000.0)/11.64;


	//Torque Calculation LEFT unit--------------------------------------------------------------------
	TorqueSetpointLeft= (ForceSetpointLeft_scaled *motorshaft * motorshaft )/outputshaft ;
	TorqueFrictionLeft= 0.000088519*AngularVelocityLeft+0.043758;
	TorqueMotorLeft=Jmotor*AngularAccelLeft+Jbiggear*Jshaftout*AngularAccelLeft*(motorshaft/biggear);

	//Set total torque and output current
	TorqueTotalLeft=TorqueSetpointLeft; //+ TorqueMotorLeft;//+ TorqueFrictionLeft;
	OutputCurrentLeft = TorqueTotalLeft/MotorConstant; //11.64 is max current
	OutputCurrentLeft = (OutputCurrentLeft*-1000.0)/11.64;



	//RAMP TEST: output ramp as current if you want a ramp test

	if (counter<=1000 && counter >=0){
	   ramp=ramp+0.1;
	}
	else if(counter>=2000 && counter<=3000){
	   ramp=50;
	}
	else if(counter>3000 && counter<=4000){
		   ramp=100;
		}
	else {
	   counter=2000;
	}


//	if(counter>20000){
//	   ramp=0;
//	}


	//RIGHT TORQUE CONTROL
	if (AngularVelocityRight >=0){
	   OutputCurrentRight = OutputCurrentRight + 60;
	}

	else{
	   OutputCurrentRight= OutputCurrentRight - 70;
	}


	//LEFT FORCE CONTROL (change signs of current)
	if (AngularVelocityLeft >0 ){
	   OutputCurrentLeft = OutputCurrentLeft + 60;
	}

	else{
	   OutputCurrentLeft= OutputCurrentLeft - 60;
	}


	//Update both angular velocity and encoder counts
	AngularVelocityRightPrev=AngularVelocityRight;
	AngularVelocityLeftPrev=AngularVelocityLeft;
	EncoderCountsRightPrev=EncoderCountsRight;
	EncoderCountsLeftPrev=EncoderCountsLeft;

	//Data that we want to print out (USE TELNET)
	//UTIL1_strcatNum32s(canmsg_buf, sizeof(canmsg_buf), EncoderCountsLeft);
	//UTIL1_chcat(canmsg_buf, sizeof(canmsg_buf), '\t');
	UTIL1_strcatNum16s(canmsg_buf, sizeof(canmsg_buf), MotorCurrentRead_right);
	UTIL1_chcat(canmsg_buf, sizeof(canmsg_buf), '\t');
	UTIL1_strcatNum32sDotValue100(canmsg_buf, sizeof(canmsg_buf), ForceSetpointRight_scaled*100);
	UTIL1_chcat(canmsg_buf, sizeof(canmsg_buf), '\t');
	UTIL1_strcatNum32sDotValue100(canmsg_buf, sizeof(canmsg_buf), ForceSetpointLeft_scaled*100);
	UTIL1_chcat(canmsg_buf, sizeof(canmsg_buf), '\t');
	UTIL1_strcatNum32sDotValue100(canmsg_buf, sizeof(canmsg_buf), OutputCurrentRight*50);
	UTIL1_chcat(canmsg_buf, sizeof(canmsg_buf), '\t');
	UTIL1_strcat(canmsg_buf, sizeof(canmsg_buf), (unsigned char*)"\r\n");

	if (counter%50==0){
	   SEGGER_RTT_WriteString(0, canmsg_buf);
	}

	//Initial Pretension to overcome friction
	if (counter<=50){
	   OutputCurrentRight=100;
	   OutputCurrentLeft=-100;
	}

	//OutputCurrentLeft=-70;
	CANOpenPDOSetTorque(OutputCurrentRight, Motor_right);
	CANOpenPDOSetTorque(OutputCurrentLeft, Motor_left);



	//Analog Right
	ADC16_SetChannelConfig(ADC16_BASE1, ADC16_CHANNEL_GROUP, &adc16ChannelConfigStruct1);
	while (0U == (kADC16_ChannelConversionDoneFlag    & ADC16_GetChannelStatusFlags(ADC16_BASE1, ADC16_CHANNEL_GROUP))) {
	}
	//ADC16_SetChannelConfig(ADC16_BASE2, ADC16_CHANNEL_GROUP, &adc16ChannelConfigStruct2);
	//while (0U == (kADC16_ChannelConversionDoneFlag    & ADC16_GetChannelStatusFlags(ADC16_BASE2, ADC16_CHANNEL_GROUP))) {
	//}
	load_right = ADC16_GetChannelConversionValue(ADC16_BASE1, ADC16_CHANNEL_GROUP);
	//temp_right = ADC16_GetChannelConversionValue(ADC16_BASE2, ADC16_CHANNEL_GROUP);
	 //Analog Left
	ADC16_SetChannelConfig(ADC16_BASE1, ADC16_CHANNEL_GROUP, &adc16ChannelConfigStruct3);
	while (0U == (kADC16_ChannelConversionDoneFlag    & ADC16_GetChannelStatusFlags(ADC16_BASE1, ADC16_CHANNEL_GROUP))) {
		}
	//ADC16_SetChannelConfig(ADC16_BASE2, ADC16_CHANNEL_GROUP, &adc16ChannelConfigStruct4);
	//while (0U == (kADC16_ChannelConversionDoneFlag    & ADC16_GetChannelStatusFlags(ADC16_BASE2, ADC16_CHANNEL_GROUP))) {
	//    }
	load_left = ADC16_GetChannelConversionValue(ADC16_BASE1, ADC16_CHANNEL_GROUP);
	//temp_left = ADC16_GetChannelConversionValue(ADC16_BASE2, ADC16_CHANNEL_GROUP);


	//Vector Assembly to send to SPI task
	//RIGHT
	//encoder counts loading buffer
	AnalogVector[0]=(uint8_t)(EncoderCountsRight  & 0xff);
	AnalogVector[1]=(uint8_t)((EncoderCountsRight >> 8)  & 0xff);
	AnalogVector[2]=(uint8_t)((EncoderCountsRight >> 16)  & 0xff);
	AnalogVector[3]=(uint8_t)((EncoderCountsRight >> 24)  & 0xff);
	//loadcell loading buffer
	AnalogVector[4]=(uint8_t)(load_right  & 0xff);
	AnalogVector[5]=(uint8_t)((load_right >> 8)  & 0xff);
	//Motor controller temperature
	AnalogVector[6]=(uint8_t)(temp_amp_right  & 0xff);
	AnalogVector[7]=(uint8_t)((temp_amp_right >> 8)  & 0xff);

	//LEFT
	//Encoder counts loading buffer
	AnalogVector[8]=(uint8_t)(EncoderCountsLeft  & 0xff);
	AnalogVector[9]=(uint8_t)((EncoderCountsLeft >> 8)  & 0xff);
	AnalogVector[10]=(uint8_t)((EncoderCountsLeft >> 16)  & 0xff);
	AnalogVector[11]=(uint8_t)((EncoderCountsLeft >> 24)  & 0xff);
	//Loadcell loading buffer
	AnalogVector[12]=(uint8_t)(load_left  & 0xff);
	AnalogVector[13]=(uint8_t)((load_left >> 8)  & 0xff);
	//Motor controller temperature
	AnalogVector[14]=(uint8_t)(temp_amp_left  & 0xff);
	AnalogVector[15]=(uint8_t)((temp_amp_left >> 8)  & 0xff);


	//vTaskDelayUntil(&xLastWakeTime, 1);
	vTaskDelay(1);
	}*/
}


#endif
#if PL_HAS_K66F_PI_SPI
static void K66F_PI_SPI(void *pvParameters)
{
    TickType_t xLastWakeTime;
    uint8_t RAnalogVector[TRANSFER_SIZE]={0};
    uint32_t count=0;
    uint32_t i;
    uint8_t slaveReceiveBuffer[TRANSFER_SIZE] = {0};
    uint8_t slaveSendBuffer[TRANSFER_SIZE] = {0};
    dspi_slave_handle_t g_s_handle;
//    SemaphoreHandle_t dspi_sem;
    dspi_slave_config_t slaveConfig;
    dspi_transfer_t slaveXfer;
    callback_message_t cb_msg;
    cb_msg.sem = xSemaphoreCreateBinary();
//    dspi_sem = cb_msg.sem;
    if (cb_msg.sem == NULL) {
         PRINTF("DSPI slave: Error creating semaphore\r\n");
         vTaskSuspend(NULL); /* This task can only execute past the call to vTaskSuspend( NULL ) if
                                   *    another task has resumed (un-suspended) it using a call to vTaskResume() */
     }
    /*Slave config*/ // could be moved to driver file
    slaveConfig.whichCtar = kDSPI_Ctar0;
    slaveConfig.ctarConfig.bitsPerFrame = 8;
    slaveConfig.ctarConfig.cpol = kDSPI_ClockPolarityActiveHigh;
    slaveConfig.ctarConfig.cpha = kDSPI_ClockPhaseFirstEdge;
    slaveConfig.enableContinuousSCK = false;
    slaveConfig.enableRxFifoOverWrite = false;
    slaveConfig.enableModifiedTimingFormat = false;
    slaveConfig.samplePoint = kDSPI_SckToSin0Clock;
    slaveXfer.dataSize = TRANSFER_SIZE;
    slaveXfer.configFlags = kDSPI_SlaveCtar0;
/*  Set dspi slave interrupt priority higher. */
#if (DSPI_SLAVE_BASE == SPI0_BASE)
    NVIC_SetPriority(SPI0_IRQn, 5);
#elif(DSPI_SLAVE_BASE == SPI1_BASE)
    NVIC_SetPriority(SPI1_IRQn, 5);
#elif(DSPI_SLAVE_BASE == SPI2_BASE)
    NVIC_SetPriority(SPI2_IRQn, 5);
#elif(DSPI_SLAVE_BASE == SPI3_BASE)
    NVIC_SetPriority(SPI3_IRQn, 5);
#elif(DSPI_SLAVE_BASE == SPI4_BASE)
    NVIC_SetPriority(SPI4_IRQn, 5);
#endif
    DSPI_SlaveInit(DSPI_SLAVE_BASEADDR, &slaveConfig);
    for (;;){
        xLastWakeTime = xTaskGetTickCount();
          if( xQueue1 != 0 )
            {
                // Receive a message on the created queue.  Block for 10 ticks if a
                // message is not immediately available.
                if( xQueueReceive( xQueue1, &RAnalogVector, ( TickType_t ) 0 ) )
                {
                    //PRINTF("ENCSPI: %i \r\n", RAnalogVector[31]);
                }
                xQueueReset( xQueue1 );
            }
        //Prepare slave
        DSPI_SlaveTransferCreateHandle(DSPI_SLAVE_BASEADDR, &g_s_handle, DSPI_SlaveUserCallback, &cb_msg);
        //Set slave transfer ready to receive/send data
        slaveXfer.txData = RAnalogVector;
        slaveXfer.rxData = slaveReceiveBuffer;
        DSPI_SlaveTransferNonBlocking(DSPI_SLAVE_BASEADDR, &g_s_handle, &slaveXfer);
         //Wait for the transfer to finish
        xSemaphoreTake(cb_msg.sem, portMAX_DELAY);
        if (cb_msg.async_status == kStatus_Success) {
//            PRINTF("DSPI slave transfer completed successfully. \r\n\r\n");
        }
        else {
//            PRINTF("DSPI slave transfer completed with error. \r\n\r\n");
        }
        xQueueSend(xQueue2, &slaveReceiveBuffer, ( TickType_t ) 1 );
    }
    vTaskDelayUntil(&xLastWakeTime, 10);
}
#endif
#if PL_HAS_BatteryMntr_I2C
static void BatteryMntr_I2C(void *pvParameters) { /* Needs implementation of MAX17205 Library */
    uint32_t count=0;
    TickType_t xLastWakeTime;
    uint8_t g_master_buff_rx[I2C_DATA_LENGTH];
    uint8_t g_master_buff_tx[I2C_DATA_LENGTH] ;
//    i2c_master_handle_t g_m_handle;
    i2c_rtos_handle_t master_rtos_handle;
    i2c_master_config_t masterConfig;
    i2c_master_transfer_t masterXfer;
    uint32_t sourceClock;
    status_t status;
    I2C_MasterGetDefaultConfig(&masterConfig);
    masterConfig.baudRate_Bps = I2C_BAUDRATE;
    sourceClock = CLOCK_GetFreq(I2C_MASTER_CLK_SRC);
    status = I2C_RTOS_Init(&master_rtos_handle, I2C_MASTER, &masterConfig, sourceClock);
    NVIC_SetPriority(I2C1_IRQn, 6);
    for (;;){
        xLastWakeTime = xTaskGetTickCount();
         //Set up i2c master to send data to slave
        for (uint32_t i = 0; i < I2C_DATA_LENGTH; i++)
        {
            g_master_buff_tx[i] = 0;
        }
        /* Following code should be implemented in a function || driver */
        memset(&masterXfer, 0, sizeof(masterXfer));
        masterXfer.slaveAddress = I2C_MASTER_SLAVE_ADDR_7BIT;
        masterXfer.direction = kI2C_Write;
        masterXfer.subaddress = 0;
        masterXfer.subaddressSize = 0;
        masterXfer.data = g_master_buff_tx;
        masterXfer.dataSize = I2C_DATA_LENGTH;
        masterXfer.flags = kI2C_TransferDefaultFlag;
        status = I2C_RTOS_Transfer(&master_rtos_handle, &masterXfer);
        if (status != kStatus_Success) {
            //SEGGER_SYSVIEW_PrintfTarget("sent success ");
        }
        //Set up master to receive data from slave.
        for (uint32_t i = 0; i < I2C_DATA_LENGTH; i++) {
            g_master_buff_rx[i] = 0;
        }
        /* Following code should be implemented in a function || driver */
        masterXfer.slaveAddress = I2C_MASTER_SLAVE_ADDR_7BIT;
        masterXfer.direction = kI2C_Read;
        masterXfer.subaddress = 0;
        masterXfer.subaddressSize = 0;
        masterXfer.data = g_master_buff_rx;
        masterXfer.dataSize = I2C_DATA_LENGTH;
        masterXfer.flags = kI2C_TransferDefaultFlag;
        status = I2C_RTOS_Transfer(&master_rtos_handle, &masterXfer);
        count++;
//        if (status = kStatus_Success) {
//           xSemaphoreGive(transition);
//        }
        vTaskDelayUntil(&xLastWakeTime, 10);
    }
}
#endif
static void SysMgmt_FAST(void *pvParameters) {
    for(;;){
        vTaskDelay(1);
    }
}
static void SysMgmt_SLOW(void *pvParameters) {
    /*ftm_config_t ftmInfo;
    uint8_t dutycycle = 50U;
    uint16_t BatteryRead;
    uint16_t temp_right = 0;
    uint16_t temp_left = 0;
    adc16_config_t adc16ConfigStruct1;
    adc16_config_t adc16ConfigStruct2;
    adc16_channel_config_t adc16ChannelConfigStruct2;
    adc16_channel_config_t adc16ChannelConfigStruct4;
    adc16_channel_config_t adc16ChannelConfigStruct5;
    ftm_chnl_pwm_signal_param_t ftmParam[1];
     Configure ftm params with frequency 25kHZ
    ftmParam[0].chnlNumber = (ftm_chnl_t) BOARD_FIRST_FTM_CHANNEL;
    ftmParam[0].level = kFTM_LowTrue;
    ftmParam[0].dutyCyclePercent = 50U;
    ftmParam[0].firstEdgeDelayPercent = 0U;
    FTM_GetDefaultConfig(&ftmInfo);
    //FTM_Init(BOARD_FTM_BASEADDR, &ftmInfo);
    //FTM_SetupPwm(BOARD_FTM_BASEADDR, ftmParam, 1U, kFTM_EdgeAlignedPwm, 25000U, FTM_SOURCE_CLOCK);
    //FTM_StartTimer(BOARD_FTM_BASEADDR, kFTM_SystemClock);
    //Analog reading init and setup
    ADC16_GetDefaultConfig(&adc16ConfigStruct1);
    ADC16_GetDefaultConfig(&adc16ConfigStruct2);
#ifdef BOARD_ADC_USE_ALT_VREF
    adc16ConfigStruct1.referenceVoltageSource = kADC16_ReferenceVoltageSourceValt;
    adc16ConfigStruct2.referenceVoltageSource = kADC16_ReferenceVoltageSourceValt;
#endif
    ADC16_Init(ADC16_BASE1, &adc16ConfigStruct1);
    ADC16_Init(ADC16_BASE2, &adc16ConfigStruct2);
    ADC16_EnableHardwareTrigger(ADC16_BASE1, false);  Make sure the software trigger is used.
    ADC16_EnableHardwareTrigger(ADC16_BASE2, false);  Make sure the software trigger is used.
#if defined(FSL_FEATURE_ADC16_HAS_CALIBRATION) && FSL_FEATURE_ADC16_HAS_CALIBRATION
    if (kStatus_Success == ADC16_DoAutoCalibration(ADC16_BASE1) && kStatus_Success == ADC16_DoAutoCalibration(ADC16_BASE2)) {
//        PRINTF("ADC16_DoAutoCalibration() Done.\r\n");
    } else {
//        PRINTF("ADC16_DoAutoCalibration() Failed.\r\n");
    }
#endif  FSL_FEATURE_ADC16_HAS_CALIBRATION
    adc16ChannelConfigStruct2.channelNumber = ADC16_USER_CHANNEL1;
    adc16ChannelConfigStruct2.enableInterruptOnConversionCompleted = false;
    adc16ChannelConfigStruct4.channelNumber = ADC16_USER_CHANNEL4;
    adc16ChannelConfigStruct4.enableInterruptOnConversionCompleted = false;
    adc16ChannelConfigStruct5.channelNumber = ADC16_USER_CHANNEL5;
    adc16ChannelConfigStruct5.enableInterruptOnConversionCompleted = false;
#if defined(FSL_FEATURE_ADC16_HAS_DIFF_MODE) && FSL_FEATURE_ADC16_HAS_DIFF_MODE
    adc16ChannelConfigStruct2.enableDifferentialConversion = false;
    adc16ChannelConfigStruct4.enableDifferentialConversion = false;
    adc16ChannelConfigStruct5.enableDifferentialConversion = false;
#endif  FSL_FEATURE_ADC16_HAS_DIFF_MODE
    for (;;) {
        //Analog readings: temperature and battery
         Temperature Read
        ADC16_SetChannelConfig(ADC16_BASE2, ADC16_CHANNEL_GROUP, &adc16ChannelConfigStruct2);
        while (0U == (kADC16_ChannelConversionDoneFlag    & ADC16_GetChannelStatusFlags(ADC16_BASE2, ADC16_CHANNEL_GROUP))) {
        }
        temp_right = ADC16_GetChannelConversionValue(ADC16_BASE2, ADC16_CHANNEL_GROUP);
        ADC16_SetChannelConfig(ADC16_BASE2, ADC16_CHANNEL_GROUP, &adc16ChannelConfigStruct4);
        while (0U == (kADC16_ChannelConversionDoneFlag    & ADC16_GetChannelStatusFlags(ADC16_BASE2, ADC16_CHANNEL_GROUP))) {
        }
        temp_left = ADC16_GetChannelConversionValue(ADC16_BASE2, ADC16_CHANNEL_GROUP);
         Battery Read
        ADC16_SetChannelConfig(ADC16_BASE2, ADC16_CHANNEL_GROUP, &adc16ChannelConfigStruct5);
        while (0U == (kADC16_ChannelConversionDoneFlag    & ADC16_GetChannelStatusFlags(ADC16_BASE2, ADC16_CHANNEL_GROUP))) {
        }
        BatteryRead = ADC16_GetChannelConversionValue(ADC16_BASE2, ADC16_CHANNEL_GROUP);
        //PRINTF("Readings: %i   %i   %i \r\n", temp_right, temp_left, BatteryRead);
//        Start PWM mode with updated duty cycle
//FTM_UpdatePwmDutycycle(BOARD_FTM_BASEADDR, (ftm_chnl_t)BOARD_FIRST_FTM_CHANNEL, kFTM_EdgeAlignedPwm, dutycycle);
//        Software trigger to update registers
//FTM_SetSoftwareTrigger(BOARD_FTM_BASEADDR, true);
        vTaskDelay(10);
    }*/
}

