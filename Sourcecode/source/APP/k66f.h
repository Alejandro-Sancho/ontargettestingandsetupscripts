/*!
 * @brief Myoswiss Demonstrator Code V.1.0
 * @
 *
 *
 *
 * @param base            CMT peripheral base address.
 * @param config          The CMT basic configuration structure.
 * @param busClock_Hz     The CMT module input clock - bus clock frequency.
 */

#ifndef K66F_H_
#define K66F_H_

/*general*/
#include "stdlib.h"
#include "math.h"

#include "Platform.h"
//#include "CANbus.h"
//#include "Analog.h"

/* FreeRTOS kernel includes. */
//#include "SEGGER_SYSVIEW_FreeRTOS.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "semphr.h"
/* imported from the previous SDK */
#include "WAIT1.h"
#include "UTIL1.h"
#include "RNG5.h"

/* Freescale includes. */
#include "fsl_device_registers.h"
#include "fsl_debug_console.h"
#include "fsl_dspi.h"
#include "fsl_dspi_freertos.h"
#include "fsl_i2c.h"
#include "fsl_i2c_freertos.h"
#include "fsl_ftm.h"
//#include "fsl_flexcan.h"
#include "fsl_gpio.h"
//#include "fsl_adc16.h"
#include "board.h"
#include "pin_mux.h"
#include "clock_config.h"

//#include "median.c"

/* SEGGER */
#include "SEGGER_SYSVIEW_Config_FreeRTOS.c"
#include "CANOpen.h"
#include "SEGGER_RTT.h"


/* Calc definitions */
#define	PI 				3.1415926535897932385
#define SQRT2			1.4142135623730950488
#define SAMPLERATE		1000
#define CUTOFF			50
#define g				9.81
#define MIN_FORCE   	5

/* LED BAR Values */
#define LED1		409u
#define LED2		819u
#define LED3		1228u
#define LED4		1638u
#define LED5		2047u
#define LED6		2457u
#define LED7		2866u
#define LED8		3200u
#define LED9		3600u
#define LED10		4095u

/* SPI definitions */
#define DSPI_SLAVE_BASE (SPI0_BASE)
#define DSPI_SLAVE_BASEADDR ((SPI_Type *)DSPI_SLAVE_BASE)
#define TRANSFER_SIZE (16)         /*! Transfer size */

/* I2C definitions */
#define I2C_MASTER_BASE I2C1_BASE
#define I2C_MASTER ((I2C_Type *)I2C_MASTER_BASE)
#define I2C_MASTER_CLK_SRC I2C1_CLK_SRC
#define I2C_MASTER_SLAVE_ADDR_7BIT (0x68)
#define I2C_BAUDRATE (400000) /* 100K */
#define I2C_DATA_LENGTH (1)  /* MAX is 256 */

/* PWM/Flextimer definitions */
//#define BOARD_FTM_BASEADDR FTM0
#define BOARD_FIRST_FTM_CHANNEL 2U
/* Get source clock for FTM driver */
//#define FTM_SOURCE_CLOCK CLOCK_GetFreq(kCLOCK_BusClk)
#define FTM_SOURCE_CLOCK (CLOCK_GetFreq(kCLOCK_BusClk)/4)

/* Analog Data definitions */
#define ADC16_BASE1 ADC0
#define ADC16_BASE2 ADC1
#define ADC16_CHANNEL_GROUP 0U
#define ADC16_USER_CHANNEL1 16U
#define ADC16_USER_CHANNEL2 13U
#define ADC16_USER_CHANNEL3 16U
#define ADC16_USER_CHANNEL4 10U
#define ADC16_USER_CHANNEL5 11U

#define Motor_right 1
#define Motor_left 2

/*handles for the queues used in communication between analog and spi task*/
QueueHandle_t xQueue1, xQueue2;

/* Data structure and callback function for slave SPI communication. */
typedef struct _callback_message_t {
    status_t async_status;
    SemaphoreHandle_t sem;
}   callback_message_t;

/* Define an enumerated type used to identify the source of the data. */
typedef enum {
	I2C_Bat, SPI_PI, CANbus_Motor
} DataSource_t;

/* Define the structure type that will be passed on the queue. */
typedef struct {
	uint8_t *data;
	DataSource_t eDataSource;
} Data_t;

void DSPI_SlaveUserCallback(SPI_Type *base, dspi_slave_handle_t *handle, status_t status, void *userData);

#endif /*K66F_H_ */
