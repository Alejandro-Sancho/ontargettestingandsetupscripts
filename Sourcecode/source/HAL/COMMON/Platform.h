/*
 * Platform.h
 *
 *  Created on: 14.02.2014
 *      Author: tastyger
 */

#ifndef PLATFORM_H_
#define PLATFORM_H_

#include "board.h"
#include "FreeRTOSConfig.h"

//#define configSD  (defined(PEcfg_SD) && PEcfg_SD==1)
//#define configRF  (defined(PEcfg_RF) && PEcfg_RF==1)

#define PL_HAS_BLUETOOTH      (1) /* No Bluetooth module */
#define PL_HAS_SHELL          (0) /* Only needed when commands are being sent to the control unit */
#define PL_HAS_RTOS           (1)
#define PL_HAS_KEYS           (1)
#define PL_HAS_ACCELEROMETER  (1) /* Battery gauge connected, but not used at the moment, battery is measured via voltage divider on unit left */
#define PL_HAS_RADIO          (0)
#define PL_HAS_RSTDIO         (1 && PL_HAS_RADIO)
#define PL_HAS_SD_CARD        (0) /* Disabled/Not Implemented at the moment, FAT protocol always copies blocks (min 512KB) that are too big to save in less than 1ms */
#define PL_HAS_DEBUG_PRINT    (1) /* By default is this the RTT terminal, when running code without looking at the msges, change Shell to BT to prevent overflow events */
#define PL_HAS_RTOS_TRACE     (configUSE_TRACE_HOOKS)
#define PL_HAS_CANOpen 		  (1)
#define PL_HAS_IMU			  (1) /* IMU at the Shin */
#define PL_HAS_B2B_UART		  (0) /* Serial Board-2-Board (B2B) communication */
#endif /* PLATFORM_H_ */
