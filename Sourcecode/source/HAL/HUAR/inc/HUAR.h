/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation: HTIM Timer HAL Layer
 ***********************************************************************/

/** @defgroup HUAR UART component of the HAL layer
 * @{
 * @brief
 * The next blank line is mandatory!
 *
 * @file
 * This header file contains the declaration of the public
 * interface to HAL component HUAR.
 */

#ifndef HUAR_H
#define HUAR_H

/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/

#include "types.h"
#include "GINIT.h"

/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/
/**
 * Return values of HUAR Component
 */
typedef enum tag_HUAR_Return_e
{
	E_HUAR_ReturnSuccess,
	E_HUAR_ReturnFailure
}HUAR_Return_e;

/**
 * The configured ports for each uart which is used in system
 */
typedef enum tag_HUAR_Port_e{
	E_HUAR_Display
}HUAR_Port_e;
/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL FUNCTION PROTOTYPES                                          */
/***********************************************************************/

/** Initialize the HUAR-Module
 *  @return E_GINIT_ReturnFailure if failed otherwise returns E_GINIT_ReturnSuccessfull
 */
GINIT_Return_e HUAR_Initialize(GINIT_InitLevel_e initLevel);

/** Reads in a blocking mode from UART
 * @param port to read the data from
 * @param pointer to the where read data will be stored
 * @param number of bytes will be read
 * @return E_HUAR_ReturnSuccessful if successfully read, HUAR_ReturnFailure if an error occured during read
 */
HUAR_Return_e HUAR_Read(HUAR_Port_e port, uint8_t *data, size_t length);

/** Writes in a blocking mode to UART
 * @param port to send the data
 * @param pointer to the data which will be written
 * @param length number of bytes will be written
 */
void HUAR_Write(HUAR_Port_e port, uint8_t *data, size_t length);


#endif /* HUAR_H */

/** @} */
