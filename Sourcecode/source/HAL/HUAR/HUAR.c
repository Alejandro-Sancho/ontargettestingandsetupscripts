/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation: RDAC Data Acquisition module of RTE layer
 ***********************************************************************/

/** @ HTIM Timer module of HAL layer
 * @{
 * @file
 * This source file contains the definition of the functions of
 * HAL component HTIM Timer module.
 */

/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/
#include "HUAR.h"
#include "GINIT.h"
#include "fsl_uart.h"
#include "board.h"

#include "pin_mux.h"
#include "clock_config.h"
/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/


/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/
typedef struct tag_HUAR_PortConfig_s
{
    uart_config_t config;
	UART_Type *base;
	uint32_t uartClock_Hz;

} HUAR_PortConfig_s;

/***********************************************************************/
/* LOCAL CONSTANTS                                                     */
/***********************************************************************/
enum{
	numOfPorts = 1
};
/***********************************************************************/
/* LOCAL VARIABLES                                                     */
/***********************************************************************/
static HUAR_PortConfig_s ports[numOfPorts];

/***********************************************************************/
/* LOCAL FUNCTION PROTOTYPES                                           */
/***********************************************************************/
/***********************************************************************/
/* GLOBAL FUNCTIONS                                                    */
/***********************************************************************/
GINIT_Return_e HUAR_Initialize(GINIT_InitLevel_e initLevel){

	switch(initLevel){

	case E_GINIT_InitLevel1 :
		UART_GetDefaultConfig(&ports[E_HUAR_Display].config);
		ports[E_HUAR_Display].base = UART2;
		ports[E_HUAR_Display].uartClock_Hz = CLOCK_GetFreq(BUS_CLK); // for uart 2, BUS_CLK instead of SysClk is needed
		ports[E_HUAR_Display].config.enableTx = true;
		ports[E_HUAR_Display].config.enableRx = true;
		ports[E_HUAR_Display].config.baudRate_Bps = 9600u;
		break;
	case E_GINIT_InitLevel2 :
	    UART_Init(ports[E_HUAR_Display].base, &ports[E_HUAR_Display].config, ports[E_HUAR_Display].uartClock_Hz);
		break;
	default:
		break;

	}
	return E_GINIT_ReturnSuccess;
}


HUAR_Return_e HUAR_Read(HUAR_Port_e port, uint8_t *data, size_t length){

	status_t retValUart;
	HUAR_Return_e retVal;
	retVal = E_HUAR_ReturnSuccess;

	retValUart = UART_ReadBlocking(ports[port].base,data,length);
	if(retValUart != kStatus_Success){
		retVal = E_HUAR_ReturnFailure;
	}

	return retVal;
}

void HUAR_Write(HUAR_Port_e port, uint8_t *data, size_t length){

	UART_WriteBlocking(ports[port].base,data,length);
}


/** @} */
