/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation:
 ***********************************************************************/

/** @defgroup HI2C I2C component of the HAL layer
 * @{
 * @brief
 * The next blank line is mandatory!
 *
 * @file
 * This header file contains the declaration of the public or private
 * interface to HAL component I2C.
 */

#ifndef HI2C_H
#define HI2C_H

/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/

#include "types.h"
#include "GINIT.h"

/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/


typedef enum tag_HI2C_Port_e{
	E_I2C_leftSide,
	E_I2C_RightSide,
	E_I2C_Trunk,
	E_I2C_Batteries
}HI2C_Port_e;


/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL FUNCTION PROTOTYPES                                          */
/***********************************************************************/
/** Initialize the HI2C-Module
 *  @return E_GINIT_ReturnFailure if failed otherwise returns E_GINIT_ReturnSuccessfull
 */
GINIT_Return_e HI2C_Initialize(GINIT_InitLevel_e initLevel);

/** Reads in a blocking mode from UART
 * @param port to write the data
 * @param address the address of the client on the I2C bus
 * @param registerAdress the register to which will be written
 * @param buf pointer to the data will be written
 * @param bufSize number of bytes will be written
 */
void HI2C_write(HI2C_Port_e port, uint32_t address, uint8_t registerAdress, uint8_t* data, uint32_t dataSize);
/** Reads in a blocking mode from UART
 * @param port to read the data from
 * @param address the address of the client on the I2C bus
 * @param registerAdress the register which will be read
 * @param buf pointer to the where read data will be stored
 * @param bufSize number of bytes will be read
 */
void HI2C_read(HI2C_Port_e port, uint32_t address, uint8_t registerAdress, uint8_t* buf, uint32_t bufSize);


#endif /* HI2C_H */

/** @} */
