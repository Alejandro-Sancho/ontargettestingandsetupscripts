/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation: RIMU
 ***********************************************************************/

/** @addtogroup HI2C
 * @{
 * @file
 * This source file contains the definition of the functions of
 * HAL component HI2C.
 */

/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/

#include "HI2C.h"
#include "types.h"
#include "fsl_i2c_freertos.h"


/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/

#define NUM_PORTS 4

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/
typedef struct tag_HI2C_PortConfig_s
{
	i2c_rtos_handle_t config;
	I2C_Type* base;
	i2c_master_config_t masterConfig;
	uint32_t srcClock_Hz;
} HI2C_PortConfig_s;

/***********************************************************************/
/* LOCAL CONSTANTS                                                     */
/***********************************************************************/





typedef enum tag_batteryState_e{
	E_BatteryOn = 1,
	E_BatteryOff
}batteryState_e;

/***********************************************************************/
/* LOCAL VARIABLES                                                     */
/***********************************************************************/
static HI2C_PortConfig_s ports[NUM_PORTS];
static batteryState_e E_BatteryState;
/***********************************************************************/
/* LOCAL FUNCTION PROTOTYPES                                           */
/***********************************************************************/
static void setPortConfigurations();
/***********************************************************************/
/* GLOBAL FUNCTIONS                                                    */
/***********************************************************************/

/** Returns the number of days left in the current year.
 *
 */
GINIT_Return_e HI2C_Initialize(GINIT_InitLevel_e initLevel)
{

	switch(initLevel){

	case E_GINIT_InitLevel1:
		E_BatteryState = E_BatteryOn;
		setPortConfigurations();
		break;

	case E_GINIT_InitLevel2:
		I2C_MasterInit(ports[E_I2C_Trunk].base, &ports[E_I2C_Trunk].masterConfig, ports[E_I2C_Trunk].srcClock_Hz);
		I2C_MasterInit(ports[E_I2C_leftSide].base, &ports[E_I2C_leftSide].masterConfig, ports[E_I2C_leftSide].srcClock_Hz);
		I2C_MasterInit(ports[E_I2C_RightSide].base, &ports[E_I2C_RightSide].masterConfig, ports[E_I2C_RightSide].srcClock_Hz);

		if (E_BatteryState == E_BatteryOn)
		{
			I2C_MasterInit(ports[E_I2C_Batteries].base, &ports[E_I2C_Batteries].masterConfig, ports[E_I2C_Batteries].srcClock_Hz);
		}

		break;

	default:
		break;

	}
	return E_GINIT_ReturnSuccess;

};

void HI2C_write(HI2C_Port_e port, uint32_t address, uint8_t registerAdress, uint8_t* data, uint32_t dataSize){

	i2c_master_transfer_t masterXfer;

	masterXfer.slaveAddress = address;
	masterXfer.direction = kI2C_Write;
	masterXfer.subaddress = (uint32_t)registerAdress;
	masterXfer.subaddressSize = 1;
	masterXfer.data = data;
	masterXfer.dataSize = dataSize;
	masterXfer.flags = kI2C_TransferDefaultFlag;

	I2C_MasterTransferBlocking(ports[port].base, &masterXfer);

}

void HI2C_read(HI2C_Port_e port, uint32_t address, uint8_t registerAdress, uint8_t* buf, uint32_t bufSize){

	i2c_master_transfer_t masterXfer;

	masterXfer.slaveAddress = address;
	masterXfer.direction = kI2C_Read;
	masterXfer.subaddress = (uint32_t)registerAdress;
	masterXfer.subaddressSize = 1;
	masterXfer.data = buf;
	masterXfer.dataSize = bufSize;
	masterXfer.flags = kI2C_TransferDefaultFlag;

	I2C_MasterTransferBlocking(ports[port].base, &masterXfer);


}

/***********************************************************************/
/* LOCAL FUNCTIONS                                                     */
/***********************************************************************/

static void setPortConfigurations(void){

	//Trunk (on board) IMU
	I2C_MasterGetDefaultConfig(&ports[E_I2C_Trunk].masterConfig);
	ports[E_I2C_Trunk].masterConfig.baudRate_Bps = 400000U;
	ports[E_I2C_Trunk].base = I2C1;
	ports[E_I2C_Trunk].srcClock_Hz = CLOCK_GetFreq(I2C1_CLK_SRC);

	//Left Side IMU
	I2C_MasterGetDefaultConfig(&ports[E_I2C_leftSide].masterConfig);
	ports[E_I2C_leftSide].masterConfig.baudRate_Bps = 400000U;
	ports[E_I2C_leftSide].base = I2C0;
	ports[E_I2C_leftSide].srcClock_Hz = CLOCK_GetFreq(I2C0_CLK_SRC);

	//Right Side IMU
	I2C_MasterGetDefaultConfig(&ports[E_I2C_RightSide].masterConfig);
	ports[E_I2C_RightSide].masterConfig.baudRate_Bps = 400000U;
	ports[E_I2C_RightSide].base = I2C3;
	ports[E_I2C_RightSide].srcClock_Hz = CLOCK_GetFreq(I2C3_CLK_SRC);

	if (E_BatteryState == E_BatteryOn){
	//Battery IMU
	I2C_MasterGetDefaultConfig(&ports[E_I2C_Batteries].masterConfig);
	ports[E_I2C_Batteries].masterConfig.baudRate_Bps = 400000U;
	ports[E_I2C_Batteries].base = I2C2;
	ports[E_I2C_Batteries].srcClock_Hz = CLOCK_GetFreq(I2C2_CLK_SRC);
	}

}


/** @} */
