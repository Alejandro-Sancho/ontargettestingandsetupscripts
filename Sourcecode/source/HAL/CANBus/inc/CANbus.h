/*
 * CANbus.h
 *
 *  Created on: 04.04.2016
 *      Author: Kai Schmidt
 */

#ifndef CANbus_H_
#define CANbus_H_

#include "UTIL1.h"
#include "PE_Types.h"
#include "PE_Error.h"
#include "fsl_flexcan.h"
#include "CANOpen.h"

/* CANBUS definitions */
#define CAN_CLKSRC kCLOCK_BusClk
#define CAN_CLK_FREQ CLOCK_GetFreq(kCLOCK_BusClk)
#define FLEXCAN_IRQn CAN0_ORed_Message_buffer_IRQn

void flexcan_callback(CAN_Type *base, flexcan_handle_t *handle, status_t status, uint32_t result, void *userData);
void CANbus_Init(void);
void CANbus_MCRMaskEnable(void);

status_t CANbus_ReadFrame(uint8_t BufferIdx,LDD_CAN_TFrame *CANbus_frame_TX);
status_t CANbus_SendFrame(uint8_t BufferIdx, LDD_CAN_TFrame *CANbus_frame_TX);

LDD_TError CANbus_Data8Assembler(uint8_t data, uint8_t datastart, LDD_CAN_TFrame *CANbus_frame_TX);
LDD_TError CANbus_Data16Assembler(uint16_t data, uint8_t datastart, LDD_CAN_TFrame *CANbus_frame_TX);
LDD_TError CANbus_Data32Assembler(uint32_t data, uint8_t datastart, LDD_CAN_TFrame *CANbus_frame_TX);

void CANbusTSK_Init(void);

#endif /* CANbus_H_ */
