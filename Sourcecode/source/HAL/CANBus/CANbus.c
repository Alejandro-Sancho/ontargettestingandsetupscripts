/*
 * CANbus.c
 *
 *  Created on: 04.04.2016
 *      Author: Kai Schmidt
 */

#include "CANbus.h"

flexcan_handle_t flexcanHandle;
flexcan_config_t flexcanConfig;

//flexcan_mb_transfer_t  rxXfer; //txXfer,
//flexcan_frame_t rxFrame; //txFrame

volatile bool txComplete = false;
volatile bool rxComplete = false;

/* Event callback function */
void CANbus_callback(CAN_Type *base, flexcan_handle_t *handle, status_t status, uint32_t result, void *userData) {
	uint32_t idRec;

    switch (status)
    {
        /* Process FlexCAN Rx event. */
    	case kStatus_FLEXCAN_UnHandled:
//        case kStatus_FLEXCAN_RxIdle:
            if (result >= 4 ) /* RX MB 0x580 | ID */
            {
            	rxComplete = true;
            	CANOpenOnFullRxBuffer(result);
            }
            break;

        /* Process FlexCAN Tx event. */
        case kStatus_FLEXCAN_TxIdle:
            if (result < 4) /* TX MB */
            {
            	CANOpenOnFreeTxBuffer(result);
            	txComplete = true;
            }
            break;

        default:
        	PRINTF("Status %i \r\n",status);
            break;
    }
 }

/* Init CANbus and get user handle; No FIFO implementation */
void CANbus_Init(void) {
	flexcan_rx_mb_config_t mbConfig;

	/* Get the initial config */
	FLEXCAN_GetDefaultConfig(&flexcanConfig);
	/*
    * config->clkSrc = kFLEXCAN_ClkSrcOsc;
    * config->baudRate = 125000U;
    * config->maxMbNum = 16;
    * config->enableLoopBack = false;
    * config->enableSelfWakeup = false;
    * config->enableIndividMask = false;
	*/

	/* Change clock and baudrate */
	flexcanConfig.clkSrc = kFLEXCAN_ClkSrcPeri; /* FlexCAN Protocol Engine clock from Peripheral Clock */
	flexcanConfig.baudRate = 1000000U; /* 1Mb/s */

	/* Init CAN0 */
	FLEXCAN_Init(CAN0, &flexcanConfig, CAN_CLK_FREQ);

	/* Set call back function and set-up interrupts */
//	FLEXCAN_TransferCreateHandle_CANOpen(CAN0, &flexcanHandle, NULL, NULL); /* IRQHandler, flexcanHandle available globally */
	FLEXCAN_TransferCreateHandle(CAN0, &flexcanHandle, CANbus_callback, NULL); /* IRQHandler, flexcanHandle available globally */

	/* TX - 4 buffers */
	for(uint8_t i = 0; i < 4; i++) {
		FLEXCAN_SetTxMbConfig(CAN0, i, true);
//		FLEXCAN_EnableMbInterrupts(CAN0, 1 << i);
	}
	/* RX - 4 Msg buffers for ID 0x581 */
	mbConfig.format = kFLEXCAN_FrameFormatStandard; /* 11 bit identifier */
	mbConfig.type = kFLEXCAN_FrameTypeData;  /* CAN Frame Type(Data) */
	mbConfig.id = FLEXCAN_ID_STD(0x581);
	for(uint8_t i = 4; i < 8; i++) {
		FLEXCAN_SetRxMbConfig(CAN0, i, &mbConfig, true);
	    /* Start receive data through Rx Message Buffer. */
		FLEXCAN_TransferReceiveNonBlocking_CANOpen(CAN0, &flexcanHandle, i);
//		880h offset + (4d × i)
		FlEXCAN_SetRxIndividualMask(CAN0 , i, 0xC0000); /* All msg related to CAN ID 1 */
	}

	/* RX - 4 Msg buffers for ID 0x582 */
	mbConfig.id = FLEXCAN_ID_STD(0x582);
	for(uint8_t i = 8; i < 12; i++) {
		FLEXCAN_SetRxMbConfig(CAN0, i, &mbConfig, true);
	    /* Start receive data through Rx Message Buffer. */
		FLEXCAN_TransferReceiveNonBlocking_CANOpen(CAN0, &flexcanHandle, i);
		FlEXCAN_SetRxIndividualMask(CAN0 , i, 0xC0000); /* All msg related to CAN ID 2 */
	}

    /*
     * The following code is moved to the init function of fsl_flexcan.h
     */
//	/* Set MCR mask (enable IRMQ) */
//	/* CAN0_MCR: WRNEN=1,SRXDIS=1,AEN=1,MAXMB&=~8,MAXMB|=7 */
//    /* Enter Freeze Mode */
//    FLEXCAN_EnterFreezeMode(CAN0);
//    CAN0->MCR |= CAN_MCR_AEN_MASK |
//    			 CAN_MCR_IRMQ_MASK |
//				 CAN_MCR_SRXDIS_MASK |
//				 CAN_MCR_WRNEN_MASK |
//                 CAN_MCR_MAXMB(FSL_FEATURE_FLEXCAN_HAS_MESSAGE_BUFFER_MAX_NUMBERn(base) - 1);
//    /* Exit Freeze Mode */
//    FLEXCAN_ExitFreezeMode(CAN0);

}


/* Read Frame */
status_t CANbus_ReadFrame(uint8_t BufferIdx, LDD_CAN_TFrame *CANbus_frame_RX) {
	status_t res;
	flexcan_frame_t rxFrame;

	res = FLEXCAN_ReadRxMb(CAN0, BufferIdx, &rxFrame);
	if (kStatus_Success == res) {
		res = kStatus_FLEXCAN_RxIdle;
	}

	/* Map rxFrame and CANbus_frame_RX */
	CANbus_frame_RX->MessageID = ((uint32_t)(((uint32_t)(rxFrame.id))) >> CAN_ID_STD_SHIFT);
	CANbus_frame_RX->Length = rxFrame.length;
	/* Data */

	for(uint8_t i = 0; i < rxFrame.length; i++) { /* Init data frame */
		if(i==0){
			CANbus_frame_RX->Data [0] = rxFrame.dataByte0;
		}
		if(i==1){
			CANbus_frame_RX->Data [1] = rxFrame.dataByte1;
		}
		if(i==2){
			CANbus_frame_RX->Data [2] = rxFrame.dataByte2;
		}
		if(i==3){
			CANbus_frame_RX->Data [3] = rxFrame.dataByte3;
		}
		if(i==4){
			CANbus_frame_RX->Data [4] = rxFrame.dataByte4;
		}
		if(i==5){
			CANbus_frame_RX->Data [5] = rxFrame.dataByte5;
		}
		if(i==6){
			CANbus_frame_RX->Data [6] = rxFrame.dataByte6;
		}
		if(i==7){
			CANbus_frame_RX->Data [7] = rxFrame.dataByte7;
		}
	}

	FLEXCAN_TransferAbortReceive(CAN0, &flexcanHandle, BufferIdx);
//	FLEXCAN_ClearMbStatusFlags(CAN0, 1 << BufferIdx);
	FLEXCAN_EnableMbInterrupts(CAN0, 1 << BufferIdx);

	return ERR_OK;
}


/* Send Frame | Not optimized wrapper, the new CAN driver structure would make all the assembler functions obsolete */
status_t CANbus_SendFrame(uint8_t BufferIdx, LDD_CAN_TFrame *CANbus_frame_TX) {
	status_t res;

	flexcan_mb_transfer_t txXfer;
	flexcan_frame_t txFrame;

	/* prepare/map handle, so it can process the frame */
	txFrame.format = kFLEXCAN_FrameFormatStandard;
	txFrame.type = kFLEXCAN_FrameTypeData;
	txFrame.id = FLEXCAN_ID_STD(CANbus_frame_TX->MessageID);
	txFrame.length = CANbus_frame_TX->Length;
	for(uint8_t i = 0; i < txFrame.length; i++) { /* Init data frame */
		if(i==0){
			txFrame.dataByte0 = CANbus_frame_TX->Data[0];
		}
		if(i==1){
			txFrame.dataByte1 = CANbus_frame_TX->Data[1];
		}
		if(i==2){
			txFrame.dataByte2 = CANbus_frame_TX->Data[2];
		}
		if(i==3){
			txFrame.dataByte3 = CANbus_frame_TX->Data[3];
		}
		if(i==4){
			txFrame.dataByte4 = CANbus_frame_TX->Data[4];
		}
		if(i==5){
			txFrame.dataByte5 = CANbus_frame_TX->Data[5];
		}
		if(i==6){
			txFrame.dataByte6 = CANbus_frame_TX->Data[6];
		}
		if(i==7){
			txFrame.dataByte7 = CANbus_frame_TX->Data[7];
		}
	}
	txXfer.frame = &txFrame;
	txXfer.mbIdx = BufferIdx;

	res = FLEXCAN_TransferSendNonBlocking(CAN0, &flexcanHandle, &txXfer);
	return res;
}


/* Put uint8_t data into data bytes of CAN Msg */
LDD_TError CANbus_Data8Assembler(uint8_t data, uint8_t datastart, LDD_CAN_TFrame *CANbus_frame_TX) { /* data start range from 0 to 7 */

	if((CANbus_frame_TX->Length-datastart+1) <= 0) {
		return ERR_OVERFLOW;
	}
	CANbus_frame_TX->Data[datastart] = (uint8_t)(data  & 0xff);

	return ERR_OK;
}


/* Put uint16_t data into data bytes of CAN Msg */
LDD_TError CANbus_Data16Assembler(uint16_t data, uint8_t datastart, LDD_CAN_TFrame *CANbus_frame_TX) { /* data start range from 0 to 7 */

	if((CANbus_frame_TX->Length-datastart+2) < 0) {
		return ERR_OVERFLOW;
	}
	CANbus_frame_TX->Data[datastart] = (uint8_t)(data  & 0xff);
	CANbus_frame_TX->Data[datastart+1] = (uint8_t)((data >> 8)  & 0xff);

	return ERR_OK;
}


/* Put uint32_t data into data bytes of CAN Msg */
LDD_TError CANbus_Data32Assembler(uint32_t data, uint8_t datastart, LDD_CAN_TFrame *CANbus_frame_TX) { /* data start range from 0 to 7 */

	if((CANbus_frame_TX->Length-datastart+4) < 0) {
		return ERR_OVERFLOW;
	}
	CANbus_frame_TX->Data[datastart] = (uint8_t)(data  & 0xff);
	CANbus_frame_TX->Data[datastart+1] = (uint8_t)((data >> 8)  & 0xff);
	CANbus_frame_TX->Data[datastart+2] = (uint8_t)((data >> 16)  & 0xff);
	CANbus_frame_TX->Data[datastart+3] = (uint8_t)((data >> 24)  & 0xff);

	return ERR_OK;
}
