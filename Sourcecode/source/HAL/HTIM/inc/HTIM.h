/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation: HTIM Timer HAL Layer
 ***********************************************************************/

/** @defgroup HTIM Timer component of the RTE layer
 * @{
 * @brief
 * The next blank line is mandatory!
 *
 * @file
 * This header file contains the declaration of the public or private
 * interface to HAL component TIMER.
 */

#ifndef HTIM_H_
#define HTIM_H_

/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/

#include "types.h"
#include "GINIT.h"

/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/

/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/
/**
 * The timers which are preconfigured
 */
typedef enum tag_HTIM_Timer_e{

	E_HTIM_10msTimer = 0,
	E_HTIM_Freerunner = 1

}HTIM_Timer_e;

typedef void (*HTIM_Callback_t)(void);
/***********************************************************************/
/* GLOBAL CONSTANTS                                                    */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL VARIABLES                                                    */
/***********************************************************************/

/***********************************************************************/
/* GLOBAL FUNCTION PROTOTYPES                                          */
/***********************************************************************/

/** Initialize the HTIM-Module
 *  @return E_GINIT_ReturnFailure if failed
 */
GINIT_Return_e HTIM_Initialize(GINIT_InitLevel_e initLevel);

/** Starts the HW timer module
 * @param timer the timer module which shall be started
 *
 */
void HTIM_StartTimer(HTIM_Timer_e timer);

/**
 * The timer calls over a callback during ISR. Be sure callback function is non blocking
 * Works only for 10ms periodic timer which is used for timing the read cycle
 * @param callback functionpointer which will be called
 */
void HTIM_SubscribeTimer(HTIM_Callback_t callback);

/**
 * Returns the countervalue of the freerunner counter
 * @return countervalue in ticks
 */
uint32_t HTIM_GetFreerunnerCounter(void);


#endif /* HTIM_H_ */

/** @} */
