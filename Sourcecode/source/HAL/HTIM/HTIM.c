/***********************************************************************
 * $Id$
 * Copyright Z�hlke Engineering AG
 * Project: MyoSwiss
 * Documentation: RDAC Data Acquisition module of RTE layer
 ***********************************************************************/

/** @ HTIM Timer module of HAL layer
 * @{
 * @file
 * This source file contains the definition of the functions of
 * HAL component HTIM Timer module.
 */

/***********************************************************************/
/* INCLUDES                                                            */
/***********************************************************************/
#include "HTIM.h"
#include "GINIT.h"
#include "Platform.h"
#include "fsl_lptmr.h"
#include "MK66F18.h"
#include "types.h"
#include "fsl_ftm.h"
#include "board.h"
#include "fsl_debug_console.h"

/***********************************************************************/
/* DEFINES                                                             */
/***********************************************************************/
/**
 * The ISR
 */
#define HTIM_TimerISR FTM2_IRQHandler


/***********************************************************************/
/* TYPES                                                               */
/***********************************************************************/

typedef struct HTIM_FTMTimerConfig2_s{
    ftm_config_t ftmConfig;
    FTM_Type * timerBase;
	int32_t interruptNumber;
	uint32_t sourceClock;
	Time_us_t timerPeriod;
	HTIM_Callback_t callback;
} HTIM_FTMTimerConfig2_s;


/***********************************************************************/
/* LOCAL CONSTANTS                                                     */
/***********************************************************************/

/***********************************************************************/
/* LOCAL VARIABLES                                                     */
/***********************************************************************/

static HTIM_FTMTimerConfig2_s S_TimerConfigsFTM[2];

/***********************************************************************/
/* LOCAL FUNCTION PROTOTYPES                                           */
/***********************************************************************/
/***********************************************************************/
/* GLOBAL FUNCTIONS                                                    */
/***********************************************************************/
GINIT_Return_e HTIM_Initialize(GINIT_InitLevel_e initLevel){

	int32_t retValTimer;
	GINIT_Return_e retVal = E_GINIT_ReturnSuccess;
	switch(initLevel){

	case E_GINIT_InitLevel1:


		S_TimerConfigsFTM[E_HTIM_10msTimer].timerBase = FTM2;
		S_TimerConfigsFTM[E_HTIM_10msTimer].interruptNumber = (int32_t)FTM2_IRQn;
		S_TimerConfigsFTM[E_HTIM_10msTimer].sourceClock = (CLOCK_GetFreq(kCLOCK_BusClk)/32);
		S_TimerConfigsFTM[E_HTIM_10msTimer].timerPeriod = 10000U;
		S_TimerConfigsFTM[E_HTIM_10msTimer].callback = NULL;

		S_TimerConfigsFTM[E_HTIM_Freerunner].timerBase = FTM3;
		S_TimerConfigsFTM[E_HTIM_Freerunner].interruptNumber = (int32_t)FTM3_IRQn;
		S_TimerConfigsFTM[E_HTIM_Freerunner].sourceClock = (CLOCK_GetFreq(kCLOCK_BusClk));
		S_TimerConfigsFTM[E_HTIM_Freerunner].timerPeriod = 4294967295u;
		S_TimerConfigsFTM[E_HTIM_Freerunner].callback = NULL;
		break;

	case E_GINIT_InitLevel2:
		FTM_GetDefaultConfig(&S_TimerConfigsFTM[E_HTIM_10msTimer].ftmConfig);
		S_TimerConfigsFTM[E_HTIM_10msTimer].ftmConfig.prescale = kFTM_Prescale_Divide_32;
		retValTimer = FTM_Init(S_TimerConfigsFTM[E_HTIM_10msTimer].timerBase, &S_TimerConfigsFTM[E_HTIM_10msTimer].ftmConfig);
		if(retValTimer != (int32_t)kStatus_Success){
			retVal =E_GINIT_ReturnFailure;
		}
	    FTM_SetTimerPeriod(S_TimerConfigsFTM[E_HTIM_10msTimer].timerBase, USEC_TO_COUNT((uint64_t)S_TimerConfigsFTM[E_HTIM_10msTimer].timerPeriod, S_TimerConfigsFTM[E_HTIM_10msTimer].sourceClock));
	    FTM_EnableInterrupts(S_TimerConfigsFTM[E_HTIM_10msTimer].timerBase, (uint32_t)kFTM_TimeOverflowInterruptEnable);
	    SetPriorityIRQ(S_TimerConfigsFTM[E_HTIM_10msTimer].interruptNumber,configMAX_SYSCALL_INTERRUPT_PRIORITY - 1);
	    EnableIRQ(S_TimerConfigsFTM[E_HTIM_10msTimer].interruptNumber);


		FTM_GetDefaultConfig(&S_TimerConfigsFTM[E_HTIM_Freerunner].ftmConfig);
		S_TimerConfigsFTM[E_HTIM_Freerunner].ftmConfig.prescale = kFTM_Prescale_Divide_1;
		retValTimer = FTM_Init(S_TimerConfigsFTM[E_HTIM_Freerunner].timerBase, &S_TimerConfigsFTM[E_HTIM_Freerunner].ftmConfig);
		if(retValTimer != (int32_t)kStatus_Success){
			retVal =E_GINIT_ReturnFailure;
		}
	    FTM_SetTimerPeriod(S_TimerConfigsFTM[E_HTIM_Freerunner].timerBase, S_TimerConfigsFTM[E_HTIM_Freerunner].timerPeriod);
	    //FTM_EnableInterrupts(S_TimerConfigsFTM[E_HTIM_Freerunner].timerBase, kFTM_TimeOverflowInterruptEnable);
	    //EnableIRQ(S_TimerConfigsFTM[E_HTIM_Freerunner].interruptNumber);
		break;

	default:
		break;
	}

	return retVal;
}

void HTIM_StartTimer(HTIM_Timer_e timer){

    FTM_StartTimer(S_TimerConfigsFTM[timer].timerBase, kFTM_SystemClock);

}

void HTIM_SubscribeTimer(HTIM_Callback_t callback){

	S_TimerConfigsFTM[E_HTIM_10msTimer].callback = callback;
}

uint32_t HTIM_GetFreerunnerCounter(void){

	//return FTM_GetCurrentTimerCount(S_TimerConfigsFTM[E_HTIM_Freerunner].timerBase);

	return 0; //TODO MYOSW-46 Fix getCurrentCount : is not working for now
}


/***********************************************************************/
/* LOCAL FUNCTIONS                                                     */
/***********************************************************************/

void HTIM_TimerISR(void){

	if(S_TimerConfigsFTM[E_HTIM_10msTimer].callback != NULL){
		S_TimerConfigsFTM[E_HTIM_10msTimer].callback();
	}
    FTM_ClearStatusFlags(S_TimerConfigsFTM[E_HTIM_10msTimer].timerBase, (uint32_t)kFTM_TimeOverflowFlag);


}



/** @} */
