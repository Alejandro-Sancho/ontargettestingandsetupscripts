# README #

### What is this repository for?  ###

This repository hosts the code to do fast tests on the motors. It might be useful for mechanical eng. dpmt. to test parts of the suit.
Initially it contains a position and a torque mode testing.

The repository contains two branches, one for each test. In the future, new tests will have their own branches.
Individual configurations of each test that want to be saved must create a new branch from that test. Never commit on the same branch.

TESTS MUST NEVER BE MERGED BACK TO MASTER! 

Repo workflow: 
-----------------------------------------------master------------------------------->
|   |                 |
|   |                 |-------------New test(future)------->
|   |
|   |-----------Torque Control Test ------------>
|
|----------Position Control Test --------------->
										|
										|-----Indiv. Test config----->